-- WordPress Backup to Dropbox SQL Dump
-- Version 1.4.5
-- http://wpb2d.com
-- Generation Time: September 2, 2013 at 02:20

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Create and use the backed up database
--

CREATE DATABASE IF NOT EXISTS startdjb_wrd17;
USE startdjb_wrd17;

--
-- Table structure for table `wp_2_commentmeta`
--

CREATE TABLE `wp_2_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table `wp_2_commentmeta` is empty
--

--
-- Table structure for table `wp_2_comments`
--

CREATE TABLE `wp_2_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table `wp_2_comments` is empty
--

--
-- Table structure for table `wp_2_links`
--

CREATE TABLE `wp_2_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table `wp_2_links` is empty
--

--
-- Table structure for table `wp_2_options`
--

CREATE TABLE `wp_2_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=349 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_2_options`
--

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('1', 'siteurl', 'http://www.lmmre.com/property', 'yes'),
('2', 'blogname', 'CT Property Search - LMMRE', 'yes'),
('3', 'blogdescription', '', 'yes'),
('261', 'new_admin_email', 'cbajda@weddingreports.com', 'yes'),
('4', 'users_can_register', '0', 'yes'),
('5', 'admin_email', 'cbajda@weddingreports.com', 'yes'),
('6', 'start_of_week', '1', 'yes'),
('7', 'use_balanceTags', '0', 'yes'),
('8', 'use_smilies', '1', 'yes'),
('9', 'require_name_email', '1', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('10', 'comments_notify', '1', 'yes'),
('11', 'posts_per_rss', '10', 'yes'),
('12', 'rss_use_excerpt', '0', 'yes'),
('13', 'mailserver_url', 'mail.example.com', 'yes'),
('14', 'mailserver_login', 'login@example.com', 'yes'),
('15', 'mailserver_pass', 'password', 'yes'),
('16', 'mailserver_port', '110', 'yes'),
('17', 'default_category', '1', 'yes'),
('18', 'default_comment_status', 'open', 'yes'),
('19', 'default_ping_status', 'open', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('20', 'default_pingback_flag', '1', 'yes'),
('21', 'posts_per_page', '3', 'yes'),
('22', 'date_format', 'F j, Y', 'yes'),
('23', 'time_format', 'g:i a', 'yes'),
('24', 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
('25', 'links_recently_updated_prepend', '<em>', 'yes'),
('26', 'links_recently_updated_append', '</em>', 'yes'),
('27', 'links_recently_updated_time', '120', 'yes'),
('28', 'comment_moderation', '0', 'yes'),
('29', 'moderation_notify', '1', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('30', 'permalink_structure', '/%postname%/', 'yes'),
('31', 'gzipcompression', '0', 'yes'),
('32', 'hack_file', '0', 'yes'),
('33', 'blog_charset', 'UTF-8', 'yes'),
('34', 'moderation_keys', '', 'no'),
('35', 'active_plugins', 'a:5:{i:1;s:19:\"akismet/akismet.php\";i:2;s:32:\"are-you-a-human/areyouahuman.php\";i:3;s:19:\"jetpack/jetpack.php\";i:4;s:52:\"wordpress-backup-to-dropbox/wp-backup-to-dropbox.php\";i:5;s:24:\"wordpress-seo/wp-seo.php\";}', 'yes'),
('36', 'home', 'http://www.lmmre.com/property', 'yes'),
('37', 'category_base', '', 'yes'),
('38', 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
('39', 'advanced_edit', '0', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('40', 'comment_max_links', '2', 'yes'),
('41', 'gmt_offset', '0', 'yes'),
('42', 'default_email_category', '1', 'yes'),
('43', 'recently_edited', '', 'no'),
('44', 'template', 'estate', 'yes'),
('45', 'stylesheet', 'estate', 'yes'),
('46', 'comment_whitelist', '1', 'yes'),
('47', 'blacklist_keys', '', 'no'),
('48', 'comment_registration', '0', 'yes'),
('49', 'html_type', 'text/html', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('50', 'use_trackback', '0', 'yes'),
('51', 'default_role', 'subscriber', 'yes'),
('52', 'db_version', '22442', 'yes'),
('53', 'uploads_use_yearmonth_folders', '1', 'yes'),
('54', 'upload_path', '', 'yes'),
('55', 'blog_public', '1', 'yes'),
('56', 'default_link_category', '2', 'yes'),
('57', 'show_on_front', 'posts', 'yes'),
('58', 'tag_base', '', 'yes'),
('59', 'show_avatars', '1', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('60', 'avatar_rating', 'G', 'yes'),
('61', 'upload_url_path', '', 'yes'),
('62', 'thumbnail_size_w', '150', 'yes'),
('63', 'thumbnail_size_h', '150', 'yes'),
('64', 'thumbnail_crop', '1', 'yes'),
('65', 'medium_size_w', '300', 'yes'),
('66', 'medium_size_h', '300', 'yes'),
('67', 'avatar_default', 'mystery', 'yes'),
('68', 'large_size_w', '1024', 'yes'),
('69', 'large_size_h', '1024', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('70', 'image_default_link_type', 'file', 'yes'),
('71', 'image_default_size', '', 'yes'),
('72', 'image_default_align', '', 'yes'),
('73', 'close_comments_for_old_posts', '0', 'yes'),
('74', 'close_comments_days_old', '14', 'yes'),
('75', 'thread_comments', '1', 'yes'),
('76', 'thread_comments_depth', '5', 'yes'),
('77', 'page_comments', '0', 'yes'),
('78', 'comments_per_page', '50', 'yes'),
('79', 'default_comments_page', 'newest', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('80', 'comment_order', 'asc', 'yes'),
('81', 'sticky_posts', 'a:0:{}', 'yes'),
('82', 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
('83', 'widget_text', 'a:0:{}', 'yes'),
('84', 'widget_rss', 'a:0:{}', 'yes'),
('85', 'uninstall_plugins', 'a:1:{s:27:\"wp-pagenavi/wp-pagenavi.php\";s:14:\"__return_false\";}', 'no'),
('86', 'timezone_string', '', 'yes'),
('87', 'page_for_posts', '0', 'yes'),
('88', 'page_on_front', '0', 'yes'),
('89', 'default_post_format', '0', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('90', 'link_manager_enabled', '0', 'yes'),
('91', 'wp_2_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:9:\"add_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
('92', 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
('93', 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
('94', 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
('95', 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
('341', 'location_children', 'a:0:{}', 'yes'),
('96', 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
('97', 'sidebars_widgets', 'a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"primary\";a:0:{}s:11:\"secondary-1\";a:0:{}s:11:\"secondary-2\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
('109', 'woo_custom_seo_template', 'a:3:{i:0;a:5:{s:4:\"name\";s:10:\"seo_info_1\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:4:\"SEO \";s:4:\"type\";s:4:\"info\";s:4:\"desc\";s:195:\"Additional SEO custom fields available: <strong>Custom Page Titles</strong>. Go to <a href=\"http://www.lmmre.com/property/wp-admin/admin.php?page=woothemes_seo\">SEO Settings</a> page to activate.\";}i:1;a:5:{s:4:\"name\";s:10:\"seo_follow\";s:3:\"std\";s:5:\"false\";s:5:\"label\";s:18:\"SEO - Set nofollow\";s:4:\"type\";s:8:\"checkbox\";s:4:\"desc\";s:81:\"Make links from this post/page <strong>not followable</strong> by search engines.\";}i:2;a:5:{s:4:\"name\";s:11:\"seo_noindex\";s:3:\"std\";s:5:\"false\";s:5:\"label\";s:13:\"SEO - Noindex\";s:4:\"type\";s:8:\"checkbox\";s:4:\"desc\";s:56:\"Set the Page/Post to not be indexed by a search engines.\";}}', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('110', 'woo_options', 'a:130:{s:18:\"woo_alt_stylesheet\";s:11:\"default.css\";s:8:\"woo_logo\";s:96:\"http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/lmmre-new-haven-ct-property.jpg\";s:13:\"woo_texttitle\";s:5:\"false\";s:18:\"woo_custom_favicon\";s:0:\"\";s:20:\"woo_google_analytics\";s:506:\"<script type=\"text/javascript\">\r\n\r\n  var _gaq = _gaq || [];\r\n  _gaq.push([\\\'_setAccount\\\', \\\'UA-31347740-1\\\']);\r\n  _gaq.push([\\\'_trackPageview\\\']);\r\n\r\n  (function() {\r\n    var ga = document.createElement(\\\'script\\\'); ga.type = \\\'text/javascript\\\'; ga.async = true;\r\n    ga.src = (\\\'https:\\\' == document.location.protocol ? \\\'https://ssl\\\' : \\\'http://www\\\') + \\\'.google-analytics.com/ga.js\\\';\r\n    var s = document.getElementsByTagName(\\\'script\\\')[0]; s.parentNode.insertBefore(ga, s);\r\n  })();\r\n\r\n</script>\";s:12:\"woo_feed_url\";s:0:\"\";s:19:\"woo_subscribe_email\";s:25:\"cbajda@weddingreports.com\";s:21:\"woo_contactform_email\";s:25:\"cbajda@weddingreports.com\";s:14:\"woo_custom_css\";s:0:\"\";s:12:\"woo_comments\";s:4:\"post\";s:19:\"woo_pagination_type\";s:15:\"paginated_links\";s:14:\"woo_body_color\";s:7:\"#000000\";s:12:\"woo_body_img\";s:0:\"\";s:15:\"woo_body_repeat\";s:9:\"no-repeat\";s:12:\"woo_body_pos\";s:8:\"top left\";s:14:\"woo_link_color\";s:0:\"\";s:20:\"woo_link_hover_color\";s:0:\"\";s:16:\"woo_button_color\";s:0:\"\";s:23:\"woo_header_company_name\";s:19:\"Levey Miller Maretz\";s:33:\"woo_header_company_contact_number\";s:12:\"203-389-5377\";s:24:\"woo_header_company_email\";s:14:\"info@lmmre.com\";s:15:\"woo_home_layout\";s:15:\"Without Sidebar\";s:26:\"woo_property_single_layout\";s:15:\"Without Sidebar\";s:27:\"woo_property_archive_layout\";s:15:\"Without Sidebar\";s:26:\"woo_property_search_layout\";s:15:\"Without Sidebar\";s:12:\"woo_featured\";s:4:\"true\";s:19:\"woo_featured_header\";s:19:\"Featured Properties\";s:17:\"woo_featured_tags\";s:17:\"Featured, feature\";s:20:\"woo_featured_entries\";s:1:\"5\";s:16:\"woo_slider_image\";s:4:\"Left\";s:15:\"woo_slider_auto\";s:4:\"true\";s:16:\"woo_slider_speed\";s:3:\"0.6\";s:19:\"woo_slider_interval\";s:1:\"4\";s:15:\"woo_more_header\";s:15:\"More Properties\";s:16:\"woo_more_entries\";s:2:\"12\";s:17:\"woo_archives_link\";s:4:\"true\";s:26:\"woo_estate_property_prefix\";s:4:\"PROP\";s:19:\"woo_estate_currency\";s:1:\"$\";s:17:\"woo_label_on_show\";s:7:\"On Show\";s:17:\"woo_label_garages\";s:5:\"Acres\";s:16:\"woo_label_garage\";s:4:\"Acre\";s:21:\"woo_label_size_metric\";s:5:\"sq ft\";s:14:\"woo_label_beds\";s:8:\"Bedrooms\";s:13:\"woo_label_bed\";s:7:\"Bedroom\";s:15:\"woo_label_baths\";s:14:\"Ceiling Height\";s:20:\"woo_label_baths_long\";s:14:\"Ceiling Height\";s:14:\"woo_label_bath\";s:14:\"Ceiling Height\";s:19:\"woo_garage_logo_big\";s:93:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-garage-big.png\";s:21:\"woo_garage_logo_small\";s:95:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-garage-small.png\";s:16:\"woo_bed_logo_big\";s:90:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bed-big.png\";s:18:\"woo_bed_logo_small\";s:92:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bed-small.png\";s:17:\"woo_bath_logo_big\";s:91:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bath-big.png\";s:19:\"woo_bath_logo_small\";s:93:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bath-small.png\";s:17:\"woo_size_logo_big\";s:91:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-size-big.png\";s:19:\"woo_size_logo_small\";s:93:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-size-small.png\";s:17:\"woo_search_header\";s:21:\"Search Our Properties\";s:23:\"woo_search_keyword_text\";s:9:\"Search...\";s:24:\"woo_displaysearch_single\";s:5:\"false\";s:27:\"woo_property_search_results\";s:1:\"3\";s:27:\"woo_feature_matching_method\";s:7:\"minimum\";s:19:\"woo_label_sale_type\";s:9:\"Sale Type\";s:18:\"woo_label_for_sale\";s:8:\"For Sale\";s:18:\"woo_label_for_rent\";s:8:\"For Rent\";s:36:\"woo_label_property_location_and_type\";s:24:\"Property Location & Type\";s:37:\"woo_label_locations_dropdown_view_all\";s:18:\"View all Locations\";s:41:\"woo_label_property_type_dropdown_view_all\";s:23:\"View all Property Types\";s:19:\"woo_label_min_price\";s:9:\"Min Price\";s:19:\"woo_label_max_price\";s:9:\"Max Price\";s:25:\"woo_label_advanced_search\";s:15:\"Advanced Search\";s:18:\"woo_label_min_size\";s:8:\"Min Size\";s:18:\"woo_label_max_size\";s:8:\"Max Size\";s:34:\"woo_label_property_details_on_show\";s:34:\"This property is currently on show\";s:29:\"woo_label_additional_features\";s:8:\"Features\";s:33:\"woo_clickable_additional_features\";s:5:\"false\";s:17:\"woo_label_gallery\";s:7:\"Gallery\";s:22:\"woo_label_virtual_tour\";s:12:\"Virtual Tour\";s:22:\"woo_label_property_map\";s:3:\"Map\";s:28:\"woo_label_related_properties\";s:28:\"More properties in this area\";s:30:\"woo_label_contact_agent_button\";s:13:\"Contact Agent\";s:26:\"woo_label_agent_email_link\";s:16:\"Email this agent\";s:21:\"woo_contact_form_link\";s:5:\"false\";s:21:\"woo_contact_form_page\";s:14:\"Select a page:\";s:26:\"woo_agent_user_role_enable\";s:5:\"false\";s:19:\"woo_agent_role_name\";s:5:\"Agent\";s:22:\"woo_agent_role_default\";s:14:\"Select a Role:\";s:15:\"woo_maps_apikey\";s:0:\"\";s:15:\"woo_maps_scroll\";s:5:\"false\";s:22:\"woo_maps_single_height\";s:3:\"250\";s:10:\"woo_coords\";s:4:\"true\";s:24:\"woo_maps_default_mapzoom\";s:1:\"9\";s:24:\"woo_maps_default_maptype\";s:12:\"G_NORMAL_MAP\";s:20:\"woo_cat_colors_pages\";s:3:\"red\";s:27:\"woo_cat_custom_marker_pages\";s:0:\"\";s:16:\"woo_cat_colors_1\";s:3:\"red\";s:23:\"woo_cat_custom_marker_1\";s:0:\"\";s:23:\"woo_location_menu_items\";s:4:\"true\";s:27:\"woo_propertytype_menu_items\";s:4:\"true\";s:23:\"woo_category_menu_items\";s:4:\"true\";s:18:\"woo_wpthumb_notice\";s:0:\"\";s:22:\"woo_post_image_support\";s:4:\"true\";s:14:\"woo_pis_resize\";s:4:\"true\";s:17:\"woo_pis_hard_crop\";s:5:\"false\";s:10:\"woo_resize\";s:5:\"false\";s:12:\"woo_auto_img\";s:5:\"false\";s:11:\"woo_thumb_w\";s:3:\"100\";s:11:\"woo_thumb_h\";s:3:\"100\";s:15:\"woo_thumb_align\";s:9:\"alignleft\";s:16:\"woo_thumb_single\";s:5:\"false\";s:12:\"woo_single_w\";s:3:\"200\";s:12:\"woo_single_h\";s:3:\"200\";s:13:\"woo_rss_thumb\";s:5:\"false\";s:19:\"woo_footer_aff_link\";s:0:\"\";s:15:\"woo_footer_left\";s:5:\"false\";s:20:\"woo_footer_left_text\";s:7:\"<p></p>\";s:16:\"woo_footer_right\";s:5:\"false\";s:21:\"woo_footer_right_text\";s:7:\"<p></p>\";s:11:\"woo_connect\";s:5:\"false\";s:17:\"woo_connect_title\";s:0:\"\";s:19:\"woo_connect_content\";s:0:\"\";s:25:\"woo_connect_newsletter_id\";s:0:\"\";s:30:\"woo_connect_mailchimp_list_url\";s:0:\"\";s:15:\"woo_connect_rss\";s:4:\"true\";s:19:\"woo_connect_twitter\";s:0:\"\";s:20:\"woo_connect_facebook\";s:0:\"\";s:19:\"woo_connect_youtube\";s:0:\"\";s:18:\"woo_connect_flickr\";s:0:\"\";s:20:\"woo_connect_linkedin\";s:0:\"\";s:21:\"woo_connect_delicious\";s:0:\"\";s:22:\"woo_connect_googleplus\";s:0:\"\";s:19:\"woo_connect_related\";s:4:\"true\";}', 'yes'),
('111', 'woo_template', 'a:145:{i:0;a:3:{s:4:\"name\";s:16:\"General Settings\";s:4:\"icon\";s:7:\"general\";s:4:\"type\";s:7:\"heading\";}i:1;a:6:{s:4:\"name\";s:16:\"Theme Stylesheet\";s:4:\"desc\";s:44:\"Select your themes alternative color scheme.\";s:2:\"id\";s:18:\"woo_alt_stylesheet\";s:3:\"std\";s:11:\"default.css\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:9:{i:0;s:15:\"light_green.css\";i:1;s:11:\"unboxed.css\";i:2;s:15:\"green_brown.css\";i:3;s:14:\"light_blue.css\";i:4;s:15:\"orange_blue.css\";i:5;s:8:\"dark.css\";i:6;s:9:\"brown.css\";i:7;s:15:\"light_brown.css\";i:8;s:11:\"default.css\";}}i:2;a:5:{s:4:\"name\";s:11:\"Custom Logo\";s:4:\"desc\";s:63:\"Upload a logo for your theme, or specify an image URL directly.\";s:2:\"id\";s:8:\"woo_logo\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:3;a:5:{s:4:\"name\";s:10:\"Text Title\";s:4:\"desc\";s:109:\"Enable if you want Blog Title and Tagline to be text-based. Setup title/tagline in WP -> Settings -> General.\";s:2:\"id\";s:13:\"woo_texttitle\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:4;a:5:{s:4:\"name\";s:14:\"Custom Favicon\";s:4:\"desc\";s:113:\"Upload a 16px x 16px <a href=\'http://www.faviconr.com/\'>ico image</a> that will represent your website\'s favicon.\";s:2:\"id\";s:18:\"woo_custom_favicon\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:5;a:5:{s:4:\"name\";s:13:\"Tracking Code\";s:4:\"desc\";s:117:\"Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.\";s:2:\"id\";s:20:\"woo_google_analytics\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"textarea\";}i:6;a:5:{s:4:\"name\";s:7:\"RSS URL\";s:4:\"desc\";s:51:\"Enter your preferred RSS URL. (Feedburner or other)\";s:2:\"id\";s:12:\"woo_feed_url\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:7;a:5:{s:4:\"name\";s:10:\"E-Mail URL\";s:4:\"desc\";s:67:\"Enter your preferred E-mail subscription URL. (Feedburner or other)\";s:2:\"id\";s:19:\"woo_subscribe_email\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:8;a:5:{s:4:\"name\";s:19:\"Contact Form E-Mail\";s:4:\"desc\";s:156:\"Enter your E-mail address to use on the Contact Form Page Template. Add the contact form by adding a new page and selecting \'Contact Form\' as page template.\";s:2:\"id\";s:21:\"woo_contactform_email\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:9;a:5:{s:4:\"name\";s:10:\"Custom CSS\";s:4:\"desc\";s:62:\"Quickly add some CSS to your theme by adding it to this block.\";s:2:\"id\";s:14:\"woo_custom_css\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"textarea\";}i:10;a:5:{s:4:\"name\";s:18:\"Post/Page Comments\";s:4:\"desc\";s:69:\"Select if you want to enable/disable comments on posts and/or pages. \";s:2:\"id\";s:12:\"woo_comments\";s:4:\"type\";s:7:\"select2\";s:7:\"options\";a:4:{s:4:\"post\";s:10:\"Posts Only\";s:4:\"page\";s:10:\"Pages Only\";s:4:\"both\";s:13:\"Pages / Posts\";s:0:\"\";s:4:\"None\";}}i:11;a:5:{s:4:\"name\";s:16:\"Pagination Style\";s:4:\"desc\";s:65:\"Select the style of pagination you would like to use on the blog.\";s:2:\"id\";s:19:\"woo_pagination_type\";s:4:\"type\";s:7:\"select2\";s:7:\"options\";a:2:{s:15:\"paginated_links\";s:7:\"Numbers\";s:6:\"simple\";s:13:\"Next/Previous\";}}i:12;a:3:{s:4:\"name\";s:15:\"Styling Options\";s:4:\"icon\";s:7:\"styling\";s:4:\"type\";s:7:\"heading\";}i:13;a:5:{s:4:\"name\";s:21:\"Body Background Color\";s:4:\"desc\";s:66:\"Pick a custom color for background color of the theme e.g. #697e09\";s:2:\"id\";s:14:\"woo_body_color\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:5:\"color\";}i:14;a:5:{s:4:\"name\";s:21:\"Body background image\";s:4:\"desc\";s:42:\"Upload an image for the theme\'s background\";s:2:\"id\";s:12:\"woo_body_img\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:15;a:6:{s:4:\"name\";s:23:\"Background image repeat\";s:4:\"desc\";s:56:\"Select how you would like to repeat the background-image\";s:2:\"id\";s:15:\"woo_body_repeat\";s:3:\"std\";s:9:\"no-repeat\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:4:{i:0;s:9:\"no-repeat\";i:1;s:8:\"repeat-x\";i:2;s:8:\"repeat-y\";i:3;s:6:\"repeat\";}}i:16;a:6:{s:4:\"name\";s:25:\"Background image position\";s:4:\"desc\";s:52:\"Select how you would like to position the background\";s:2:\"id\";s:12:\"woo_body_pos\";s:3:\"std\";s:3:\"top\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:9:{i:0;s:8:\"top left\";i:1;s:10:\"top center\";i:2;s:9:\"top right\";i:3;s:11:\"center left\";i:4;s:13:\"center center\";i:5;s:12:\"center right\";i:6;s:11:\"bottom left\";i:7;s:13:\"bottom center\";i:8;s:12:\"bottom right\";}}i:17;a:5:{s:4:\"name\";s:10:\"Link Color\";s:4:\"desc\";s:66:\"Pick a custom color for links or add a hex color code e.g. #697e09\";s:2:\"id\";s:14:\"woo_link_color\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:5:\"color\";}i:18;a:5:{s:4:\"name\";s:16:\"Link Hover Color\";s:4:\"desc\";s:72:\"Pick a custom color for links hover or add a hex color code e.g. #697e09\";s:2:\"id\";s:20:\"woo_link_hover_color\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:5:\"color\";}i:19;a:5:{s:4:\"name\";s:12:\"Button Color\";s:4:\"desc\";s:68:\"Pick a custom color for buttons or add a hex color code e.g. #697e09\";s:2:\"id\";s:16:\"woo_button_color\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:5:\"color\";}i:20;a:3:{s:4:\"name\";s:19:\"Header Contact Info\";s:4:\"icon\";s:6:\"header\";s:4:\"type\";s:7:\"heading\";}i:21;a:5:{s:4:\"name\";s:12:\"Company Name\";s:4:\"desc\";s:54:\"Specify the name that will be displayed in the header.\";s:2:\"id\";s:23:\"woo_header_company_name\";s:3:\"std\";s:21:\"Estate Property Group\";s:4:\"type\";s:4:\"text\";}i:22;a:5:{s:4:\"name\";s:22:\"Company Contact Number\";s:4:\"desc\";s:64:\"Specify the contact number that will be displayed in the header.\";s:2:\"id\";s:33:\"woo_header_company_contact_number\";s:3:\"std\";s:15:\"+27 21 555 5555\";s:4:\"type\";s:4:\"text\";}i:23;a:5:{s:4:\"name\";s:21:\"Company Email Address\";s:4:\"desc\";s:63:\"Specify the email address that will be displayed in the header.\";s:2:\"id\";s:24:\"woo_header_company_email\";s:3:\"std\";s:16:\"info@example.com\";s:4:\"type\";s:4:\"text\";}i:24;a:3:{s:4:\"name\";s:7:\"Layouts\";s:4:\"icon\";s:6:\"layout\";s:4:\"type\";s:7:\"heading\";}i:25;a:6:{s:4:\"name\";s:15:\"Homepage layout\";s:4:\"desc\";s:33:\"Select a layout for your homepage\";s:2:\"id\";s:15:\"woo_home_layout\";s:3:\"std\";s:12:\"With sidebar\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:2:{i:0;s:12:\"With sidebar\";i:1;s:15:\"Without Sidebar\";}}i:26;a:6:{s:4:\"name\";s:26:\"Property Singlepage layout\";s:4:\"desc\";s:35:\"Select a layout for your singlepage\";s:2:\"id\";s:26:\"woo_property_single_layout\";s:3:\"std\";s:15:\"Without sidebar\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:2:{i:0;s:12:\"With sidebar\";i:1;s:15:\"Without Sidebar\";}}i:27;a:6:{s:4:\"name\";s:27:\"Property Archivepage layout\";s:4:\"desc\";s:103:\"Select a layout for your property taxonomy archivepage (Locations, Property Types, Additional Features)\";s:2:\"id\";s:27:\"woo_property_archive_layout\";s:3:\"std\";s:15:\"Without sidebar\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:2:{i:0;s:12:\"With sidebar\";i:1;s:15:\"Without Sidebar\";}}i:28;a:6:{s:4:\"name\";s:26:\"Property Searchpage layout\";s:4:\"desc\";s:35:\"Select a layout for your searchpage\";s:2:\"id\";s:26:\"woo_property_search_layout\";s:3:\"std\";s:15:\"Without sidebar\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:2:{i:0;s:12:\"With sidebar\";i:1;s:15:\"Without Sidebar\";}}i:29;a:3:{s:4:\"name\";s:14:\"Featured Panel\";s:4:\"icon\";s:8:\"featured\";s:4:\"type\";s:7:\"heading\";}i:30;a:5:{s:4:\"name\";s:21:\"Enable Featured Panel\";s:4:\"desc\";s:42:\"Show the featured panel on the front page.\";s:2:\"id\";s:12:\"woo_featured\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:31;a:5:{s:4:\"name\";s:20:\"Featured Panel Title\";s:4:\"desc\";s:89:\"Include a short title for your featured panel on the home page, e.g. Featured Properties.\";s:2:\"id\";s:19:\"woo_featured_header\";s:3:\"std\";s:19:\"Featured Properties\";s:4:\"type\";s:4:\"text\";}i:32;a:5:{s:4:\"name\";s:12:\"Featured Tag\";s:4:\"desc\";s:252:\"Add comma separated list for the tags that you would like to have displayed in the featured section on your homepage. For example, if you add \'tag1, tag3\' here, then all properties tagged with either \'tag1\' or \'tag3\' will be shown in the featured area.\";s:2:\"id\";s:17:\"woo_featured_tags\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:33;a:6:{s:4:\"name\";s:16:\"Featured Entries\";s:4:\"desc\";s:79:\"Select the number of property entries that should appear in the Featured panel.\";s:2:\"id\";s:20:\"woo_featured_entries\";s:3:\"std\";s:1:\"3\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:20:{i:0;s:16:\"Select a number:\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";i:8;s:1:\"8\";i:9;s:1:\"9\";i:10;s:2:\"10\";i:11;s:2:\"11\";i:12;s:2:\"12\";i:13;s:2:\"13\";i:14;s:2:\"14\";i:15;s:2:\"15\";i:16;s:2:\"16\";i:17;s:2:\"17\";i:18;s:2:\"18\";i:19;s:2:\"19\";}}i:34;a:6:{s:4:\"name\";s:21:\"Slider Image Position\";s:4:\"desc\";s:50:\"Select the alignment for the featured slider image\";s:2:\"id\";s:16:\"woo_slider_image\";s:3:\"std\";s:4:\"Left\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:2:{i:0;s:4:\"Left\";i:1;s:5:\"Right\";}}i:35;a:5:{s:4:\"name\";s:10:\"Auto Start\";s:4:\"desc\";s:86:\"Set the slider to start sliding automatically. Adjust the speed of sliding underneath.\";s:2:\"id\";s:15:\"woo_slider_auto\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:36;a:6:{s:4:\"name\";s:15:\"Animation Speed\";s:4:\"desc\";s:74:\"The time in <b>seconds</b> the animation between frames will take e.g. 0.6\";s:2:\"id\";s:16:\"woo_slider_speed\";s:3:\"std\";d:0.59999999999999997779553950749686919152736663818359375;s:4:\"type\";s:6:\"select\";s:7:\"options\";a:21:{i:0;s:3:\"0.0\";i:1;s:3:\"0.1\";i:2;s:3:\"0.2\";i:3;s:3:\"0.3\";i:4;s:3:\"0.4\";i:5;s:3:\"0.5\";i:6;s:3:\"0.6\";i:7;s:3:\"0.7\";i:8;s:3:\"0.8\";i:9;s:3:\"0.9\";i:10;s:3:\"1.0\";i:11;s:3:\"1.1\";i:12;s:3:\"1.2\";i:13;s:3:\"1.3\";i:14;s:3:\"1.4\";i:15;s:3:\"1.5\";i:16;s:3:\"1.6\";i:17;s:3:\"1.7\";i:18;s:3:\"1.8\";i:19;s:3:\"1.9\";i:20;s:3:\"2.0\";}}i:37;a:6:{s:4:\"name\";s:19:\"Auto Slide Interval\";s:4:\"desc\";s:118:\"The time in <b>seconds</b> each slide pauses for, before sliding to the next. Only when using Auto Start option above.\";s:2:\"id\";s:19:\"woo_slider_interval\";s:3:\"std\";s:1:\"4\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:10:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";i:4;s:1:\"5\";i:5;s:1:\"6\";i:6;s:1:\"7\";i:7;s:1:\"8\";i:8;s:1:\"9\";i:9;s:2:\"10\";}}i:38;a:3:{s:4:\"name\";s:21:\"More Properties Panel\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:39;a:5:{s:4:\"name\";s:21:\"More Properties Title\";s:4:\"desc\";s:93:\"Include a short title for the More Properties section on the home page, e.g. More Properties.\";s:2:\"id\";s:15:\"woo_more_header\";s:3:\"std\";s:15:\"More Properties\";s:4:\"type\";s:4:\"text\";}i:40;a:6:{s:4:\"name\";s:12:\"More Entries\";s:4:\"desc\";s:70:\"Select the number of entries that should appear in the Featured panel.\";s:2:\"id\";s:16:\"woo_more_entries\";s:3:\"std\";s:1:\"3\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:5:{i:0;s:16:\"Select a number:\";i:1;s:1:\"3\";i:2;s:1:\"6\";i:3;s:1:\"9\";i:4;s:2:\"12\";}}i:41;a:5:{s:4:\"name\";s:27:\"Enable More Properties Link\";s:4:\"desc\";s:57:\"Show the archives link below the more properties section.\";s:2:\"id\";s:17:\"woo_archives_link\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:42;a:3:{s:4:\"name\";s:15:\"Property Labels\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:43;a:5:{s:4:\"name\";s:22:\"Property Prefix Symbol\";s:4:\"desc\";s:113:\"Specify the prefix that will be attached to a property to give it a unique number that can be used in the search.\";s:2:\"id\";s:26:\"woo_estate_property_prefix\";s:3:\"std\";s:4:\"PROP\";s:4:\"type\";s:4:\"text\";}i:44;a:5:{s:4:\"name\";s:15:\"Currency Symbol\";s:4:\"desc\";s:65:\"Specify the currency that your properties price will be shown in.\";s:2:\"id\";s:19:\"woo_estate_currency\";s:3:\"std\";s:1:\"$\";s:4:\"type\";s:4:\"text\";}i:45;a:5:{s:4:\"name\";s:13:\"On Show Label\";s:4:\"desc\";s:71:\"Specify the text that will be displayed on the On Show property labels.\";s:2:\"id\";s:17:\"woo_label_on_show\";s:3:\"std\";s:7:\"On Show\";s:4:\"type\";s:4:\"text\";}i:46;a:5:{s:4:\"name\";s:20:\"Garages Plural Label\";s:4:\"desc\";s:81:\"Specify the text that will be displayed on the frontend for more than one Garage.\";s:2:\"id\";s:17:\"woo_label_garages\";s:3:\"std\";s:7:\"Garages\";s:4:\"type\";s:4:\"text\";}i:47;a:5:{s:4:\"name\";s:22:\"Garages Singular Label\";s:4:\"desc\";s:76:\"Specify the text that will be displayed on the frontend for a single Garage.\";s:2:\"id\";s:16:\"woo_label_garage\";s:3:\"std\";s:6:\"Garage\";s:4:\"type\";s:4:\"text\";}i:48;a:5:{s:4:\"name\";s:21:\"Unit of Measure Label\";s:4:\"desc\";s:80:\"Specify the text that will be displayed on the frontend for the Unit of Measure.\";s:2:\"id\";s:21:\"woo_label_size_metric\";s:3:\"std\";s:5:\"sq ft\";s:4:\"type\";s:4:\"text\";}i:49;a:5:{s:4:\"name\";s:17:\"Beds Plural Label\";s:4:\"desc\";s:78:\"Specify the text that will be displayed on the frontend for more than one Bed.\";s:2:\"id\";s:14:\"woo_label_beds\";s:3:\"std\";s:4:\"Beds\";s:4:\"type\";s:4:\"text\";}i:50;a:5:{s:4:\"name\";s:19:\"Beds Singular Label\";s:4:\"desc\";s:73:\"Specify the text that will be displayed on the frontend for a single Bed.\";s:2:\"id\";s:13:\"woo_label_bed\";s:3:\"std\";s:3:\"Bed\";s:4:\"type\";s:4:\"text\";}i:51;a:5:{s:4:\"name\";s:18:\"Baths Plural Label\";s:4:\"desc\";s:80:\"Specify the text that will be displayed on the frontend for more than one Baths.\";s:2:\"id\";s:15:\"woo_label_baths\";s:3:\"std\";s:5:\"Baths\";s:4:\"type\";s:4:\"text\";}i:52;a:5:{s:4:\"name\";s:22:\"Bathrooms Plural Label\";s:4:\"desc\";s:102:\"Specify the text that will be displayed on the frontend search and backend for more than one Bathroom.\";s:2:\"id\";s:20:\"woo_label_baths_long\";s:3:\"std\";s:9:\"Bathrooms\";s:4:\"type\";s:4:\"text\";}i:53;a:5:{s:4:\"name\";s:20:\"Baths Singular Label\";s:4:\"desc\";s:74:\"Specify the text that will be displayed on the frontend for a single Bath.\";s:2:\"id\";s:14:\"woo_label_bath\";s:3:\"std\";s:4:\"Bath\";s:4:\"type\";s:4:\"text\";}i:54;a:3:{s:4:\"name\";s:14:\"Property Icons\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:55;a:5:{s:4:\"name\";s:16:\"Garages Big Icon\";s:4:\"desc\";s:194:\"Upload a big icon for property garages, or specify the image address of your online icon. (http://yoursite.com/icon-big.png) <br/><strong>For best results use a 25px x 25px sized image.</strong>\";s:2:\"id\";s:19:\"woo_garage_logo_big\";s:3:\"std\";s:93:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-garage-big.png\";s:4:\"type\";s:6:\"upload\";}i:56;a:5:{s:4:\"name\";s:18:\"Garages Small Icon\";s:4:\"desc\";s:198:\"Upload a small icon for property garages, or specify the image address of your online icon. (http://yoursite.com/icon-small.png) <br/><strong>For best results use a 18px x 18px sized image.</strong>\";s:2:\"id\";s:21:\"woo_garage_logo_small\";s:3:\"std\";s:95:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-garage-small.png\";s:4:\"type\";s:6:\"upload\";}i:57;a:5:{s:4:\"name\";s:13:\"Beds Big Icon\";s:4:\"desc\";s:191:\"Upload a big icon for property beds, or specify the image address of your online icon. (http://yoursite.com/icon-big.png) <br/><strong>For best results use a 25px x 25px sized image.</strong>\";s:2:\"id\";s:16:\"woo_bed_logo_big\";s:3:\"std\";s:90:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bed-big.png\";s:4:\"type\";s:6:\"upload\";}i:58;a:5:{s:4:\"name\";s:15:\"Beds Small Icon\";s:4:\"desc\";s:195:\"Upload a small icon for property beds, or specify the image address of your online icon. (http://yoursite.com/icon-small.png) <br/><strong>For best results use a 18px x 18px sized image.</strong>\";s:2:\"id\";s:18:\"woo_bed_logo_small\";s:3:\"std\";s:92:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bed-small.png\";s:4:\"type\";s:6:\"upload\";}i:59;a:5:{s:4:\"name\";s:14:\"Baths Big Icon\";s:4:\"desc\";s:192:\"Upload a big icon for property baths, or specify the image address of your online icon. (http://yoursite.com/icon-big.png) <br/><strong>For best results use a 25px x 25px sized image.</strong>\";s:2:\"id\";s:17:\"woo_bath_logo_big\";s:3:\"std\";s:91:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bath-big.png\";s:4:\"type\";s:6:\"upload\";}i:60;a:5:{s:4:\"name\";s:16:\"Baths Small Icon\";s:4:\"desc\";s:196:\"Upload a small icon for property baths, or specify the image address of your online icon. (http://yoursite.com/icon-small.png) <br/><strong>For best results use a 18px x 18px sized image.</strong>\";s:2:\"id\";s:19:\"woo_bath_logo_small\";s:3:\"std\";s:93:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bath-small.png\";s:4:\"type\";s:6:\"upload\";}i:61;a:5:{s:4:\"name\";s:13:\"Size Big Icon\";s:4:\"desc\";s:195:\"Upload a big icon for the property size, or specify the image address of your online icon. (http://yoursite.com/icon-big.png) <br/><strong>For best results use a 25px x 25px sized image.</strong>\";s:2:\"id\";s:17:\"woo_size_logo_big\";s:3:\"std\";s:91:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-size-big.png\";s:4:\"type\";s:6:\"upload\";}i:62;a:5:{s:4:\"name\";s:15:\"Size Small Icon\";s:4:\"desc\";s:199:\"Upload a small icon for the property size, or specify the image address of your online icon. (http://yoursite.com/icon-small.png) <br/><strong>For best results use a 18px x 18px sized image.</strong>\";s:2:\"id\";s:19:\"woo_size_logo_small\";s:3:\"std\";s:93:\"http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-size-small.png\";s:4:\"type\";s:6:\"upload\";}i:63;a:3:{s:4:\"name\";s:15:\"Property Search\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:64;a:5:{s:4:\"name\";s:16:\"Search box Title\";s:4:\"desc\";s:86:\"Include a short title for the search box on the home page, e.g. Search Our Properties.\";s:2:\"id\";s:17:\"woo_search_header\";s:3:\"std\";s:21:\"Search Our Properties\";s:4:\"type\";s:4:\"text\";}i:65;a:5:{s:4:\"name\";s:19:\"Search Keyword Text\";s:4:\"desc\";s:53:\"Default text that is displayed in the search textbox.\";s:2:\"id\";s:23:\"woo_search_keyword_text\";s:3:\"std\";s:9:\"Search...\";s:4:\"type\";s:4:\"text\";}i:66;a:5:{s:4:\"name\";s:42:\"Display Searchbox on Single property pages\";s:4:\"desc\";s:80:\"Enable if you want the searchbox to be displayed when viewing single properties.\";s:2:\"id\";s:24:\"woo_displaysearch_single\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:67;a:6:{s:4:\"name\";s:14:\"Search Results\";s:4:\"desc\";s:75:\"Select the number of entries that should appear on the search results page.\";s:2:\"id\";s:27:\"woo_property_search_results\";s:3:\"std\";s:1:\"3\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:20:{i:0;s:16:\"Select a number:\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";i:8;s:1:\"8\";i:9;s:1:\"9\";i:10;s:2:\"10\";i:11;s:2:\"11\";i:12;s:2:\"12\";i:13;s:2:\"13\";i:14;s:2:\"14\";i:15;s:2:\"15\";i:16;s:2:\"16\";i:17;s:2:\"17\";i:18;s:2:\"18\";i:19;s:2:\"19\";}}i:68;a:6:{s:4:\"name\";s:34:\"Search by Features Matching Method\";s:4:\"desc\";s:321:\"Choose the matching method for Search Results. <br /><strong>Exact Match</strong> means only properties with the same number of baths, beds, garages searched for will be returned while <strong>Minimum Value</strong> means that all properties with at least the amount of baths, beds, garages searched for will be returned.\";s:2:\"id\";s:27:\"woo_feature_matching_method\";s:3:\"std\";s:5:\"exact\";s:4:\"type\";s:5:\"radio\";s:7:\"options\";a:2:{s:5:\"exact\";s:11:\"Exact Match\";s:7:\"minimum\";s:13:\"Minimum Value\";}}i:69;a:5:{s:4:\"name\";s:15:\"Sale Type Label\";s:4:\"desc\";s:89:\"Specify the text that will be displayed on the frontend search and backend for Sale Type.\";s:2:\"id\";s:19:\"woo_label_sale_type\";s:3:\"std\";s:9:\"Sale Type\";s:4:\"type\";s:4:\"text\";}i:70;a:5:{s:4:\"name\";s:14:\"For Sale Label\";s:4:\"desc\";s:97:\"Specify the text that will be displayed on the frontend search and backend for the For Sale text.\";s:2:\"id\";s:18:\"woo_label_for_sale\";s:3:\"std\";s:8:\"For Sale\";s:4:\"type\";s:4:\"text\";}i:71;a:5:{s:4:\"name\";s:14:\"For Rent Label\";s:4:\"desc\";s:97:\"Specify the text that will be displayed on the frontend search and backend for the For Rent text.\";s:2:\"id\";s:18:\"woo_label_for_rent\";s:3:\"std\";s:8:\"For Rent\";s:4:\"type\";s:4:\"text\";}i:72;a:5:{s:4:\"name\";s:30:\"Property Location & Type Label\";s:4:\"desc\";s:102:\"Specify the text that will be displayed on the frontend search for the Property Location & Type label.\";s:2:\"id\";s:36:\"woo_label_property_location_and_type\";s:3:\"std\";s:28:\"Property Location &amp; Type\";s:4:\"type\";s:4:\"text\";}i:73;a:5:{s:4:\"name\";s:33:\"Locations Dropdown View All Label\";s:4:\"desc\";s:98:\"Specify the text that will be displayed on the frontend search Locations dropdown View All option.\";s:2:\"id\";s:37:\"woo_label_locations_dropdown_view_all\";s:3:\"std\";s:18:\"View all Locations\";s:4:\"type\";s:4:\"text\";}i:74;a:5:{s:4:\"name\";s:37:\"Property Type Dropdown View All Label\";s:4:\"desc\";s:102:\"Specify the text that will be displayed on the frontend search Property Type dropdown View All option.\";s:2:\"id\";s:41:\"woo_label_property_type_dropdown_view_all\";s:3:\"std\";s:23:\"View all Property Types\";s:4:\"type\";s:4:\"text\";}i:75;a:5:{s:4:\"name\";s:15:\"Min Price Label\";s:4:\"desc\";s:87:\"Specify the text that will be displayed on the frontend search for the Min Price label.\";s:2:\"id\";s:19:\"woo_label_min_price\";s:3:\"std\";s:9:\"Min Price\";s:4:\"type\";s:4:\"text\";}i:76;a:5:{s:4:\"name\";s:15:\"Max Price Label\";s:4:\"desc\";s:87:\"Specify the text that will be displayed on the frontend search for the Max Price label.\";s:2:\"id\";s:19:\"woo_label_max_price\";s:3:\"std\";s:9:\"Max Price\";s:4:\"type\";s:4:\"text\";}i:77;a:5:{s:4:\"name\";s:28:\"Advanced Search Button Label\";s:4:\"desc\";s:86:\"Specify the text that will be displayed on the frontend search Advanced Search Button.\";s:2:\"id\";s:25:\"woo_label_advanced_search\";s:3:\"std\";s:15:\"Advanced Search\";s:4:\"type\";s:4:\"text\";}i:78;a:5:{s:4:\"name\";s:14:\"Min Size Label\";s:4:\"desc\";s:86:\"Specify the text that will be displayed on the frontend search for the Min Size label.\";s:2:\"id\";s:18:\"woo_label_min_size\";s:3:\"std\";s:8:\"Min Size\";s:4:\"type\";s:4:\"text\";}i:79;a:5:{s:4:\"name\";s:14:\"Max Size Label\";s:4:\"desc\";s:86:\"Specify the text that will be displayed on the frontend search for the Max Size label.\";s:2:\"id\";s:18:\"woo_label_max_size\";s:3:\"std\";s:8:\"Max Size\";s:4:\"type\";s:4:\"text\";}i:80;a:3:{s:4:\"name\";s:20:\"Single Property Page\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:81;a:5:{s:4:\"name\";s:13:\"On Show Label\";s:4:\"desc\";s:102:\"Specify the text that will be displayed below the address for the property if the property is on show.\";s:2:\"id\";s:34:\"woo_label_property_details_on_show\";s:3:\"std\";s:34:\"This property is currently on show\";s:4:\"type\";s:4:\"text\";}i:82;a:5:{s:4:\"name\";s:25:\"Additional Features Label\";s:4:\"desc\";s:87:\"Specify the text that will be displayed above the Additional Features for the property.\";s:2:\"id\";s:29:\"woo_label_additional_features\";s:3:\"std\";s:8:\"Features\";s:4:\"type\";s:4:\"text\";}i:83;a:5:{s:4:\"name\";s:25:\"Additional Features Links\";s:4:\"desc\";s:109:\"Enable if you want the Features panel on a single property page to have links to their taxonomy archive page.\";s:2:\"id\";s:33:\"woo_clickable_additional_features\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:84;a:5:{s:4:\"name\";s:13:\"Gallery Label\";s:4:\"desc\";s:75:\"Specify the text that will be displayed above the Gallery for the property.\";s:2:\"id\";s:17:\"woo_label_gallery\";s:3:\"std\";s:7:\"Gallery\";s:4:\"type\";s:4:\"text\";}i:85;a:5:{s:4:\"name\";s:18:\"Virtual Tour Label\";s:4:\"desc\";s:80:\"Specify the text that will be displayed above the Virtual Tour for the property.\";s:2:\"id\";s:22:\"woo_label_virtual_tour\";s:3:\"std\";s:12:\"Virtual Tour\";s:4:\"type\";s:4:\"text\";}i:86;a:5:{s:4:\"name\";s:18:\"Property Map Label\";s:4:\"desc\";s:78:\"Specify the text that will be displayed above the Google Map for the property.\";s:2:\"id\";s:22:\"woo_label_property_map\";s:3:\"std\";s:12:\"Property Map\";s:4:\"type\";s:4:\"text\";}i:87;a:5:{s:4:\"name\";s:29:\"Related Properties Tour Label\";s:4:\"desc\";s:69:\"Specify the text that will be displayed above the Related Properties.\";s:2:\"id\";s:28:\"woo_label_related_properties\";s:3:\"std\";s:28:\"More properties in this area\";s:4:\"type\";s:4:\"text\";}i:88;a:5:{s:4:\"name\";s:26:\"Contact Agent Button Label\";s:4:\"desc\";s:68:\"Specify the text that will be displayed on the Contact Agent button.\";s:2:\"id\";s:30:\"woo_label_contact_agent_button\";s:3:\"std\";s:13:\"Contact Agent\";s:4:\"type\";s:4:\"text\";}i:89;a:5:{s:4:\"name\";s:17:\"Email Agent Label\";s:4:\"desc\";s:71:\"Specify the text that will be displayed on the link to email the Agent.\";s:2:\"id\";s:26:\"woo_label_agent_email_link\";s:3:\"std\";s:16:\"Email this agent\";s:4:\"type\";s:4:\"text\";}i:90;a:5:{s:4:\"name\";s:32:\"Contact Agent using Contact Form\";s:4:\"desc\";s:108:\"Will use the contact form as the means to contact the properties agent instead of opening your email client.\";s:2:\"id\";s:21:\"woo_contact_form_link\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:91;a:6:{s:4:\"name\";s:17:\"Contact Form Page\";s:4:\"desc\";s:108:\"Select the page that your Contact Form is on. Remember to apply the Contact Form page template to this page.\";s:2:\"id\";s:21:\"woo_contact_form_page\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:1:{i:0;s:14:\"Select a page:\";}}i:92;a:3:{s:4:\"name\";s:11:\"Agent Setup\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:93;a:5:{s:4:\"name\";s:13:\"New User Role\";s:4:\"desc\";s:32:\"Create new User Role for Agents.\";s:2:\"id\";s:26:\"woo_agent_user_role_enable\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:94;a:5:{s:4:\"name\";s:15:\"Agent Role Name\";s:4:\"desc\";s:54:\"Specify the name that will be used to identify Agents.\";s:2:\"id\";s:19:\"woo_agent_role_name\";s:3:\"std\";s:5:\"Agent\";s:4:\"type\";s:4:\"text\";}i:95;a:6:{s:4:\"name\";s:26:\"Agent Default Capabilities\";s:4:\"desc\";s:50:\"Select the default user capabilities for an agent.\";s:2:\"id\";s:22:\"woo_agent_role_default\";s:3:\"std\";s:14:\"Select a Role:\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:6:{i:0;s:14:\"Select a Role:\";s:13:\"administrator\";s:13:\"administrator\";s:6:\"editor\";s:6:\"editor\";s:6:\"author\";s:6:\"author\";s:11:\"contributor\";s:11:\"contributor\";s:10:\"subscriber\";s:10:\"subscriber\";}}i:96;a:3:{s:4:\"name\";s:4:\"Maps\";s:4:\"icon\";s:4:\"maps\";s:4:\"type\";s:7:\"heading\";}i:97;a:6:{s:4:\"name\";s:19:\"Google Maps API Key\";s:4:\"desc\";s:171:\"Enter your Google Maps API key before using any of Postcard\'s mapping functionality. <a href=\'http://code.google.com/apis/maps/signup.html\'>Signup for an API key here</a>.\";s:2:\"id\";s:15:\"woo_maps_apikey\";s:3:\"std\";s:0:\"\";s:5:\"class\";s:6:\"hidden\";s:4:\"type\";s:4:\"text\";}i:98;a:5:{s:4:\"name\";s:19:\"Disable Mousescroll\";s:4:\"desc\";s:112:\"Turn off the mouse scroll action for all the Google Maps on the site. This could improve usability on your site.\";s:2:\"id\";s:15:\"woo_maps_scroll\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:99;a:5:{s:4:\"name\";s:22:\"Single Page Map Height\";s:4:\"desc\";s:60:\"Height in pixels for the maps displayed on Single.php pages.\";s:2:\"id\";s:22:\"woo_maps_single_height\";s:3:\"std\";s:3:\"250\";s:4:\"type\";s:4:\"text\";}i:100;a:5:{s:4:\"name\";s:40:\"Enable Latitude & Longitude Coordinates:\";s:4:\"desc\";s:64:\"Enable or disable coordinates in the head of single posts pages.\";s:2:\"id\";s:10:\"woo_coords\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:101;a:6:{s:4:\"name\";s:22:\"Default Map Zoom Level\";s:4:\"desc\";s:63:\"Set this to adjust the default in the post & page edit backend.\";s:2:\"id\";s:24:\"woo_maps_default_mapzoom\";s:3:\"std\";s:1:\"9\";s:4:\"type\";s:7:\"select2\";s:7:\"options\";a:20:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";i:8;s:1:\"8\";i:9;s:1:\"9\";i:10;s:2:\"10\";i:11;s:2:\"11\";i:12;s:2:\"12\";i:13;s:2:\"13\";i:14;s:2:\"14\";i:15;s:2:\"15\";i:16;s:2:\"16\";i:17;s:2:\"17\";i:18;s:2:\"18\";i:19;s:2:\"19\";}}i:102;a:6:{s:4:\"name\";s:16:\"Default Map Type\";s:4:\"desc\";s:53:\"Set this to the default rendered in the post backend.\";s:2:\"id\";s:24:\"woo_maps_default_maptype\";s:3:\"std\";s:6:\"Normal\";s:4:\"type\";s:7:\"select2\";s:7:\"options\";a:4:{s:12:\"G_NORMAL_MAP\";s:6:\"Normal\";s:15:\"G_SATELLITE_MAP\";s:9:\"Satellite\";s:12:\"G_HYBRID_MAP\";s:6:\"Hybrid\";s:14:\"G_PHYSICAL_MAP\";s:7:\"Terrain\";}}i:103;a:3:{s:4:\"name\";s:24:\"Colored & Custom Markers\";s:4:\"icon\";s:4:\"maps\";s:4:\"type\";s:7:\"heading\";}i:104;a:6:{s:4:\"name\";s:16:\"Marker Pin Color\";s:4:\"desc\";s:33:\"Choose from a preset colored pin.\";s:2:\"id\";s:20:\"woo_cat_colors_pages\";s:3:\"std\";s:3:\"red\";s:4:\"type\";s:7:\"select2\";s:7:\"options\";a:9:{s:4:\"blue\";s:4:\"Blue\";s:3:\"red\";s:3:\"Red\";s:5:\"green\";s:5:\"Green\";s:6:\"yellow\";s:6:\"Yellow\";s:4:\"pink\";s:4:\"Pink\";s:6:\"purple\";s:6:\"Purple\";s:4:\"teal\";s:4:\"Teal\";s:5:\"white\";s:5:\"White\";s:5:\"black\";s:5:\"Black\";}}i:105;a:6:{s:4:\"name\";s:0:\"\";s:4:\"desc\";s:132:\"Add a custom image. Find more <a href=\'http://groups.google.com/group/google-chart-api/web/chart-types-for-map-pins?pli=1\'>here</a>.\";s:2:\"id\";s:27:\"woo_cat_custom_marker_pages\";s:3:\"std\";s:0:\"\";s:5:\"class\";s:6:\"hidden\";s:4:\"type\";s:4:\"text\";}i:106;a:7:{s:4:\"name\";s:13:\"Uncategorized\";s:4:\"desc\";s:33:\"Choose from a preset colored pin.\";s:2:\"id\";s:16:\"woo_cat_colors_1\";s:3:\"std\";s:3:\"red\";s:4:\"type\";s:7:\"select2\";s:5:\"class\";s:6:\"hidden\";s:7:\"options\";a:9:{s:4:\"blue\";s:4:\"Blue\";s:3:\"red\";s:3:\"Red\";s:5:\"green\";s:5:\"Green\";s:6:\"yellow\";s:6:\"Yellow\";s:4:\"pink\";s:4:\"Pink\";s:6:\"purple\";s:6:\"Purple\";s:4:\"teal\";s:4:\"Teal\";s:5:\"white\";s:5:\"White\";s:5:\"black\";s:5:\"Black\";}}i:107;a:6:{s:4:\"name\";s:0:\"\";s:4:\"desc\";s:132:\"Add a custom image. Find more <a href=\'http://groups.google.com/group/google-chart-api/web/chart-types-for-map-pins?pli=1\'>here</a>.\";s:2:\"id\";s:23:\"woo_cat_custom_marker_1\";s:3:\"std\";s:0:\"\";s:5:\"class\";s:6:\"hidden\";s:4:\"type\";s:4:\"text\";}i:108;a:3:{s:4:\"name\";s:18:\"Navigation Options\";s:4:\"icon\";s:3:\"nav\";s:4:\"type\";s:7:\"heading\";}i:109;a:5:{s:4:\"name\";s:50:\"Display Locations Taxonomy in Secondary Menu area:\";s:4:\"desc\";s:209:\"Enabling this will output your Property Locations as menu links to their archive pages.<br /><strong> Note: If you enable the WordPress Menu Management option below, this option will not be outputted.</strong>\";s:2:\"id\";s:23:\"woo_location_menu_items\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:110;a:5:{s:4:\"name\";s:55:\"Display Property Types Taxonomy in Secondary Menu area:\";s:4:\"desc\";s:205:\"Enabling this will output your Property Types as menu links to their archive pages.<br /><strong> Note: If you enable the WordPress Menu Management option below, this option will not be outputted.</strong>\";s:2:\"id\";s:27:\"woo_propertytype_menu_items\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:111;a:5:{s:4:\"name\";s:56:\"Display Post Categories Taxonomy in Secondary Menu area:\";s:4:\"desc\";s:206:\"Enabling this will output your Post Categories as menu links to their archive pages.<br /><strong> Note: If you enable the WordPress Menu Management option below, this option will not be outputted.</strong>\";s:2:\"id\";s:23:\"woo_category_menu_items\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:112;a:3:{s:4:\"name\";s:14:\"Dynamic Images\";s:4:\"type\";s:7:\"heading\";s:4:\"icon\";s:5:\"image\";}i:113;a:5:{s:4:\"name\";s:22:\"Dynamic Image Resizing\";s:4:\"desc\";s:0:\"\";s:2:\"id\";s:18:\"woo_wpthumb_notice\";s:3:\"std\";s:220:\"There are two alternative methods of dynamically resizing the thumbnails in the theme, <strong>WP Post Thumbnail</strong> or <strong>TimThumb - Custom Settings panel</strong>. We recommend using WP Post Thumbnail option.\";s:4:\"type\";s:4:\"info\";}i:114;a:6:{s:4:\"name\";s:17:\"WP Post Thumbnail\";s:4:\"desc\";s:170:\"Use WordPress post thumbnail to assign a post thumbnail. Will enable the <strong>Featured Image panel</strong> in your post sidebar where you can assign a post thumbnail.\";s:2:\"id\";s:22:\"woo_post_image_support\";s:3:\"std\";s:4:\"true\";s:5:\"class\";s:9:\"collapsed\";s:4:\"type\";s:8:\"checkbox\";}i:115;a:6:{s:4:\"name\";s:42:\"WP Post Thumbnail - Dynamic Image Resizing\";s:4:\"desc\";s:113:\"The post thumbnail will be dynamically resized using native WP resize functionality. <em>(Requires PHP 5.2+)</em>\";s:2:\"id\";s:14:\"woo_pis_resize\";s:3:\"std\";s:4:\"true\";s:5:\"class\";s:6:\"hidden\";s:4:\"type\";s:8:\"checkbox\";}i:116;a:6:{s:4:\"name\";s:29:\"WP Post Thumbnail - Hard Crop\";s:4:\"desc\";s:119:\"The post thumbnail will be cropped to match the target aspect ratio (only used if \'Dynamic Image Resizing\' is enabled).\";s:2:\"id\";s:17:\"woo_pis_hard_crop\";s:3:\"std\";s:4:\"true\";s:5:\"class\";s:11:\"hidden last\";s:4:\"type\";s:8:\"checkbox\";}i:117;a:5:{s:4:\"name\";s:32:\"TimThumb - Custom Settings Panel\";s:4:\"desc\";s:358:\"This will enable the <a href=\'http://code.google.com/p/timthumb/\'>TimThumb</a> (thumb.php) script which dynamically resizes images added through the <strong>custom settings panel below the post</strong>. Make sure your themes <em>cache</em> folder is writable. <a href=\'http://www.woothemes.com/2008/10/troubleshooting-image-resizer-thumbphp/\'>Need help?</a>\";s:2:\"id\";s:10:\"woo_resize\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:118;a:5:{s:4:\"name\";s:25:\"Automatic Image Thumbnail\";s:4:\"desc\";s:81:\"If no thumbnail is specifified then the first uploaded image in the post is used.\";s:2:\"id\";s:12:\"woo_auto_img\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:119;a:5:{s:4:\"name\";s:26:\"Thumbnail Image Dimensions\";s:4:\"desc\";s:109:\"Enter an integer value i.e. 250 for the desired size which will be used when dynamically creating the images.\";s:2:\"id\";s:20:\"woo_image_dimensions\";s:3:\"std\";s:0:\"\";s:4:\"type\";a:2:{i:0;a:4:{s:2:\"id\";s:11:\"woo_thumb_w\";s:4:\"type\";s:4:\"text\";s:3:\"std\";i:100;s:4:\"meta\";s:5:\"Width\";}i:1;a:4:{s:2:\"id\";s:11:\"woo_thumb_h\";s:4:\"type\";s:4:\"text\";s:3:\"std\";i:100;s:4:\"meta\";s:6:\"Height\";}}}i:120;a:6:{s:4:\"name\";s:25:\"Thumbnail Image alignment\";s:4:\"desc\";s:47:\"Select how to align your thumbnails with posts.\";s:2:\"id\";s:15:\"woo_thumb_align\";s:3:\"std\";s:9:\"alignleft\";s:4:\"type\";s:5:\"radio\";s:7:\"options\";a:3:{s:9:\"alignleft\";s:4:\"Left\";s:10:\"alignright\";s:5:\"Right\";s:11:\"aligncenter\";s:6:\"Center\";}}i:121;a:5:{s:4:\"name\";s:30:\"Show thumbnail in Single Posts\";s:4:\"desc\";s:48:\"Show the attached image in the single post page.\";s:2:\"id\";s:16:\"woo_thumb_single\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:122;a:5:{s:4:\"name\";s:23:\"Single Image Dimensions\";s:4:\"desc\";s:69:\"Enter an integer value i.e. 250 for the image size. Max width is 576.\";s:2:\"id\";s:20:\"woo_image_dimensions\";s:3:\"std\";s:0:\"\";s:4:\"type\";a:2:{i:0;a:4:{s:2:\"id\";s:12:\"woo_single_w\";s:4:\"type\";s:4:\"text\";s:3:\"std\";i:200;s:4:\"meta\";s:5:\"Width\";}i:1;a:4:{s:2:\"id\";s:12:\"woo_single_h\";s:4:\"type\";s:4:\"text\";s:3:\"std\";i:200;s:4:\"meta\";s:6:\"Height\";}}}i:123;a:5:{s:4:\"name\";s:25:\"Add thumbnail to RSS feed\";s:4:\"desc\";s:68:\"Add the the image uploaded via your Custom Settings to your RSS feed\";s:2:\"id\";s:13:\"woo_rss_thumb\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:124;a:3:{s:4:\"name\";s:20:\"Footer Customization\";s:4:\"icon\";s:6:\"footer\";s:4:\"type\";s:7:\"heading\";}i:125;a:5:{s:4:\"name\";s:21:\"Custom Affiliate Link\";s:4:\"desc\";s:71:\"Add an affiliate link to the WooThemes logo in the footer of the theme.\";s:2:\"id\";s:19:\"woo_footer_aff_link\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:126;a:5:{s:4:\"name\";s:27:\"Enable Custom Footer (Left)\";s:4:\"desc\";s:58:\"Activate to add the custom text below to the theme footer.\";s:2:\"id\";s:15:\"woo_footer_left\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:127;a:5:{s:4:\"name\";s:18:\"Custom Text (Left)\";s:4:\"desc\";s:66:\"Custom HTML and Text that will appear in the footer of your theme.\";s:2:\"id\";s:20:\"woo_footer_left_text\";s:3:\"std\";s:7:\"<p></p>\";s:4:\"type\";s:8:\"textarea\";}i:128;a:5:{s:4:\"name\";s:28:\"Enable Custom Footer (Right)\";s:4:\"desc\";s:184:\"Activate to add the custom text below to the theme footer. Enabling the custom right footer disables the WooThemes logo link, which means you can\'t use the affiliate link option above.\";s:2:\"id\";s:16:\"woo_footer_right\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:129;a:5:{s:4:\"name\";s:19:\"Custom Text (Right)\";s:4:\"desc\";s:66:\"Custom HTML and Text that will appear in the footer of your theme.\";s:2:\"id\";s:21:\"woo_footer_right_text\";s:3:\"std\";s:7:\"<p></p>\";s:4:\"type\";s:8:\"textarea\";}i:130;a:3:{s:4:\"name\";s:19:\"Subscribe & Connect\";s:4:\"type\";s:7:\"heading\";s:4:\"icon\";s:7:\"connect\";}i:131;a:5:{s:4:\"name\";s:40:\"Enable Subscribe & Connect - Single Post\";s:4:\"desc\";s:168:\"Enable the subscribe & connect area on single posts. You can also add this as a <a href=\'http://www.lmmre.com/property/wp-admin/widgets.php\'>widget</a> in your sidebar.\";s:2:\"id\";s:11:\"woo_connect\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:132;a:5:{s:4:\"name\";s:15:\"Subscribe Title\";s:4:\"desc\";s:57:\"Enter the title to show in your subscribe & connect area.\";s:2:\"id\";s:17:\"woo_connect_title\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:133;a:5:{s:4:\"name\";s:4:\"Text\";s:4:\"desc\";s:37:\"Change the default text in this area.\";s:2:\"id\";s:19:\"woo_connect_content\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"textarea\";}i:134;a:5:{s:4:\"name\";s:35:\"Subscribe By E-mail ID (Feedburner)\";s:4:\"desc\";s:162:\"Enter your <a href=\'http://www.woothemes.com/tutorials/how-to-find-your-feedburner-id-for-email-subscription/\'>Feedburner ID</a> for the e-mail subscription form.\";s:2:\"id\";s:25:\"woo_connect_newsletter_id\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:135;a:6:{s:4:\"name\";s:32:\"Subscribe By E-mail to MailChimp\";i:0;s:9:\"woothemes\";s:4:\"desc\";s:189:\"If you have a MailChimp account you can enter the <a href=\"http://woochimp.heroku.com\" target=\"_blank\">MailChimp List Subscribe URL</a> to allow your users to subscribe to a MailChimp List.\";s:2:\"id\";s:30:\"woo_connect_mailchimp_list_url\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:136;a:5:{s:4:\"name\";s:10:\"Enable RSS\";s:4:\"desc\";s:34:\"Enable the subscribe and RSS icon.\";s:2:\"id\";s:15:\"woo_connect_rss\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}i:137;a:5:{s:4:\"name\";s:11:\"Twitter URL\";s:4:\"desc\";s:99:\"Enter your  <a href=\'http://www.twitter.com/\'>Twitter</a> URL e.g. http://www.twitter.com/woothemes\";s:2:\"id\";s:19:\"woo_connect_twitter\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:138;a:5:{s:4:\"name\";s:12:\"Facebook URL\";s:4:\"desc\";s:102:\"Enter your  <a href=\'http://www.facebook.com/\'>Facebook</a> URL e.g. http://www.facebook.com/woothemes\";s:2:\"id\";s:20:\"woo_connect_facebook\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:139;a:5:{s:4:\"name\";s:11:\"YouTube URL\";s:4:\"desc\";s:99:\"Enter your  <a href=\'http://www.youtube.com/\'>YouTube</a> URL e.g. http://www.youtube.com/woothemes\";s:2:\"id\";s:19:\"woo_connect_youtube\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:140;a:5:{s:4:\"name\";s:10:\"Flickr URL\";s:4:\"desc\";s:96:\"Enter your  <a href=\'http://www.flickr.com/\'>Flickr</a> URL e.g. http://www.flickr.com/woothemes\";s:2:\"id\";s:18:\"woo_connect_flickr\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:141;a:5:{s:4:\"name\";s:12:\"LinkedIn URL\";s:4:\"desc\";s:113:\"Enter your  <a href=\'http://www.www.linkedin.com.com/\'>LinkedIn</a> URL e.g. http://www.linkedin.com/in/woothemes\";s:2:\"id\";s:20:\"woo_connect_linkedin\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:142;a:5:{s:4:\"name\";s:13:\"Delicious URL\";s:4:\"desc\";s:104:\"Enter your <a href=\'http://www.delicious.com/\'>Delicious</a> URL e.g. http://www.delicious.com/woothemes\";s:2:\"id\";s:21:\"woo_connect_delicious\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:143;a:5:{s:4:\"name\";s:11:\"Google+ URL\";s:4:\"desc\";s:112:\"Enter your <a href=\'http://plus.google.com/\'>Google+</a> URL e.g. https://plus.google.com/104560124403688998123/\";s:2:\"id\";s:22:\"woo_connect_googleplus\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";}i:144;a:5:{s:4:\"name\";s:20:\"Enable Related Posts\";s:4:\"desc\";s:158:\"Enable related posts in the subscribe area. Uses posts with the same <strong>tags</strong> to find related posts. Note: Will not show in the Subscribe widget.\";s:2:\"id\";s:19:\"woo_connect_related\";s:3:\"std\";s:4:\"true\";s:4:\"type\";s:8:\"checkbox\";}}', 'yes'),
('99', 'WPLANG', '', 'yes'),
('100', 'cron', 'a:4:{i:1378086906;a:1:{s:20:\"jetpack_clean_nonces\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1378119360;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1378131547;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
('101', 'dashboard_widget_options', 'a:4:{s:25:\"dashboard_recent_comments\";a:1:{s:5:\"items\";i:5;}s:24:\"dashboard_incoming_links\";a:5:{s:4:\"home\";s:29:\"http://www.lmmre.com/property\";s:4:\"link\";s:105:\"http://blogsearch.google.com/blogsearch?scoring=d&partner=wordpress&q=link:http://www.lmmre.com/property/\";s:3:\"url\";s:138:\"http://blogsearch.google.com/blogsearch_feeds?scoring=d&ie=utf-8&num=10&output=rss&partner=wordpress&q=link:http://www.lmmre.com/property/\";s:5:\"items\";i:10;s:9:\"show_date\";b:0;}s:17:\"dashboard_primary\";a:7:{s:4:\"link\";s:26:\"http://wordpress.org/news/\";s:3:\"url\";s:31:\"http://wordpress.org/news/feed/\";s:5:\"title\";s:14:\"WordPress Blog\";s:5:\"items\";i:2;s:12:\"show_summary\";i:1;s:11:\"show_author\";i:0;s:9:\"show_date\";i:1;}s:19:\"dashboard_secondary\";a:7:{s:4:\"link\";s:28:\"http://planet.wordpress.org/\";s:3:\"url\";s:33:\"http://planet.wordpress.org/feed/\";s:5:\"title\";s:20:\"Other WordPress News\";s:5:\"items\";i:5;s:12:\"show_summary\";i:0;s:11:\"show_author\";i:0;s:9:\"show_date\";i:0;}}', 'yes'),
('102', 'theme_mods_twentytwelve', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1367578812;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
('103', 'current_theme', 'Estate', 'yes'),
('104', 'theme_mods_estate', 'a:2:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:12:\"primary-menu\";i:390;s:14:\"secondary-menu\";i:19;}}', 'yes'),
('105', 'theme_switched', '', 'yes'),
('106', 'woo_timthumb_update', '', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('107', 'woo_framework_version', '5.5.5', 'yes'),
('262', 'recently_activated', 'a:1:{s:23:\"admin-bar/admin-bar.php\";i:1371822666;}', 'yes'),
('112', 'woo_themename', 'Estate', 'yes'),
('113', 'woo_shortname', 'woo', 'yes'),
('114', 'woo_manual', 'http://www.woothemes.com/support/theme-documentation/estate/', 'yes'),
('115', 'woo_custom_template', 'a:13:{i:0;a:4:{s:4:\"name\";s:5:\"image\";s:5:\"label\";s:5:\"Image\";s:4:\"type\";s:6:\"upload\";s:4:\"desc\";s:19:\"Upload file here...\";}i:1;a:5:{s:4:\"name\";s:7:\"address\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:16:\"Physical Address\";s:4:\"type\";s:4:\"text\";s:4:\"desc\";s:43:\"Enter the physical address of the property.\";}i:2;a:5:{s:4:\"name\";s:5:\"price\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:10:\"Price in $\";s:4:\"type\";s:4:\"text\";s:4:\"desc\";s:62:\"Enter the price of the property excluding the currency symbol.\";}i:3;a:5:{s:4:\"name\";s:4:\"size\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:13:\"Size in sq ft\";s:4:\"type\";s:4:\"text\";s:4:\"desc\";s:61:\"Enter the size of the property excluding the Unit of Measure.\";}i:4;a:6:{s:4:\"name\";s:7:\"garages\";s:5:\"label\";s:15:\"Number of Acres\";s:4:\"desc\";s:42:\"Enter the number of Acres of the property.\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:7:\"options\";a:11:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";i:8;s:1:\"8\";i:9;s:1:\"9\";i:10;s:3:\"10+\";}}i:5;a:6:{s:4:\"name\";s:4:\"beds\";s:5:\"label\";s:18:\"Number of Bedrooms\";s:4:\"desc\";s:45:\"Enter the number of Bedrooms of the property.\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:7:\"options\";a:11:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";i:8;s:1:\"8\";i:9;s:1:\"9\";i:10;s:3:\"10+\";}}i:6;a:6:{s:4:\"name\";s:9:\"bathrooms\";s:5:\"label\";s:24:\"Number of Ceiling Height\";s:4:\"desc\";s:51:\"Enter the number of Ceiling Height of the property.\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:7:\"options\";a:11:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";i:8;s:1:\"8\";i:9;s:1:\"9\";i:10;s:3:\"10+\";}}i:7;a:6:{s:4:\"name\";s:9:\"sale_type\";s:5:\"label\";s:9:\"Sale Type\";s:4:\"desc\";s:47:\"Specify if the property is for Sale or for Rent\";s:3:\"std\";s:4:\"sale\";s:4:\"type\";s:5:\"radio\";s:7:\"options\";a:2:{s:4:\"sale\";s:8:\"For Sale\";s:4:\"rent\";s:8:\"For Rent\";}}i:8;a:6:{s:4:\"name\";s:11:\"sale_metric\";s:5:\"label\";s:17:\"Payment Frequency\";s:4:\"desc\";s:35:\"How often do the payments get made?\";s:3:\"std\";s:8:\"Once off\";s:4:\"type\";s:6:\"select\";s:7:\"options\";a:4:{s:4:\"once\";s:8:\"Once off\";s:4:\"week\";s:8:\"Per Week\";s:5:\"month\";s:9:\"Per Month\";s:4:\"sqft\";s:15:\"Per Square Foot\";}}i:9;a:5:{s:4:\"name\";s:7:\"on_show\";s:3:\"std\";s:5:\"false\";s:5:\"label\";s:7:\"On Show\";s:4:\"type\";s:8:\"checkbox\";s:4:\"desc\";s:32:\"This house is currently on show.\";}i:10;a:5:{s:4:\"name\";s:5:\"embed\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:10:\"Embed Code\";s:4:\"type\";s:8:\"textarea\";s:4:\"desc\";s:30:\"Add your video embed code here\";}i:11;a:5:{s:4:\"name\";s:10:\"agent_name\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:12:\"Agent\'s Name\";s:4:\"type\";s:4:\"text\";s:4:\"desc\";s:32:\"Enter agent\'s name e.g. John Doe\";}i:12;a:5:{s:4:\"name\";s:11:\"agent_email\";s:3:\"std\";s:0:\"\";s:5:\"label\";s:25:\"Agent\'s Contact Infoemail\";s:4:\"type\";s:4:\"text\";s:4:\"desc\";s:19:\"Enter agent\'s email\";}}', 'yes'),
('116', 'woo_framework_template', 'a:22:{i:0;a:3:{s:4:\"name\";s:14:\"Admin Settings\";s:4:\"icon\";s:7:\"general\";s:4:\"type\";s:7:\"heading\";}i:1;a:6:{s:4:\"name\";s:21:\"Super User (username)\";s:4:\"desc\";s:254:\"Enter your <strong>username</strong> to hide the Framework Settings and Update Framework from other users. Can be reset from the <a href=\"http://www.lmmre.com/property/wp-admin/options.php\">WP options page</a> under <code>framework_woo_super_user</code>.\";s:2:\"id\";s:24:\"framework_woo_super_user\";s:3:\"std\";s:0:\"\";s:5:\"class\";s:4:\"text\";s:4:\"type\";s:4:\"text\";}i:2;a:5:{s:4:\"name\";s:33:\"Disable Backup Settings Menu Item\";s:4:\"desc\";s:73:\"Disable the <strong>Backup Settings</strong> menu item in the theme menu.\";s:2:\"id\";s:32:\"framework_woo_backupmenu_disable\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:3;a:5:{s:4:\"name\";s:25:\"Theme Update Notification\";s:4:\"desc\";s:101:\"This will enable notices on your theme options page that there is an update available for your theme.\";s:2:\"id\";s:35:\"framework_woo_theme_version_checker\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:4;a:5:{s:4:\"name\";s:32:\"WooFramework Update Notification\";s:4:\"desc\";s:107:\"This will enable notices on your theme options page that there is an update available for the WooFramework.\";s:2:\"id\";s:39:\"framework_woo_framework_version_checker\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:5;a:3:{s:4:\"name\";s:14:\"Theme Settings\";s:4:\"icon\";s:7:\"general\";s:4:\"type\";s:7:\"heading\";}i:6;a:5:{s:4:\"name\";s:26:\"Remove Generator Meta Tags\";s:4:\"desc\";s:81:\"This disables the output of generator meta tags in the HEAD section of your site.\";s:2:\"id\";s:31:\"framework_woo_disable_generator\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:7;a:5:{s:4:\"name\";s:17:\"Image Placeholder\";s:4:\"desc\";s:151:\"Set a default image placeholder for your thumbnails. Use this if you want a default image to be shown if you haven\'t added a custom image to your post.\";s:2:\"id\";s:27:\"framework_woo_default_image\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:8;a:5:{s:4:\"name\";s:29:\"Disable Shortcodes Stylesheet\";s:4:\"desc\";s:76:\"This disables the output of shortcodes.css in the HEAD section of your site.\";s:2:\"id\";s:32:\"framework_woo_disable_shortcodes\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:9;a:5:{s:4:\"name\";s:39:\"Output \"Tracking Code\" Option in Header\";s:4:\"desc\";s:112:\"This will output the <strong>Tracking Code</strong> option in your header instead of the footer of your website.\";s:2:\"id\";s:32:\"framework_woo_move_tracking_code\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:10;a:3:{s:4:\"name\";s:8:\"Branding\";s:4:\"icon\";s:4:\"misc\";s:4:\"type\";s:7:\"heading\";}i:11;a:5:{s:4:\"name\";s:20:\"Options panel header\";s:4:\"desc\";s:50:\"Change the header image for the WooThemes Backend.\";s:2:\"id\";s:34:\"framework_woo_backend_header_image\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:12;a:5:{s:4:\"name\";s:18:\"Options panel icon\";s:4:\"desc\";s:56:\"Change the icon image for the WordPress backend sidebar.\";s:2:\"id\";s:26:\"framework_woo_backend_icon\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:13;a:5:{s:4:\"name\";s:20:\"WordPress login logo\";s:4:\"desc\";s:92:\"Change the logo image for the WordPress login page.<br /><br />Optimal logo size is 274x63px\";s:2:\"id\";s:31:\"framework_woo_custom_login_logo\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:6:\"upload\";}i:14;a:6:{s:4:\"name\";s:19:\"WordPress login URL\";s:4:\"desc\";s:72:\"Change the URL that the logo image on the WordPress login page links to.\";s:2:\"id\";s:35:\"framework_woo_custom_login_logo_url\";s:3:\"std\";s:0:\"\";s:5:\"class\";s:4:\"text\";s:4:\"type\";s:4:\"text\";}i:15;a:6:{s:4:\"name\";s:26:\"WordPress login logo Title\";s:4:\"desc\";s:63:\"Change the title of the logo image on the WordPress login page.\";s:2:\"id\";s:37:\"framework_woo_custom_login_logo_title\";s:3:\"std\";s:0:\"\";s:5:\"class\";s:4:\"text\";s:4:\"type\";s:4:\"text\";}i:16;a:3:{s:4:\"name\";s:17:\"WordPress Toolbar\";s:4:\"icon\";s:6:\"header\";s:4:\"type\";s:7:\"heading\";}i:17;a:5:{s:4:\"name\";s:25:\"Disable WordPress Toolbar\";s:4:\"desc\";s:30:\"Disable the WordPress Toolbar.\";s:2:\"id\";s:31:\"framework_woo_admin_bar_disable\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:18;a:5:{s:4:\"name\";s:44:\"Enable the WooFramework Toolbar enhancements\";s:4:\"desc\";s:128:\"Enable several WooFramework-specific enhancements to the WordPress Toolbar, such as custom navigation items for \"Theme Options\".\";s:2:\"id\";s:36:\"framework_woo_admin_bar_enhancements\";s:3:\"std\";s:0:\"\";s:4:\"type\";s:8:\"checkbox\";}i:19;a:3:{s:4:\"name\";s:11:\"PressTrends\";s:4:\"icon\";s:11:\"presstrends\";s:4:\"type\";s:7:\"heading\";}i:20;a:5:{s:4:\"name\";s:27:\"Enable PressTrends Tracking\";s:4:\"desc\";s:44:\"Enable sending of usage data to PressTrends.\";s:2:\"id\";s:32:\"framework_woo_presstrends_enable\";s:3:\"std\";s:5:\"false\";s:4:\"type\";s:8:\"checkbox\";}i:21;a:5:{s:4:\"name\";s:20:\"What is PressTrends?\";s:4:\"desc\";s:0:\"\";s:2:\"id\";s:30:\"framework_woo_presstrends_info\";s:3:\"std\";s:353:\"PressTrends is a simple usage tracker that allows us to see how our customers are using WooThemes themes - so that we can help improve them for you. <strong>None</strong> of your personal data is sent to PressTrends.<br /><br />For more information, please view the PressTrends <a href=\"http://presstrends.io/privacy\" target=\"_blank\">privacy policy</a>.\";s:4:\"type\";s:4:\"info\";}}', 'yes'),
('118', 'woo_alt_stylesheet', 'default.css', 'yes'),
('119', 'woo_logo', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/lmmre-new-haven-ct-property.jpg', 'yes'),
('120', 'woo_texttitle', 'false', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('121', 'woo_custom_favicon', '', 'yes'),
('122', 'woo_google_analytics', '<script type=\"text/javascript\">\r\n\r\n  var _gaq = _gaq || [];\r\n  _gaq.push([\\\'_setAccount\\\', \\\'UA-31347740-1\\\']);\r\n  _gaq.push([\\\'_trackPageview\\\']);\r\n\r\n  (function() {\r\n    var ga = document.createElement(\\\'script\\\'); ga.type = \\\'text/javascript\\\'; ga.async = true;\r\n    ga.src = (\\\'https:\\\' == document.location.protocol ? \\\'https://ssl\\\' : \\\'http://www\\\') + \\\'.google-analytics.com/ga.js\\\';\r\n    var s = document.getElementsByTagName(\\\'script\\\')[0]; s.parentNode.insertBefore(ga, s);\r\n  })();\r\n\r\n</script>', 'yes'),
('123', 'woo_feed_url', '', 'yes'),
('124', 'woo_subscribe_email', 'cbajda@weddingreports.com', 'yes'),
('125', 'woo_contactform_email', 'cbajda@weddingreports.com', 'yes'),
('126', 'woo_custom_css', '', 'yes'),
('127', 'woo_comments', 'post', 'yes'),
('128', 'woo_pagination_type', 'paginated_links', 'yes'),
('129', 'woo_body_color', '#000000', 'yes'),
('130', 'woo_body_img', '', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('131', 'woo_body_repeat', 'no-repeat', 'yes'),
('132', 'woo_body_pos', 'top left', 'yes'),
('133', 'woo_link_color', '', 'yes'),
('134', 'woo_link_hover_color', '', 'yes'),
('135', 'woo_button_color', '', 'yes'),
('136', 'woo_header_company_name', 'Levey Miller Maretz', 'yes'),
('137', 'woo_header_company_contact_number', '203-389-5377', 'yes'),
('138', 'woo_header_company_email', 'info@lmmre.com', 'yes'),
('139', 'woo_home_layout', 'Without Sidebar', 'yes'),
('140', 'woo_property_single_layout', 'Without Sidebar', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('141', 'woo_property_archive_layout', 'Without Sidebar', 'yes'),
('142', 'woo_property_search_layout', 'Without Sidebar', 'yes'),
('143', 'woo_featured', 'true', 'yes'),
('144', 'woo_featured_header', 'Featured Properties', 'yes'),
('145', 'woo_featured_tags', 'Featured, feature', 'yes'),
('146', 'woo_featured_entries', '5', 'yes'),
('147', 'woo_slider_image', 'Left', 'yes'),
('148', 'woo_slider_auto', 'true', 'yes'),
('149', 'woo_slider_speed', '0.6', 'yes'),
('150', 'woo_slider_interval', '4', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('151', 'woo_more_header', 'More Properties', 'yes'),
('152', 'woo_more_entries', '12', 'yes'),
('153', 'woo_archives_link', 'true', 'yes'),
('154', 'woo_estate_property_prefix', 'PROP', 'yes'),
('155', 'woo_estate_currency', '$', 'yes'),
('156', 'woo_label_on_show', 'On Show', 'yes'),
('157', 'woo_label_garages', 'Acres', 'yes'),
('158', 'woo_label_garage', 'Acre', 'yes'),
('159', 'woo_label_size_metric', 'sq ft', 'yes'),
('160', 'woo_label_beds', 'Bedrooms', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('161', 'woo_label_bed', 'Bedroom', 'yes'),
('162', 'woo_label_baths', 'Ceiling Height', 'yes'),
('163', 'woo_label_baths_long', 'Ceiling Height', 'yes'),
('164', 'woo_label_bath', 'Ceiling Height', 'yes'),
('165', 'woo_garage_logo_big', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-garage-big.png', 'yes'),
('166', 'woo_garage_logo_small', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-garage-small.png', 'yes'),
('167', 'woo_bed_logo_big', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bed-big.png', 'yes'),
('168', 'woo_bed_logo_small', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bed-small.png', 'yes'),
('169', 'woo_bath_logo_big', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bath-big.png', 'yes'),
('170', 'woo_bath_logo_small', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-bath-small.png', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('171', 'woo_size_logo_big', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-size-big.png', 'yes'),
('172', 'woo_size_logo_small', 'http://www.lmmre.com/property/wp-content/themes/estate/images/ico-property/ico-size-small.png', 'yes'),
('173', 'woo_search_header', 'Search Our Properties', 'yes'),
('174', 'woo_search_keyword_text', 'Search...', 'yes'),
('175', 'woo_displaysearch_single', 'false', 'yes'),
('176', 'woo_property_search_results', '3', 'yes'),
('177', 'woo_feature_matching_method', 'minimum', 'yes'),
('178', 'woo_label_sale_type', 'Sale Type', 'yes'),
('179', 'woo_label_for_sale', 'For Sale', 'yes'),
('180', 'woo_label_for_rent', 'For Rent', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('181', 'woo_label_property_location_and_type', 'Property Location & Type', 'yes'),
('182', 'woo_label_locations_dropdown_view_all', 'View all Locations', 'yes'),
('183', 'woo_label_property_type_dropdown_view_all', 'View all Property Types', 'yes'),
('184', 'woo_label_min_price', 'Min Price', 'yes'),
('185', 'woo_label_max_price', 'Max Price', 'yes'),
('186', 'woo_label_advanced_search', 'Advanced Search', 'yes'),
('187', 'woo_label_min_size', 'Min Size', 'yes'),
('188', 'woo_label_max_size', 'Max Size', 'yes'),
('189', 'woo_label_property_details_on_show', 'This property is currently on show', 'yes'),
('190', 'woo_label_additional_features', 'Features', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('191', 'woo_clickable_additional_features', 'false', 'yes'),
('192', 'woo_label_gallery', 'Gallery', 'yes'),
('193', 'woo_label_virtual_tour', 'Virtual Tour', 'yes'),
('194', 'woo_label_property_map', 'Map', 'yes'),
('195', 'woo_label_related_properties', 'More properties in this area', 'yes'),
('196', 'woo_label_contact_agent_button', 'Contact Agent', 'yes'),
('197', 'woo_label_agent_email_link', 'Email this agent', 'yes'),
('198', 'woo_contact_form_link', 'false', 'yes'),
('199', 'woo_contact_form_page', 'Select a page:', 'yes'),
('200', 'woo_agent_user_role_enable', 'false', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('201', 'woo_agent_role_name', 'Agent', 'yes'),
('202', 'woo_agent_role_default', 'Select a Role:', 'yes'),
('203', 'woo_maps_apikey', '', 'yes'),
('204', 'woo_maps_scroll', 'false', 'yes'),
('205', 'woo_maps_single_height', '250', 'yes'),
('206', 'woo_coords', 'true', 'yes'),
('207', 'woo_maps_default_mapzoom', '9', 'yes'),
('208', 'woo_maps_default_maptype', 'G_NORMAL_MAP', 'yes'),
('209', 'woo_cat_colors_pages', 'red', 'yes'),
('210', 'woo_cat_custom_marker_pages', '', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('211', 'woo_cat_colors_1', 'red', 'yes'),
('212', 'woo_cat_custom_marker_1', '', 'yes'),
('213', 'woo_location_menu_items', 'true', 'yes'),
('214', 'woo_propertytype_menu_items', 'true', 'yes'),
('215', 'woo_category_menu_items', 'true', 'yes'),
('216', 'woo_wpthumb_notice', '', 'yes'),
('217', 'woo_post_image_support', 'true', 'yes'),
('218', 'woo_pis_resize', 'true', 'yes'),
('219', 'woo_pis_hard_crop', 'false', 'yes'),
('220', 'woo_resize', 'false', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('221', 'woo_auto_img', 'false', 'yes'),
('222', 'woo_thumb_w', '100', 'yes'),
('223', 'woo_thumb_h', '100', 'yes'),
('224', 'woo_thumb_align', 'alignleft', 'yes'),
('225', 'woo_thumb_single', 'false', 'yes'),
('226', 'woo_single_w', '200', 'yes'),
('227', 'woo_single_h', '200', 'yes'),
('228', 'woo_rss_thumb', 'false', 'yes'),
('229', 'woo_footer_aff_link', '', 'yes'),
('230', 'woo_footer_left', 'false', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('231', 'woo_footer_left_text', '<p></p>', 'yes'),
('232', 'woo_footer_right', 'false', 'yes'),
('233', 'woo_footer_right_text', '<p></p>', 'yes'),
('234', 'woo_connect', 'false', 'yes'),
('235', 'woo_connect_title', '', 'yes'),
('236', 'woo_connect_content', '', 'yes'),
('237', 'woo_connect_newsletter_id', '', 'yes'),
('238', 'woo_connect_mailchimp_list_url', '', 'yes'),
('239', 'woo_connect_rss', 'true', 'yes'),
('240', 'woo_connect_twitter', '', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('241', 'woo_connect_facebook', '', 'yes'),
('242', 'woo_connect_youtube', '', 'yes'),
('243', 'woo_connect_flickr', '', 'yes'),
('244', 'woo_connect_linkedin', '', 'yes'),
('245', 'woo_connect_delicious', '', 'yes'),
('246', 'woo_connect_googleplus', '', 'yes'),
('247', 'woo_connect_related', 'true', 'yes'),
('263', 'jetpack_activated', '1', 'yes'),
('264', 'jetpack_options', 'a:9:{s:7:\"version\";s:16:\"2.0.2:1367580238\";s:11:\"old_version\";s:16:\"2.0.2:1367580238\";s:28:\"fallback_no_verify_ssl_certs\";i:0;s:9:\"time_diff\";i:0;s:2:\"id\";i:52223557;s:10:\"blog_token\";s:65:\"y&o%prHsokPzt97uEK@eqKJkMJ0HKIFv.PofPLTu1sUo4)btfVV(&KDlDgdJuB^(L\";s:6:\"public\";i:1;s:11:\"user_tokens\";a:1:{i:2;s:67:\"@w4^AY49D*pvKpVauoepKYoB6Eu@5&Gh.XEICG0&SW30VY*7fC@#K6U$7dEptt04$.2\";}s:11:\"master_user\";i:2;}', 'yes'),
('265', 'backup-to-dropbox-premium-extensions', 'a:0:{}', 'no');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('266', 'wpseo', 'a:5:{s:15:\"ms_defaults_set\";b:1;s:7:\"version\";s:5:\"1.4.1\";s:14:\"tracking_popup\";s:4:\"done\";s:11:\"ignore_tour\";s:6:\"ignore\";s:11:\"theme_check\";a:1:{s:11:\"description\";b:1;}}', 'yes'),
('268', 'wpseo_xml', 'a:1:{s:36:\"post_types-attachment-not_in_sitemap\";b:1;}', 'yes'),
('270', 'wpseo_social', 'a:3:{s:9:\"opengraph\";s:0:\"\";s:10:\"fb_adminid\";s:0:\"\";s:8:\"fb_appid\";s:0:\"\";}', 'yes'),
('271', 'wpseo_titles', 'a:11:{s:14:\"noindex-author\";s:0:\"\";s:14:\"disable-author\";s:0:\"\";s:15:\"noindex-archive\";s:0:\"\";s:16:\"noindex-category\";s:0:\"\";s:16:\"noindex-post_tag\";s:0:\"\";s:19:\"noindex-post_format\";s:0:\"\";s:16:\"noindex-subpages\";s:0:\"\";s:12:\"hide-rsdlink\";s:0:\"\";s:14:\"hide-feedlinks\";s:0:\"\";s:16:\"hide-wlwmanifest\";s:0:\"\";s:14:\"hide-shortlink\";s:0:\"\";}', 'yes'),
('273', 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
('274', 'woo_custom_upload_tracking', 'a:0:{}', 'yes'),
('280', 'category_children', 'a:0:{}', 'yes'),
('283', 'jetpack_active_modules', 'a:18:{i:0;s:10:\"vaultpress\";i:1;s:19:\"gravatar-hovercards\";i:3;s:5:\"stats\";i:5;s:13:\"subscriptions\";i:7;s:8:\"json-api\";i:9;s:13:\"post-by-email\";i:11;s:7:\"widgets\";i:13;s:10:\"custom-css\";i:15;s:12:\"contact-form\";i:17;s:11:\"mobile-push\";i:19;s:18:\"after-the-deadline\";i:21;s:5:\"latex\";i:23;s:10:\"sharedaddy\";i:25;s:10:\"shortcodes\";i:27;s:10:\"shortlinks\";i:29;s:21:\"enhanced-distribution\";i:31;s:9:\"publicize\";i:33;s:5:\"notes\";}', 'yes'),
('284', 'stats_options', 'a:6:{s:9:\"admin_bar\";b:1;s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"blog_id\";i:52223557;s:12:\"do_not_track\";b:1;s:10:\"hide_smile\";b:0;s:7:\"version\";s:1:\"7\";}', 'yes'),
('285', 'sharing-options', 'a:1:{s:6:\"global\";a:5:{s:12:\"button_style\";s:9:\"icon-text\";s:13:\"sharing_label\";s:11:\"Share this:\";s:10:\"open_links\";s:4:\"same\";s:4:\"show\";a:0:{}s:6:\"custom\";a:0:{}}}', 'yes');

INSERT INTO `wp_2_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
('286', 'stats_cache', 'a:2:{s:32:\"b2649495c7cae987fe5fc0d2590048ea\";a:1:{i:1377882787;a:7:{i:0;a:4:{s:7:\"post_id\";s:1:\"0\";s:10:\"post_title\";s:9:\"Home page\";s:14:\"post_permalink\";s:30:\"http://www.lmmre.com/property/\";s:5:\"views\";s:2:\"43\";}i:1;a:4:{s:7:\"post_id\";s:3:\"293\";s:10:\"post_title\";s:33:\"490 ORCHARD STREET, NEW HAVEN, CT\";s:14:\"post_permalink\";s:71:\"http://www.lmmre.com/property/property/490-orchard-street-new-haven-ct/\";s:5:\"views\";s:1:\"5\";}i:2;a:4:{s:7:\"post_id\";s:3:\"779\";s:10:\"post_title\";s:24:\"3035 Whitney Ave, Hamden\";s:14:\"post_permalink\";s:63:\"http://www.lmmre.com/property/property/3035-whitney-ave-hamden/\";s:5:\"views\";s:1:\"2\";}i:3;a:4:{s:7:\"post_id\";s:3:\"264\";s:10:\"post_title\";s:24:\"80 AMITY ROAD, NEW HAVEN\";s:14:\"post_permalink\";s:63:\"http://www.lmmre.com/property/property/80-amity-road-new-haven/\";s:5:\"views\";s:1:\"1\";}i:4;a:4:{s:7:\"post_id\";s:3:\"250\";s:10:\"post_title\";s:27:\"88 BRADLEY ROAD, WOODBRIDGE\";s:14:\"post_permalink\";s:66:\"http://www.lmmre.com/property/property/88-bradley-road-woodbridge/\";s:5:\"views\";s:1:\"1\";}i:5;a:4:{s:7:\"post_id\";s:3:\"177\";s:10:\"post_title\";s:27:\"1684 DIXWELL AVENUE, HAMDEN\";s:14:\"post_permalink\";s:66:\"http://www.lmmre.com/property/property/1684-dixwell-avenue-hamden/\";s:5:\"views\";s:1:\"1\";}i:6;a:4:{s:7:\"post_id\";s:3:\"204\";s:10:\"post_title\";s:34:\"1079 ELLA T GRASSO BLVD. NEW HAVEN\";s:14:\"post_permalink\";s:73:\"http://www.lmmre.com/property/property/1079-ella-t-grasso-blvd-new-haven/\";s:5:\"views\";s:1:\"1\";}}}s:32:\"32200403110e40fa701970a56b762076\";a:1:{i:1377882787;a:5:{i:0;a:2:{s:10:\"searchterm\";s:31:\"levey miller maretz real estate\";s:5:\"views\";s:1:\"3\";}i:1;a:2:{s:10:\"searchterm\";s:37:\"3035 whitney avenue, hamden for lease\";s:5:\"views\";s:1:\"2\";}i:2;a:2:{s:10:\"searchterm\";s:19:\"levey miller maretz\";s:5:\"views\";s:1:\"2\";}i:3;a:2:{s:10:\"searchterm\";s:31:\"490 orchard street new haven ct\";s:5:\"views\";s:1:\"1\";}i:4;a:2:{s:10:\"searchterm\";s:33:\"206 whalley avenue, new haven, ct\";s:5:\"views\";s:1:\"1\";}}}}', 'yes'),
('324', 'woo_exclude', 'a:5:{i:0;i:326;i:1;i:263;i:2;i:267;i:3;i:264;i:4;i:260;}', 'yes'),
('298', 'akismet_available_servers', 'a:4:{s:12:\"66.135.58.61\";b:1;s:12:\"66.135.58.62\";b:1;s:12:\"72.233.69.88\";b:1;s:12:\"72.233.69.89\";b:1;}', 'yes'),
('299', 'akismet_connectivity_time', '1369920870', 'yes'),
('314', 'subscription_options', 'a:2:{s:10:\"invitation\";s:226:\"Howdy.\r\n\r\nYou recently followed this blog\'s posts. This means you will receive each new post by email.\r\n\r\nTo activate, click confirm below. If you believe this is an error, ignore this message and we\'ll never bother you again.\";s:14:\"comment_follow\";s:239:\"Howdy.\r\n\r\nYou recently followed one of my posts. This means you will receive an email when new comments are posted.\r\n\r\nTo activate, click confirm below. If you believe this is an error, ignore this message and we\'ll never bother you again.\";}', 'yes'),
('348', 'rewrite_rules', 'a:100:{s:11:\"property/?$\";s:30:\"index.php?post_type=woo_estate\";s:41:\"property/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?post_type=woo_estate&feed=$matches[1]\";s:36:\"property/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?post_type=woo_estate&feed=$matches[1]\";s:28:\"property/page/([0-9]{1,})/?$\";s:48:\"index.php?post_type=woo_estate&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"property/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"property/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"property/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"property/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"property/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"property/([^/]+)/trackback/?$\";s:37:\"index.php?woo_estate=$matches[1]&tb=1\";s:49:\"property/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?woo_estate=$matches[1]&feed=$matches[2]\";s:44:\"property/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?woo_estate=$matches[1]&feed=$matches[2]\";s:37:\"property/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?woo_estate=$matches[1]&paged=$matches[2]\";s:44:\"property/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?woo_estate=$matches[1]&cpage=$matches[2]\";s:29:\"property/([^/]+)(/[0-9]+)?/?$\";s:49:\"index.php?woo_estate=$matches[1]&page=$matches[2]\";s:25:\"property/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"property/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"property/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"property/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"property/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"location/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?location=$matches[1]&feed=$matches[2]\";s:42:\"location/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?location=$matches[1]&feed=$matches[2]\";s:35:\"location/(.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?location=$matches[1]&paged=$matches[2]\";s:17:\"location/(.+?)/?$\";s:30:\"index.php?location=$matches[1]\";s:53:\"propertytype/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?propertytype=$matches[1]&feed=$matches[2]\";s:48:\"propertytype/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?propertytype=$matches[1]&feed=$matches[2]\";s:41:\"propertytype/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?propertytype=$matches[1]&paged=$matches[2]\";s:23:\"propertytype/([^/]+)/?$\";s:34:\"index.php?propertytype=$matches[1]\";s:57:\"propertyfeatures/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?propertyfeatures=$matches[1]&feed=$matches[2]\";s:52:\"propertyfeatures/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?propertyfeatures=$matches[1]&feed=$matches[2]\";s:45:\"propertyfeatures/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?propertyfeatures=$matches[1]&paged=$matches[2]\";s:27:\"propertyfeatures/([^/]+)/?$\";s:38:\"index.php?propertyfeatures=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:29:\"comments/page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:20:\"(.?.+?)(/[0-9]+)?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:20:\"([^/]+)(/[0-9]+)?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";}', 'yes'),
('345', 'db_upgraded', '', 'yes');

--
-- Table structure for table `wp_2_postmeta`
--

CREATE TABLE `wp_2_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM AUTO_INCREMENT=4074 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_2_postmeta`
--

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2793', '293', 'price', '350000'),
('1310', '153', 'woo_maps_zoom', '14'),
('3779', '151', 'agent_name', 'Noah Meyer'),
('1303', '153', 'sale_type', 'rent'),
('1304', '153', 'sale_metric', 'Per Month'),
('1305', '153', 'on_show', 'false'),
('1306', '153', 'woo_maps_enable', 'on'),
('1307', '153', 'woo_maps_address', '59 Amity Road, Woodbridge, CT 06525, USA'),
('1308', '153', 'woo_maps_long', '-72.97795559999997'),
('1309', '153', 'woo_maps_lat', '41.3359146');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4', '6', '_menu_item_type', 'custom'),
('5', '6', '_menu_item_menu_item_parent', '0'),
('6', '6', '_menu_item_object_id', '6'),
('7', '6', '_menu_item_object', 'custom'),
('8', '6', '_menu_item_target', ''),
('9', '6', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('10', '6', '_menu_item_xfn', ''),
('11', '6', '_menu_item_url', 'http://www.lmmre.com/about-property-management-in-ct/'),
('2798', '293', 'sale_type', 'sale'),
('13', '7', '_menu_item_type', 'custom');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('14', '7', '_menu_item_menu_item_parent', '0'),
('15', '7', '_menu_item_object_id', '7'),
('16', '7', '_menu_item_object', 'custom'),
('17', '7', '_menu_item_target', ''),
('18', '7', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('19', '7', '_menu_item_xfn', ''),
('20', '7', '_menu_item_url', 'http://www.lmmre.com/property'),
('22', '8', '_menu_item_type', 'custom'),
('23', '8', '_menu_item_menu_item_parent', '0'),
('24', '8', '_menu_item_object_id', '8');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('25', '8', '_menu_item_object', 'custom'),
('26', '8', '_menu_item_target', ''),
('27', '8', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('28', '8', '_menu_item_xfn', ''),
('29', '8', '_menu_item_url', 'http://www.lmmre.com/services/'),
('2796', '293', 'beds', ''),
('31', '9', '_menu_item_type', 'custom'),
('32', '9', '_menu_item_menu_item_parent', '0'),
('33', '9', '_menu_item_object_id', '9'),
('34', '9', '_menu_item_object', 'custom');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('35', '9', '_menu_item_target', ''),
('36', '9', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('37', '9', '_menu_item_xfn', ''),
('38', '9', '_menu_item_url', 'http://www.lmmre.com/new-haven-county-ct-client-list/'),
('2794', '293', 'size', '3750'),
('40', '10', '_menu_item_type', 'custom'),
('41', '10', '_menu_item_menu_item_parent', '0'),
('42', '10', '_menu_item_object_id', '10'),
('43', '10', '_menu_item_object', 'custom'),
('44', '10', '_menu_item_target', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('45', '10', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('46', '10', '_menu_item_xfn', ''),
('47', '10', '_menu_item_url', 'http://www.lmmre.com/blog/category/latest-news/'),
('49', '11', '_menu_item_type', 'custom'),
('50', '11', '_menu_item_menu_item_parent', '0'),
('51', '11', '_menu_item_object_id', '11'),
('52', '11', '_menu_item_object', 'custom'),
('53', '11', '_menu_item_target', ''),
('54', '11', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('55', '11', '_menu_item_xfn', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('56', '11', '_menu_item_url', 'http://www.lmmre.com/contact-us/'),
('2791', '293', 'image', ''),
('2792', '293', 'address', '490 ORCHARD STREET, NEW HAVEN'),
('65', '14', '_menu_item_type', 'custom'),
('66', '14', '_menu_item_menu_item_parent', '0'),
('67', '14', '_menu_item_object_id', '14'),
('68', '14', '_menu_item_object', 'custom'),
('69', '14', '_menu_item_target', ''),
('70', '14', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('71', '14', '_menu_item_xfn', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('72', '14', '_menu_item_url', 'http://www.lmmre.com'),
('74', '15', '_yoast_wpseo_linkdex', '0'),
('75', '15', '_edit_last', '3'),
('76', '15', '_edit_lock', '1377787499:3'),
('77', '16', '_wp_attached_file', '2013/05/240-winthrop-ave-new-haven.jpg'),
('78', '16', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:376;s:6:\"height\";i:293;s:4:\"file\";s:38:\"2013/05/240-winthrop-ave-new-haven.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"240-winthrop-ave-new-haven-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"240-winthrop-ave-new-haven-300x233.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:233;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('79', '15', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/240-winthrop-ave-new-haven.jpg'),
('80', '15', 'address', '240 WINTHROP AVENUE NEW HAVEN CT'),
('81', '15', 'price', '2900000'),
('82', '15', 'size', '76000');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3594', '18', 'agent_email', 'noah@lmmre.com'),
('3593', '18', 'agent_name', 'Noah Meyer'),
('86', '15', 'sale_type', 'sale'),
('87', '15', 'sale_metric', 'Once off'),
('88', '15', 'on_show', 'false'),
('89', '15', 'woo_maps_enable', 'on'),
('90', '15', 'woo_maps_address', '240 Winthrop Avenue, New Haven, CT 06511, USA'),
('91', '15', 'woo_maps_long', '-72.94885299999999'),
('92', '15', 'woo_maps_lat', '41.309514'),
('93', '15', 'woo_maps_zoom', '16');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('94', '15', 'woo_maps_type', 'G_NORMAL_MAP'),
('95', '15', 'woo_maps_pov_pitch', '-20'),
('96', '15', 'woo_maps_pov_yaw', '20'),
('97', '15', '_yoast_wpseo_focuskw', ''),
('98', '15', '_yoast_wpseo_title', ''),
('99', '15', '_yoast_wpseo_metadesc', ''),
('100', '15', '_yoast_wpseo_meta-robots-noindex', '0'),
('101', '15', '_yoast_wpseo_meta-robots-nofollow', '0'),
('102', '15', '_yoast_wpseo_meta-robots-adv', 'none'),
('103', '15', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('104', '15', '_yoast_wpseo_redirect', ''),
('105', '18', '_yoast_wpseo_linkdex', '0'),
('106', '18', '_edit_last', '3'),
('107', '18', '_edit_lock', '1373203371:3'),
('108', '19', '_wp_attached_file', '2013/05/orange-street-new-haven.jpg'),
('109', '19', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:390;s:6:\"height\";i:293;s:4:\"file\";s:35:\"2013/05/orange-street-new-haven.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"orange-street-new-haven-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"orange-street-new-haven-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('110', '18', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/orange-street-new-haven.jpg'),
('111', '18', 'address', '442 ORANGE STREET NEW HAVEN, CT'),
('112', '18', 'price', '2100000'),
('113', '18', 'size', '44736');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('114', '18', 'garages', '.32'),
('3595', '20', 'agent_name', 'Marty Ruff'),
('117', '18', 'sale_type', 'sale'),
('118', '18', 'sale_metric', 'Once off'),
('119', '18', 'on_show', 'false'),
('120', '18', 'woo_maps_enable', 'on'),
('121', '18', 'woo_maps_address', '442 Orange Street, New Haven, CT 06511, USA'),
('122', '18', 'woo_maps_long', '-72.91920800000003'),
('123', '18', 'woo_maps_lat', '41.312782'),
('124', '18', 'woo_maps_zoom', '16');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('125', '18', 'woo_maps_type', 'G_NORMAL_MAP'),
('126', '18', 'woo_maps_pov_pitch', '-20'),
('127', '18', 'woo_maps_pov_yaw', '20'),
('128', '18', '_yoast_wpseo_focuskw', ''),
('129', '18', '_yoast_wpseo_title', ''),
('130', '18', '_yoast_wpseo_metadesc', ''),
('131', '18', '_yoast_wpseo_meta-robots-noindex', '0'),
('132', '18', '_yoast_wpseo_meta-robots-nofollow', '0'),
('133', '18', '_yoast_wpseo_meta-robots-adv', 'none'),
('134', '18', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('135', '18', '_yoast_wpseo_redirect', ''),
('136', '20', '_yoast_wpseo_linkdex', '0'),
('137', '20', '_edit_last', '3'),
('138', '20', '_edit_lock', '1373203472:3'),
('1288', '151', '_thumbnail_id', '152'),
('1289', '153', '_yoast_wpseo_linkdex', '0'),
('1290', '153', '_edit_last', '3'),
('1291', '153', '_edit_lock', '1373245587:3'),
('1292', '154', '_wp_attached_file', '2013/05/59-Amity.jpg'),
('1293', '154', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:20:\"2013/05/59-Amity.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"59-Amity-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"59-Amity-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"59-Amity-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1343741072;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:7:\"0.00625\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('141', '20', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/ServeAttachment-300x224.jpeg'),
('142', '20', 'address', '30 Elm Street West Haven CT'),
('143', '20', 'price', '1350000'),
('144', '20', 'size', '38038'),
('145', '20', 'garages', '1'),
('3597', '316', 'agent_name', 'Shawn Reilly'),
('147', '20', 'bathrooms', '12'),
('148', '20', 'sale_type', 'sale'),
('149', '20', 'sale_metric', 'Once off'),
('150', '20', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('151', '20', 'woo_maps_enable', 'on'),
('152', '20', 'woo_maps_address', '30 Elm Street, West Haven, CT 06516, USA'),
('153', '20', 'woo_maps_long', '-72.94050240000001'),
('154', '20', 'woo_maps_lat', '41.2796266'),
('155', '20', 'woo_maps_zoom', '15'),
('156', '20', 'woo_maps_type', 'G_NORMAL_MAP'),
('157', '20', 'woo_maps_pov_pitch', '-20'),
('158', '20', 'woo_maps_pov_yaw', '20'),
('159', '20', '_yoast_wpseo_focuskw', ''),
('160', '20', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('161', '20', '_yoast_wpseo_metadesc', ''),
('162', '20', '_yoast_wpseo_meta-robots-noindex', '0'),
('163', '20', '_yoast_wpseo_meta-robots-nofollow', '0'),
('164', '20', '_yoast_wpseo_meta-robots-adv', 'none'),
('165', '20', '_yoast_wpseo_canonical', ''),
('166', '20', '_yoast_wpseo_redirect', ''),
('294', '50', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1195;s:6:\"height\";i:935;s:4:\"file\";s:25:\"2013/05/land-for-sale.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"land-for-sale-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"land-for-sale-300x234.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:234;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"land-for-sale-1024x801.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:801;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('168', '15', 'Lot Size', '4.87 Acres'),
('293', '50', '_wp_attached_file', '2013/05/land-for-sale.jpg'),
('1854', '204', 'woo_maps_address', '1079 Ella T Grasso Boulevard, New Haven, CT 06511, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('174', '31', '_wp_attached_file', '2013/05/30-Elm.jpg'),
('175', '31', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:18:\"2013/05/30-Elm.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"30-Elm-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"30-Elm-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('1853', '204', 'woo_maps_enable', 'on'),
('1852', '204', 'on_show', 'false'),
('1851', '204', 'sale_metric', 'Once off'),
('1850', '204', 'sale_type', 'sale'),
('1849', '204', 'bathrooms', ''),
('1848', '204', 'beds', ''),
('1847', '204', 'garages', '0.26'),
('1846', '204', 'size', '5374');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1845', '204', 'price', '628500'),
('1844', '204', 'address', '1079 ELLA T GRASSO BLVD. NEW HAVEN'),
('1843', '204', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/10791-300x225.jpg'),
('1842', '204', '_wpas_done_all', '1'),
('1841', '204', '_edit_lock', '1373250372:3'),
('1840', '204', '_edit_last', '3'),
('1483', '147', '_thumbnail_id', '174'),
('1839', '204', '_yoast_wpseo_linkdex', '0'),
('462', '75', '_yoast_wpseo_linkdex', '0'),
('197', '36', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('198', '36', '_edit_last', '3'),
('199', '36', '_edit_lock', '1373202973:3'),
('200', '36', 'image', ''),
('201', '36', '245 AMITY ROAD', '245 AMITY ROAD'),
('202', '36', 'price', '14.00'),
('203', '36', 'size', '1,350'),
('3592', '15', 'agent_email', 'Marty@lmmre.com'),
('207', '36', 'sale_type', 'rent'),
('208', '36', 'sale_metric', 'Per Square Foot'),
('209', '36', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('210', '36', 'woo_maps_enable', 'on'),
('211', '36', 'woo_maps_zoom', '16'),
('212', '36', 'woo_maps_type', 'G_NORMAL_MAP'),
('213', '36', '_yoast_wpseo_focuskw', ''),
('214', '36', '_yoast_wpseo_title', ''),
('215', '36', '_yoast_wpseo_metadesc', ''),
('216', '36', '_yoast_wpseo_meta-robots-noindex', '0'),
('217', '36', '_yoast_wpseo_meta-robots-nofollow', '0'),
('218', '36', '_yoast_wpseo_meta-robots-adv', 'none'),
('219', '36', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('220', '36', '_yoast_wpseo_redirect', ''),
('221', '36', 'address', '245 AMITY ROAD'),
('3591', '15', 'agent_name', 'Marty Ruff'),
('228', '36', 'woo_maps_address', '245 Amity Road, Woodbridge, CT 06525, USA'),
('224', '36', 'woo_maps_long', '-72.98316499999999'),
('225', '36', 'woo_maps_lat', '41.343378'),
('226', '36', 'woo_maps_pov_pitch', '3.827237222939619'),
('227', '36', 'woo_maps_pov_yaw', '106.34347112138357'),
('229', '41', '_yoast_wpseo_linkdex', '0'),
('230', '41', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('231', '41', '_edit_lock', '1373203048:3'),
('232', '41', 'image', ''),
('3590', '15', '_thumbnail_id', '16'),
('235', '41', 'bathrooms', '9'),
('236', '41', 'sale_type', 'rent'),
('237', '41', 'sale_metric', 'Per Square Foot'),
('238', '41', 'on_show', 'false'),
('239', '41', 'woo_maps_enable', 'on'),
('240', '41', 'woo_maps_zoom', '17'),
('241', '41', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('242', '41', '_yoast_wpseo_focuskw', ''),
('243', '41', '_yoast_wpseo_title', ''),
('244', '41', '_yoast_wpseo_metadesc', ''),
('245', '41', '_yoast_wpseo_meta-robots-noindex', '0'),
('246', '41', '_yoast_wpseo_meta-robots-nofollow', '0'),
('247', '41', '_yoast_wpseo_meta-robots-adv', 'none'),
('248', '41', '_yoast_wpseo_canonical', ''),
('249', '41', '_yoast_wpseo_redirect', ''),
('250', '43', '_wp_attached_file', '2013/05/131-Bradley.jpg'),
('251', '43', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:23:\"2013/05/131-Bradley.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"131-Bradley-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"131-Bradley-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('252', '41', 'address', '131 BRADLEY ROAD'),
('253', '41', 'price', '10'),
('254', '41', 'size', '6000'),
('255', '41', 'woo_maps_address', '131 Bradley Road, Woodbridge, CT 06525, USA'),
('256', '41', 'woo_maps_long', '-72.97735'),
('257', '41', 'woo_maps_lat', '41.348718'),
('258', '41', 'woo_maps_pov_pitch', '-20'),
('259', '41', 'woo_maps_pov_yaw', '20'),
('3780', '151', 'agent_email', 'noah@lmmre.com'),
('1294', '153', '_thumbnail_id', '154');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1295', '153', '_wpas_done_all', '1'),
('1296', '153', 'image', ''),
('1297', '153', 'address', '59 AMITY ROAD, WOODBRIDGE'),
('1298', '153', 'price', '550.00'),
('1299', '153', 'size', '300'),
('262', '45', '_wp_attached_file', '2013/05/245-Amity-5.jpg'),
('263', '45', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:360;s:4:\"file\";s:23:\"2013/05/245-Amity-5.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"245-Amity-5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"245-Amity-5-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:4.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:10:\"Picasa 2.7\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1223120584;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"6\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00165400002152\";s:5:\"title\";s:0:\"\";}}'),
('264', '47', '_yoast_wpseo_linkdex', '0'),
('265', '47', '_edit_last', '3'),
('266', '47', '_edit_lock', '1373206407:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('267', '47', 'image', ''),
('268', '47', 'address', '410 Ella Grasso Blvd, New Haven, CT'),
('269', '47', 'price', '6363450'),
('270', '47', 'size', '16.11'),
('271', '47', 'garages', 'Up to 16'),
('3638', '204', 'agent_name', 'Marty Ruff'),
('274', '47', 'sale_type', 'sale'),
('275', '47', 'on_show', 'false'),
('276', '47', 'woo_maps_enable', 'on'),
('277', '47', 'woo_maps_address', '196 Ella T Grasso Boulevard, New Haven, CT 06519, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('278', '47', 'woo_maps_long', '-72.9370538'),
('279', '47', 'woo_maps_lat', '41.2870774'),
('280', '47', 'woo_maps_zoom', '16'),
('281', '47', 'woo_maps_type', 'G_SATELLITE_MAP'),
('282', '47', 'woo_maps_pov_pitch', '-20'),
('283', '47', 'woo_maps_pov_yaw', '20'),
('284', '47', '_yoast_wpseo_focuskw', ''),
('285', '47', '_yoast_wpseo_title', ''),
('286', '47', '_yoast_wpseo_metadesc', ''),
('287', '47', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('288', '47', '_yoast_wpseo_meta-robots-nofollow', '0'),
('289', '47', '_yoast_wpseo_meta-robots-adv', 'none'),
('290', '47', '_yoast_wpseo_canonical', ''),
('291', '47', '_yoast_wpseo_redirect', ''),
('292', '47', 'sale_metric', 'Once off'),
('295', '15', 'sale_metric', 'Per Square Foot'),
('1125', '135', '_wpas_done_all', '1'),
('297', '0', '', ''),
('298', '58', '_yoast_wpseo_linkdex', '0'),
('299', '58', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('300', '58', '_edit_lock', '1373207910:3'),
('301', '58', '_wpas_done_all', '1'),
('302', '58', 'image', ''),
('303', '58', 'address', '1060 SOUTH COLONY ROAD, WALLINGFORD'),
('304', '58', 'price', '8.00'),
('305', '58', 'size', '4,000-5,200'),
('306', '58', 'garages', ''),
('307', '58', 'beds', ''),
('308', '58', 'bathrooms', '8'),
('309', '58', 'sale_type', 'rent');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('310', '58', 'sale_metric', 'Per Square Foot'),
('311', '58', 'on_show', 'false'),
('312', '58', 'woo_maps_enable', 'false'),
('313', '58', 'woo_maps_address', '414 South Elm Street, Wallingford, CT 06492, USA'),
('314', '58', 'woo_maps_long', '-72.8228759765625'),
('315', '58', 'woo_maps_lat', '41.44066745847661'),
('316', '58', 'woo_maps_zoom', '12'),
('317', '58', 'woo_maps_type', 'G_NORMAL_MAP'),
('318', '58', 'woo_maps_pov_pitch', '-19.925483509285584'),
('319', '58', 'woo_maps_pov_yaw', '38.32768287100332');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('320', '58', '_yoast_wpseo_focuskw', ''),
('321', '58', '_yoast_wpseo_title', ''),
('322', '58', '_yoast_wpseo_metadesc', ''),
('323', '58', '_yoast_wpseo_meta-robots-noindex', '0'),
('324', '58', '_yoast_wpseo_meta-robots-nofollow', '0'),
('325', '58', '_yoast_wpseo_meta-robots-adv', 'none'),
('326', '58', '_yoast_wpseo_canonical', ''),
('327', '58', '_yoast_wpseo_redirect', ''),
('328', '59', '_wp_attached_file', '2013/05/1060-S-Colony.jpg'),
('329', '59', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:25:\"2013/05/1060-S-Colony.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1060-S-Colony-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1060-S-Colony-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('330', '58', '_thumbnail_id', '59'),
('1124', '135', '_edit_lock', '1373206725:3'),
('1123', '135', '_edit_last', '3'),
('333', '63', '_yoast_wpseo_linkdex', '0'),
('334', '63', '_edit_last', '3'),
('335', '63', '_edit_lock', '1373242357:3'),
('336', '64', '_wp_attached_file', '2013/05/390Eastern1.jpg'),
('337', '64', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:23:\"2013/05/390Eastern1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"390Eastern1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"390Eastern1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('338', '63', 'image', ''),
('339', '63', 'address', '390 EASTERN STREET, NEW HAVEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('340', '63', 'price', '395000'),
('341', '63', 'size', '3,300'),
('342', '63', 'garages', '1.5'),
('3719', '68', 'agent_email', 'steve@lmmre.com'),
('344', '63', 'bathrooms', '14'),
('345', '63', 'sale_type', 'sale'),
('346', '63', 'sale_metric', 'Once off'),
('347', '63', 'on_show', 'false'),
('348', '63', 'woo_maps_enable', 'false'),
('349', '63', 'woo_maps_address', '390 Eastern Street, New Haven, CT 06513, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('350', '63', 'woo_maps_long', '-72.87320899999997'),
('351', '63', 'woo_maps_lat', '41.3167959'),
('352', '63', 'woo_maps_zoom', '14'),
('353', '63', 'woo_maps_type', 'G_NORMAL_MAP'),
('354', '63', 'woo_maps_pov_pitch', '-20'),
('355', '63', 'woo_maps_pov_yaw', '20'),
('356', '63', '_yoast_wpseo_focuskw', ''),
('357', '63', '_yoast_wpseo_title', ''),
('358', '63', '_yoast_wpseo_metadesc', ''),
('359', '63', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('360', '63', '_yoast_wpseo_meta-robots-nofollow', '0'),
('361', '63', '_yoast_wpseo_meta-robots-adv', 'none'),
('362', '63', '_yoast_wpseo_canonical', ''),
('363', '63', '_yoast_wpseo_redirect', ''),
('364', '63', '_wpas_done_all', '1'),
('365', '65', '_yoast_wpseo_linkdex', '0'),
('366', '65', '_edit_last', '3'),
('367', '65', '_edit_lock', '1373208363:3'),
('368', '66', '_wp_attached_file', '2013/05/129-Amity.jpg.jpg'),
('369', '66', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1984;s:6:\"height\";i:1408;s:4:\"file\";s:25:\"2013/05/129-Amity.jpg.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"129-Amity.jpg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"129-Amity.jpg-300x212.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"129-Amity.jpg-1024x726.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:726;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:12:\"MX850 series\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1366716857;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('370', '65', '_wpas_done_all', '1'),
('371', '65', 'image', ''),
('372', '65', 'address', '129 AMITY ROAD, NEW HAVEN'),
('373', '65', 'price', '1250000.00'),
('374', '65', 'size', '8648'),
('3669', '260', 'agent_name', 'Steve Miller'),
('378', '65', 'sale_type', 'sale'),
('379', '65', 'sale_metric', 'Once off'),
('380', '65', 'on_show', 'false'),
('381', '65', 'woo_maps_enable', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('382', '65', 'woo_maps_address', '129 Amity Road, Woodbridge, CT 06525, USA'),
('383', '65', 'woo_maps_long', '-72.97925709999998'),
('384', '65', 'woo_maps_lat', '41.3377419'),
('385', '65', 'woo_maps_zoom', '13'),
('386', '65', 'woo_maps_type', 'G_NORMAL_MAP'),
('387', '65', 'woo_maps_pov_pitch', '-20'),
('388', '65', 'woo_maps_pov_yaw', '20'),
('389', '65', '_yoast_wpseo_focuskw', ''),
('390', '65', '_yoast_wpseo_title', ''),
('391', '65', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('392', '65', '_yoast_wpseo_meta-robots-noindex', '0'),
('393', '65', '_yoast_wpseo_meta-robots-nofollow', '0'),
('394', '65', '_yoast_wpseo_meta-robots-adv', 'none'),
('395', '65', '_yoast_wpseo_canonical', ''),
('396', '65', '_yoast_wpseo_redirect', ''),
('1122', '135', '_yoast_wpseo_linkdex', '0'),
('398', '68', '_yoast_wpseo_linkdex', '0'),
('399', '68', '_edit_last', '3'),
('400', '68', '_edit_lock', '1373242465:3'),
('401', '69', '_wp_attached_file', '2013/05/2-Broadway.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('402', '69', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:22:\"2013/05/2-Broadway.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"2-Broadway-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"2-Broadway-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('403', '68', '_wpas_done_all', '1'),
('404', '68', 'image', ''),
('405', '68', 'address', '2 BROADWAY, HAMDEN'),
('406', '68', 'price', '10.00 '),
('407', '68', 'size', '800'),
('3718', '68', 'agent_name', 'Steve Miller'),
('410', '68', 'bathrooms', '9'),
('411', '68', 'sale_type', 'rent'),
('412', '68', 'sale_metric', 'Per Square Foot');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('413', '68', 'on_show', 'false'),
('414', '68', 'woo_maps_enable', 'false'),
('415', '68', 'woo_maps_address', '2 Broadway, Hamden, CT 06518, USA'),
('416', '68', 'woo_maps_long', '-72.8941097'),
('417', '68', 'woo_maps_lat', '41.4033521'),
('418', '68', 'woo_maps_zoom', '11'),
('419', '68', 'woo_maps_type', 'G_NORMAL_MAP'),
('420', '68', 'woo_maps_pov_pitch', '-20'),
('421', '68', 'woo_maps_pov_yaw', '20'),
('422', '68', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('423', '68', '_yoast_wpseo_title', ''),
('424', '68', '_yoast_wpseo_metadesc', ''),
('425', '68', '_yoast_wpseo_meta-robots-noindex', '0'),
('426', '68', '_yoast_wpseo_meta-robots-nofollow', '0'),
('427', '68', '_yoast_wpseo_meta-robots-adv', 'none'),
('428', '68', '_yoast_wpseo_canonical', ''),
('429', '68', '_yoast_wpseo_redirect', ''),
('430', '70', '_yoast_wpseo_linkdex', '0'),
('431', '70', '_edit_last', '3'),
('432', '70', '_edit_lock', '1373242658:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('433', '71', '_wp_attached_file', '2013/05/214-Amity-Rd.-1.jpg'),
('434', '71', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:27:\"2013/05/214-Amity-Rd.-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"214-Amity-Rd.-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"214-Amity-Rd.-1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('435', '70', '_wpas_done_all', '1'),
('436', '70', 'image', ''),
('437', '70', 'address', '214 AMITY ROAD, WOODBRIDGE'),
('438', '70', 'price', '12.00 '),
('439', '70', 'size', '1000'),
('3722', '75', 'agent_name', 'Steve Miller'),
('442', '70', 'bathrooms', '8'),
('443', '70', 'sale_type', 'rent');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('444', '70', 'sale_metric', 'Per Square Foot'),
('445', '70', 'on_show', 'false'),
('446', '70', 'woo_maps_enable', 'on'),
('447', '70', 'woo_maps_address', '214 Amity Road, Woodbridge, CT 06525, USA'),
('448', '70', 'woo_maps_long', '-72.98265449999997'),
('449', '70', 'woo_maps_lat', '41.3410897'),
('450', '70', 'woo_maps_zoom', '14'),
('451', '70', 'woo_maps_type', 'G_NORMAL_MAP'),
('452', '70', 'woo_maps_pov_pitch', '-20'),
('453', '70', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('454', '70', '_yoast_wpseo_focuskw', ''),
('455', '70', '_yoast_wpseo_title', ''),
('456', '70', '_yoast_wpseo_metadesc', ''),
('457', '70', '_yoast_wpseo_meta-robots-noindex', '0'),
('458', '70', '_yoast_wpseo_meta-robots-nofollow', '0'),
('459', '70', '_yoast_wpseo_meta-robots-adv', 'none'),
('460', '70', '_yoast_wpseo_canonical', ''),
('461', '70', '_yoast_wpseo_redirect', ''),
('463', '75', '_edit_last', '3'),
('464', '75', '_edit_lock', '1373251582:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('465', '76', '_wp_attached_file', '2013/05/15-RESEARCH-BRANFORD.jpg'),
('466', '76', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:32:\"2013/05/15-RESEARCH-BRANFORD.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"15-RESEARCH-BRANFORD-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"15-RESEARCH-BRANFORD-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('467', '75', '_thumbnail_id', '76'),
('468', '75', '_wpas_done_all', '1'),
('469', '75', 'image', ''),
('470', '75', 'address', '15 RESEARCH DRIVE, BRANFORD'),
('471', '75', 'price', '750000'),
('472', '75', 'size', '8800'),
('473', '75', 'garages', '1.97'),
('474', '75', 'beds', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('475', '75', 'bathrooms', ''),
('476', '75', 'sale_type', 'sale'),
('477', '75', 'sale_metric', 'Once off'),
('478', '75', 'on_show', 'false'),
('479', '75', 'woo_maps_enable', 'false'),
('480', '75', 'woo_maps_address', '15 Research Drive, Branford, CT 06405, USA'),
('481', '75', 'woo_maps_long', '-72.76801999999998'),
('482', '75', 'woo_maps_lat', '41.304716'),
('483', '75', 'woo_maps_zoom', '11'),
('484', '75', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('485', '75', 'woo_maps_pov_pitch', '-20'),
('486', '75', 'woo_maps_pov_yaw', '20'),
('487', '75', '_yoast_wpseo_focuskw', ''),
('488', '75', '_yoast_wpseo_title', ''),
('489', '75', '_yoast_wpseo_metadesc', ''),
('490', '75', '_yoast_wpseo_meta-robots-noindex', '0'),
('491', '75', '_yoast_wpseo_meta-robots-nofollow', '0'),
('492', '75', '_yoast_wpseo_meta-robots-adv', 'none'),
('493', '75', '_yoast_wpseo_canonical', ''),
('494', '75', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1251', '147', '_edit_lock', '1373288404:3'),
('496', '70', '_thumbnail_id', '71'),
('497', '68', '_thumbnail_id', '69'),
('499', '65', '_thumbnail_id', '66'),
('500', '63', '_thumbnail_id', '64'),
('501', '36', '_thumbnail_id', '45'),
('502', '47', '_thumbnail_id', '50'),
('503', '41', '_thumbnail_id', '43'),
('504', '82', '_yoast_wpseo_linkdex', '0'),
('505', '82', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('506', '82', '_edit_lock', '1375752911:3'),
('507', '82', 'image', ''),
('508', '82', 'garages', '0.18'),
('3724', '89', 'agent_name', 'Marty Ruff'),
('510', '82', 'bathrooms', '12'),
('511', '82', 'sale_type', 'sale'),
('512', '82', 'sale_metric', 'Once off'),
('513', '82', 'on_show', 'false'),
('514', '82', 'woo_maps_enable', 'on'),
('515', '82', 'woo_maps_zoom', '13');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('516', '82', 'woo_maps_type', 'G_NORMAL_MAP'),
('517', '82', '_yoast_wpseo_focuskw', ''),
('518', '82', '_yoast_wpseo_title', ''),
('519', '82', '_yoast_wpseo_metadesc', ''),
('520', '82', '_yoast_wpseo_meta-robots-noindex', '0'),
('521', '82', '_yoast_wpseo_meta-robots-nofollow', '0'),
('522', '82', '_yoast_wpseo_meta-robots-adv', 'none'),
('523', '82', '_yoast_wpseo_canonical', ''),
('524', '82', '_yoast_wpseo_redirect', ''),
('525', '83', '_wp_attached_file', '2013/05/149-Ramsdell-002.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('526', '83', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:28:\"2013/05/149-Ramsdell-002.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"149-Ramsdell-002-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"149-Ramsdell-002-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"149-Ramsdell-002-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1335180517;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:6:\"0.0025\";s:5:\"title\";s:0:\"\";}}'),
('527', '82', '_thumbnail_id', '83'),
('528', '82', '_wpas_done_all', '1'),
('529', '82', 'address', '149 RAMSDELL STREET, NEW HAVEN'),
('530', '82', 'price', '249000.00'),
('531', '82', 'size', '6000'),
('532', '82', 'woo_maps_address', '149 Ramsdell Street, New Haven, CT 06515, USA'),
('533', '82', 'woo_maps_long', '-72.97524299999998'),
('534', '82', 'woo_maps_lat', '41.331939'),
('535', '82', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('536', '82', 'woo_maps_pov_yaw', '20'),
('1250', '147', '_edit_last', '3'),
('538', '86', '_yoast_wpseo_linkdex', '0'),
('539', '86', '_edit_last', '3'),
('540', '86', '_edit_lock', '1373205034:3'),
('541', '87', '_wp_attached_file', '2013/05/881-Whalley.jpg'),
('542', '87', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:400;s:4:\"file\";s:23:\"2013/05/881-Whalley.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"881-Whalley-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"881-Whalley-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:10:\"Picasa 2.0\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('543', '86', '_thumbnail_id', '87'),
('544', '86', '_wpas_done_all', '1'),
('545', '86', 'image', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('546', '86', 'address', '881 WHALLEY AVENUE, NEW HAVEN'),
('547', '86', 'price', '14.00 '),
('548', '86', 'size', '2500'),
('3623', '63', 'agent_name', 'Marty Ruff'),
('3621', '86', 'agent_name', 'Steve Miller'),
('552', '86', 'sale_type', 'rent'),
('553', '86', 'sale_metric', 'Per Square Foot'),
('554', '86', 'on_show', 'false'),
('555', '86', 'woo_maps_enable', 'on'),
('1249', '147', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('557', '86', 'woo_maps_address', '881 Whalley Avenue, New Haven, CT 06515, USA'),
('558', '86', 'woo_maps_long', '-72.95969159999999'),
('559', '86', 'woo_maps_lat', '41.3268262'),
('560', '86', 'woo_maps_zoom', '13'),
('561', '86', 'woo_maps_type', 'G_NORMAL_MAP'),
('562', '86', 'woo_maps_pov_pitch', '-20'),
('563', '86', 'woo_maps_pov_yaw', '20'),
('564', '86', '_yoast_wpseo_focuskw', ''),
('565', '86', '_yoast_wpseo_title', ''),
('566', '86', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('567', '86', '_yoast_wpseo_meta-robots-noindex', '0'),
('568', '86', '_yoast_wpseo_meta-robots-nofollow', '0'),
('569', '86', '_yoast_wpseo_meta-robots-adv', 'none'),
('570', '86', '_yoast_wpseo_canonical', ''),
('571', '86', '_yoast_wpseo_redirect', ''),
('573', '89', '_yoast_wpseo_linkdex', '0'),
('574', '89', '_edit_last', '3'),
('575', '89', '_edit_lock', '1373242805:3'),
('576', '90', '_wp_attached_file', '2013/05/1130-Sherman1.jpg'),
('577', '90', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:25:\"2013/05/1130-Sherman1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1130-Sherman1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1130-Sherman1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('578', '91', '_wp_attached_file', '2013/05/1130-Sherman.jpg'),
('579', '91', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2013/05/1130-Sherman.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"1130-Sherman-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"1130-Sherman-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('580', '89', '_thumbnail_id', '91'),
('581', '89', '_wpas_done_all', '1'),
('582', '89', 'image', ''),
('583', '89', 'address', '1130 SHERMAN AVENUE, NEW HAVEN'),
('584', '89', 'price', '1100000.00'),
('585', '89', 'size', '8400'),
('586', '89', 'garages', '.73'),
('3726', '92', 'agent_name', 'Marty Ruff');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('588', '89', 'bathrooms', '18'),
('589', '89', 'sale_type', 'sale'),
('590', '89', 'sale_metric', 'Once off'),
('591', '89', 'on_show', 'false'),
('592', '89', 'woo_maps_enable', 'on'),
('593', '89', 'woo_maps_address', '1130 Sherman Avenue, Hamden, CT 06514, USA'),
('594', '89', 'woo_maps_long', '-72.92004659999998'),
('595', '89', 'woo_maps_lat', '41.3959434'),
('596', '89', 'woo_maps_zoom', '13'),
('597', '89', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('598', '89', 'woo_maps_pov_pitch', '-20'),
('599', '89', 'woo_maps_pov_yaw', '20'),
('600', '89', '_yoast_wpseo_focuskw', ''),
('601', '89', '_yoast_wpseo_title', ''),
('602', '89', '_yoast_wpseo_metadesc', ''),
('603', '89', '_yoast_wpseo_meta-robots-noindex', '0'),
('604', '89', '_yoast_wpseo_meta-robots-nofollow', '0'),
('605', '89', '_yoast_wpseo_meta-robots-adv', 'none'),
('606', '89', '_yoast_wpseo_canonical', ''),
('607', '89', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('608', '92', '_yoast_wpseo_linkdex', '0'),
('609', '92', '_edit_last', '3'),
('610', '92', '_edit_lock', '1373242893:3'),
('611', '93', '_wp_attached_file', '2013/05/565-Washington-Avenue-001.jpg'),
('612', '93', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:37:\"2013/05/565-Washington-Avenue-001.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"565-Washington-Avenue-001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"565-Washington-Avenue-001-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"565-Washington-Avenue-001-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1252508463;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:5:\"0.001\";s:5:\"title\";s:0:\"\";}}'),
('613', '92', '_thumbnail_id', '93'),
('614', '92', '_wpas_done_all', '1'),
('615', '92', 'image', ''),
('616', '92', 'address', '565 WASHINGTON AVENUE, NORTH HAVEN'),
('617', '92', 'price', '14.50 ');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('618', '92', 'size', '3625'),
('619', '92', 'garages', ''),
('620', '92', 'beds', ''),
('621', '92', 'bathrooms', ''),
('622', '92', 'sale_type', 'rent'),
('623', '92', 'sale_metric', 'Per Square Foot'),
('624', '92', 'on_show', 'false'),
('625', '92', 'woo_maps_enable', 'on'),
('626', '92', 'woo_maps_address', '565 Washington Avenue, New Haven, CT 06519, USA'),
('627', '92', 'woo_maps_long', '-72.93780830000003');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('628', '92', 'woo_maps_lat', '41.2952425'),
('629', '92', 'woo_maps_zoom', '14'),
('630', '92', 'woo_maps_type', 'G_NORMAL_MAP'),
('631', '92', 'woo_maps_pov_pitch', '-20'),
('632', '92', 'woo_maps_pov_yaw', '20'),
('633', '92', '_yoast_wpseo_focuskw', ''),
('634', '92', '_yoast_wpseo_title', ''),
('635', '92', '_yoast_wpseo_metadesc', ''),
('636', '92', '_yoast_wpseo_meta-robots-noindex', '0'),
('637', '92', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('638', '92', '_yoast_wpseo_meta-robots-adv', 'none'),
('639', '92', '_yoast_wpseo_canonical', ''),
('640', '92', '_yoast_wpseo_redirect', ''),
('641', '94', '_yoast_wpseo_linkdex', '0'),
('642', '94', '_edit_last', '3'),
('643', '94', '_edit_lock', '1373243083:3'),
('644', '95', '_wp_attached_file', '2013/05/230-Old-Gate.jpg'),
('645', '95', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2013/05/230-Old-Gate.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"230-Old-Gate-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"230-Old-Gate-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('646', '94', '_thumbnail_id', '95'),
('647', '94', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('648', '94', 'image', ''),
('649', '94', 'address', '230 OLD GATE LANE, MILFORD'),
('650', '94', 'size', '20,000'),
('651', '94', 'garages', '9'),
('654', '94', 'sale_type', 'rent'),
('655', '94', 'sale_metric', 'Once off'),
('656', '94', 'on_show', 'false'),
('657', '94', 'woo_maps_enable', 'on'),
('658', '94', 'woo_maps_address', '230 Old Gate Lane, Milford, CT 06460, USA'),
('659', '94', 'woo_maps_long', '-73.02981799999998');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('660', '94', 'woo_maps_lat', '41.232598'),
('661', '94', 'woo_maps_zoom', '15'),
('662', '94', 'woo_maps_type', 'G_NORMAL_MAP'),
('663', '94', 'woo_maps_pov_pitch', '-20'),
('664', '94', 'woo_maps_pov_yaw', '20'),
('665', '94', '_yoast_wpseo_focuskw', ''),
('666', '94', '_yoast_wpseo_title', ''),
('667', '94', '_yoast_wpseo_metadesc', ''),
('668', '94', '_yoast_wpseo_meta-robots-noindex', '0'),
('669', '94', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('670', '94', '_yoast_wpseo_meta-robots-adv', 'none'),
('671', '94', '_yoast_wpseo_canonical', ''),
('672', '94', '_yoast_wpseo_redirect', ''),
('673', '96', '_yoast_wpseo_linkdex', '0'),
('674', '96', '_edit_last', '3'),
('675', '96', '_edit_lock', '1373243198:3'),
('676', '97', '_wp_attached_file', '2013/05/3030-Whitney-photo.jpg'),
('677', '97', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:30:\"2013/05/3030-Whitney-photo.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"3030-Whitney-photo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"3030-Whitney-photo-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.399999999999999911182158029987476766109466552734375;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:9:\"iPhone 4S\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1364732393;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"4.28\";s:3:\"iso\";s:2:\"50\";s:13:\"shutter_speed\";s:17:\"0.000598086124402\";s:5:\"title\";s:0:\"\";}}'),
('678', '96', '_thumbnail_id', '97'),
('679', '96', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('680', '96', 'image', ''),
('681', '96', 'address', '3030 WHITNEY AVENUE'),
('682', '96', 'price', '16.50'),
('683', '96', 'size', '1900'),
('684', '96', 'garages', ''),
('685', '96', 'beds', ''),
('686', '96', 'bathrooms', '10'),
('687', '96', 'sale_type', 'rent'),
('688', '96', 'sale_metric', 'Per Square Foot'),
('689', '96', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('690', '96', 'woo_maps_enable', 'on'),
('691', '96', 'woo_maps_address', '3030 Whitney Avenue, Hamden, CT 06518, USA'),
('692', '96', 'woo_maps_long', '-72.89939500000003'),
('693', '96', 'woo_maps_lat', '41.405446'),
('694', '96', 'woo_maps_zoom', '12'),
('695', '96', 'woo_maps_type', 'G_NORMAL_MAP'),
('696', '96', 'woo_maps_pov_pitch', '-20'),
('697', '96', 'woo_maps_pov_yaw', '20'),
('698', '96', '_yoast_wpseo_focuskw', ''),
('699', '96', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('700', '96', '_yoast_wpseo_metadesc', ''),
('701', '96', '_yoast_wpseo_meta-robots-noindex', '0'),
('702', '96', '_yoast_wpseo_meta-robots-nofollow', '0'),
('703', '96', '_yoast_wpseo_meta-robots-adv', 'none'),
('704', '96', '_yoast_wpseo_canonical', ''),
('705', '96', '_yoast_wpseo_redirect', ''),
('706', '100', '_yoast_wpseo_linkdex', '0'),
('707', '100', '_edit_last', '3'),
('708', '100', '_edit_lock', '1373243217:3'),
('709', '101', '_wp_attached_file', '2013/05/176-Boyden.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('710', '101', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:240;s:4:\"file\";s:22:\"2013/05/176-Boyden.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"176-Boyden-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"176-Boyden-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('711', '100', '_thumbnail_id', '101'),
('712', '100', '_wpas_done_all', '1'),
('713', '100', 'image', ''),
('714', '100', 'address', '176 BOYDEN STREET, WATERBURY '),
('715', '100', 'price', '149000'),
('716', '100', 'size', '1700'),
('717', '100', 'garages', ''),
('718', '100', 'beds', ''),
('719', '100', 'bathrooms', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('720', '100', 'sale_type', 'sale'),
('721', '100', 'sale_metric', 'Once off'),
('722', '100', 'on_show', 'false'),
('723', '100', 'woo_maps_enable', 'on'),
('724', '100', 'woo_maps_address', '176 Boyden Street, Waterbury, CT 06704, USA'),
('725', '100', 'woo_maps_long', '-73.046558'),
('726', '100', 'woo_maps_lat', '41.587843'),
('727', '100', 'woo_maps_zoom', '15'),
('728', '100', 'woo_maps_type', 'G_NORMAL_MAP'),
('729', '100', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('730', '100', 'woo_maps_pov_yaw', '20'),
('731', '100', '_yoast_wpseo_focuskw', ''),
('732', '100', '_yoast_wpseo_title', ''),
('733', '100', '_yoast_wpseo_metadesc', ''),
('734', '100', '_yoast_wpseo_meta-robots-noindex', '0'),
('735', '100', '_yoast_wpseo_meta-robots-nofollow', '0'),
('736', '100', '_yoast_wpseo_meta-robots-adv', 'none'),
('737', '100', '_yoast_wpseo_canonical', ''),
('738', '100', '_yoast_wpseo_redirect', ''),
('739', '105', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('740', '105', '_edit_last', '3'),
('741', '105', '_edit_lock', '1373243379:3'),
('742', '106', '_wp_attached_file', '2013/05/23-Raccio-Park.jpg'),
('743', '106', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:26:\"2013/05/23-Raccio-Park.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"23-Raccio-Park-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"23-Raccio-Park-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('744', '105', '_thumbnail_id', '106'),
('745', '105', '_wpas_done_all', '1'),
('746', '105', 'image', ''),
('747', '105', 'address', '23 RACCIO PARK ROAD, HAMDEN'),
('748', '105', 'price', '695000'),
('749', '105', 'size', '9780');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('750', '105', 'garages', ''),
('751', '105', 'beds', ''),
('752', '105', 'bathrooms', ''),
('753', '105', 'sale_type', 'sale'),
('754', '105', 'sale_metric', 'Once off'),
('755', '105', 'on_show', 'false'),
('756', '105', 'woo_maps_enable', 'on'),
('757', '105', 'woo_maps_address', '23 Raccio Park Road, Hamden, CT 06514, USA'),
('758', '105', 'woo_maps_long', '-72.9178245'),
('759', '105', 'woo_maps_lat', '41.3920327');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('760', '105', 'woo_maps_zoom', '13'),
('761', '105', 'woo_maps_type', 'G_NORMAL_MAP'),
('762', '105', 'woo_maps_pov_pitch', '-20'),
('763', '105', 'woo_maps_pov_yaw', '20'),
('764', '105', '_yoast_wpseo_focuskw', ''),
('765', '105', '_yoast_wpseo_title', ''),
('766', '105', '_yoast_wpseo_metadesc', ''),
('767', '105', '_yoast_wpseo_meta-robots-noindex', '0'),
('768', '105', '_yoast_wpseo_meta-robots-nofollow', '0'),
('769', '105', '_yoast_wpseo_meta-robots-adv', 'none');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('770', '105', '_yoast_wpseo_canonical', ''),
('771', '105', '_yoast_wpseo_redirect', ''),
('772', '107', '_yoast_wpseo_linkdex', '0'),
('773', '107', '_edit_last', '3'),
('774', '107', '_edit_lock', '1373243495:3'),
('775', '108', '_wp_attached_file', '2013/05/80-Skiff-St..jpg'),
('776', '108', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1824;s:6:\"height\";i:1136;s:4:\"file\";s:24:\"2013/05/80-Skiff-St..jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"80-Skiff-St.-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"80-Skiff-St.-300x186.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"80-Skiff-St.-1024x637.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:637;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:12:\"MX850 series\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1299238984;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('777', '107', '_thumbnail_id', '108'),
('778', '107', '_wpas_done_all', '1'),
('779', '107', 'image', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('780', '107', 'address', '80 SKIFF STREET, HAMDEN'),
('781', '107', 'price', '395000'),
('782', '107', 'size', '1900'),
('3738', '110', 'agent_name', 'Michael Gordon'),
('785', '107', 'bathrooms', '15'),
('786', '107', 'sale_type', 'sale'),
('787', '107', 'sale_metric', 'Once off'),
('788', '107', 'on_show', 'false'),
('789', '107', 'woo_maps_enable', 'on'),
('790', '107', 'woo_maps_address', '80 Skiff Street, Hamden, CT 06514, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('791', '107', 'woo_maps_long', '-72.91285599999998'),
('792', '107', 'woo_maps_lat', '41.373899'),
('793', '107', 'woo_maps_zoom', '14'),
('794', '107', 'woo_maps_type', 'G_NORMAL_MAP'),
('795', '107', 'woo_maps_pov_pitch', '-20'),
('796', '107', 'woo_maps_pov_yaw', '20'),
('797', '107', '_yoast_wpseo_focuskw', ''),
('798', '107', '_yoast_wpseo_title', ''),
('799', '107', '_yoast_wpseo_metadesc', ''),
('800', '107', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('801', '107', '_yoast_wpseo_meta-robots-nofollow', '0'),
('802', '107', '_yoast_wpseo_meta-robots-adv', 'none'),
('803', '107', '_yoast_wpseo_canonical', ''),
('804', '107', '_yoast_wpseo_redirect', ''),
('805', '110', '_yoast_wpseo_linkdex', '0'),
('806', '110', '_edit_last', '3'),
('807', '110', '_edit_lock', '1373243455:3'),
('808', '110', '_wpas_done_all', '1'),
('809', '110', 'image', ''),
('810', '110', 'address', '77 WHITING STREET, PLAINVILLE');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('811', '110', 'price', '399000'),
('812', '110', 'size', '1000'),
('813', '110', 'garages', ''),
('814', '110', 'beds', ''),
('815', '110', 'bathrooms', ''),
('816', '110', 'sale_type', 'sale'),
('817', '110', 'sale_metric', 'Once off'),
('818', '110', 'on_show', 'false'),
('819', '110', 'woo_maps_enable', 'on'),
('820', '110', 'woo_maps_address', '77 Whiting Street, Plainville, CT 06062, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('821', '110', 'woo_maps_long', '-72.86713859999998'),
('822', '110', 'woo_maps_lat', '41.6679358'),
('823', '110', 'woo_maps_zoom', '13'),
('824', '110', 'woo_maps_type', 'G_NORMAL_MAP'),
('825', '110', 'woo_maps_pov_pitch', '-20'),
('826', '110', 'woo_maps_pov_yaw', '20'),
('827', '110', '_yoast_wpseo_focuskw', ''),
('828', '110', '_yoast_wpseo_title', ''),
('829', '110', '_yoast_wpseo_metadesc', ''),
('830', '110', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('831', '110', '_yoast_wpseo_meta-robots-nofollow', '0'),
('832', '110', '_yoast_wpseo_meta-robots-adv', 'none'),
('833', '110', '_yoast_wpseo_canonical', ''),
('834', '110', '_yoast_wpseo_redirect', ''),
('835', '112', '_wp_attached_file', '2013/05/77-Whiting-Street.jpg'),
('836', '112', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1824;s:6:\"height\";i:1136;s:4:\"file\";s:29:\"2013/05/77-Whiting-Street.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"77-Whiting-Street-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"77-Whiting-Street-300x186.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"77-Whiting-Street-1024x637.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:637;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:12:\"MX850 series\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1300443705;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('837', '110', '_thumbnail_id', '112'),
('838', '113', '_yoast_wpseo_linkdex', '0'),
('839', '113', '_edit_last', '3'),
('840', '113', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/opportunity-300x225.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('841', '113', 'garages', '1.01'),
('3745', '114', 'agent_name', 'Marty Ruff'),
('844', '113', 'sale_type', 'sale'),
('845', '113', 'sale_metric', 'Once off'),
('846', '113', 'on_show', 'false'),
('847', '113', 'woo_maps_enable', 'on'),
('848', '113', 'woo_maps_zoom', '13'),
('849', '113', 'woo_maps_type', 'G_NORMAL_MAP'),
('850', '113', '_yoast_wpseo_focuskw', ''),
('851', '113', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('852', '113', '_yoast_wpseo_metadesc', ''),
('853', '113', '_yoast_wpseo_meta-robots-noindex', '0'),
('854', '113', '_yoast_wpseo_meta-robots-nofollow', '0'),
('855', '113', '_yoast_wpseo_meta-robots-adv', 'none'),
('856', '113', '_yoast_wpseo_canonical', ''),
('857', '113', '_yoast_wpseo_redirect', ''),
('858', '113', '_edit_lock', '1376581811:3'),
('859', '113', 'address', '715 BOSTON POST ROAD, WEST HAVEN'),
('860', '113', '_wpas_done_all', '1'),
('861', '114', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('862', '114', '_edit_last', '3'),
('863', '114', 'image', ''),
('864', '114', 'garages', ''),
('865', '114', 'beds', ''),
('866', '114', 'bathrooms', '8-20'),
('867', '114', 'sale_type', 'sale'),
('868', '114', 'sale_metric', 'Once off'),
('869', '114', 'on_show', 'false'),
('870', '114', 'woo_maps_enable', 'on'),
('871', '114', 'woo_maps_zoom', '15');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('872', '114', 'woo_maps_type', 'G_NORMAL_MAP'),
('873', '114', '_yoast_wpseo_focuskw', ''),
('874', '114', '_yoast_wpseo_title', ''),
('875', '114', '_yoast_wpseo_metadesc', ''),
('876', '114', '_yoast_wpseo_meta-robots-noindex', '0'),
('877', '114', '_yoast_wpseo_meta-robots-nofollow', '0'),
('878', '114', '_yoast_wpseo_meta-robots-adv', 'none'),
('879', '114', '_yoast_wpseo_canonical', ''),
('880', '114', '_yoast_wpseo_redirect', ''),
('881', '114', '_edit_lock', '1376581366:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('882', '115', '_wp_attached_file', '2013/05/196-Frontage.jpg'),
('883', '115', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2013/05/196-Frontage.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"196-Frontage-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"196-Frontage-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('884', '114', '_thumbnail_id', '115'),
('885', '114', '_wpas_done_all', '1'),
('886', '114', 'address', '196 FRONTAGE ROAD, WEST HAVEN'),
('887', '114', 'price', '349000'),
('888', '114', 'size', '3200'),
('889', '114', 'woo_maps_address', '196 Frontage Road, East Haven, CT 06512, USA'),
('890', '114', 'woo_maps_long', '-72.88503300000002'),
('891', '114', 'woo_maps_lat', '41.287616');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('892', '114', 'woo_maps_pov_pitch', '-20'),
('893', '114', 'woo_maps_pov_yaw', '20'),
('1708', '193', '_edit_lock', '1373204654:3'),
('4040', '113', 'price', '949000'),
('896', '113', 'size', '32964'),
('897', '113', 'woo_maps_address', '715 Boston Post Road, West Haven, CT 06516, USA'),
('898', '113', 'woo_maps_long', '-72.97204249999999'),
('899', '113', 'woo_maps_lat', '41.2885183'),
('900', '113', 'woo_maps_pov_pitch', '-20'),
('901', '113', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('902', '118', '_yoast_wpseo_linkdex', '0'),
('903', '118', '_edit_last', '3'),
('904', '118', '_edit_lock', '1374155175:3'),
('905', '119', '_wp_attached_file', '2013/05/105-Sanford.jpg'),
('906', '119', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:23:\"2013/05/105-Sanford.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"105-Sanford-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"105-Sanford-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('907', '118', '_thumbnail_id', '119'),
('908', '118', '_wpas_done_all', '1'),
('909', '118', 'image', ''),
('910', '118', 'address', '105 SANFORD STREET, HAMDEN'),
('3897', '794', '_wp_attached_file', '2013/06/1400-Whalley-2nd-Floor.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('912', '118', 'size', '1240'),
('916', '118', 'sale_type', 'rent'),
('917', '118', 'sale_metric', 'Per Month'),
('918', '118', 'on_show', 'false'),
('919', '118', 'woo_maps_enable', 'on'),
('920', '118', 'woo_maps_address', '105 Sanford Street, Hamden, CT 06514, USA'),
('921', '118', 'woo_maps_long', '-72.91555599999998'),
('922', '118', 'woo_maps_lat', '41.382091'),
('923', '118', 'woo_maps_zoom', '11'),
('924', '118', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('925', '118', 'woo_maps_pov_pitch', '-20'),
('926', '118', 'woo_maps_pov_yaw', '20'),
('927', '118', '_yoast_wpseo_focuskw', ''),
('928', '118', '_yoast_wpseo_title', ''),
('929', '118', '_yoast_wpseo_metadesc', ''),
('930', '118', '_yoast_wpseo_meta-robots-noindex', '0'),
('931', '118', '_yoast_wpseo_meta-robots-nofollow', '0'),
('932', '118', '_yoast_wpseo_meta-robots-adv', 'none'),
('933', '118', '_yoast_wpseo_canonical', ''),
('934', '118', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('935', '120', '_yoast_wpseo_linkdex', '0'),
('936', '120', '_edit_last', '3'),
('937', '120', '_edit_lock', '1373244421:3'),
('938', '120', '_thumbnail_id', '747'),
('939', '120', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/success-sign-300x199.jpg'),
('940', '120', 'address', '306 EAST JOHNSON AVENUE, CHESHIRE'),
('941', '120', 'price', '1200000'),
('942', '120', 'garages', '14+'),
('943', '120', 'beds', ''),
('944', '120', 'bathrooms', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('945', '120', 'sale_type', 'sale'),
('946', '120', 'sale_metric', 'Once off'),
('947', '120', 'on_show', 'false'),
('948', '120', 'woo_maps_enable', 'on'),
('949', '120', 'woo_maps_zoom', '12'),
('950', '120', 'woo_maps_type', 'G_NORMAL_MAP'),
('951', '120', '_yoast_wpseo_focuskw', ''),
('952', '120', '_yoast_wpseo_title', ''),
('953', '120', '_yoast_wpseo_metadesc', ''),
('954', '120', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('955', '120', '_yoast_wpseo_meta-robots-nofollow', '0'),
('956', '120', '_yoast_wpseo_meta-robots-adv', 'none'),
('957', '120', '_yoast_wpseo_canonical', ''),
('958', '120', '_yoast_wpseo_redirect', ''),
('959', '120', '_wpas_done_all', '1'),
('960', '120', 'woo_maps_address', '306 East Johnson Avenue, Cheshire, CT 06410, USA'),
('961', '120', 'woo_maps_long', '-72.87823200000003'),
('962', '120', 'woo_maps_lat', '41.55413900000001'),
('963', '120', 'woo_maps_pov_pitch', '-20'),
('964', '120', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('965', '121', '_yoast_wpseo_linkdex', '0'),
('966', '121', '_edit_last', '3'),
('967', '121', '_edit_lock', '1373244413:3'),
('968', '122', '_wp_attached_file', '2013/05/19-marietta-3.jpg'),
('969', '122', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2848;s:6:\"height\";i:2134;s:4:\"file\";s:25:\"2013/05/19-marietta-3.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"19-marietta-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"19-marietta-3-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"19-marietta-3-1024x767.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.70000000000000017763568394002504646778106689453125;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1203420192;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"6\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00156500004232\";s:5:\"title\";s:0:\"\";}}'),
('970', '121', '_thumbnail_id', '122'),
('971', '121', '_wpas_done_all', '1'),
('972', '121', 'image', ''),
('973', '121', 'address', '19 MARIETTA STREET, HAMDEN'),
('974', '121', 'price', '249000');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('975', '121', 'size', '5000'),
('976', '121', 'garages', ''),
('977', '121', 'beds', ''),
('978', '121', 'bathrooms', ''),
('979', '121', 'sale_type', 'sale'),
('980', '121', 'sale_metric', 'Once off'),
('981', '121', 'on_show', 'false'),
('982', '121', 'woo_maps_enable', 'on'),
('983', '121', 'woo_maps_address', '19 Marietta Street, Hamden, CT 06514, USA'),
('984', '121', 'woo_maps_long', '-72.92037399999998');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('985', '121', 'woo_maps_lat', '41.366193'),
('986', '121', 'woo_maps_zoom', '13'),
('987', '121', 'woo_maps_type', 'G_NORMAL_MAP'),
('988', '121', 'woo_maps_pov_pitch', '-20'),
('989', '121', 'woo_maps_pov_yaw', '20'),
('990', '121', '_yoast_wpseo_focuskw', ''),
('991', '121', '_yoast_wpseo_title', ''),
('992', '121', '_yoast_wpseo_metadesc', ''),
('993', '121', '_yoast_wpseo_meta-robots-noindex', '0'),
('994', '121', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('995', '121', '_yoast_wpseo_meta-robots-adv', 'none'),
('996', '121', '_yoast_wpseo_canonical', ''),
('997', '121', '_yoast_wpseo_redirect', ''),
('998', '123', '_yoast_wpseo_linkdex', '0'),
('999', '123', '_edit_last', '3'),
('1000', '123', '_edit_lock', '1373244483:3'),
('1001', '123', '_thumbnail_id', '50'),
('1002', '123', '_wpas_done_all', '1'),
('1003', '123', 'image', ''),
('1004', '123', 'address', 'CHRISTIAN LANE, BERLIN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1005', '123', 'price', '1600000'),
('1006', '123', 'garages', '20'),
('1007', '123', 'beds', ''),
('1008', '123', 'bathrooms', ''),
('1009', '123', 'sale_type', 'sale'),
('1010', '123', 'sale_metric', 'Once off'),
('1011', '123', 'on_show', 'false'),
('1012', '123', 'woo_maps_enable', 'on'),
('1013', '123', 'woo_maps_address', 'Christian Lane, Berlin, CT 06037, USA'),
('1014', '123', 'woo_maps_long', '-72.75016069999998');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1015', '123', 'woo_maps_lat', '41.6405665'),
('1016', '123', 'woo_maps_zoom', '13'),
('1017', '123', 'woo_maps_type', 'G_NORMAL_MAP'),
('1018', '123', 'woo_maps_pov_pitch', '-20'),
('1019', '123', 'woo_maps_pov_yaw', '20'),
('1020', '123', '_yoast_wpseo_focuskw', ''),
('1021', '123', '_yoast_wpseo_title', ''),
('1022', '123', '_yoast_wpseo_metadesc', ''),
('1023', '123', '_yoast_wpseo_meta-robots-noindex', '0'),
('1024', '123', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1025', '123', '_yoast_wpseo_meta-robots-adv', 'none'),
('1026', '123', '_yoast_wpseo_canonical', ''),
('1027', '123', '_yoast_wpseo_redirect', ''),
('1028', '124', '_yoast_wpseo_linkdex', '0'),
('1029', '124', '_edit_last', '3'),
('1030', '124', '_edit_lock', '1373244704:3'),
('1031', '125', '_wp_attached_file', '2013/05/495-Blake-Street-1.jpg'),
('1032', '125', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:360;s:4:\"file\";s:30:\"2013/05/495-Blake-Street-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"495-Blake-Street-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"495-Blake-Street-1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:4.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:10:\"Picasa 2.6\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1178195192;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"6\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00135399959981\";s:5:\"title\";s:0:\"\";}}'),
('1033', '124', '_thumbnail_id', '125'),
('1034', '124', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1035', '124', 'image', ''),
('1036', '124', 'address', '495 BLAKE STREET, NEW HAVEN'),
('1037', '124', 'price', '12.00 '),
('1038', '124', 'size', '40000'),
('1039', '124', 'garages', ''),
('1040', '124', 'beds', ''),
('1041', '124', 'bathrooms', '9-18'),
('1042', '124', 'sale_type', 'rent'),
('1043', '124', 'sale_metric', 'Per Square Foot'),
('1044', '124', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1045', '124', 'woo_maps_enable', 'on'),
('1046', '124', 'woo_maps_address', '495 Blake Street, New Haven, CT 06515, USA'),
('1047', '124', 'woo_maps_long', '-72.95966099999998'),
('1048', '124', 'woo_maps_lat', '41.328832'),
('1049', '124', 'woo_maps_zoom', '15'),
('1050', '124', 'woo_maps_type', 'G_NORMAL_MAP'),
('1051', '124', 'woo_maps_pov_pitch', '-20'),
('1052', '124', 'woo_maps_pov_yaw', '20'),
('1053', '124', '_yoast_wpseo_focuskw', ''),
('1054', '124', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1055', '124', '_yoast_wpseo_metadesc', ''),
('1056', '124', '_yoast_wpseo_meta-robots-noindex', '0'),
('1057', '124', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1058', '124', '_yoast_wpseo_meta-robots-adv', 'none'),
('1059', '124', '_yoast_wpseo_canonical', ''),
('1060', '124', '_yoast_wpseo_redirect', ''),
('1061', '126', '_yoast_wpseo_linkdex', '0'),
('1062', '126', '_edit_last', '3'),
('1063', '126', '_edit_lock', '1373244912:3'),
('1707', '193', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1065', '126', '_wpas_done_all', '1'),
('1066', '126', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/success-sign-300x199.jpg'),
('1067', '126', 'garages', ''),
('1068', '126', 'beds', ''),
('1069', '126', 'bathrooms', '8-16'),
('1070', '126', 'sale_type', 'sale'),
('1071', '126', 'sale_metric', 'Once off'),
('1072', '126', 'on_show', 'false'),
('1073', '126', 'woo_maps_enable', 'on'),
('1074', '126', 'woo_maps_zoom', '15');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1075', '126', 'woo_maps_type', 'G_NORMAL_MAP'),
('1076', '126', '_yoast_wpseo_focuskw', ''),
('1077', '126', '_yoast_wpseo_title', ''),
('1078', '126', '_yoast_wpseo_metadesc', ''),
('1079', '126', '_yoast_wpseo_meta-robots-noindex', '0'),
('1080', '126', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1081', '126', '_yoast_wpseo_meta-robots-adv', 'none'),
('1082', '126', '_yoast_wpseo_canonical', ''),
('1083', '126', '_yoast_wpseo_redirect', ''),
('1084', '126', 'address', '22 HEMINGWAY AVENUE, EAST HAVEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1085', '126', 'price', '395000'),
('1086', '126', 'size', '6510'),
('1087', '126', 'woo_maps_address', '22 Hemingway Avenue, East Haven, CT 06512, USA'),
('1088', '126', 'woo_maps_long', '-72.87260559999999'),
('1089', '126', 'woo_maps_lat', '41.2671766'),
('1090', '126', 'woo_maps_pov_pitch', '-20'),
('1091', '126', 'woo_maps_pov_yaw', '20'),
('1092', '129', '_yoast_wpseo_linkdex', '0'),
('1093', '129', '_edit_last', '3'),
('1094', '129', '_edit_lock', '1373245181:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1095', '129', '_wpas_done_all', '1'),
('1096', '129', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/glass_of_wine_by_hiddenrainbow-d4y756j-300x225.png'),
('1097', '129', 'address', 'HAMDEN'),
('1098', '129', 'price', '349000'),
('3766', '138', 'agent_name', 'Noah Meyer'),
('1102', '129', 'sale_type', 'sale'),
('1103', '129', 'sale_metric', 'Once off'),
('1104', '129', 'on_show', 'false'),
('1105', '129', 'woo_maps_enable', 'on'),
('1106', '129', 'woo_maps_address', 'Hamden, CT, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1107', '129', 'woo_maps_long', '-72.90260639999997'),
('1108', '129', 'woo_maps_lat', '41.3838782'),
('1109', '129', 'woo_maps_zoom', '10'),
('1110', '129', 'woo_maps_type', 'G_NORMAL_MAP'),
('1111', '129', 'woo_maps_pov_pitch', '-20'),
('1112', '129', 'woo_maps_pov_yaw', '20'),
('1113', '129', '_yoast_wpseo_focuskw', ''),
('1114', '129', '_yoast_wpseo_title', ''),
('1115', '129', '_yoast_wpseo_metadesc', ''),
('1116', '129', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1117', '129', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1118', '129', '_yoast_wpseo_meta-robots-adv', 'none'),
('1119', '129', '_yoast_wpseo_canonical', ''),
('1120', '129', '_yoast_wpseo_redirect', ''),
('1706', '193', '_yoast_wpseo_linkdex', '0'),
('1126', '135', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/images2.jpg'),
('1127', '135', 'address', 'NEW HAVEN'),
('1128', '135', 'price', '2900000'),
('1129', '135', 'size', '8625'),
('3643', '566', 'agent_name', 'Steve Miller');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1132', '135', 'bathrooms', '10'),
('1133', '135', 'sale_type', 'sale'),
('1134', '135', 'sale_metric', 'Once off'),
('1135', '135', 'on_show', 'false'),
('1136', '135', 'woo_maps_enable', 'on'),
('1137', '135', 'woo_maps_address', 'New Haven, CT, USA'),
('1138', '135', 'woo_maps_long', '-72.92788350000001'),
('1139', '135', 'woo_maps_lat', '41.308274'),
('1140', '135', 'woo_maps_zoom', '9'),
('1141', '135', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1142', '135', 'woo_maps_pov_pitch', '-20'),
('1143', '135', 'woo_maps_pov_yaw', '20'),
('1144', '135', '_yoast_wpseo_focuskw', ''),
('1145', '135', '_yoast_wpseo_title', ''),
('1146', '135', '_yoast_wpseo_metadesc', ''),
('1147', '135', '_yoast_wpseo_meta-robots-noindex', '0'),
('1148', '135', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1149', '135', '_yoast_wpseo_meta-robots-adv', 'none'),
('1150', '135', '_yoast_wpseo_canonical', ''),
('1151', '135', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1152', '136', '_wp_attached_file', '2013/05/business.jpg'),
('1153', '136', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:120;s:6:\"height\";i:101;s:4:\"file\";s:20:\"2013/05/business.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1155', '138', '_yoast_wpseo_linkdex', '0'),
('1156', '138', '_edit_last', '3'),
('1157', '138', '_edit_lock', '1373245169:3'),
('1158', '139', '_wp_attached_file', '2013/05/33-Rossotto-Dr.jpg'),
('1159', '139', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:328;s:6:\"height\";i:246;s:4:\"file\";s:26:\"2013/05/33-Rossotto-Dr.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"33-Rossotto-Dr-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"33-Rossotto-Dr-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1160', '138', '_thumbnail_id', '139'),
('1161', '138', '_wpas_done_all', '1'),
('1162', '138', 'image', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1163', '138', 'address', '33 ROSOTTO DRIVE, HAMDEN'),
('1164', '138', 'price', '8.50'),
('1165', '138', 'size', '10000'),
('1166', '138', 'garages', ''),
('1167', '138', 'beds', ''),
('1168', '138', 'bathrooms', ''),
('1169', '138', 'sale_type', 'rent'),
('1170', '138', 'sale_metric', 'Per Square Foot'),
('1171', '138', 'on_show', 'false'),
('1172', '138', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1173', '138', 'woo_maps_address', '33 Rossotto Drive, Hamden, CT 06514, USA'),
('1174', '138', 'woo_maps_long', '-72.918882'),
('1175', '138', 'woo_maps_lat', '41.394195'),
('1176', '138', 'woo_maps_zoom', '14'),
('1177', '138', 'woo_maps_type', 'G_NORMAL_MAP'),
('1178', '138', 'woo_maps_pov_pitch', '-20'),
('1179', '138', 'woo_maps_pov_yaw', '20'),
('1180', '138', '_yoast_wpseo_focuskw', ''),
('1181', '138', '_yoast_wpseo_title', ''),
('1182', '138', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1183', '138', '_yoast_wpseo_meta-robots-noindex', '0'),
('1184', '138', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1185', '138', '_yoast_wpseo_meta-robots-adv', 'none'),
('1186', '138', '_yoast_wpseo_canonical', ''),
('1187', '138', '_yoast_wpseo_redirect', ''),
('1188', '129', '_thumbnail_id', '750'),
('1190', '141', '_yoast_wpseo_linkdex', '0'),
('1191', '141', '_edit_last', '3'),
('1192', '141', '_edit_lock', '1373204974:3'),
('1193', '142', '_wp_attached_file', '2013/05/201-Orange-Street.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1194', '142', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:360;s:4:\"file\";s:29:\"2013/05/201-Orange-Street.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"201-Orange-Street-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"201-Orange-Street-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:3;s:6:\"credit\";s:10:\"Picasa 2.7\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1136117423;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:13:\"7.59999990463\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00278399977833\";s:5:\"title\";s:0:\"\";}}'),
('1195', '141', '_thumbnail_id', '142'),
('1196', '141', '_wpas_done_all', '1'),
('1197', '141', 'image', ''),
('1198', '141', 'address', '201 ORANGE STREET, NEW HAVEN'),
('1199', '141', 'price', '18.00'),
('1200', '141', 'size', '3000'),
('1203', '141', 'bathrooms', '9'),
('1204', '141', 'sale_type', 'rent'),
('1205', '141', 'sale_metric', 'Per Square Foot');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1206', '141', 'on_show', 'false'),
('1207', '141', 'woo_maps_enable', 'on'),
('1208', '141', 'woo_maps_address', '201 Orange Street, New Haven, CT 06510, USA'),
('1209', '141', 'woo_maps_long', '-72.92312090000001'),
('1210', '141', 'woo_maps_lat', '41.306921'),
('1211', '141', 'woo_maps_zoom', '15'),
('1212', '141', 'woo_maps_type', 'G_NORMAL_MAP'),
('1213', '141', 'woo_maps_pov_pitch', '-18.558184213604097'),
('1214', '141', 'woo_maps_pov_yaw', '20.047808250808917'),
('1215', '141', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1216', '141', '_yoast_wpseo_title', ''),
('1217', '141', '_yoast_wpseo_metadesc', ''),
('1218', '141', '_yoast_wpseo_meta-robots-noindex', '0'),
('1219', '141', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1220', '141', '_yoast_wpseo_meta-robots-adv', 'none'),
('1221', '141', '_yoast_wpseo_canonical', ''),
('1222', '141', '_yoast_wpseo_redirect', ''),
('1223', '143', '_yoast_wpseo_linkdex', '0'),
('1224', '143', '_edit_last', '3'),
('1225', '143', '_edit_lock', '1373245474:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1226', '144', '_wp_attached_file', '2013/05/2977-Whitney.jpg'),
('1227', '144', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2013/05/2977-Whitney.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"2977-Whitney-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"2977-Whitney-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1228', '143', '_thumbnail_id', '144'),
('1229', '143', '_wpas_done_all', '1'),
('1230', '143', 'image', ''),
('3777', '153', 'agent_name', 'Michael Gordon'),
('1233', '143', 'bathrooms', '0'),
('1234', '143', 'sale_type', 'rent'),
('1235', '143', 'sale_metric', 'Per Square Foot'),
('1236', '143', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1237', '143', 'woo_maps_enable', 'false'),
('1238', '143', 'woo_maps_zoom', '14'),
('1239', '143', 'woo_maps_type', 'G_NORMAL_MAP'),
('1240', '143', '_yoast_wpseo_focuskw', ''),
('1241', '143', '_yoast_wpseo_title', ''),
('1242', '143', '_yoast_wpseo_metadesc', ''),
('1243', '143', '_yoast_wpseo_meta-robots-noindex', '0'),
('1244', '143', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1245', '143', '_yoast_wpseo_meta-robots-adv', 'none'),
('1246', '143', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1247', '143', '_yoast_wpseo_redirect', ''),
('2414', '269', '_wp_attached_file', '2013/05/30-Hazel-St..jpg'),
('2801', '293', 'woo_maps_enable', 'on'),
('2800', '293', 'on_show', 'false'),
('2799', '293', 'sale_metric', 'Once off'),
('1255', '151', '_yoast_wpseo_linkdex', '0'),
('1256', '151', '_edit_last', '3'),
('1257', '151', '_edit_lock', '1373245586:3'),
('1258', '152', '_wp_attached_file', '2013/05/33-Rossotto-Dr1.jpg'),
('1259', '152', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:328;s:6:\"height\";i:246;s:4:\"file\";s:27:\"2013/05/33-Rossotto-Dr1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"33-Rossotto-Dr1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"33-Rossotto-Dr1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1311', '153', 'woo_maps_type', 'G_NORMAL_MAP'),
('1261', '151', '_wpas_done_all', '1'),
('1262', '151', 'image', ''),
('1263', '151', 'address', '33 ROSSOTTO DRIVE, HAMDEN'),
('1264', '151', 'price', '6.00 '),
('1265', '151', 'size', '20000'),
('1266', '151', 'garages', ''),
('1267', '151', 'beds', ''),
('1268', '151', 'bathrooms', '16'),
('1269', '151', 'sale_type', 'rent');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1270', '151', 'sale_metric', 'Per Square Foot'),
('1271', '151', 'on_show', 'false'),
('1272', '151', 'woo_maps_enable', 'on'),
('1273', '151', 'woo_maps_address', '33 Rossotto Drive, Hamden, CT 06514, USA'),
('1274', '151', 'woo_maps_long', '-72.918882'),
('1275', '151', 'woo_maps_lat', '41.394195'),
('1276', '151', 'woo_maps_zoom', '14'),
('1277', '151', 'woo_maps_type', 'G_NORMAL_MAP'),
('1278', '151', 'woo_maps_pov_pitch', '-20'),
('1279', '151', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1280', '151', '_yoast_wpseo_focuskw', ''),
('1281', '151', '_yoast_wpseo_title', ''),
('1282', '151', '_yoast_wpseo_metadesc', ''),
('1283', '151', '_yoast_wpseo_meta-robots-noindex', '0'),
('1284', '151', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1285', '151', '_yoast_wpseo_meta-robots-adv', 'none'),
('1286', '151', '_yoast_wpseo_canonical', ''),
('1287', '151', '_yoast_wpseo_redirect', ''),
('1312', '153', 'woo_maps_pov_pitch', '-20'),
('1313', '153', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1314', '153', '_yoast_wpseo_focuskw', ''),
('1315', '153', '_yoast_wpseo_title', ''),
('1316', '153', '_yoast_wpseo_metadesc', ''),
('1317', '153', '_yoast_wpseo_meta-robots-noindex', '0'),
('1318', '153', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1319', '153', '_yoast_wpseo_meta-robots-adv', 'none'),
('1320', '153', '_yoast_wpseo_canonical', ''),
('1321', '153', '_yoast_wpseo_redirect', ''),
('2790', '293', '_wpas_done_all', '1'),
('1323', '159', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1324', '159', '_edit_last', '3'),
('1325', '159', '_edit_lock', '1373204078:3'),
('1326', '159', '_wpas_done_all', '1'),
('1327', '159', 'image', ''),
('1328', '159', 'address', '223 WHALLEY AVENUE'),
('1329', '159', 'price', '2000'),
('1330', '159', 'size', '5000'),
('1331', '159', 'garages', '0'),
('1333', '159', 'bathrooms', '0'),
('1334', '159', 'sale_type', 'rent');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1335', '159', 'sale_metric', 'Per Month'),
('1336', '159', 'on_show', 'false'),
('1337', '159', 'woo_maps_enable', 'on'),
('1338', '159', 'woo_maps_address', '223 Whalley Avenue, New Haven, CT 06511, USA'),
('1339', '159', 'woo_maps_long', '-72.93987900000002'),
('1340', '159', 'woo_maps_lat', '41.3161119'),
('1341', '159', 'woo_maps_zoom', '14'),
('1342', '159', 'woo_maps_type', 'G_NORMAL_MAP'),
('1343', '159', 'woo_maps_pov_pitch', '-20'),
('1344', '159', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1345', '159', '_yoast_wpseo_focuskw', ''),
('1346', '159', '_yoast_wpseo_title', ''),
('1347', '159', '_yoast_wpseo_metadesc', ''),
('1348', '159', '_yoast_wpseo_meta-robots-noindex', '0'),
('1349', '159', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1350', '159', '_yoast_wpseo_meta-robots-adv', 'none'),
('1351', '159', '_yoast_wpseo_canonical', ''),
('1352', '159', '_yoast_wpseo_redirect', ''),
('1353', '161', '_wp_attached_file', '2013/05/223-Whalley-Ave-1.jpg'),
('1354', '161', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:360;s:4:\"file\";s:29:\"2013/05/223-Whalley-Ave-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"223-Whalley-Ave-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"223-Whalley-Ave-1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:3;s:6:\"credit\";s:10:\"Picasa 2.7\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1191249181;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:13:\"7.59999990463\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00482500065118\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1355', '159', '_thumbnail_id', '161'),
('1359', '164', '_wp_attached_file', '2013/05/1522-Whalley.jpg'),
('1360', '164', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:598;s:4:\"file\";s:24:\"2013/05/1522-Whalley.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"1522-Whalley-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"1522-Whalley-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:8:\"iPhone 4\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1361447720;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.85\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:17:\"0.000733137829912\";s:5:\"title\";s:0:\"\";}}'),
('3605', '159', 'agent_name', 'Michael Gordon'),
('4073', '293', '_wp_trash_meta_time', '1377882858'),
('4072', '293', '_wp_trash_meta_status', 'publish'),
('4071', '15', 'garages', '5'),
('1389', '166', '_yoast_wpseo_linkdex', '0'),
('1390', '166', '_edit_last', '3'),
('1391', '166', '_edit_lock', '1373207998:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1392', '167', '_wp_attached_file', '2013/05/1207-South-Broad.jpg'),
('1393', '167', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:360;s:4:\"file\";s:28:\"2013/05/1207-South-Broad.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"1207-South-Broad-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"1207-South-Broad-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1394', '166', '_thumbnail_id', '167'),
('1395', '166', '_wpas_done_all', '1'),
('1396', '166', 'image', ''),
('1397', '166', 'address', '1207 SOUTH BROAD STREET, WALLINGFORD'),
('1398', '166', 'price', '12.00 '),
('1399', '166', 'size', '5000'),
('1400', '166', 'garages', ''),
('1401', '166', 'beds', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1402', '166', 'bathrooms', '8'),
('1403', '166', 'sale_type', 'rent'),
('1404', '166', 'sale_metric', 'Per Square Foot'),
('1405', '166', 'on_show', 'false'),
('1406', '166', 'woo_maps_enable', 'on'),
('1407', '166', 'woo_maps_address', '1207 South Broad Street, Wallingford, CT 06492, USA'),
('1408', '166', 'woo_maps_long', '-72.8104113'),
('1409', '166', 'woo_maps_lat', '41.5049306'),
('1410', '166', 'woo_maps_zoom', '13'),
('1411', '166', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1412', '166', 'woo_maps_pov_pitch', '-20'),
('1413', '166', 'woo_maps_pov_yaw', '20'),
('1414', '166', '_yoast_wpseo_focuskw', ''),
('1415', '166', '_yoast_wpseo_title', ''),
('1416', '166', '_yoast_wpseo_metadesc', ''),
('1417', '166', '_yoast_wpseo_meta-robots-noindex', '0'),
('1418', '166', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1419', '166', '_yoast_wpseo_meta-robots-adv', 'none'),
('1420', '166', '_yoast_wpseo_canonical', ''),
('1421', '166', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1422', '169', '_yoast_wpseo_linkdex', '0'),
('1423', '169', '_edit_last', '3'),
('1424', '169', '_edit_lock', '1373247611:3'),
('1425', '169', '_wpas_done_all', '1'),
('1426', '169', 'image', ''),
('1427', '169', 'address', '2364 FOXON ROAD, NORTH BRANFORD'),
('1428', '169', 'price', '10.00'),
('1429', '169', 'size', '1200'),
('1430', '169', 'garages', ''),
('1431', '169', 'beds', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1432', '169', 'bathrooms', '8'),
('1433', '169', 'sale_type', 'rent'),
('1434', '169', 'sale_metric', 'Per Square Foot'),
('1435', '169', 'on_show', 'false'),
('1436', '169', 'woo_maps_enable', 'on'),
('1437', '169', 'woo_maps_address', '2364 Foxon Road, North Branford, CT 06471, USA'),
('1438', '169', 'woo_maps_long', '-72.74975560000001'),
('1439', '169', 'woo_maps_lat', '41.3413704'),
('1440', '169', 'woo_maps_zoom', '13'),
('1441', '169', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1442', '169', 'woo_maps_pov_pitch', '-20'),
('1443', '169', 'woo_maps_pov_yaw', '20'),
('1444', '169', '_yoast_wpseo_focuskw', ''),
('1445', '169', '_yoast_wpseo_title', ''),
('1446', '169', '_yoast_wpseo_metadesc', ''),
('1447', '169', '_yoast_wpseo_meta-robots-noindex', '0'),
('1448', '169', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1449', '169', '_yoast_wpseo_meta-robots-adv', 'none'),
('1450', '169', '_yoast_wpseo_canonical', ''),
('1451', '169', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1452', '171', '_wp_attached_file', '2013/05/2364-Foxon.jpg'),
('1453', '171', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:22:\"2013/05/2364-Foxon.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"2364-Foxon-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"2364-Foxon-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"2364-Foxon-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1326890483;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:5:\"0.002\";s:5:\"title\";s:0:\"\";}}'),
('1454', '169', '_thumbnail_id', '171'),
('2789', '293', '_edit_lock', '1373202067:3'),
('1485', '147', 'image', ''),
('1484', '147', '_wpas_done_all', '1'),
('1458', '173', '_yoast_wpseo_linkdex', '0'),
('1459', '173', '_edit_last', '3'),
('1460', '173', 'image', ''),
('3784', '173', 'agent_name', 'Marty Ruff');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1463', '173', 'bathrooms', '9'),
('1464', '173', 'sale_type', 'sale'),
('1465', '173', 'sale_metric', 'Once off'),
('1466', '173', 'on_show', 'false'),
('1467', '173', 'woo_maps_enable', 'on'),
('1468', '173', 'woo_maps_zoom', '9'),
('1469', '173', 'woo_maps_type', 'G_NORMAL_MAP'),
('1470', '173', '_yoast_wpseo_focuskw', ''),
('1471', '173', '_yoast_wpseo_title', ''),
('1472', '173', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1473', '173', '_yoast_wpseo_meta-robots-noindex', '0'),
('1474', '173', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1475', '173', '_yoast_wpseo_meta-robots-adv', 'none'),
('1476', '173', '_yoast_wpseo_canonical', ''),
('1477', '173', '_yoast_wpseo_redirect', ''),
('1478', '173', '_edit_lock', '1373247701:3'),
('1479', '174', '_wp_attached_file', '2013/05/1314-State-st.jpg'),
('1480', '174', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:3264;s:6:\"height\";i:2448;s:4:\"file\";s:25:\"2013/05/1314-State-st.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1314-State-st-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1314-State-st-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"1314-State-st-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:4;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:24:\"Canon PowerShot SX100 IS\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1323726749;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:3:\"8.2\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:5:\"0.001\";s:5:\"title\";s:0:\"\";}}'),
('1481', '173', '_wpas_done_all', '1'),
('1486', '147', 'address', '1314 STATE STREET, NEW HAVEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1487', '147', 'price', '185000'),
('1488', '147', 'size', '4972'),
('1489', '147', 'garages', '.12'),
('3619', '141', 'agent_name', 'OFFICE'),
('1492', '147', 'sale_type', 'sale'),
('1493', '147', 'sale_metric', 'Once off'),
('1494', '147', 'on_show', 'false'),
('1495', '147', 'woo_maps_enable', 'on'),
('1496', '147', 'woo_maps_address', '1314 State Street, New Haven, CT 06511, USA'),
('1497', '147', 'woo_maps_long', '-72.89925599999998');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1498', '147', 'woo_maps_lat', '41.319354'),
('1499', '147', 'woo_maps_zoom', '14'),
('1500', '147', 'woo_maps_type', 'G_NORMAL_MAP'),
('1501', '147', 'woo_maps_pov_pitch', '-20'),
('1502', '147', 'woo_maps_pov_yaw', '20'),
('1503', '147', '_yoast_wpseo_focuskw', ''),
('1504', '147', '_yoast_wpseo_title', ''),
('1505', '147', '_yoast_wpseo_metadesc', ''),
('1506', '147', '_yoast_wpseo_meta-robots-noindex', '0'),
('1507', '147', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1508', '147', '_yoast_wpseo_meta-robots-adv', 'none'),
('1509', '147', '_yoast_wpseo_canonical', ''),
('1510', '147', '_yoast_wpseo_redirect', ''),
('1511', '176', '_wp_attached_file', '2013/05/497-Main-St..jpg'),
('1512', '176', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2013/05/497-Main-St..jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"497-Main-St.-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"497-Main-St.-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('1513', '173', '_thumbnail_id', '176'),
('1514', '177', '_yoast_wpseo_linkdex', '0'),
('1515', '177', '_edit_last', '3'),
('1516', '177', '_edit_lock', '1373247771:3'),
('1517', '178', '_wp_attached_file', '2013/05/1684-Dixwell.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1518', '178', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:24:\"2013/05/1684-Dixwell.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"1684-Dixwell-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"1684-Dixwell-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('1519', '177', '_thumbnail_id', '178'),
('1520', '177', '_wpas_done_all', '1'),
('1521', '177', 'image', ''),
('1522', '177', 'address', '1684 DIXWELL AVENUE, HAMDEN'),
('1523', '177', 'price', '12.00 '),
('1524', '177', 'size', '4500'),
('1525', '177', 'garages', ''),
('1526', '177', 'beds', ''),
('1527', '177', 'bathrooms', '8-10');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1528', '177', 'sale_type', 'rent'),
('1529', '177', 'sale_metric', 'Per Square Foot'),
('1530', '177', 'on_show', 'false'),
('1531', '177', 'woo_maps_enable', 'on'),
('1532', '177', 'woo_maps_address', '1684 Dixwell Avenue, Hamden, CT 06514, USA'),
('1533', '177', 'woo_maps_long', '-72.92729299999996'),
('1534', '177', 'woo_maps_lat', '41.358221'),
('1535', '177', 'woo_maps_zoom', '13'),
('1536', '177', 'woo_maps_type', 'G_NORMAL_MAP'),
('1537', '177', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1538', '177', 'woo_maps_pov_yaw', '20'),
('1539', '177', '_yoast_wpseo_focuskw', ''),
('1540', '177', '_yoast_wpseo_title', ''),
('1541', '177', '_yoast_wpseo_metadesc', ''),
('1542', '177', '_yoast_wpseo_meta-robots-noindex', '0'),
('1543', '177', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1544', '177', '_yoast_wpseo_meta-robots-adv', 'none'),
('1545', '177', '_yoast_wpseo_canonical', ''),
('1546', '177', '_yoast_wpseo_redirect', ''),
('1547', '179', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1548', '179', '_edit_last', '3'),
('1549', '179', '_edit_lock', '1373249526:3'),
('1550', '180', '_wp_attached_file', '2013/05/57-Ozick-Front-001.jpg'),
('1551', '180', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:240;s:4:\"file\";s:30:\"2013/05/57-Ozick-Front-001.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"57-Ozick-Front-001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"57-Ozick-Front-001-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:3.20000000000000017763568394002504646778106689453125;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1324376894;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"7.84375\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:4:\"0.01\";s:5:\"title\";s:0:\"\";}}'),
('1552', '179', '_thumbnail_id', '180'),
('1553', '179', '_wpas_done_all', '1'),
('1554', '179', 'image', ''),
('1555', '179', 'address', '57 OZICK DRIVE, DURHAM'),
('1556', '179', 'garages', ''),
('1557', '179', 'beds', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1558', '179', 'bathrooms', ''),
('1559', '179', 'sale_type', 'rent'),
('1560', '179', 'sale_metric', 'Per Month'),
('1561', '179', 'on_show', 'false'),
('1562', '179', 'woo_maps_enable', 'on'),
('1563', '179', 'woo_maps_address', '57 Ozick Drive, Durham, CT 06422, USA'),
('1564', '179', 'woo_maps_long', '-72.7191828'),
('1565', '179', 'woo_maps_lat', '41.4683015'),
('1566', '179', 'woo_maps_zoom', '15'),
('1567', '179', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1568', '179', 'woo_maps_pov_pitch', '-20'),
('1569', '179', 'woo_maps_pov_yaw', '20'),
('1570', '179', '_yoast_wpseo_focuskw', ''),
('1571', '179', '_yoast_wpseo_title', ''),
('1572', '179', '_yoast_wpseo_metadesc', ''),
('1573', '179', '_yoast_wpseo_meta-robots-noindex', '0'),
('1574', '179', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1575', '179', '_yoast_wpseo_meta-robots-adv', 'none'),
('1576', '179', '_yoast_wpseo_canonical', ''),
('1577', '179', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1578', '181', '_yoast_wpseo_linkdex', '0'),
('1579', '181', '_edit_last', '3'),
('1580', '181', '_edit_lock', '1373249603:3'),
('1581', '181', '_wpas_done_all', '1'),
('1582', '181', 'image', ''),
('1583', '181', 'address', '493 WEST MAIN STREET, CHESHIRE'),
('1584', '181', 'price', '1050000'),
('1585', '181', 'size', '76350'),
('1586', '181', 'garages', '3'),
('1587', '181', 'beds', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1588', '181', 'bathrooms', ''),
('1589', '181', 'sale_type', 'sale'),
('1590', '181', 'sale_metric', 'Once off'),
('1591', '181', 'on_show', 'false'),
('1592', '181', 'woo_maps_enable', 'on'),
('1593', '181', 'woo_maps_address', '493 West Main Street, Cheshire, CT 06410, USA'),
('1594', '181', 'woo_maps_long', '-72.913432'),
('1595', '181', 'woo_maps_lat', '41.506774'),
('1596', '181', 'woo_maps_zoom', '12'),
('1597', '181', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1598', '181', 'woo_maps_pov_pitch', '-20'),
('1599', '181', 'woo_maps_pov_yaw', '20'),
('1600', '181', '_yoast_wpseo_focuskw', ''),
('1601', '181', '_yoast_wpseo_title', ''),
('1602', '181', '_yoast_wpseo_metadesc', ''),
('1603', '181', '_yoast_wpseo_meta-robots-noindex', '0'),
('1604', '181', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1605', '181', '_yoast_wpseo_meta-robots-adv', 'none'),
('1606', '181', '_yoast_wpseo_canonical', ''),
('1607', '181', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1608', '182', '_wp_attached_file', '2013/05/493-E.-Main-001.jpg'),
('1609', '182', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:240;s:4:\"file\";s:27:\"2013/05/493-E.-Main-001.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"493-E.-Main-001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"493-E.-Main-001-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:3.20000000000000017763568394002504646778106689453125;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1300803599;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"7.84375\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:4:\"0.01\";s:5:\"title\";s:0:\"\";}}'),
('1610', '181', '_thumbnail_id', '182'),
('1611', '185', '_yoast_wpseo_linkdex', '0'),
('1612', '185', '_edit_last', '3'),
('1613', '185', '_edit_lock', '1373249446:3'),
('1614', '186', '_wp_attached_file', '2013/05/2558-Whitney-Ave.jpg'),
('1615', '186', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2998;s:6:\"height\";i:1999;s:4:\"file\";s:28:\"2013/05/2558-Whitney-Ave.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"2558-Whitney-Ave-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"2558-Whitney-Ave-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"2558-Whitney-Ave-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:4.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:31:\"KODAK V1253 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1208852703;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:3:\"7.9\";s:3:\"iso\";s:2:\"64\";s:13:\"shutter_speed\";s:5:\"0.002\";s:5:\"title\";s:0:\"\";}}'),
('1616', '185', '_thumbnail_id', '186'),
('1617', '185', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1618', '185', 'image', ''),
('1619', '185', 'address', '2558 WHITNEY AVENUE, HAMDEN'),
('1620', '185', 'price', '16'),
('1621', '185', 'size', '1700'),
('1622', '185', 'garages', ''),
('1623', '185', 'beds', ''),
('1624', '185', 'bathrooms', '8'),
('1625', '185', 'sale_type', 'rent'),
('1626', '185', 'sale_metric', 'Per Square Foot'),
('1627', '185', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1628', '185', 'woo_maps_enable', 'on'),
('1629', '185', 'woo_maps_address', '2558 Whitney Avenue, Hamden, CT 06518, USA'),
('1630', '185', 'woo_maps_long', '-72.89935559999998'),
('1631', '185', 'woo_maps_lat', '41.3897108'),
('1632', '185', 'woo_maps_zoom', '12'),
('1633', '185', 'woo_maps_type', 'G_NORMAL_MAP'),
('1634', '185', 'woo_maps_pov_pitch', '-20'),
('1635', '185', 'woo_maps_pov_yaw', '20'),
('1636', '185', '_yoast_wpseo_focuskw', ''),
('1637', '185', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1638', '185', '_yoast_wpseo_metadesc', ''),
('1639', '185', '_yoast_wpseo_meta-robots-noindex', '0'),
('1640', '185', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1641', '185', '_yoast_wpseo_meta-robots-adv', 'none'),
('1642', '185', '_yoast_wpseo_canonical', ''),
('1643', '185', '_yoast_wpseo_redirect', ''),
('1644', '187', '_yoast_wpseo_linkdex', '0'),
('1645', '187', '_edit_last', '3'),
('1646', '187', '_edit_lock', '1373249888:3'),
('1647', '188', '_wp_attached_file', '2013/05/163-boston-post-rd-2.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1648', '188', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:32:\"2013/05/163-boston-post-rd-2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"163-boston-post-rd-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"163-boston-post-rd-2-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:5.61000000000000031974423109204508364200592041015625;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:17:\"hp PhotoSmart 620\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1009843338;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"5.59\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:8:\"0.001743\";s:5:\"title\";s:0:\"\";}}'),
('1649', '187', '_thumbnail_id', '188'),
('1650', '187', '_wpas_done_all', '1'),
('1651', '187', 'image', ''),
('1652', '187', 'address', '163 BOSTON POST ROAD, WEST HAVEN'),
('1653', '187', 'price', '2.00'),
('1656', '187', 'bathrooms', '18'),
('1657', '187', 'sale_type', 'rent'),
('1658', '187', 'sale_metric', 'Per Square Foot'),
('1659', '187', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1660', '187', 'woo_maps_enable', 'on'),
('1661', '187', 'woo_maps_address', '163 Boston Post Road, West Haven, CT 06516, USA'),
('1662', '187', 'woo_maps_long', '-72.95718590000001'),
('1663', '187', 'woo_maps_lat', '41.296952'),
('1664', '187', 'woo_maps_zoom', '15'),
('1665', '187', 'woo_maps_type', 'G_NORMAL_MAP'),
('1666', '187', 'woo_maps_pov_pitch', '-20'),
('1667', '187', 'woo_maps_pov_yaw', '20'),
('1668', '187', '_yoast_wpseo_focuskw', ''),
('1669', '187', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1670', '187', '_yoast_wpseo_metadesc', ''),
('1671', '187', '_yoast_wpseo_meta-robots-noindex', '0'),
('1672', '187', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1673', '187', '_yoast_wpseo_meta-robots-adv', 'none'),
('1674', '187', '_yoast_wpseo_canonical', ''),
('1675', '187', '_yoast_wpseo_redirect', ''),
('1676', '189', '_yoast_wpseo_linkdex', '0'),
('1677', '189', '_edit_last', '3'),
('1678', '189', '_edit_lock', '1373206088:3'),
('1679', '189', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/land-for-sale-300x234.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1680', '189', 'address', '410 ELLA GRASSO BLVD. NEW HAVEN'),
('1681', '189', 'price', 'CALL LISTING AGENT'),
('1682', '189', 'garages', 'Up to 16 '),
('1685', '189', 'sale_type', 'sale'),
('1686', '189', 'sale_metric', 'Once off'),
('1687', '189', 'on_show', 'false'),
('1688', '189', 'woo_maps_enable', 'on'),
('1689', '189', 'woo_maps_zoom', '14'),
('1690', '189', 'woo_maps_type', 'G_NORMAL_MAP'),
('1691', '189', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1692', '189', '_yoast_wpseo_title', ''),
('1693', '189', '_yoast_wpseo_metadesc', ''),
('1694', '189', '_yoast_wpseo_meta-robots-noindex', '0'),
('1695', '189', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1696', '189', '_yoast_wpseo_meta-robots-adv', 'none'),
('1697', '189', '_yoast_wpseo_canonical', ''),
('1698', '189', '_yoast_wpseo_redirect', ''),
('1699', '189', '_wpas_done_all', '1'),
('1700', '189', 'woo_maps_address', '410 Ella T Grasso Boulevard, New Haven, CT 06519, USA'),
('1701', '189', 'woo_maps_long', '-72.94216670000003');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1702', '189', 'woo_maps_lat', '41.2916011'),
('1703', '189', 'woo_maps_pov_pitch', '-20'),
('1704', '189', 'woo_maps_pov_yaw', '20'),
('2788', '293', '_edit_last', '3'),
('1709', '194', '_wp_attached_file', '2013/05/155-Truman-St.jpg'),
('1710', '194', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:25:\"2013/05/155-Truman-St.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"155-Truman-St-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"155-Truman-St-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"155-Truman-St-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1310044803;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:5:\"0.001\";s:5:\"title\";s:0:\"\";}}'),
('1711', '193', '_thumbnail_id', '194'),
('1712', '193', 'image', ''),
('1713', '193', 'address', '155 TRUMAN STREET, NEW HAVEN'),
('1714', '193', 'price', '2000');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3617', '147', 'agent_name', 'Noah Meyer'),
('1717', '193', 'bathrooms', '14'),
('1718', '193', 'sale_type', 'rent'),
('1719', '193', 'sale_metric', 'Per Month'),
('1720', '193', 'on_show', 'false'),
('1721', '193', 'woo_maps_enable', 'on'),
('1722', '193', 'woo_maps_zoom', '14'),
('1723', '193', 'woo_maps_type', 'G_NORMAL_MAP'),
('1724', '193', '_yoast_wpseo_focuskw', ''),
('1725', '193', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1726', '193', '_yoast_wpseo_metadesc', ''),
('1727', '193', '_yoast_wpseo_meta-robots-noindex', '0'),
('1728', '193', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1729', '193', '_yoast_wpseo_meta-robots-adv', 'none'),
('1730', '193', '_yoast_wpseo_canonical', ''),
('1731', '193', '_yoast_wpseo_redirect', ''),
('1732', '193', '_wpas_done_all', '1'),
('1733', '193', 'size', '5000'),
('1734', '193', 'woo_maps_address', '155 Truman Street, New Haven, CT 06519, USA'),
('1735', '193', 'woo_maps_long', '-72.94256769999998');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1736', '193', 'woo_maps_lat', '41.2920693'),
('1737', '193', 'woo_maps_pov_pitch', '-20'),
('1738', '193', 'woo_maps_pov_yaw', '20'),
('1739', '195', '_yoast_wpseo_linkdex', '0'),
('1740', '195', '_edit_last', '3'),
('1741', '195', '_edit_lock', '1373250004:3'),
('1742', '196', '_wp_attached_file', '2013/05/42-Fairview-Avenue.jpg'),
('1743', '196', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:400;s:4:\"file\";s:30:\"2013/05/42-Fairview-Avenue.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"42-Fairview-Avenue-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"42-Fairview-Avenue-300x234.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:234;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1744', '195', '_thumbnail_id', '196'),
('1745', '195', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1746', '195', 'image', ''),
('1747', '195', 'address', '42 FAIRVIEW AVENUE, HAMDEN'),
('1748', '195', 'price', '219000'),
('1749', '195', 'garages', ''),
('1750', '195', 'beds', ''),
('1751', '195', 'bathrooms', ''),
('1752', '195', 'sale_type', 'sale'),
('1753', '195', 'sale_metric', 'Once off'),
('1754', '195', 'on_show', 'false'),
('1755', '195', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1756', '195', 'woo_maps_address', '42 Fairview Avenue, Hamden, CT 06514, USA'),
('1757', '195', 'woo_maps_long', '-72.94165599999997'),
('1758', '195', 'woo_maps_lat', '41.33941'),
('1759', '195', 'woo_maps_zoom', '14'),
('1760', '195', 'woo_maps_type', 'G_NORMAL_MAP'),
('1761', '195', 'woo_maps_pov_pitch', '-20'),
('1762', '195', 'woo_maps_pov_yaw', '20'),
('1763', '195', '_yoast_wpseo_focuskw', ''),
('1764', '195', '_yoast_wpseo_title', ''),
('1765', '195', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1766', '195', '_yoast_wpseo_meta-robots-noindex', '0'),
('1767', '195', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1768', '195', '_yoast_wpseo_meta-robots-adv', 'none'),
('1769', '195', '_yoast_wpseo_canonical', ''),
('1770', '195', '_yoast_wpseo_redirect', ''),
('1771', '197', '_yoast_wpseo_linkdex', '0'),
('1772', '197', '_edit_last', '3'),
('1773', '197', '_edit_lock', '1373250050:3'),
('1774', '198', '_wp_attached_file', '2013/05/35-Meridian.jpg'),
('1775', '198', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:23:\"2013/05/35-Meridian.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"35-Meridian-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"35-Meridian-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1776', '197', '_thumbnail_id', '198'),
('1777', '197', 'image', ''),
('1778', '197', 'address', '35 MERIDIAN STREET, MERIDEN'),
('1779', '197', 'garages', ''),
('1780', '197', 'beds', ''),
('1781', '197', 'bathrooms', ''),
('1782', '197', 'sale_type', 'sale'),
('1783', '197', 'sale_metric', 'Once off'),
('1784', '197', 'on_show', 'false'),
('1785', '197', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1786', '197', 'woo_maps_zoom', '14'),
('1787', '197', 'woo_maps_type', 'G_NORMAL_MAP'),
('1788', '197', '_yoast_wpseo_focuskw', ''),
('1789', '197', '_yoast_wpseo_title', ''),
('1790', '197', '_yoast_wpseo_metadesc', ''),
('1791', '197', '_yoast_wpseo_meta-robots-noindex', '0'),
('1792', '197', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1793', '197', '_yoast_wpseo_meta-robots-adv', 'none'),
('1794', '197', '_yoast_wpseo_canonical', ''),
('1795', '197', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1796', '197', '_wpas_done_all', '1'),
('1797', '197', 'price', '159900'),
('1798', '197', 'size', '8602'),
('1799', '197', 'woo_maps_address', '35 Meridian Street, Meriden, CT 06451, USA'),
('1800', '197', 'woo_maps_long', '-72.80745300000001'),
('1801', '197', 'woo_maps_lat', '41.532334'),
('1802', '197', 'woo_maps_pov_pitch', '-20'),
('1803', '197', 'woo_maps_pov_yaw', '20'),
('2787', '293', '_yoast_wpseo_linkdex', '0'),
('1805', '201', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1806', '201', '_edit_last', '3'),
('1807', '201', '_edit_lock', '1377787204:3'),
('1808', '203', '_wp_attached_file', '2013/05/lmmre-new-haven-ct-property.jpg'),
('1809', '203', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:80;s:4:\"file\";s:39:\"2013/05/lmmre-new-haven-ct-property.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"lmmre-new-haven-ct-property-150x80.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:80;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1810', '201', '_thumbnail_id', '50'),
('1811', '201', '_wpas_done_all', '1'),
('1812', '201', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/land-for-sale-300x234.jpg'),
('1813', '201', 'address', '64 ROCKY TOP ROAD, HAMDEN'),
('1814', '201', 'price', '2350000.00'),
('1815', '201', 'garages', '18.2');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3803', '205', 'agent_name', 'Marty Ruff'),
('1818', '201', 'sale_type', 'sale'),
('1819', '201', 'sale_metric', 'Once off'),
('1820', '201', 'on_show', 'false'),
('1821', '201', 'woo_maps_enable', 'on'),
('1822', '201', 'woo_maps_address', '64 Rocky Top Road, Hamden, CT 06514, USA'),
('1823', '201', 'woo_maps_long', '-72.9193219'),
('1824', '201', 'woo_maps_lat', '41.40875399999999'),
('1825', '201', 'woo_maps_zoom', '13'),
('1826', '201', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1827', '201', 'woo_maps_pov_pitch', '-20'),
('1828', '201', 'woo_maps_pov_yaw', '20'),
('1829', '201', '_yoast_wpseo_focuskw', ''),
('1830', '201', '_yoast_wpseo_title', ''),
('1831', '201', '_yoast_wpseo_metadesc', ''),
('1832', '201', '_yoast_wpseo_meta-robots-noindex', '0'),
('1833', '201', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1834', '201', '_yoast_wpseo_meta-robots-adv', 'none'),
('1835', '201', '_yoast_wpseo_canonical', ''),
('1836', '201', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1855', '204', 'woo_maps_long', '-72.95279699999998'),
('1856', '204', 'woo_maps_lat', '41.307806'),
('1857', '204', 'woo_maps_zoom', '15'),
('1858', '204', 'woo_maps_type', 'G_NORMAL_MAP'),
('1859', '204', 'woo_maps_pov_pitch', '-20'),
('1860', '204', 'woo_maps_pov_yaw', '20'),
('1861', '204', '_yoast_wpseo_focuskw', ''),
('1862', '204', '_yoast_wpseo_title', ''),
('1863', '204', '_yoast_wpseo_metadesc', ''),
('1864', '204', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1865', '204', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1866', '204', '_yoast_wpseo_meta-robots-adv', 'none'),
('1867', '204', '_yoast_wpseo_canonical', ''),
('1868', '204', '_yoast_wpseo_redirect', ''),
('1869', '205', '_yoast_wpseo_linkdex', '0'),
('1870', '205', '_edit_last', '3'),
('1871', '205', '_edit_lock', '1373250418:3'),
('1872', '206', '_wp_attached_file', '2013/05/30-Washington-Ave.-jpg.jpg'),
('1873', '206', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:360;s:4:\"file\";s:34:\"2013/05/30-Washington-Ave.-jpg.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"30-Washington-Ave.-jpg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"30-Washington-Ave.-jpg-300x168.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1874', '205', '_thumbnail_id', '206');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1875', '205', '_wpas_done_all', '1'),
('1876', '205', 'image', ''),
('1877', '205', 'address', '30 WASHINGTON AVENUE, NORTH HAVEN'),
('1934', '205', 'price', '15'),
('1879', '205', 'size', '1250'),
('1880', '205', 'garages', ''),
('1881', '205', 'beds', ''),
('1882', '205', 'bathrooms', ''),
('1883', '205', 'sale_type', 'rent'),
('1884', '205', 'sale_metric', 'Per Square Foot');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1885', '205', 'on_show', 'false'),
('1886', '205', 'woo_maps_enable', 'on'),
('1887', '205', 'woo_maps_address', '30 Washington Avenue, North Haven, CT 06473, USA'),
('1888', '205', 'woo_maps_long', '-72.859691'),
('1889', '205', 'woo_maps_lat', '41.3907326'),
('1890', '205', 'woo_maps_zoom', '14'),
('1891', '205', 'woo_maps_type', 'G_NORMAL_MAP'),
('1892', '205', 'woo_maps_pov_pitch', '-16.089148216588825'),
('1893', '205', 'woo_maps_pov_yaw', '31.99581007786764'),
('1894', '205', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1895', '205', '_yoast_wpseo_title', ''),
('1896', '205', '_yoast_wpseo_metadesc', ''),
('1897', '205', '_yoast_wpseo_meta-robots-noindex', '0'),
('1898', '205', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1899', '205', '_yoast_wpseo_meta-robots-adv', 'none'),
('1900', '205', '_yoast_wpseo_canonical', ''),
('1901', '205', '_yoast_wpseo_redirect', ''),
('1902', '217', '_yoast_wpseo_linkdex', '0'),
('1903', '217', '_edit_last', '3'),
('1904', '217', '_edit_lock', '1373210614:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1905', '218', '_menu_item_type', 'custom'),
('1906', '218', '_menu_item_menu_item_parent', '0'),
('1907', '218', '_menu_item_object_id', '218'),
('1908', '218', '_menu_item_object', 'custom'),
('1909', '218', '_menu_item_target', ''),
('1910', '218', '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
('1911', '218', '_menu_item_xfn', ''),
('1912', '218', '_menu_item_url', 'http://www.lmmre.com'),
('1914', '218', '_wpas_done_all', '1'),
('1915', '217', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1916', '217', 'image', ''),
('1917', '217', 'garages', '1.7'),
('1918', '217', 'beds', ''),
('1919', '217', 'bathrooms', ''),
('1920', '217', 'sale_type', 'rent'),
('1921', '217', 'sale_metric', 'Per Square Foot'),
('1922', '217', 'on_show', 'false'),
('1923', '217', 'woo_maps_enable', 'on'),
('1924', '217', 'woo_maps_zoom', '13'),
('1925', '217', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1926', '217', '_yoast_wpseo_focuskw', ''),
('1927', '217', '_yoast_wpseo_title', ''),
('1928', '217', '_yoast_wpseo_metadesc', ''),
('1929', '217', '_yoast_wpseo_meta-robots-noindex', '0'),
('1930', '217', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1931', '217', '_yoast_wpseo_meta-robots-adv', 'none'),
('1932', '217', '_yoast_wpseo_canonical', ''),
('1933', '217', '_yoast_wpseo_redirect', ''),
('1935', '217', 'address', '540 EAST MAIN STREET, ANSONIA'),
('1936', '217', 'price', '3.00');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1937', '217', 'size', '77896'),
('1938', '217', 'woo_maps_address', '540 East Main Street, Ansonia, CT 06401, USA'),
('1939', '217', 'woo_maps_long', '-73.07840299999998'),
('1940', '217', 'woo_maps_lat', '41.34605200000001'),
('1941', '217', 'woo_maps_pov_pitch', '-20'),
('1942', '217', 'woo_maps_pov_yaw', '20'),
('1943', '221', '_wp_attached_file', '2013/05/540-east-main-st-Ansonia.jpg'),
('1944', '221', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1945;s:6:\"height\";i:1526;s:4:\"file\";s:36:\"2013/05/540-east-main-st-Ansonia.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"540-east-main-st-Ansonia-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"540-east-main-st-Ansonia-300x235.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:235;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"540-east-main-st-Ansonia-1024x803.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:803;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:7:\"ADR6300\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1329729844;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"4.92\";s:3:\"iso\";s:3:\"109\";s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1945', '217', '_thumbnail_id', '221'),
('1946', '222', '_wp_attached_file', '2013/05/1079.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1947', '222', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:328;s:6:\"height\";i:246;s:4:\"file\";s:16:\"2013/05/1079.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"1079-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"1079-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2084', '239', '_wp_attached_file', '2013/05/12-Foch.jpg'),
('2085', '239', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:390;s:6:\"height\";i:293;s:4:\"file\";s:19:\"2013/05/12-Foch.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"12-Foch-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"12-Foch-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('1958', '226', '_wpas_done_all', '1'),
('3633', '189', '_thumbnail_id', '50'),
('1952', '226', '_yoast_wpseo_linkdex', '0'),
('1953', '226', '_edit_last', '3'),
('1954', '226', '_edit_lock', '1373291620:3'),
('1955', '228', '_wp_attached_file', '2013/05/41-Middletown-001.jpg'),
('1956', '228', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:29:\"2013/05/41-Middletown-001.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"41-Middletown-001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"41-Middletown-001-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"41-Middletown-001-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1316948021;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:6:\"0.0025\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1957', '226', '_thumbnail_id', '228'),
('1959', '226', 'image', ''),
('1960', '226', 'address', '41 MIDDLETOWN AVENUE, NORTH HAVEN'),
('1961', '226', 'price', '925000'),
('1962', '226', 'size', '9600'),
('3870', '779', 'agent_name', 'Bernie Diana'),
('1965', '226', 'bathrooms', '9'),
('1966', '226', 'sale_type', 'sale'),
('1967', '226', 'sale_metric', 'Once off'),
('1968', '226', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1969', '226', 'woo_maps_enable', 'on'),
('1970', '226', 'woo_maps_address', '41 Middletown Avenue, North Haven, CT 06473, USA'),
('1971', '226', 'woo_maps_long', '-72.86683879999998'),
('1972', '226', 'woo_maps_lat', '41.3373781'),
('1973', '226', 'woo_maps_zoom', '13'),
('1974', '226', 'woo_maps_type', 'G_NORMAL_MAP'),
('1975', '226', 'woo_maps_pov_pitch', '-20'),
('1976', '226', 'woo_maps_pov_yaw', '20'),
('1977', '226', '_yoast_wpseo_focuskw', ''),
('1978', '226', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1979', '226', '_yoast_wpseo_metadesc', ''),
('1980', '226', '_yoast_wpseo_meta-robots-noindex', '0'),
('1981', '226', '_yoast_wpseo_meta-robots-nofollow', '0'),
('1982', '226', '_yoast_wpseo_meta-robots-adv', 'none'),
('1983', '226', '_yoast_wpseo_canonical', ''),
('1984', '226', '_yoast_wpseo_redirect', ''),
('1985', '229', '_yoast_wpseo_linkdex', '0'),
('1986', '229', '_edit_last', '3'),
('1987', '229', '_edit_lock', '1373250611:3'),
('2081', '238', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2072', '235', '_thumbnail_id', '50'),
('2073', '235', 'address', '135 SANFORD STREET, HAMDEN'),
('2074', '235', 'price', '850000'),
('2075', '235', 'woo_maps_address', '135 Sanford Street, Hamden, CT 06514, USA'),
('2076', '235', 'woo_maps_long', '-72.91759009999998'),
('2077', '235', 'woo_maps_lat', '41.3825807'),
('2078', '235', 'woo_maps_pov_pitch', '-20'),
('2079', '235', 'woo_maps_pov_yaw', '20'),
('1991', '231', '_wp_attached_file', '2013/05/LMM-Logo-1.jpg'),
('1992', '231', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1347;s:6:\"height\";i:418;s:4:\"file\";s:22:\"2013/05/LMM-Logo-1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"LMM-Logo-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"LMM-Logo-1-300x93.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:93;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"LMM-Logo-1-1024x317.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:317;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:9:\"HP oj6300\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('1993', '229', '_wpas_done_all', '1'),
('1994', '229', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/images.jpg'),
('2012', '229', 'address', '19 UNIVERSAL DRIVE, NORTH HAVEN'),
('1995', '229', 'garages', '1'),
('1996', '229', 'beds', ''),
('1997', '229', 'bathrooms', ''),
('1998', '229', 'sale_type', 'rent'),
('3502', '229', 'sale_metric', 'Per Month'),
('2000', '229', 'on_show', 'false'),
('2001', '229', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2002', '229', 'woo_maps_zoom', '13'),
('2003', '229', 'woo_maps_type', 'G_NORMAL_MAP'),
('2004', '229', '_yoast_wpseo_focuskw', ''),
('2005', '229', '_yoast_wpseo_title', ''),
('2006', '229', '_yoast_wpseo_metadesc', ''),
('2007', '229', '_yoast_wpseo_meta-robots-noindex', '0'),
('2008', '229', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2009', '229', '_yoast_wpseo_meta-robots-adv', 'none'),
('2010', '229', '_yoast_wpseo_canonical', ''),
('2011', '229', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2013', '229', 'price', '6250'),
('2014', '229', 'woo_maps_address', '19 Universal Drive, North Haven, CT 06473, USA'),
('2015', '229', 'woo_maps_long', '-72.86950769999999'),
('2016', '229', 'woo_maps_lat', '41.3500357'),
('2017', '229', 'woo_maps_pov_pitch', '-20'),
('2018', '229', 'woo_maps_pov_yaw', '20'),
('2020', '233', '_yoast_wpseo_linkdex', '0'),
('2021', '233', '_edit_last', '3'),
('2022', '233', '_edit_lock', '1373291716:3'),
('2024', '233', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2025', '233', 'image', ''),
('2026', '233', 'address', '82 MIDDLETOWN AVENUE, NORTH HAVEN'),
('2027', '233', 'price', '4166.67'),
('2028', '233', 'garages', ''),
('2029', '233', 'beds', ''),
('2030', '233', 'bathrooms', ''),
('2031', '233', 'sale_type', 'rent'),
('2032', '233', 'sale_metric', 'Per Month'),
('2033', '233', 'on_show', 'false'),
('2034', '233', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2035', '233', 'woo_maps_address', '82 Middletown Avenue, North Haven, CT 06473, USA'),
('2036', '233', 'woo_maps_long', '-72.86460749999998'),
('2037', '233', 'woo_maps_lat', '41.339304'),
('2038', '233', 'woo_maps_zoom', '15'),
('2039', '233', 'woo_maps_type', 'G_NORMAL_MAP'),
('2040', '233', 'woo_maps_pov_pitch', '-20'),
('2041', '233', 'woo_maps_pov_yaw', '20'),
('2042', '233', '_yoast_wpseo_focuskw', ''),
('2043', '233', '_yoast_wpseo_title', ''),
('2044', '233', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2045', '233', '_yoast_wpseo_meta-robots-noindex', '0'),
('2046', '233', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2047', '233', '_yoast_wpseo_meta-robots-adv', 'none'),
('2048', '233', '_yoast_wpseo_canonical', ''),
('2049', '233', '_yoast_wpseo_redirect', ''),
('2050', '235', '_yoast_wpseo_linkdex', '0'),
('2051', '235', '_edit_last', '3'),
('2052', '235', '_edit_lock', '1373250819:3'),
('2053', '235', '_wpas_done_all', '1'),
('2054', '235', 'image', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2055', '235', 'garages', '2.76'),
('2056', '235', 'beds', ''),
('2057', '235', 'bathrooms', ''),
('2058', '235', 'sale_type', 'sale'),
('2059', '235', 'sale_metric', 'Once off'),
('2060', '235', 'on_show', 'false'),
('2061', '235', 'woo_maps_enable', 'on'),
('2062', '235', 'woo_maps_zoom', '16'),
('2063', '235', 'woo_maps_type', 'G_NORMAL_MAP'),
('2064', '235', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2065', '235', '_yoast_wpseo_title', ''),
('2066', '235', '_yoast_wpseo_metadesc', ''),
('2067', '235', '_yoast_wpseo_meta-robots-noindex', '0'),
('2068', '235', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2069', '235', '_yoast_wpseo_meta-robots-adv', 'none'),
('2070', '235', '_yoast_wpseo_canonical', ''),
('2071', '235', '_yoast_wpseo_redirect', ''),
('2082', '238', '_edit_last', '3'),
('2083', '238', '_edit_lock', '1373250886:3'),
('2086', '238', '_thumbnail_id', '239');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2087', '238', '_wpas_done_all', '1'),
('2088', '238', 'image', ''),
('2089', '238', 'address', '12 FOCH STREET, HAMDEN'),
('2090', '238', 'price', '20'),
('2091', '238', 'size', '2052'),
('2092', '238', 'garages', ''),
('2093', '238', 'beds', ''),
('2094', '238', 'bathrooms', '8'),
('2095', '238', 'sale_type', 'rent'),
('2096', '238', 'sale_metric', 'Per Square Foot');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2097', '238', 'on_show', 'false'),
('2098', '238', 'woo_maps_enable', 'on'),
('2099', '238', 'woo_maps_address', '12 Foch Street, Hamden, CT 06514, USA'),
('2100', '238', 'woo_maps_long', '-72.92726390000001'),
('2101', '238', 'woo_maps_lat', '41.3561539'),
('2102', '238', 'woo_maps_zoom', '13'),
('2103', '238', 'woo_maps_type', 'G_NORMAL_MAP'),
('2104', '238', 'woo_maps_pov_pitch', '-20'),
('2105', '238', 'woo_maps_pov_yaw', '20'),
('2106', '238', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2107', '238', '_yoast_wpseo_title', ''),
('2108', '238', '_yoast_wpseo_metadesc', ''),
('2109', '238', '_yoast_wpseo_meta-robots-noindex', '0'),
('2110', '238', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2111', '238', '_yoast_wpseo_meta-robots-adv', 'none'),
('2112', '238', '_yoast_wpseo_canonical', ''),
('2113', '238', '_yoast_wpseo_redirect', ''),
('2114', '240', '_yoast_wpseo_linkdex', '0'),
('2115', '240', '_edit_last', '3'),
('2116', '240', '_edit_lock', '1373250930:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2117', '241', '_wp_attached_file', '2013/05/116-Anthony.jpg'),
('2118', '241', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:23:\"2013/05/116-Anthony.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"116-Anthony-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"116-Anthony-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2119', '240', '_thumbnail_id', '241'),
('2120', '240', '_wpas_done_all', '1'),
('2121', '240', 'image', ''),
('2122', '240', 'address', '116 ANTHONY STREET, NEW HAVEN'),
('2123', '240', 'price', '1000'),
('2124', '240', 'size', '716'),
('2125', '240', 'garages', ''),
('2127', '240', 'bathrooms', '8');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2128', '240', 'sale_type', 'rent'),
('2129', '240', 'sale_metric', 'Per Month'),
('2130', '240', 'on_show', 'false'),
('2131', '240', 'woo_maps_enable', 'on'),
('2132', '240', 'woo_maps_address', '116 Anthony Street, New Haven, CT 06515, USA'),
('2133', '240', 'woo_maps_long', '-72.97449'),
('2134', '240', 'woo_maps_lat', '41.331814'),
('2135', '240', 'woo_maps_zoom', '14'),
('2136', '240', 'woo_maps_type', 'G_NORMAL_MAP'),
('2137', '240', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2138', '240', 'woo_maps_pov_yaw', '20'),
('2139', '240', '_yoast_wpseo_focuskw', ''),
('2140', '240', '_yoast_wpseo_title', ''),
('2141', '240', '_yoast_wpseo_metadesc', ''),
('2142', '240', '_yoast_wpseo_meta-robots-noindex', '0'),
('2143', '240', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2144', '240', '_yoast_wpseo_meta-robots-adv', 'none'),
('2145', '240', '_yoast_wpseo_canonical', ''),
('2146', '240', '_yoast_wpseo_redirect', ''),
('2147', '243', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2148', '243', '_edit_last', '3'),
('2149', '243', '_edit_lock', '1373210474:3'),
('2150', '243', '_wpas_done_all', '1'),
('2151', '243', 'image', ''),
('2152', '243', 'garages', ''),
('2153', '243', 'beds', ''),
('2154', '243', 'bathrooms', ''),
('2155', '243', 'sale_type', 'rent'),
('2156', '243', 'sale_metric', 'Per Square Foot'),
('2157', '243', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2158', '243', 'woo_maps_enable', 'on'),
('2159', '243', 'woo_maps_zoom', '13'),
('2160', '243', 'woo_maps_type', 'G_NORMAL_MAP'),
('2161', '243', '_yoast_wpseo_focuskw', ''),
('2162', '243', '_yoast_wpseo_title', ''),
('2163', '243', '_yoast_wpseo_metadesc', ''),
('2164', '243', '_yoast_wpseo_meta-robots-noindex', '0'),
('2165', '243', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2166', '243', '_yoast_wpseo_meta-robots-adv', 'none'),
('2167', '243', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2168', '243', '_yoast_wpseo_redirect', ''),
('2170', '246', '_wp_attached_file', '2013/05/1635R-Dixwell.jpg'),
('2171', '246', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:390;s:6:\"height\";i:293;s:4:\"file\";s:25:\"2013/05/1635R-Dixwell.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1635R-Dixwell-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1635R-Dixwell-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2172', '243', '_thumbnail_id', '246'),
('2173', '243', 'address', '1635 R DIXWELL AVENUE, HAMDEN'),
('2174', '243', 'price', '10'),
('2175', '243', 'woo_maps_address', '1635 Dixwell Avenue, Hamden, CT 06514, USA'),
('2176', '243', 'woo_maps_long', '-72.92732380000001'),
('2177', '243', 'woo_maps_lat', '41.3569463'),
('2178', '243', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2179', '243', 'woo_maps_pov_yaw', '20'),
('2180', '247', '_yoast_wpseo_linkdex', '0'),
('2181', '247', '_edit_last', '3'),
('2182', '247', '_edit_lock', '1373203776:3'),
('2183', '248', '_wp_attached_file', '2013/05/275-Poplar.jpg'),
('2184', '248', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:22:\"2013/05/275-Poplar.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"275-Poplar-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"275-Poplar-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2185', '247', '_thumbnail_id', '248'),
('2186', '247', '_wpas_done_all', '1'),
('2187', '247', 'image', ''),
('2188', '247', 'address', '275 POPLAR STREET, NEW HAVEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2189', '247', 'price', '400000'),
('2190', '247', 'size', '6988'),
('2191', '247', 'garages', ''),
('2192', '247', 'beds', ''),
('2193', '247', 'bathrooms', '9'),
('2194', '247', 'sale_type', 'sale'),
('2195', '247', 'sale_metric', 'Once off'),
('2196', '247', 'on_show', 'false'),
('2197', '247', 'woo_maps_enable', 'on'),
('2198', '247', 'woo_maps_address', '275 Poplar Street, New Haven, CT 06513, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2199', '247', 'woo_maps_long', '-72.896838'),
('2200', '247', 'woo_maps_lat', '41.309302'),
('2201', '247', 'woo_maps_zoom', '13'),
('2202', '247', 'woo_maps_type', 'G_NORMAL_MAP'),
('2203', '247', 'woo_maps_pov_pitch', '-20'),
('2204', '247', 'woo_maps_pov_yaw', '20'),
('2205', '247', '_yoast_wpseo_focuskw', ''),
('2206', '247', '_yoast_wpseo_title', ''),
('2207', '247', '_yoast_wpseo_metadesc', ''),
('2208', '247', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2209', '247', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2210', '247', '_yoast_wpseo_meta-robots-adv', 'none'),
('2211', '247', '_yoast_wpseo_canonical', ''),
('2212', '247', '_yoast_wpseo_redirect', ''),
('2213', '249', '_yoast_wpseo_linkdex', '0'),
('2214', '249', '_edit_last', '3'),
('2215', '249', '_edit_lock', '1373210405:3'),
('2216', '249', 'image', ''),
('2217', '249', 'address', '0 HUNTER\'S MOUNTAIN ROAD, NAUGATUCK'),
('2218', '249', 'garages', '80');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3714', '243', 'agent_name', 'Steve Miller'),
('2221', '249', 'sale_type', 'sale'),
('2222', '249', 'sale_metric', 'Once off'),
('2223', '249', 'on_show', 'false'),
('2224', '249', 'woo_maps_enable', 'on'),
('2225', '249', 'woo_maps_zoom', '16'),
('2226', '249', 'woo_maps_type', 'G_NORMAL_MAP'),
('2227', '249', '_yoast_wpseo_focuskw', ''),
('2228', '249', '_yoast_wpseo_title', ''),
('2229', '249', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2230', '249', '_yoast_wpseo_meta-robots-noindex', '0'),
('2231', '249', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2232', '249', '_yoast_wpseo_meta-robots-adv', 'none'),
('2233', '249', '_yoast_wpseo_canonical', ''),
('2234', '249', '_yoast_wpseo_redirect', ''),
('2235', '249', '_wpas_done_all', '1'),
('2236', '249', 'price', '2000000'),
('2237', '249', 'woo_maps_address', '0 Hunters Mountain Road, Naugatuck, CT 06770, USA'),
('2238', '249', 'woo_maps_long', '-73.06297570000004'),
('2239', '249', 'woo_maps_lat', '41.4786908');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2240', '249', 'woo_maps_pov_pitch', '-20'),
('2241', '249', 'woo_maps_pov_yaw', '20'),
('2242', '250', '_yoast_wpseo_linkdex', '0'),
('2243', '250', '_edit_last', '3'),
('2244', '250', '_edit_lock', '1373208519:3'),
('2245', '251', '_wp_attached_file', '2013/05/88-Bradley.jpg'),
('2246', '251', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:238;s:4:\"file\";s:22:\"2013/05/88-Bradley.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"88-Bradley-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"88-Bradley-300x178.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:10:\"Picasa 2.0\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2247', '250', '_thumbnail_id', '251'),
('2248', '250', '_wpas_done_all', '1'),
('2249', '250', 'image', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2250', '250', 'address', '88 BRADLEY ROAD, WOODBRIDGE'),
('2251', '250', 'price', '11'),
('2252', '250', 'size', '5800'),
('3673', '267', 'agent_name', 'Steve Miller'),
('2255', '250', 'bathrooms', '8'),
('2256', '250', 'sale_type', 'rent'),
('2257', '250', 'sale_metric', 'Per Square Foot'),
('2258', '250', 'on_show', 'false'),
('2259', '250', 'woo_maps_enable', 'on'),
('2260', '250', 'woo_maps_address', '88 Bradley Road, Woodbridge, CT 06525, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2261', '250', 'woo_maps_long', '-72.9779337'),
('2262', '250', 'woo_maps_lat', '41.3459028'),
('2263', '250', 'woo_maps_zoom', '15'),
('2264', '250', 'woo_maps_type', 'G_NORMAL_MAP'),
('2265', '250', 'woo_maps_pov_pitch', '-6.666666666666668'),
('2266', '250', 'woo_maps_pov_yaw', '60'),
('2267', '250', '_yoast_wpseo_focuskw', ''),
('2268', '250', '_yoast_wpseo_title', ''),
('2269', '250', '_yoast_wpseo_metadesc', ''),
('2270', '250', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2271', '250', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2272', '250', '_yoast_wpseo_meta-robots-adv', 'none'),
('2273', '250', '_yoast_wpseo_canonical', ''),
('2274', '250', '_yoast_wpseo_redirect', ''),
('2276', '254', '_yoast_wpseo_linkdex', '0'),
('2277', '254', '_edit_last', '3'),
('2278', '254', '_edit_lock', '1373250980:3'),
('2279', '254', '_wpas_done_all', '1'),
('2280', '254', 'image', ''),
('2281', '254', 'address', '2 THORN STREET, NEW HAVEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2282', '254', 'price', '1250000'),
('2283', '254', 'size', '39119'),
('2284', '254', 'garages', ''),
('3613', '240', 'agent_name', 'Steve Miller'),
('2286', '254', 'bathrooms', '16-25'),
('2287', '254', 'sale_type', 'sale'),
('2288', '254', 'sale_metric', 'Once off'),
('2289', '254', 'on_show', 'false'),
('2290', '254', 'woo_maps_enable', 'on'),
('2291', '254', 'woo_maps_address', '2 Thorn Street, New Haven, CT 06519, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2292', '254', 'woo_maps_long', '-72.94110999999998'),
('2293', '254', 'woo_maps_lat', '41.29685'),
('2294', '254', 'woo_maps_zoom', '17'),
('2295', '254', 'woo_maps_type', 'G_NORMAL_MAP'),
('2296', '254', 'woo_maps_pov_pitch', '-20'),
('2297', '254', 'woo_maps_pov_yaw', '20'),
('2298', '254', '_yoast_wpseo_focuskw', ''),
('2299', '254', '_yoast_wpseo_title', ''),
('2300', '254', '_yoast_wpseo_metadesc', ''),
('2301', '254', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2302', '254', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2303', '254', '_yoast_wpseo_meta-robots-adv', 'none'),
('2304', '254', '_yoast_wpseo_canonical', ''),
('2305', '254', '_yoast_wpseo_redirect', ''),
('2306', '256', '_wp_attached_file', '2013/05/2-THORN-STREET.jpg'),
('2307', '256', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:26:\"2013/05/2-THORN-STREET.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"2-THORN-STREET-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"2-THORN-STREET-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2308', '254', '_thumbnail_id', '256'),
('2309', '258', '_yoast_wpseo_linkdex', '0'),
('2310', '258', '_edit_last', '3'),
('2311', '258', '_edit_lock', '1373206233:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2312', '259', '_wp_attached_file', '2013/05/30-Printers-Lane.jpg'),
('2313', '259', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:28:\"2013/05/30-Printers-Lane.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"30-Printers-Lane-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"30-Printers-Lane-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2314', '258', '_thumbnail_id', '259'),
('2315', '258', '_wpas_done_all', '1'),
('2316', '258', 'image', ''),
('2317', '258', 'address', '30 PRINTERS LANE, NEW HAVEN'),
('2318', '258', 'price', '349000'),
('3636', '47', 'agent_name', 'Marty Ruff'),
('2321', '258', 'bathrooms', '14'),
('2322', '258', 'sale_type', 'sale');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2323', '258', 'sale_metric', 'Once off'),
('2324', '258', 'on_show', 'false'),
('2325', '258', 'woo_maps_enable', 'on'),
('2326', '258', 'woo_maps_address', '30 Printers Lane, New Haven, CT 06519, USA'),
('2327', '258', 'woo_maps_long', '-72.94732699999997'),
('2328', '258', 'woo_maps_lat', '41.2947499'),
('2329', '258', 'woo_maps_zoom', '13'),
('2330', '258', 'woo_maps_type', 'G_NORMAL_MAP'),
('2331', '258', 'woo_maps_pov_pitch', '-20'),
('2332', '258', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2333', '258', '_yoast_wpseo_focuskw', ''),
('2334', '258', '_yoast_wpseo_title', ''),
('2335', '258', '_yoast_wpseo_metadesc', ''),
('2336', '258', '_yoast_wpseo_meta-robots-noindex', '0'),
('2337', '258', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2338', '258', '_yoast_wpseo_meta-robots-adv', 'none'),
('2339', '258', '_yoast_wpseo_canonical', ''),
('2340', '258', '_yoast_wpseo_redirect', ''),
('2341', '260', '_yoast_wpseo_linkdex', '0'),
('2342', '260', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2343', '260', '_edit_lock', '1373208466:3'),
('2344', '260', '_wpas_done_all', '1'),
('2345', '260', 'image', ''),
('3671', '250', 'agent_name', 'Steve Miller'),
('2348', '260', 'bathrooms', '20+'),
('2349', '260', 'sale_type', 'rent'),
('2350', '260', 'sale_metric', 'Per Square Foot'),
('2351', '260', 'on_show', 'false'),
('2352', '260', 'woo_maps_enable', 'on'),
('2353', '260', 'woo_maps_zoom', '14');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2354', '260', 'woo_maps_type', 'G_NORMAL_MAP'),
('2355', '260', '_yoast_wpseo_focuskw', ''),
('2356', '260', '_yoast_wpseo_title', ''),
('2357', '260', '_yoast_wpseo_metadesc', ''),
('2358', '260', '_yoast_wpseo_meta-robots-noindex', '0'),
('2359', '260', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2360', '260', '_yoast_wpseo_meta-robots-adv', 'none'),
('2361', '260', '_yoast_wpseo_canonical', ''),
('2362', '260', '_yoast_wpseo_redirect', ''),
('2363', '262', '_wp_attached_file', '2013/05/136-BRADLEY001.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2364', '262', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:26:\"2013/05/136-BRADLEY001.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"136-BRADLEY001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"136-BRADLEY001-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"136-BRADLEY001-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1349422239;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:8:\"0.003125\";s:5:\"title\";s:0:\"\";}}'),
('2365', '260', '_thumbnail_id', '262'),
('2366', '260', 'address', '136 BRADLEY ROAD, WOODBRIDGE'),
('2367', '260', 'price', '5.00'),
('2368', '260', 'size', '19120'),
('2369', '260', 'woo_maps_address', '136 Bradley Road, Woodbridge, CT 06525, USA'),
('2370', '260', 'woo_maps_long', '-72.97601600000002'),
('2371', '260', 'woo_maps_lat', '41.347796'),
('2372', '260', 'woo_maps_pov_pitch', '-20'),
('2373', '260', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2374', '258', 'size', '5000'),
('2375', '263', '_yoast_wpseo_linkdex', '0'),
('2376', '263', '_edit_last', '3'),
('2377', '263', '_edit_lock', '1373202650:3'),
('2378', '264', '_yoast_wpseo_linkdex', '0'),
('2379', '264', '_edit_last', '3'),
('2380', '264', '_edit_lock', '1373291889:3'),
('2381', '264', '_wpas_done_all', '1'),
('2382', '264', 'image', ''),
('2383', '264', 'address', '80 AMITY ROAD, NEW HAVEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2384', '264', 'price', '3500'),
('2385', '264', 'size', '1736'),
('2386', '264', 'garages', '.28'),
('3627', '569', 'agent_name', 'Steve Miller'),
('2389', '264', 'sale_type', 'rent'),
('2390', '264', 'sale_metric', 'Per Month'),
('2391', '264', 'on_show', 'false'),
('2392', '264', 'woo_maps_enable', 'on'),
('2393', '264', 'woo_maps_address', '80 Amity Road, New Haven, CT 06515, USA'),
('2394', '264', 'woo_maps_long', '-72.97865969999998');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2395', '264', 'woo_maps_lat', '41.3364434'),
('2396', '264', 'woo_maps_zoom', '13'),
('2397', '264', 'woo_maps_type', 'G_NORMAL_MAP'),
('2398', '264', 'woo_maps_pov_pitch', '-20'),
('2399', '264', 'woo_maps_pov_yaw', '20'),
('2400', '264', '_yoast_wpseo_focuskw', ''),
('2401', '264', '_yoast_wpseo_title', ''),
('2402', '264', '_yoast_wpseo_metadesc', ''),
('2403', '264', '_yoast_wpseo_meta-robots-noindex', '0'),
('2404', '264', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2405', '264', '_yoast_wpseo_meta-robots-adv', 'none'),
('2406', '264', '_yoast_wpseo_canonical', ''),
('2407', '264', '_yoast_wpseo_redirect', ''),
('2408', '265', '_wp_attached_file', '2013/05/80-Amity.jpg'),
('2409', '265', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:20:\"2013/05/80-Amity.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"80-Amity-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"80-Amity-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2410', '264', '_thumbnail_id', '265'),
('2411', '267', '_yoast_wpseo_linkdex', '0'),
('2412', '267', '_edit_last', '3'),
('2413', '267', '_edit_lock', '1373208706:3'),
('2415', '269', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:360;s:4:\"file\";s:24:\"2013/05/30-Hazel-St..jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"30-Hazel-St.-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"30-Hazel-St.-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.70000000000000017763568394002504646778106689453125;s:6:\"credit\";s:10:\"Picasa 2.0\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1162543559;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"6\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00201299972832\";s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2416', '267', '_thumbnail_id', '269'),
('2417', '267', '_wpas_done_all', '1'),
('2418', '267', 'image', ''),
('2419', '267', 'address', '30 HAZEL TERRACE, WOODBRIDGE'),
('2420', '267', 'price', '5'),
('2421', '267', 'size', '9030'),
('2422', '267', 'garages', ''),
('2423', '267', 'beds', ''),
('2424', '267', 'bathrooms', '8'),
('2425', '267', 'sale_type', 'rent');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2426', '267', 'sale_metric', 'Per Square Foot'),
('2427', '267', 'on_show', 'false'),
('2428', '267', 'woo_maps_enable', 'on'),
('2429', '267', 'woo_maps_address', '30 Hazel Terrace, Woodbridge, CT 06525, USA'),
('2430', '267', 'woo_maps_long', '-72.98362120000002'),
('2431', '267', 'woo_maps_lat', '41.3398714'),
('2432', '267', 'woo_maps_zoom', '15'),
('2433', '267', 'woo_maps_type', 'G_NORMAL_MAP'),
('2434', '267', 'woo_maps_pov_pitch', '-20'),
('2435', '267', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2436', '267', '_yoast_wpseo_focuskw', ''),
('2437', '267', '_yoast_wpseo_title', ''),
('2438', '267', '_yoast_wpseo_metadesc', ''),
('2439', '267', '_yoast_wpseo_meta-robots-noindex', '0'),
('2440', '267', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2441', '267', '_yoast_wpseo_meta-robots-adv', 'none'),
('2442', '267', '_yoast_wpseo_canonical', ''),
('2443', '267', '_yoast_wpseo_redirect', ''),
('2444', '263', '_wpas_done_all', '1'),
('2445', '263', 'image', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3586', '263', 'agent_name', 'Steve Miller'),
('2449', '263', 'sale_type', 'rent'),
('2450', '263', 'sale_metric', 'Per Month'),
('2451', '263', 'on_show', 'false'),
('2452', '263', 'woo_maps_enable', 'on'),
('2453', '263', 'woo_maps_address', '1646 Litchfield Turnpike, Woodbridge, CT 06525, USA'),
('2454', '263', 'woo_maps_long', '-72.97854000000001'),
('2455', '263', 'woo_maps_lat', '41.339797'),
('2456', '263', 'woo_maps_zoom', '13'),
('2457', '263', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2458', '263', 'woo_maps_pov_pitch', '-20'),
('2459', '263', 'woo_maps_pov_yaw', '20'),
('2460', '263', '_yoast_wpseo_focuskw', ''),
('2461', '263', '_yoast_wpseo_title', ''),
('2462', '263', '_yoast_wpseo_metadesc', ''),
('2463', '263', '_yoast_wpseo_meta-robots-noindex', '0'),
('2464', '263', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2465', '263', '_yoast_wpseo_meta-robots-adv', 'none'),
('2466', '263', '_yoast_wpseo_canonical', ''),
('2467', '263', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2468', '270', '_wp_attached_file', '2013/05/1-Acre-Litchfield-Tpke..jpg'),
('2469', '270', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1504;s:6:\"height\";i:1312;s:4:\"file\";s:35:\"2013/05/1-Acre-Litchfield-Tpke..jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"1-Acre-Litchfield-Tpke.-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"1-Acre-Litchfield-Tpke.-300x261.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:261;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"1-Acre-Litchfield-Tpke.-1024x893.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:893;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:12:\"MX850 series\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1349708096;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2470', '263', '_thumbnail_id', '270'),
('2471', '249', '_thumbnail_id', '50'),
('2472', '271', '_yoast_wpseo_linkdex', '0'),
('2473', '271', '_edit_last', '3'),
('2474', '271', '_edit_lock', '1373210265:3'),
('2475', '271', '_wpas_done_all', '1'),
('2476', '271', 'image', ''),
('2477', '271', 'address', '1635 DIXWELL AVENUE, HAMDEN');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2478', '271', 'price', '20'),
('2479', '271', 'size', '18198'),
('2480', '271', 'garages', ''),
('2481', '271', 'beds', ''),
('2482', '271', 'bathrooms', '14'),
('2483', '271', 'sale_type', 'rent'),
('2484', '271', 'sale_metric', 'Per Square Foot'),
('2485', '271', 'on_show', 'false'),
('2486', '271', 'woo_maps_enable', 'on'),
('2487', '271', 'woo_maps_address', '1635 Dixwell Avenue, Hamden, CT 06514, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2488', '271', 'woo_maps_long', '-72.92732380000001'),
('2489', '271', 'woo_maps_lat', '41.3569463'),
('2490', '271', 'woo_maps_zoom', '13'),
('2491', '271', 'woo_maps_type', 'G_NORMAL_MAP'),
('2492', '271', 'woo_maps_pov_pitch', '-20'),
('2493', '271', 'woo_maps_pov_yaw', '20'),
('2494', '271', '_yoast_wpseo_focuskw', ''),
('2495', '271', '_yoast_wpseo_title', ''),
('2496', '271', '_yoast_wpseo_metadesc', ''),
('2497', '271', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2498', '271', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2499', '271', '_yoast_wpseo_meta-robots-adv', 'none'),
('2500', '271', '_yoast_wpseo_canonical', ''),
('2501', '271', '_yoast_wpseo_redirect', ''),
('2502', '273', '_wp_attached_file', '2013/05/1635-Dixwell.jpg'),
('2503', '273', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:390;s:6:\"height\";i:177;s:4:\"file\";s:24:\"2013/05/1635-Dixwell.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"1635-Dixwell-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"1635-Dixwell-300x136.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:136;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2504', '271', '_thumbnail_id', '273'),
('2505', '274', '_yoast_wpseo_linkdex', '0'),
('2506', '274', '_edit_last', '3'),
('2507', '274', '_edit_lock', '1373207570:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2508', '274', '_wpas_done_all', '1'),
('2509', '274', 'image', ''),
('2510', '274', 'address', '383 ORANGE STREET, NEW HAVEN'),
('2511', '274', 'price', '18'),
('2512', '274', 'size', '1955'),
('3602', '247', 'agent_email', 'steve@lmmre.com'),
('3601', '247', 'agent_name', 'Steve Miller'),
('2516', '274', 'sale_type', 'rent'),
('2517', '274', 'sale_metric', 'Per Square Foot'),
('2518', '274', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2519', '274', 'woo_maps_enable', 'on'),
('2520', '274', 'woo_maps_address', '383 Orange Street, New Haven, CT 06511, USA'),
('2521', '274', 'woo_maps_long', '-72.91977399999996'),
('2522', '274', 'woo_maps_lat', '41.3109309'),
('2523', '274', 'woo_maps_zoom', '14'),
('2524', '274', 'woo_maps_type', 'G_NORMAL_MAP'),
('2525', '274', 'woo_maps_pov_pitch', '-20'),
('2526', '274', 'woo_maps_pov_yaw', '20'),
('2527', '274', '_yoast_wpseo_focuskw', ''),
('2528', '274', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2529', '274', '_yoast_wpseo_metadesc', ''),
('2530', '274', '_yoast_wpseo_meta-robots-noindex', '0'),
('2531', '274', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2532', '274', '_yoast_wpseo_meta-robots-adv', 'none'),
('2533', '274', '_yoast_wpseo_canonical', ''),
('2534', '274', '_yoast_wpseo_redirect', ''),
('2535', '275', '_yoast_wpseo_linkdex', '0'),
('2536', '275', '_edit_last', '3'),
('2537', '275', '_edit_lock', '1373210209:3'),
('2538', '276', '_wp_attached_file', '2013/05/4244-Madison-Ave.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2539', '276', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:28:\"2013/05/4244-Madison-Ave.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"4244-Madison-Ave-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"4244-Madison-Ave-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('2540', '275', '_thumbnail_id', '276'),
('2541', '275', '_wpas_done_all', '1'),
('2542', '275', 'image', ''),
('2543', '275', 'address', '4244 MADISON AVENUE, TRUMBULL'),
('2544', '275', 'price', '1250000'),
('2545', '275', 'garages', ''),
('2546', '275', 'beds', ''),
('2547', '275', 'bathrooms', ''),
('2548', '275', 'sale_type', 'sale');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2549', '275', 'sale_metric', 'Once off'),
('2550', '275', 'on_show', 'false'),
('2551', '275', 'woo_maps_enable', 'on'),
('2552', '275', 'woo_maps_address', '4244 Madison Avenue, Trumbull, CT 06611, USA'),
('2553', '275', 'woo_maps_long', '-73.23101250000002'),
('2554', '275', 'woo_maps_lat', '41.2346907'),
('2555', '275', 'woo_maps_zoom', '15'),
('2556', '275', 'woo_maps_type', 'G_NORMAL_MAP'),
('2557', '275', 'woo_maps_pov_pitch', '-20'),
('2558', '275', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2559', '275', '_yoast_wpseo_focuskw', ''),
('2560', '275', '_yoast_wpseo_title', ''),
('2561', '275', '_yoast_wpseo_metadesc', ''),
('2562', '275', '_yoast_wpseo_meta-robots-noindex', '0'),
('2563', '275', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2564', '275', '_yoast_wpseo_meta-robots-adv', 'none'),
('2565', '275', '_yoast_wpseo_canonical', ''),
('2566', '275', '_yoast_wpseo_redirect', ''),
('2567', '277', '_yoast_wpseo_linkdex', '0'),
('2568', '277', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2569', '277', '_edit_lock', '1373210160:3'),
('2570', '278', '_wp_attached_file', '2013/05/39-Oregon-Ave.1.jpg'),
('2571', '278', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2272;s:6:\"height\";i:1704;s:4:\"file\";s:27:\"2013/05/39-Oregon-Ave.1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"39-Oregon-Ave.1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"39-Oregon-Ave.1-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"39-Oregon-Ave.1-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1286975465;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:5:\"0.008\";s:5:\"title\";s:0:\"\";}}'),
('2572', '277', '_thumbnail_id', '278'),
('2573', '277', '_wpas_done_all', '1'),
('2574', '277', 'image', ''),
('2575', '277', 'address', '39 OREGON AVENUE, HAMDEN'),
('2576', '277', 'price', '269000'),
('2577', '277', 'size', '4435'),
('2578', '277', 'garages', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2579', '277', 'beds', ''),
('2580', '277', 'bathrooms', '11-15 '),
('2581', '277', 'sale_type', 'sale'),
('2582', '277', 'sale_metric', 'Once off'),
('2583', '277', 'on_show', 'false'),
('2584', '277', 'woo_maps_enable', 'on'),
('2585', '277', 'woo_maps_address', '39 Oregon Avenue, Hamden, CT 06514, USA'),
('2586', '277', 'woo_maps_long', '-72.92943000000002'),
('2587', '277', 'woo_maps_lat', '41.351233'),
('2588', '277', 'woo_maps_zoom', '13');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2589', '277', 'woo_maps_type', 'G_NORMAL_MAP'),
('2590', '277', 'woo_maps_pov_pitch', '-20'),
('2591', '277', 'woo_maps_pov_yaw', '20'),
('2592', '277', '_yoast_wpseo_focuskw', ''),
('2593', '277', '_yoast_wpseo_title', ''),
('2594', '277', '_yoast_wpseo_metadesc', ''),
('2595', '277', '_yoast_wpseo_meta-robots-noindex', '0'),
('2596', '277', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2597', '277', '_yoast_wpseo_meta-robots-adv', 'none'),
('2598', '277', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2599', '277', '_yoast_wpseo_redirect', ''),
('2608', '281', '_thumbnail_id', '136'),
('2605', '281', '_yoast_wpseo_linkdex', '0'),
('2606', '281', '_edit_last', '3'),
('2607', '281', '_edit_lock', '1373210102:3'),
('2609', '281', '_wpas_done_all', '1'),
('2610', '281', 'image', ''),
('2611', '281', 'address', '610 FIRST AVENUE, WEST HAVEN'),
('2612', '281', 'price', '3500000'),
('2613', '281', 'size', '34704');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2614', '281', 'garages', '1.49'),
('3706', '277', 'agent_name', 'Steve Miller'),
('2616', '281', 'bathrooms', '16'),
('2617', '281', 'sale_type', 'sale'),
('2618', '281', 'sale_metric', 'Once off'),
('2619', '281', 'on_show', 'false'),
('2620', '281', 'woo_maps_enable', 'on'),
('2621', '281', 'woo_maps_address', '610 1st Avenue, West Haven, CT 06516, USA'),
('2622', '281', 'woo_maps_long', '-72.947812'),
('2623', '281', 'woo_maps_lat', '41.281667');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2624', '281', 'woo_maps_zoom', '17'),
('2625', '281', 'woo_maps_type', 'G_NORMAL_MAP'),
('2626', '281', 'woo_maps_pov_pitch', '-20'),
('2627', '281', 'woo_maps_pov_yaw', '20'),
('2628', '281', '_yoast_wpseo_focuskw', ''),
('2629', '281', '_yoast_wpseo_title', ''),
('2630', '281', '_yoast_wpseo_metadesc', ''),
('2631', '281', '_yoast_wpseo_meta-robots-noindex', '0'),
('2632', '281', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2633', '281', '_yoast_wpseo_meta-robots-adv', 'none');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2634', '281', '_yoast_wpseo_canonical', ''),
('2635', '281', '_yoast_wpseo_redirect', ''),
('3243', '233', '_thumbnail_id', '50'),
('3242', '274', '_thumbnail_id', '368'),
('3241', '368', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:800;s:4:\"file\";s:25:\"2013/05/383-Orange-St.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"383-Orange-St-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"383-Orange-St-169x300.jpg\";s:5:\"width\";i:169;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.399999999999999911182158029987476766109466552734375;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:10:\"DROID RAZR\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1359989383;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:3:\"4.6\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:7:\"0.00164\";s:5:\"title\";s:0:\"\";}}'),
('3240', '368', '_wp_attached_file', '2013/05/383-Orange-St.jpg'),
('2665', '284', '_yoast_wpseo_linkdex', '0'),
('2666', '284', '_edit_last', '3'),
('2667', '284', '_edit_lock', '1373251298:3'),
('2668', '284', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/Banquet-300x195.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2669', '284', 'price', '5000000'),
('3822', '758', '_edit_last', '3'),
('3819', '284', 'size', '29000'),
('2673', '284', 'sale_type', 'sale'),
('2674', '284', 'sale_metric', 'Once off'),
('2675', '284', 'on_show', 'false'),
('2676', '284', 'woo_maps_enable', 'on'),
('2677', '284', 'woo_maps_zoom', '9'),
('2678', '284', 'woo_maps_type', 'G_NORMAL_MAP'),
('2679', '284', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2680', '284', '_yoast_wpseo_title', ''),
('2681', '284', '_yoast_wpseo_metadesc', ''),
('2682', '284', '_yoast_wpseo_meta-robots-noindex', '0'),
('2683', '284', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2684', '284', '_yoast_wpseo_meta-robots-adv', 'none'),
('2685', '284', '_yoast_wpseo_canonical', ''),
('2686', '284', '_yoast_wpseo_redirect', ''),
('2691', '284', 'woo_maps_pov_pitch', '-20'),
('2687', '284', '_wpas_done_all', '1'),
('2688', '284', 'woo_maps_address', 'New Haven, CT, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2689', '284', 'woo_maps_long', '-72.92788350000001'),
('2690', '284', 'woo_maps_lat', '41.308274'),
('2692', '284', 'woo_maps_pov_yaw', '20'),
('2693', '284', '_thumbnail_id', '754'),
('2694', '287', '_yoast_wpseo_linkdex', '0'),
('2695', '287', '_edit_last', '3'),
('2696', '287', '_edit_lock', '1373209960:3'),
('3467', '602', '_wp_attached_file', '2013/05/Daycare.jpg'),
('2698', '287', 'image', ''),
('2699', '287', 'price', '89000');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2700', '287', 'size', '2200'),
('2701', '287', 'garages', ''),
('2702', '287', 'beds', ''),
('2703', '287', 'bathrooms', ''),
('2704', '287', 'sale_type', 'sale'),
('2705', '287', 'sale_metric', 'Once off'),
('2706', '287', 'on_show', 'false'),
('2707', '287', 'woo_maps_enable', 'on'),
('2708', '287', 'woo_maps_zoom', '10'),
('2709', '287', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2710', '287', '_yoast_wpseo_focuskw', ''),
('2711', '287', '_yoast_wpseo_title', ''),
('2712', '287', '_yoast_wpseo_metadesc', ''),
('2713', '287', '_yoast_wpseo_meta-robots-noindex', '0'),
('2714', '287', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2715', '287', '_yoast_wpseo_meta-robots-adv', 'none'),
('2716', '287', '_yoast_wpseo_canonical', ''),
('2717', '287', '_yoast_wpseo_redirect', ''),
('2718', '287', '_wpas_done_all', '1'),
('2719', '287', 'woo_maps_address', 'Shoreline Drive, Stratford, CT 06615, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2720', '287', 'woo_maps_long', '-73.13244850000001'),
('2721', '287', 'woo_maps_lat', '41.1478319'),
('2722', '287', 'woo_maps_pov_pitch', '-20'),
('2723', '287', 'woo_maps_pov_yaw', '20'),
('2724', '288', '_yoast_wpseo_linkdex', '0'),
('2725', '288', '_edit_last', '3'),
('2726', '288', '_edit_lock', '1373209803:3'),
('2727', '288', '_thumbnail_id', '737'),
('2728', '288', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/bistro-230x300.jpg'),
('2729', '288', 'address', 'NEW HAVEN COUNTY');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2730', '288', 'price', '349000'),
('2731', '288', 'size', '4600'),
('2732', '288', 'garages', ''),
('2733', '288', 'beds', ''),
('2734', '288', 'bathrooms', ''),
('2735', '288', 'sale_type', 'sale'),
('2736', '288', 'sale_metric', 'Once off'),
('2737', '288', 'on_show', 'false'),
('2738', '288', 'woo_maps_enable', 'on'),
('2739', '288', 'woo_maps_zoom', '9');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2740', '288', 'woo_maps_type', 'G_NORMAL_MAP'),
('2741', '288', '_yoast_wpseo_focuskw', ''),
('2742', '288', '_yoast_wpseo_title', ''),
('2743', '288', '_yoast_wpseo_metadesc', ''),
('2744', '288', '_yoast_wpseo_meta-robots-noindex', '0'),
('2745', '288', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2746', '288', '_yoast_wpseo_meta-robots-adv', 'none'),
('2747', '288', '_yoast_wpseo_canonical', ''),
('2748', '288', '_yoast_wpseo_redirect', ''),
('2749', '288', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2750', '288', 'woo_maps_address', 'New Haven, CT, USA'),
('2751', '288', 'woo_maps_long', '-72.8042797'),
('2752', '288', 'woo_maps_lat', '41.3266911'),
('2753', '288', 'woo_maps_pov_pitch', '-20'),
('2754', '288', 'woo_maps_pov_yaw', '20'),
('2755', '288', '_wp_old_slug', 'superb-restaurantbar'),
('2756', '290', '_yoast_wpseo_linkdex', '0'),
('2757', '290', '_edit_last', '3'),
('2758', '290', '_edit_lock', '1373251265:3'),
('2759', '290', '_thumbnail_id', '736');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2760', '290', '_wpas_done_all', '1'),
('2761', '290', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/patio-300x175.jpeg'),
('2762', '290', 'address', 'NEW HAVEN COUNTY'),
('2763', '290', 'price', '399000'),
('2764', '290', 'size', '5000'),
('3821', '758', '_yoast_wpseo_linkdex', '0'),
('3820', '303', 'size', '2500'),
('2768', '290', 'sale_type', 'sale'),
('2769', '290', 'sale_metric', 'Once off'),
('2770', '290', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2771', '290', 'woo_maps_enable', 'on'),
('2772', '290', 'woo_maps_address', 'New Haven, CT, USA'),
('2773', '290', 'woo_maps_long', '-72.8042797'),
('2774', '290', 'woo_maps_lat', '41.3266911'),
('2775', '290', 'woo_maps_zoom', '9'),
('2776', '290', 'woo_maps_type', 'G_NORMAL_MAP'),
('2777', '290', 'woo_maps_pov_pitch', '-19.126625569939016'),
('2778', '290', 'woo_maps_pov_yaw', '18.37736491315691'),
('2779', '290', '_yoast_wpseo_focuskw', ''),
('2780', '290', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2781', '290', '_yoast_wpseo_metadesc', ''),
('2782', '290', '_yoast_wpseo_meta-robots-noindex', '0'),
('2783', '290', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2784', '290', '_yoast_wpseo_meta-robots-adv', 'none'),
('2785', '290', '_yoast_wpseo_canonical', ''),
('2786', '290', '_yoast_wpseo_redirect', ''),
('2802', '293', 'woo_maps_address', '490 Orchard Street, New Haven, CT 06511, USA'),
('2803', '293', 'woo_maps_long', '-72.93999500000001'),
('2804', '293', 'woo_maps_lat', '41.314708'),
('2805', '293', 'woo_maps_zoom', '15');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2806', '293', 'woo_maps_type', 'G_NORMAL_MAP'),
('2807', '293', 'woo_maps_pov_pitch', '-20'),
('2808', '293', 'woo_maps_pov_yaw', '20'),
('2809', '293', '_yoast_wpseo_focuskw', ''),
('2810', '293', '_yoast_wpseo_title', ''),
('2811', '293', '_yoast_wpseo_metadesc', ''),
('2812', '293', '_yoast_wpseo_meta-robots-noindex', '0'),
('2813', '293', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2814', '293', '_yoast_wpseo_meta-robots-adv', 'none'),
('2815', '293', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2816', '293', '_yoast_wpseo_redirect', ''),
('2817', '296', '_yoast_wpseo_linkdex', '0'),
('2818', '296', '_edit_last', '3'),
('2819', '296', '_edit_lock', '1373207367:3'),
('2820', '296', '_wpas_done_all', '1'),
('2821', '296', 'image', ''),
('2822', '296', 'garages', '0.36'),
('2823', '296', 'beds', ''),
('2824', '296', 'bathrooms', ''),
('2825', '296', 'sale_type', 'sale');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2826', '296', 'sale_metric', 'Once off'),
('2827', '296', 'on_show', 'false'),
('2828', '296', 'woo_maps_enable', 'on'),
('2829', '296', 'woo_maps_zoom', '16'),
('2830', '296', 'woo_maps_type', 'G_NORMAL_MAP'),
('2831', '296', '_yoast_wpseo_focuskw', ''),
('2832', '296', '_yoast_wpseo_title', ''),
('2833', '296', '_yoast_wpseo_metadesc', ''),
('2834', '296', '_yoast_wpseo_meta-robots-noindex', '0'),
('2835', '296', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2836', '296', '_yoast_wpseo_meta-robots-adv', 'none'),
('2837', '296', '_yoast_wpseo_canonical', ''),
('2838', '296', '_yoast_wpseo_redirect', ''),
('2839', '296', 'address', '399 QUINNIPIAC AVENUE, NEW HAVEN'),
('2840', '296', 'price', '169500'),
('2841', '296', 'woo_maps_address', '399 Quinnipiac Avenue, New Haven, CT 06513, USA'),
('2842', '296', 'woo_maps_long', '-72.889522'),
('2843', '296', 'woo_maps_lat', '41.301229'),
('2844', '296', 'woo_maps_pov_pitch', '-20'),
('2845', '296', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2846', '296', '_thumbnail_id', '50'),
('2847', '299', '_wp_attached_file', '2013/06/490-Orchard-Street-001-001.jpg'),
('2848', '299', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:240;s:4:\"file\";s:38:\"2013/06/490-Orchard-Street-001-001.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"490-Orchard-Street-001-001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"490-Orchard-Street-001-001-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1347443127;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:6:\"0.0025\";s:5:\"title\";s:0:\"\";}}'),
('2849', '293', '_thumbnail_id', '299'),
('2850', '300', '_yoast_wpseo_linkdex', '0'),
('2851', '300', '_edit_last', '3'),
('2852', '300', '_edit_lock', '1373209382:3'),
('2853', '300', '_thumbnail_id', '735'),
('2854', '300', '_wpas_done_all', '1'),
('2855', '300', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Pizza-YUMM-300x208.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3694', '736', '_wp_attached_file', '2013/05/patio.jpeg'),
('2859', '300', 'sale_type', 'sale'),
('2860', '300', 'sale_metric', 'Once off'),
('2861', '300', 'on_show', 'false'),
('2862', '300', 'woo_maps_enable', 'false'),
('2863', '300', 'woo_maps_zoom', '9'),
('2864', '300', 'woo_maps_type', 'G_NORMAL_MAP'),
('2865', '300', '_yoast_wpseo_focuskw', ''),
('2866', '300', '_yoast_wpseo_title', ''),
('2867', '300', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2868', '300', '_yoast_wpseo_meta-robots-noindex', '0'),
('2869', '300', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2870', '300', '_yoast_wpseo_meta-robots-adv', 'none'),
('2871', '300', '_yoast_wpseo_canonical', ''),
('2872', '300', '_yoast_wpseo_redirect', ''),
('2873', '301', '_yoast_wpseo_linkdex', '0'),
('2874', '301', '_edit_last', '3'),
('2875', '301', '_edit_lock', '1372428476:3'),
('3507', '301', '_thumbnail_id', '643'),
('2877', '301', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2878', '301', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/wedding-300x247.jpg'),
('2879', '301', 'garages', '4.5'),
('2880', '301', 'beds', ''),
('3512', '301', 'woo_maps_address', 'New Haven, CT, USA'),
('2882', '301', 'sale_type', 'sale'),
('2883', '301', 'sale_metric', ''),
('2884', '301', 'on_show', 'false'),
('2885', '301', 'woo_maps_enable', 'on'),
('2886', '301', 'woo_maps_zoom', '9'),
('2887', '301', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2888', '301', '_yoast_wpseo_focuskw', ''),
('2889', '301', '_yoast_wpseo_title', ''),
('2890', '301', '_yoast_wpseo_metadesc', ''),
('2891', '301', '_yoast_wpseo_meta-robots-noindex', '0'),
('2892', '301', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2893', '301', '_yoast_wpseo_meta-robots-adv', 'none'),
('2894', '301', '_yoast_wpseo_canonical', ''),
('2895', '301', '_yoast_wpseo_redirect', ''),
('2896', '301', 'price', '1950000'),
('2897', '301', 'size', '8301');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2898', '303', '_yoast_wpseo_linkdex', '0'),
('2899', '303', '_edit_last', '3'),
('2900', '303', 'image', ''),
('3704', '281', 'agent_name', 'Harold Kent'),
('2904', '303', 'sale_type', 'sale'),
('2905', '303', 'sale_metric', 'Once off'),
('2906', '303', 'on_show', 'false'),
('2907', '303', 'woo_maps_enable', 'false'),
('2908', '303', 'woo_maps_zoom', '9'),
('2909', '303', 'woo_maps_type', 'G_NORMAL_MAP');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2910', '303', '_yoast_wpseo_focuskw', ''),
('2911', '303', '_yoast_wpseo_title', ''),
('2912', '303', '_yoast_wpseo_metadesc', ''),
('2913', '303', '_yoast_wpseo_meta-robots-noindex', '0'),
('2914', '303', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2915', '303', '_yoast_wpseo_meta-robots-adv', 'none'),
('2916', '303', '_yoast_wpseo_canonical', ''),
('2917', '303', '_yoast_wpseo_redirect', ''),
('2918', '303', '_edit_lock', '1373251377:3'),
('3473', '604', '_wp_attached_file', '2013/06/Italian.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2920', '303', '_wpas_done_all', '1'),
('2921', '303', 'price', '189000'),
('2922', '305', '_yoast_wpseo_linkdex', '0'),
('2923', '305', '_edit_last', '3'),
('2924', '305', '_edit_lock', '1373209164:3'),
('2925', '305', '_thumbnail_id', '136'),
('2926', '305', '_wpas_done_all', '1'),
('2927', '305', 'image', ''),
('2928', '305', 'price', '89000'),
('2929', '305', 'garages', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2930', '305', 'beds', ''),
('2931', '305', 'bathrooms', ''),
('2932', '305', 'sale_type', 'sale'),
('2933', '305', 'sale_metric', 'Once off'),
('2934', '305', 'on_show', 'false'),
('2935', '305', 'woo_maps_enable', 'false'),
('2936', '305', 'woo_maps_zoom', '9'),
('2937', '305', 'woo_maps_type', 'G_NORMAL_MAP'),
('2938', '305', '_yoast_wpseo_focuskw', ''),
('2939', '305', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2940', '305', '_yoast_wpseo_metadesc', ''),
('2941', '305', '_yoast_wpseo_meta-robots-noindex', '0'),
('2942', '305', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2943', '305', '_yoast_wpseo_meta-robots-adv', 'none'),
('2944', '305', '_yoast_wpseo_canonical', ''),
('2945', '305', '_yoast_wpseo_redirect', ''),
('2946', '308', '_yoast_wpseo_linkdex', '0'),
('2947', '308', '_edit_last', '3'),
('2948', '308', '_edit_lock', '1373209093:3'),
('2949', '308', '_thumbnail_id', '618');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2950', '308', '_wpas_done_all', '1'),
('2951', '308', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Mexican.jpg'),
('2952', '308', 'price', '299000'),
('2953', '308', 'garages', ''),
('2954', '308', 'beds', ''),
('2955', '308', 'bathrooms', ''),
('2956', '308', 'sale_type', 'sale'),
('2957', '308', 'sale_metric', 'Once off'),
('2958', '308', 'on_show', 'false'),
('2959', '308', 'woo_maps_enable', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2960', '308', 'woo_maps_zoom', '9'),
('2961', '308', 'woo_maps_type', 'G_NORMAL_MAP'),
('2962', '308', '_yoast_wpseo_focuskw', ''),
('2963', '308', '_yoast_wpseo_title', ''),
('2964', '308', '_yoast_wpseo_metadesc', ''),
('2965', '308', '_yoast_wpseo_meta-robots-noindex', '0'),
('2966', '308', '_yoast_wpseo_meta-robots-nofollow', '0'),
('2967', '308', '_yoast_wpseo_meta-robots-adv', 'none'),
('2968', '308', '_yoast_wpseo_canonical', ''),
('2969', '308', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2970', '309', '_yoast_wpseo_linkdex', '0'),
('2971', '309', '_edit_last', '3'),
('2972', '309', '_edit_lock', '1373207781:3'),
('2973', '309', '_thumbnail_id', '617'),
('2974', '309', '_wpas_done_all', '1'),
('2975', '309', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Waterfront-300x224.jpg'),
('2976', '309', 'garages', ''),
('2977', '309', 'beds', ''),
('2978', '309', 'bathrooms', ''),
('2979', '309', 'sale_type', 'sale');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2980', '309', 'sale_metric', 'Once off'),
('2981', '309', 'on_show', 'false'),
('2982', '309', 'woo_maps_enable', 'false'),
('2983', '309', 'woo_maps_zoom', '9'),
('2984', '309', 'woo_maps_type', 'G_NORMAL_MAP'),
('2985', '309', '_yoast_wpseo_focuskw', ''),
('2986', '309', '_yoast_wpseo_title', ''),
('2987', '309', '_yoast_wpseo_metadesc', ''),
('2988', '309', '_yoast_wpseo_meta-robots-noindex', '0'),
('2989', '309', '_yoast_wpseo_meta-robots-nofollow', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('2990', '309', '_yoast_wpseo_meta-robots-adv', 'none'),
('2991', '309', '_yoast_wpseo_canonical', ''),
('2992', '309', '_yoast_wpseo_redirect', ''),
('2993', '310', '_yoast_wpseo_linkdex', '0'),
('2994', '310', '_edit_last', '3'),
('2995', '310', '_edit_lock', '1373209053:3'),
('3488', '310', '_thumbnail_id', '613'),
('2997', '310', '_wpas_done_all', '1'),
('2998', '310', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Break-lunch-dinner-300x238.jpg'),
('3682', '313', 'agent_email', 'shawn@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3681', '313', 'agent_name', 'Shawn Reilly'),
('3002', '310', 'sale_type', 'sale'),
('3003', '310', 'sale_metric', 'Once off'),
('3004', '310', 'on_show', 'false'),
('3005', '310', 'woo_maps_enable', 'false'),
('3006', '310', 'woo_maps_zoom', '9'),
('3007', '310', 'woo_maps_type', 'G_NORMAL_MAP'),
('3008', '310', '_yoast_wpseo_focuskw', ''),
('3009', '310', '_yoast_wpseo_title', ''),
('3010', '310', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3011', '310', '_yoast_wpseo_meta-robots-noindex', '0'),
('3012', '310', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3013', '310', '_yoast_wpseo_meta-robots-adv', 'none'),
('3014', '310', '_yoast_wpseo_canonical', ''),
('3015', '310', '_yoast_wpseo_redirect', ''),
('3269', '506', '_yoast_wpseo_linkdex', '0'),
('3017', '313', '_yoast_wpseo_linkdex', '0'),
('3018', '313', '_edit_last', '3'),
('3019', '313', '_edit_lock', '1373209026:3'),
('3020', '313', '_thumbnail_id', '136');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3021', '313', '_wpas_done_all', '1'),
('3022', '313', 'image', ''),
('3023', '313', 'garages', ''),
('3024', '313', 'beds', ''),
('3025', '313', 'bathrooms', ''),
('3026', '313', 'sale_type', 'sale'),
('3027', '313', 'sale_metric', 'Once off'),
('3028', '313', 'on_show', 'false'),
('3029', '313', 'woo_maps_enable', 'false'),
('3030', '313', 'woo_maps_zoom', '9');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3031', '313', 'woo_maps_type', 'G_NORMAL_MAP'),
('3032', '313', '_yoast_wpseo_focuskw', ''),
('3033', '313', '_yoast_wpseo_title', ''),
('3034', '313', '_yoast_wpseo_metadesc', ''),
('3035', '313', '_yoast_wpseo_meta-robots-noindex', '0'),
('3036', '313', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3037', '313', '_yoast_wpseo_meta-robots-adv', 'none'),
('3038', '313', '_yoast_wpseo_canonical', ''),
('3039', '313', '_yoast_wpseo_redirect', ''),
('3040', '314', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3041', '314', '_edit_last', '3'),
('3042', '314', '_edit_lock', '1373208985:3'),
('3460', '597', '_wp_attached_file', '2013/06/Ice-Cream.jpg'),
('3044', '314', '_wpas_done_all', '1'),
('3045', '314', 'image', ''),
('3680', '310', 'agent_email', 'shawn@lmmre.com'),
('3679', '310', 'agent_name', 'Shawn Reilly'),
('3049', '314', 'sale_type', 'sale'),
('3050', '314', 'sale_metric', 'Once off'),
('3051', '314', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3052', '314', 'woo_maps_enable', 'false'),
('3053', '314', 'woo_maps_zoom', '9'),
('3054', '314', 'woo_maps_type', 'G_NORMAL_MAP'),
('3055', '314', '_yoast_wpseo_focuskw', ''),
('3056', '314', '_yoast_wpseo_title', ''),
('3057', '314', '_yoast_wpseo_metadesc', ''),
('3058', '314', '_yoast_wpseo_meta-robots-noindex', '0'),
('3059', '314', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3060', '314', '_yoast_wpseo_meta-robots-adv', 'none'),
('3061', '314', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3062', '314', '_yoast_wpseo_redirect', ''),
('3063', '316', '_yoast_wpseo_linkdex', '0'),
('3064', '316', '_edit_last', '3'),
('3065', '316', '_edit_lock', '1373203610:3'),
('3485', '316', '_thumbnail_id', '609'),
('3067', '316', '_wpas_done_all', '1'),
('3068', '316', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Take-out.jpg'),
('3069', '316', 'price', '129000'),
('3600', '274', 'agent_email', 'noah@lmmre.com'),
('3599', '274', 'agent_name', 'Noah Meyer');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3073', '316', 'sale_type', 'sale'),
('3074', '316', 'sale_metric', 'Once off'),
('3075', '316', 'on_show', 'false'),
('3076', '316', 'woo_maps_enable', 'false'),
('3077', '316', 'woo_maps_zoom', '9'),
('3078', '316', 'woo_maps_type', 'G_NORMAL_MAP'),
('3079', '316', '_yoast_wpseo_focuskw', ''),
('3080', '316', '_yoast_wpseo_title', ''),
('3081', '316', '_yoast_wpseo_metadesc', ''),
('3082', '316', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3083', '316', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3084', '316', '_yoast_wpseo_meta-robots-adv', 'none'),
('3085', '316', '_yoast_wpseo_canonical', ''),
('3086', '316', '_yoast_wpseo_redirect', ''),
('3087', '317', '_yoast_wpseo_linkdex', '0'),
('3088', '317', '_edit_last', '3'),
('3089', '317', '_edit_lock', '1373328653:3'),
('3504', '317', '_thumbnail_id', '781'),
('3091', '317', '_wpas_done_all', '1'),
('3092', '317', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/pizzammmmm-300x277.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3093', '317', 'price', '199000'),
('3678', '314', 'agent_email', 'shawn@lmmre.com'),
('3677', '314', 'agent_name', 'Shawn Reilly'),
('3097', '317', 'sale_type', 'sale'),
('3098', '317', 'sale_metric', 'Once off'),
('3099', '317', 'on_show', 'false'),
('3100', '317', 'woo_maps_enable', 'false'),
('3101', '317', 'woo_maps_zoom', '9'),
('3102', '317', 'woo_maps_type', 'G_NORMAL_MAP'),
('3103', '317', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3104', '317', '_yoast_wpseo_title', ''),
('3105', '317', '_yoast_wpseo_metadesc', ''),
('3106', '317', '_yoast_wpseo_meta-robots-noindex', '0'),
('3107', '317', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3108', '317', '_yoast_wpseo_meta-robots-adv', 'none'),
('3109', '317', '_yoast_wpseo_canonical', ''),
('3110', '317', '_yoast_wpseo_redirect', ''),
('3111', '318', '_yoast_wpseo_linkdex', '0'),
('3112', '318', '_edit_last', '3'),
('3113', '318', '_edit_lock', '1373207432:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3464', '599', '_wp_attached_file', '2013/06/Pizza.jpg'),
('3115', '318', '_wpas_done_all', '1'),
('3116', '318', 'image', ''),
('3117', '318', 'price', '149000'),
('3118', '318', 'garages', ''),
('3119', '318', 'beds', ''),
('3120', '318', 'bathrooms', ''),
('3121', '318', 'sale_type', 'sale'),
('3122', '318', 'sale_metric', 'Once off'),
('3123', '318', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3124', '318', 'woo_maps_enable', 'false'),
('3125', '318', 'woo_maps_zoom', '9'),
('3126', '318', 'woo_maps_type', 'G_NORMAL_MAP'),
('3127', '318', '_yoast_wpseo_focuskw', ''),
('3128', '318', '_yoast_wpseo_title', ''),
('3129', '318', '_yoast_wpseo_metadesc', ''),
('3130', '318', '_yoast_wpseo_meta-robots-noindex', '0'),
('3131', '318', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3132', '318', '_yoast_wpseo_meta-robots-adv', 'none'),
('3133', '318', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3134', '318', '_yoast_wpseo_redirect', ''),
('3135', '319', '_yoast_wpseo_linkdex', '0'),
('3136', '319', '_edit_last', '3'),
('3137', '319', '_edit_lock', '1373208826:3'),
('3489', '614', '_wp_attached_file', '2013/06/burger-and-fries.jpg'),
('3139', '319', '_wpas_done_all', '1'),
('3140', '319', 'image', ''),
('3141', '319', 'price', '249000'),
('3654', '296', 'agent_name', 'Marty Ruff'),
('3144', '319', 'bathrooms', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3145', '319', 'sale_type', 'sale'),
('3146', '319', 'sale_metric', 'Once off'),
('3147', '319', 'on_show', 'false'),
('3148', '319', 'woo_maps_enable', 'false'),
('3149', '319', 'woo_maps_zoom', '9'),
('3150', '319', 'woo_maps_type', 'G_NORMAL_MAP'),
('3151', '319', '_yoast_wpseo_focuskw', ''),
('3152', '319', '_yoast_wpseo_title', ''),
('3153', '319', '_yoast_wpseo_metadesc', ''),
('3154', '319', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3155', '319', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3156', '319', '_yoast_wpseo_meta-robots-adv', 'none'),
('3157', '319', '_yoast_wpseo_canonical', ''),
('3158', '319', '_yoast_wpseo_redirect', ''),
('3159', '300', 'price', '775000'),
('3160', '309', 'price', '599000'),
('3161', '310', 'price', '149000'),
('3162', '313', 'price', '470000'),
('3163', '314', 'price', '199000'),
('3164', '326', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3165', '326', '_edit_last', '3'),
('3166', '326', '_edit_lock', '1373295814:2'),
('3169', '328', '_wp_attached_file', '2013/06/59-Amity.jpg'),
('3170', '328', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:600;s:4:\"file\";s:20:\"2013/06/59-Amity.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"59-Amity-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"59-Amity-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:19:\"Canon PowerShot A85\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1343741072;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:7:\"5.40625\";s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:7:\"0.00625\";s:5:\"title\";s:0:\"\";}}'),
('3171', '326', '_thumbnail_id', '328'),
('3172', '326', '_wpas_done_all', '1'),
('3173', '326', 'image', ''),
('3174', '326', 'address', '59 AMITY ROAD, WOODBRIDGE'),
('3175', '326', 'price', '400'),
('3176', '326', 'size', '120');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3611', '254', 'agent_name', 'Steve Miller'),
('3179', '326', 'bathrooms', '8'),
('3180', '326', 'sale_type', 'rent'),
('3181', '326', 'sale_metric', 'Per Month'),
('3182', '326', 'on_show', 'false'),
('3183', '326', 'woo_maps_enable', 'on'),
('3184', '326', 'woo_maps_address', '59 Amity Road, Woodbridge, CT 06525, USA'),
('3185', '326', 'woo_maps_long', '-72.9780743'),
('3186', '326', 'woo_maps_lat', '41.3360639'),
('3187', '326', 'woo_maps_zoom', '14');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3188', '326', 'woo_maps_type', 'G_NORMAL_MAP'),
('3189', '326', 'woo_maps_pov_pitch', '-20'),
('3190', '326', 'woo_maps_pov_yaw', '20'),
('3191', '326', '_yoast_wpseo_focuskw', ''),
('3192', '326', '_yoast_wpseo_title', ''),
('3193', '326', '_yoast_wpseo_metadesc', ''),
('3194', '326', '_yoast_wpseo_meta-robots-noindex', '0'),
('3195', '326', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3196', '326', '_yoast_wpseo_meta-robots-adv', 'none'),
('3197', '326', '_yoast_wpseo_canonical', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3198', '326', '_yoast_wpseo_redirect', ''),
('3302', '566', '_edit_lock', '1374168193:3'),
('3200', '333', '_yoast_wpseo_linkdex', '0'),
('3201', '333', '_edit_last', '3'),
('3202', '333', 'image', ''),
('3203', '333', 'garages', '2.5'),
('3584', '263', 'address', '1646 Litchfield Turnpike, Woodbridge, CT'),
('3205', '333', 'bathrooms', '16+'),
('3206', '333', 'sale_type', 'rent'),
('3207', '333', 'sale_metric', 'Per Square Foot');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3208', '333', 'on_show', 'false'),
('3209', '333', 'woo_maps_enable', 'on'),
('3210', '333', 'woo_maps_zoom', '13'),
('3211', '333', 'woo_maps_type', 'G_NORMAL_MAP'),
('3212', '333', '_yoast_wpseo_focuskw', ''),
('3213', '333', '_yoast_wpseo_title', ''),
('3214', '333', '_yoast_wpseo_metadesc', ''),
('3215', '333', '_yoast_wpseo_meta-robots-noindex', '0'),
('3216', '333', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3217', '333', '_yoast_wpseo_meta-robots-adv', 'none');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3218', '333', '_yoast_wpseo_canonical', ''),
('3219', '333', '_yoast_wpseo_redirect', ''),
('3220', '333', '_edit_lock', '1373207041:3'),
('3582', '333', 'agent_name', 'Marty Ruff'),
('3583', '333', 'agent_email', 'Marty@lmmre.com'),
('3585', '263', 'price', '4166.67'),
('3587', '263', 'agent_email', 'steve@lmmre.com'),
('3588', '36', 'agent_name', 'Diane Urbano'),
('3589', '36', 'agent_email', 'Diane@lmmre.com'),
('3596', '20', 'agent_email', 'Marty@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3598', '316', 'agent_email', 'shawn@lmmre.com'),
('3224', '333', '_wpas_done_all', '1'),
('3225', '333', 'address', '29 FLAX MILL ROAD, BRANFORD, CT'),
('3226', '333', 'price', '3.95'),
('3227', '333', 'size', '6000'),
('3228', '333', 'woo_maps_address', '29 Flax Mill Road, Branford, CT 06405, USA'),
('3229', '333', 'woo_maps_long', '-72.77812999999998'),
('3230', '333', 'woo_maps_lat', '41.303828'),
('3231', '333', 'woo_maps_pov_pitch', '-18.264735825229476'),
('3232', '333', 'woo_maps_pov_yaw', '21.507492923835827');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3301', '566', '_edit_last', '3'),
('3300', '566', '_yoast_wpseo_linkdex', '0'),
('3244', '204', '_thumbnail_id', '222'),
('3461', '597', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:259;s:6:\"height\";i:194;s:4:\"file\";s:21:\"2013/06/Ice-Cream.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"Ice-Cream-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3580', '586', 'agent_name', 'Michael Gordon'),
('3646', '506', 'agent_email', 'shawn@lmmre.com'),
('3281', '506', 'sale_type', 'sale'),
('3270', '506', '_edit_last', '3'),
('3271', '506', '_edit_lock', '1373296561:3'),
('3272', '506', '_thumbnail_id', '136');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3273', '506', '_wpas_done_all', '1'),
('3274', '506', 'image', ''),
('3275', '506', 'address', '970 STATE STREET, NEW HAVEN, CT'),
('3276', '506', 'price', '10000'),
('3277', '506', 'size', '1200'),
('3648', '582', 'agent_email', 'steve@lmmre.com'),
('3647', '582', 'agent_name', 'Steve Miller'),
('3612', '254', 'agent_email', 'steve@lmmre.com'),
('4070', '831', '_thumbnail_id', '613'),
('3606', '159', 'agent_email', 'michael@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3607', '82', 'agent_name', 'Arin Hayden'),
('3608', '82', 'agent_email', 'arin@lmmre.com'),
('3609', '326', 'agent_name', 'Michael Gordon'),
('3610', '326', 'agent_email', 'michael@lmmre.com'),
('3265', '501', '_wp_attached_file', '2013/06/29-Flax-Mill-b.jpg'),
('3266', '501', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:26:\"2013/06/29-Flax-Mill-b.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"29-Flax-Mill-b-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"29-Flax-Mill-b-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:15:\"BlackBerry 9930\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";}}'),
('3282', '506', 'sale_metric', 'Once off'),
('3283', '506', 'on_show', 'false'),
('3284', '506', 'woo_maps_enable', 'on'),
('3285', '506', 'woo_maps_address', '970 State Street, New Haven, CT 06511, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3286', '506', 'woo_maps_long', '-72.91001289999997'),
('3287', '506', 'woo_maps_lat', '41.31546700000001'),
('3288', '506', 'woo_maps_zoom', '12'),
('3289', '506', 'woo_maps_type', 'G_NORMAL_MAP'),
('3290', '506', 'woo_maps_pov_pitch', '-20'),
('3291', '506', 'woo_maps_pov_yaw', '20'),
('3292', '506', '_yoast_wpseo_focuskw', ''),
('3293', '506', '_yoast_wpseo_title', ''),
('3294', '506', '_yoast_wpseo_metadesc', ''),
('3295', '506', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3296', '506', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3297', '506', '_yoast_wpseo_meta-robots-adv', 'none'),
('3298', '506', '_yoast_wpseo_canonical', ''),
('3299', '506', '_yoast_wpseo_redirect', ''),
('3303', '566', '_wpas_done_all', '1'),
('3304', '566', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/1400-Whalley1-300x225.jpg'),
('3305', '566', 'address', '1400 WHALLEY AVENUE, NEW HAVEN'),
('3306', '566', 'price', '1200'),
('3307', '566', 'size', '1200'),
('3649', '567', 'agent_name', 'Steve Miller');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3311', '566', 'sale_type', 'rent'),
('3312', '566', 'sale_metric', 'Per Month'),
('3313', '566', 'on_show', 'false'),
('3314', '566', 'woo_maps_enable', 'on'),
('3315', '566', 'woo_maps_address', '1400 Whalley Avenue, New Haven, CT 06515, USA'),
('3316', '566', 'woo_maps_long', '-72.97579859999996'),
('3317', '566', 'woo_maps_lat', '41.333141'),
('3318', '566', 'woo_maps_zoom', '14'),
('3319', '566', 'woo_maps_type', 'G_NORMAL_MAP'),
('3320', '566', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3321', '566', 'woo_maps_pov_yaw', '20'),
('3322', '566', '_yoast_wpseo_focuskw', ''),
('3323', '566', '_yoast_wpseo_title', ''),
('3324', '566', '_yoast_wpseo_metadesc', ''),
('3325', '566', '_yoast_wpseo_meta-robots-noindex', '0'),
('3326', '566', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3327', '566', '_yoast_wpseo_meta-robots-adv', 'none'),
('3328', '566', '_yoast_wpseo_canonical', ''),
('3329', '566', '_yoast_wpseo_redirect', ''),
('3330', '567', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3331', '567', '_edit_last', '3'),
('3332', '567', '_edit_lock', '1374168180:3'),
('3333', '567', '_wpas_done_all', '1'),
('3334', '567', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/1400-Whalley-2nd-Floor-300x225.jpg'),
('3335', '567', 'address', '1400 WHALLEY AVENUE, NEW HAVEN'),
('3336', '567', 'price', '1200'),
('3337', '567', 'garages', '.23'),
('3651', '333', '_thumbnail_id', '501'),
('3339', '567', 'bathrooms', '8'),
('3340', '567', 'sale_type', 'rent');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3341', '567', 'sale_metric', 'Per Month'),
('3342', '567', 'on_show', 'false'),
('3343', '567', 'woo_maps_enable', 'on'),
('3344', '567', 'woo_maps_address', '1400 Whalley Avenue, New Haven, CT 06515, USA'),
('3345', '567', 'woo_maps_long', '-72.97579859999996'),
('3346', '567', 'woo_maps_lat', '41.333141'),
('3347', '567', 'woo_maps_zoom', '13'),
('3348', '567', 'woo_maps_type', 'G_NORMAL_MAP'),
('3349', '567', 'woo_maps_pov_pitch', '-20'),
('3350', '567', 'woo_maps_pov_yaw', '20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3351', '567', '_yoast_wpseo_focuskw', ''),
('3352', '567', '_yoast_wpseo_title', ''),
('3353', '567', '_yoast_wpseo_metadesc', ''),
('3354', '567', '_yoast_wpseo_meta-robots-noindex', '0'),
('3355', '567', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3356', '567', '_yoast_wpseo_meta-robots-adv', 'none'),
('3357', '567', '_yoast_wpseo_canonical', ''),
('3358', '567', '_yoast_wpseo_redirect', ''),
('3359', '569', '_yoast_wpseo_linkdex', '0'),
('3360', '569', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3361', '569', '_edit_lock', '1373205189:3'),
('3362', '569', '_wpas_done_all', '1'),
('3363', '569', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Westerleigh-300x163.jpg'),
('3364', '569', 'address', '154 WESTERLEIGH ROAD, NEW HAVEN'),
('3365', '569', 'size', '1200'),
('3366', '569', 'garages', ''),
('3479', '607', '_wp_attached_file', '2013/06/space-for-rent.jpg'),
('3369', '569', 'sale_type', 'rent'),
('3370', '569', 'sale_metric', 'Per Month'),
('3371', '569', 'on_show', 'false');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3372', '569', 'woo_maps_enable', 'on'),
('3373', '569', 'woo_maps_address', '154 Westerleigh Road, New Haven, CT 06515, USA'),
('3374', '569', 'woo_maps_long', '-72.9759297'),
('3375', '569', 'woo_maps_lat', '41.3329102'),
('3376', '569', 'woo_maps_zoom', '13'),
('3377', '569', 'woo_maps_type', 'G_NORMAL_MAP'),
('3378', '569', 'woo_maps_pov_pitch', '-23.27265908633242'),
('3379', '569', 'woo_maps_pov_yaw', '18.20529747145913'),
('3380', '569', '_yoast_wpseo_focuskw', ''),
('3381', '569', '_yoast_wpseo_title', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3382', '569', '_yoast_wpseo_metadesc', ''),
('3383', '569', '_yoast_wpseo_meta-robots-noindex', '0'),
('3384', '569', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3385', '569', '_yoast_wpseo_meta-robots-adv', 'none'),
('3386', '569', '_yoast_wpseo_canonical', ''),
('3387', '569', '_yoast_wpseo_redirect', ''),
('3388', '569', 'price', '1000'),
('3389', '567', 'size', '1000'),
('3390', '576', '_wp_attached_file', '2013/06/images.jpg'),
('3391', '576', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:279;s:6:\"height\";i:180;s:4:\"file\";s:18:\"2013/06/images.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"images-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3392', '582', '_yoast_wpseo_linkdex', '0'),
('3393', '582', '_edit_last', '3'),
('3394', '582', '_edit_lock', '1375792194:3'),
('3395', '582', '_wpas_done_all', '1'),
('3396', '582', 'image', ''),
('3397', '582', 'address', '150 WESTERLEIGH ROAD, NEW HAVEN'),
('3398', '582', 'price', '1400'),
('3399', '582', 'size', '2100'),
('3403', '582', 'sale_type', 'rent'),
('3404', '582', 'sale_metric', 'Per Month');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3405', '582', 'on_show', 'false'),
('3406', '582', 'woo_maps_enable', 'on'),
('3407', '582', 'woo_maps_address', '150 Westerleigh Road, New Haven, CT 06515, USA'),
('3408', '582', 'woo_maps_long', '-72.97615100000002'),
('3409', '582', 'woo_maps_lat', '41.332946'),
('3410', '582', 'woo_maps_zoom', '12'),
('3411', '582', 'woo_maps_type', 'G_NORMAL_MAP'),
('3412', '582', 'woo_maps_pov_pitch', '-20'),
('3413', '582', 'woo_maps_pov_yaw', '20'),
('3414', '582', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3415', '582', '_yoast_wpseo_title', ''),
('3416', '582', '_yoast_wpseo_metadesc', ''),
('3417', '582', '_yoast_wpseo_meta-robots-noindex', '0'),
('3418', '582', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3419', '582', '_yoast_wpseo_meta-robots-adv', 'none'),
('3420', '582', '_yoast_wpseo_canonical', ''),
('3421', '582', '_yoast_wpseo_redirect', ''),
('3556', '41', 'agent_email', 'Marty@lmmre.com'),
('3555', '41', 'agent_name', 'Marty Ruff'),
('3424', '586', '_yoast_wpseo_linkdex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3425', '586', '_edit_last', '3'),
('3426', '586', '_edit_lock', '1376507805:3'),
('3427', '586', '_wpas_done_all', '1'),
('3428', '586', 'image', ''),
('3429', '586', 'address', '206 WHALLEY AVENUE, NEW HAVEN'),
('3430', '586', 'price', '3500'),
('3431', '586', 'size', '2000'),
('3581', '586', 'agent_email', 'michael@lmmre.com'),
('3435', '586', 'sale_type', 'rent'),
('3436', '586', 'sale_metric', 'Per Month');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3437', '586', 'on_show', 'false'),
('3438', '586', 'woo_maps_enable', 'on'),
('3439', '586', 'woo_maps_address', '206 Whalley Avenue, New Haven, CT 06511, USA'),
('3440', '586', 'woo_maps_long', '-72.93950029999996'),
('3441', '586', 'woo_maps_lat', '41.3156097'),
('3442', '586', 'woo_maps_zoom', '13'),
('3443', '586', 'woo_maps_type', 'G_NORMAL_MAP'),
('3444', '586', 'woo_maps_pov_pitch', '-20'),
('3445', '586', 'woo_maps_pov_yaw', '20'),
('3446', '586', '_yoast_wpseo_focuskw', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3447', '586', '_yoast_wpseo_title', ''),
('3448', '586', '_yoast_wpseo_metadesc', ''),
('3449', '586', '_yoast_wpseo_meta-robots-noindex', '0'),
('3450', '586', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3451', '586', '_yoast_wpseo_meta-robots-adv', 'none'),
('3452', '586', '_yoast_wpseo_canonical', ''),
('3453', '586', '_yoast_wpseo_redirect', ''),
('3869', '779', 'on_show', 'false'),
('3474', '604', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:240;s:6:\"height\";i:157;s:4:\"file\";s:19:\"2013/06/Italian.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Italian-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3456', '593', '_wp_attached_file', '2013/06/office-for-lease.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3457', '593', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:276;s:6:\"height\";i:183;s:4:\"file\";s:28:\"2013/06/office-for-lease.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"office-for-lease-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3462', '314', '_thumbnail_id', '597'),
('3465', '599', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:276;s:6:\"height\";i:183;s:4:\"file\";s:17:\"2013/06/Pizza.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"Pizza-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3466', '318', '_thumbnail_id', '599'),
('3468', '602', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:266;s:6:\"height\";i:189;s:4:\"file\";s:19:\"2013/05/Daycare.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Daycare-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3469', '287', '_thumbnail_id', '602'),
('3475', '303', '_thumbnail_id', '604'),
('3477', '606', '_wp_attached_file', '2013/06/office-for-lease1.jpg'),
('3478', '606', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:276;s:6:\"height\";i:183;s:4:\"file\";s:29:\"2013/06/office-for-lease1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"office-for-lease1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3480', '607', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:684;s:6:\"height\";i:673;s:4:\"file\";s:26:\"2013/06/space-for-rent.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"space-for-rent-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"space-for-rent-300x295.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3481', '608', '_wp_attached_file', '2013/06/Now-leasing.jpg'),
('3482', '608', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:319;s:6:\"height\";i:279;s:4:\"file\";s:23:\"2013/06/Now-leasing.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Now-leasing-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Now-leasing-300x262.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:262;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3483', '609', '_wp_attached_file', '2013/06/Take-out.jpg'),
('3484', '609', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:275;s:6:\"height\";i:183;s:4:\"file\";s:20:\"2013/06/Take-out.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Take-out-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3486', '613', '_wp_attached_file', '2013/06/Break-lunch-dinner.jpg'),
('3487', '613', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:340;s:6:\"height\";i:270;s:4:\"file\";s:30:\"2013/06/Break-lunch-dinner.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"Break-lunch-dinner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"Break-lunch-dinner-300x238.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:238;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3490', '614', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1622;s:6:\"height\";i:1200;s:4:\"file\";s:28:\"2013/06/burger-and-fries.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"burger-and-fries-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"burger-and-fries-300x221.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"burger-and-fries-1024x757.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:757;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:5;s:6:\"credit\";s:26:\"ANGELA AURELIO PHOTOGRAPHY\";s:6:\"camera\";s:10:\"NIKON D700\";s:7:\"caption\";s:45:\",OTIV FOOD DRINKS COCKTAILS RESTAURANT LOUNGE\";s:17:\"created_timestamp\";i:1350967659;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"60\";s:3:\"iso\";s:3:\"200\";s:13:\"shutter_speed\";s:5:\"0.002\";s:5:\"title\";s:5:\"MOTIV\";}}'),
('3491', '319', '_thumbnail_id', '614'),
('3492', '615', '_wp_attached_file', '2013/06/Westerleigh.jpg'),
('3493', '615', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:304;s:6:\"height\";i:166;s:4:\"file\";s:23:\"2013/06/Westerleigh.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Westerleigh-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Westerleigh-300x163.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:163;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3494', '569', '_thumbnail_id', '615'),
('3495', '569', 'beds', ''),
('3496', '569', 'bathrooms', ''),
('3497', '617', '_wp_attached_file', '2013/06/Waterfront.jpg'),
('3498', '617', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:550;s:6:\"height\";i:412;s:4:\"file\";s:22:\"2013/06/Waterfront.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"Waterfront-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"Waterfront-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3499', '618', '_wp_attached_file', '2013/06/Mexican.jpg'),
('3500', '618', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:275;s:6:\"height\";i:183;s:4:\"file\";s:19:\"2013/06/Mexican.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Mexican-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3503', '229', '_thumbnail_id', '576'),
('3505', '643', '_wp_attached_file', '2013/06/wedding.jpg'),
('3506', '643', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:900;s:6:\"height\";i:743;s:4:\"file\";s:19:\"2013/06/wedding.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"wedding-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"wedding-300x247.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:247;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3615', '193', 'agent_name', 'Marty Ruff'),
('3614', '240', 'agent_email', 'steve@lmmre.com'),
('3513', '301', 'woo_maps_long', '-72.8042797'),
('3514', '301', 'woo_maps_lat', '41.3266911'),
('3515', '301', 'woo_maps_pov_pitch', '-20'),
('3516', '301', 'woo_maps_pov_yaw', '20'),
('3909', '801', '_edit_lock', '1374609824:3'),
('3898', '794', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:34:\"2013/06/1400-Whalley-2nd-Floor.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1400-Whalley-2nd-Floor-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1400-Whalley-2nd-Floor-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.399999999999999911182158029987476766109466552734375;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:9:\"iPhone 4S\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1374136525;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"4.28\";s:3:\"iso\";s:2:\"50\";s:13:\"shutter_speed\";s:17:\"0.000706214689266\";s:5:\"title\";s:0:\"\";}}'),
('3861', '779', '_yoast_wpseo_linkdex', '0'),
('3862', '779', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3863', '779', '_edit_lock', '1374079614:3'),
('3864', '779', '_wpas_done_all', '1'),
('3865', '779', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/3035-W-300x225.jpg'),
('3866', '779', 'address', '3035 whitney ave hamden'),
('3867', '779', 'sale_type', 'sale'),
('3868', '779', 'sale_metric', 'Once off'),
('3522', '586', 'agent', 'sfjks@yahoo.com'),
('3645', '506', 'agent_name', 'Shawn Reilly'),
('3524', '314', 'agentname', 'Shawn Reilly'),
('3525', '314', 'agentcontact', 'shawn@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3526', '316', 'agentname', 'Shawn Reilly'),
('3527', '316', 'agentcontact', 'Shawn@lmmre.com'),
('3528', '313', 'agentname', 'Shawn Reilly'),
('3529', '313', 'agentcontact', 'shawn@lmmre.com'),
('3530', '310', 'agentname', 'Shawn Reilly'),
('3531', '310', 'agentcontact', 'shawn@lmmre.com'),
('3532', '36', 'agentname', 'Diane Urbano'),
('3533', '36', 'agentcontact', 'Diane@lmmre.com'),
('3534', '250', 'agentname', 'STEVE MILLER'),
('3535', '250', 'agentcontact', 'STEVE@LMMRE.COM');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3536', '41', 'agentname', 'MARTY RUFF'),
('3537', '41', 'agentcontact', 'marty@lmmre.com'),
('3538', '717', '_wp_attached_file', '2013/05/ServeAttachment.jpeg'),
('3539', '717', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:627;s:6:\"height\";i:470;s:4:\"file\";s:28:\"2013/05/ServeAttachment.jpeg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"ServeAttachment-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"ServeAttachment-300x224.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3540', '20', '_thumbnail_id', '717'),
('3541', '58', 'agentname', 'MARTY RUFF'),
('3542', '58', 'agentcontact', 'marty@lmmre.com'),
('3543', '68', 'agentname', 'STEVE MILLER'),
('3544', '68', 'agentcontact', 'STEVE@LMMRE.COM'),
('3545', '70', 'agentname', 'MARTY RUFF');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3546', '70', 'agentcontact', 'marty@lmmre.com'),
('3547', '293', 'agentname', 'Shawn Reilly'),
('3548', '293', 'agentcontact', 'shawn@lmmre.com'),
('3549', '296', 'agentname', 'MARTY RUFF'),
('3550', '296', 'agentcontact', 'marty@lmmre.com'),
('3551', '94', 'price', '----CALL FOR INFO----'),
('3553', '293', 'agent_name', 'Shawn Reilly'),
('3554', '293', 'agent_email', 'shawn@lmmre.com'),
('3999', '582', '_thumbnail_id', '606'),
('3998', '82', '_wp_trash_meta_time', '1375753019');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3997', '82', '_wp_trash_meta_status', 'publish'),
('3616', '193', 'agent_email', 'Marty@lmmre.com'),
('3618', '147', 'agent_email', 'noah@lmmre.com'),
('3620', '141', 'agent_email', 'info@lmmre.com'),
('3622', '86', 'agent_email', 'steve@lmmre.com'),
('3624', '63', 'agent_email', 'Marty@lmmre.com'),
('3625', '264', 'agent_name', 'Steve Miller'),
('3626', '264', 'agent_email', 'steve@lmmre.com'),
('3628', '569', 'agent_email', 'steve@lmmre.com'),
('3629', '189', 'agent_name', 'Marty Ruff');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3630', '189', 'agent_email', 'Marty@lmmre.com'),
('3634', '258', 'agent_name', 'Steve Miller'),
('3635', '258', 'agent_email', 'steve@lmmre.com'),
('3637', '47', 'agent_email', 'Marty@lmmre.com'),
('3639', '204', 'agent_email', 'Marty@lmmre.com'),
('3640', '135', 'agent_name', 'Shawn Reilly'),
('3641', '135', 'agent_email', 'shawn@lmmre.com'),
('3644', '566', 'agent_email', 'steve@lmmre.com'),
('3650', '567', 'agent_email', 'steve@lmmre.com'),
('3652', '319', 'agent_name', 'Shawn Reilly');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3653', '319', 'agent_email', 'shawn@lmmre.com'),
('3655', '296', 'agent_email', 'Marty@lmmre.com'),
('3656', '318', 'size', '2000'),
('3657', '318', 'agent_name', 'Shawn Reilly'),
('3658', '318', 'agent_email', 'shawn@lmmre.com'),
('3659', '309', 'agent_name', 'Shawn Reilly'),
('3660', '309', 'agent_email', 'shawn@lmmre.com'),
('3661', '309', 'woo_maps_pov_pitch', '-17.812957566861748'),
('3662', '309', 'woo_maps_pov_yaw', '32.49752830346671'),
('3663', '58', 'agent_name', 'Marty Ruff');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3664', '58', 'agent_email', 'Marty@lmmre.com'),
('3665', '166', 'agent_name', 'Michael Gordon'),
('3666', '166', 'agent_email', 'michael@lmmre.com'),
('3667', '65', 'agent_name', 'Steve Miller'),
('3668', '65', 'agent_email', 'steve@lmmre.com'),
('3670', '260', 'agent_email', 'steve@lmmre.com'),
('3672', '250', 'agent_email', 'steve@lmmre.com'),
('3674', '267', 'agent_email', 'steve@lmmre.com'),
('3675', '317', 'agent_name', 'Shawn Reilly'),
('3676', '317', 'agent_email', 'shawn@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3683', '308', 'agent_name', 'Shawn Reilly'),
('3684', '308', 'agent_email', 'shawn@lmmre.com'),
('3685', '305', 'agent_name', 'Shawn Reilly'),
('3686', '305', 'agent_email', 'shawn@lmmre.com'),
('3687', '303', 'agent_name', 'Shawn Reilly'),
('3688', '303', 'agent_email', 'shawn@lmmre.com'),
('3689', '735', '_wp_attached_file', '2013/06/Pizza-YUMM.jpg'),
('3690', '735', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:333;s:4:\"file\";s:22:\"2013/06/Pizza-YUMM.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"Pizza-YUMM-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"Pizza-YUMM-300x208.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:208;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3691', '300', 'size', '2000'),
('3692', '300', 'agent_name', 'Shawn Reilly');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3693', '300', 'agent_email', 'shawn@lmmre.com'),
('3695', '736', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:280;s:4:\"file\";s:18:\"2013/05/patio.jpeg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"patio-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"patio-300x175.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3696', '290', 'agent_name', 'Shawn Reilly'),
('3697', '290', 'agent_email', 'shawn@lmmre.com'),
('3698', '737', '_wp_attached_file', '2013/05/bistro.jpg'),
('3699', '737', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:521;s:4:\"file\";s:18:\"2013/05/bistro.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"bistro-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"bistro-230x300.jpg\";s:5:\"width\";i:230;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3700', '288', 'agent_name', 'Shawn Reilly'),
('3701', '288', 'agent_email', 'shawn@lmmre.com'),
('3702', '287', 'agent_name', 'Shawn Reilly'),
('3703', '287', 'agent_email', 'shawn@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3705', '281', 'agent_email', 'Harold@lmmre.com'),
('3707', '277', 'agent_email', 'steve@lmmre.com'),
('3708', '275', 'agent_name', 'Steve Miller'),
('3709', '275', 'agent_email', 'steve@lmmre.com'),
('3710', '271', 'agent_name', 'Steve Miller'),
('3711', '271', 'agent_email', 'steve@lmmre.com'),
('3712', '249', 'agent_name', 'Steve Miller'),
('3713', '249', 'agent_email', 'steve@lmmre.com'),
('3715', '243', 'agent_email', 'steve@lmmre.com'),
('3716', '217', 'agent_name', 'Marty Ruff');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3717', '217', 'agent_email', 'Marty@lmmre.com'),
('3720', '70', 'agent_name', 'Marty Ruff'),
('3721', '70', 'agent_email', 'Marty@lmmre.com'),
('3723', '75', 'agent_email', 'steve@lmmre.com'),
('3725', '89', 'agent_email', 'Marty@lmmre.com'),
('3727', '92', 'agent_email', 'Marty@lmmre.com'),
('3728', '94', 'agent_name', 'Marty Ruff'),
('3729', '94', 'agent_email', 'Marty@lmmre.com'),
('3730', '96', 'agent_name', 'Marty Ruff'),
('3731', '96', 'agent_email', 'Marty@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3732', '100', 'agent_name', 'Michael Gordon'),
('3733', '100', 'agent_email', 'michael@lmmre.com'),
('3734', '105', 'agent_name', 'Michael Gordon'),
('3735', '105', 'agent_email', 'michael@lmmre.com'),
('3736', '107', 'agent_name', 'Michael Gordon'),
('3737', '107', 'agent_email', 'michael@lmmre.com'),
('3739', '110', 'agent_email', 'michael@lmmre.com'),
('3740', '745', '_wp_attached_file', '2013/05/opportunity.jpg'),
('3741', '745', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:798;s:6:\"height\";i:601;s:4:\"file\";s:23:\"2013/05/opportunity.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"opportunity-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"opportunity-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:6.70000000000000017763568394002504646778106689453125;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:27:\"HP PhotoSmart R707 (V01.00)\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1124468790;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"17.5\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:7:\"0.00232\";s:5:\"title\";s:0:\"\";}}'),
('3742', '113', '_thumbnail_id', '745');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3743', '113', 'agent_name', 'Steve Miller'),
('3744', '113', 'agent_email', 'steve@lmmre.com'),
('3746', '114', 'agent_email', 'Marty@lmmre.com'),
('3747', '118', 'agent_name', 'Marty Ruff'),
('3748', '118', 'agent_email', 'marty@lmmre.com'),
('3749', '747', '_wp_attached_file', '2013/05/success-sign-300x199.jpg'),
('3750', '747', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:199;s:4:\"file\";s:32:\"2013/05/success-sign-300x199.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"success-sign-300x199-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3751', '120', 'agent_name', 'Michael Gordon'),
('3752', '120', 'agent_email', 'michael@lmmre.com'),
('3753', '121', 'agent_name', 'Noah Meyer');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3754', '121', 'agent_email', 'noah@lmmre.com'),
('3755', '123', 'agent_name', 'Steve Miller'),
('3756', '123', 'agent_email', 'steve@lmmre.com'),
('3757', '124', 'agent_name', 'Steve Miller'),
('3758', '124', 'agent_email', 'steve@lmmre.com'),
('3759', '126', '_thumbnail_id', '747'),
('3760', '126', 'agent_name', 'Steve Miller'),
('3761', '126', 'agent_email', 'steve@lmmre.com'),
('3762', '750', '_wp_attached_file', '2013/05/glass_of_wine_by_hiddenrainbow-d4y756j.png'),
('3763', '750', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:50:\"2013/05/glass_of_wine_by_hiddenrainbow-d4y756j.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:50:\"glass_of_wine_by_hiddenrainbow-d4y756j-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:50:\"glass_of_wine_by_hiddenrainbow-d4y756j-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3764', '129', 'agent_name', 'Shawn Reilly'),
('3765', '129', 'agent_email', 'shawn@lmmre.com'),
('3767', '138', 'agent_email', 'noah@lmmre.com'),
('3768', '143', 'address', '2985 WHITNEY AVENUE, NEW HAVEN'),
('3769', '143', 'size', '744'),
('3770', '143', 'agent_name', 'Noah Meyer'),
('3771', '143', 'agent_email', 'noah@lmmre.com'),
('3772', '143', 'woo_maps_address', '2985 Whitney Avenue, Hamden, CT 06518, USA'),
('3773', '143', 'woo_maps_long', '-72.89822199999998'),
('3774', '143', 'woo_maps_lat', '41.4042134');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3775', '143', 'woo_maps_pov_pitch', '-20'),
('3776', '143', 'woo_maps_pov_yaw', '20'),
('3778', '153', 'agent_email', 'michael@lmmre.com'),
('3781', '169', 'agent_name', 'Michael Gordon'),
('3782', '169', 'agent_email', 'michael@lmmre.com'),
('3783', '173', 'address', '497 MAIN STREET, ANSONIA'),
('3785', '173', 'agent_email', 'Marty@lmmre.com'),
('3786', '177', 'agent_name', 'Marty Ruff'),
('3787', '177', 'agent_email', 'Marty@lmmre.com'),
('3788', '185', 'agent_name', 'Marty Ruff');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3789', '185', 'agent_email', 'Marty@lmmre.com'),
('3790', '179', 'agent_name', 'Marty Ruff'),
('3791', '179', 'agent_email', 'Marty@lmmre.com'),
('3792', '181', 'agent_name', 'Steve Miller'),
('3793', '181', 'agent_email', 'steve@lmmre.com'),
('3794', '187', 'size', '5600 & 5500'),
('3795', '187', 'agent_name', 'Arin Hayden'),
('3796', '187', 'agent_email', 'arin@lmmre.com'),
('3797', '195', 'agent_name', 'Marty Ruff'),
('3798', '195', 'agent_email', 'Marty@lmmre.com');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3799', '197', 'agent_name', 'Marty Ruff'),
('3800', '197', 'agent_email', 'Marty@lmmre.com'),
('3801', '201', 'agent_name', 'Marty Ruff'),
('3802', '201', 'agent_email', 'Marty@lmmre.com'),
('3804', '205', 'agent_email', 'Marty@lmmre.com'),
('3805', '226', 'agent_name', 'Bernie Diana'),
('3806', '226', 'agent_email', 'bernie@lmmre.com'),
('3807', '229', 'agent_name', 'BERNIE DIANA'),
('3808', '229', 'agent_email', 'BERNIE@LMMRE.COM'),
('3809', '233', 'agent_name', 'Steve Miller');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3810', '233', 'agent_email', 'steve@lmmre.com'),
('3811', '235', 'agent_name', 'Steve Miller'),
('3812', '235', 'agent_email', 'steve@lmmre.com'),
('3813', '238', 'agent_name', 'Steve Miller'),
('3814', '238', 'agent_email', 'steve@lmmre.com'),
('3815', '754', '_wp_attached_file', '2013/05/Banquet.jpg'),
('3816', '754', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:448;s:6:\"height\";i:292;s:4:\"file\";s:19:\"2013/05/Banquet.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Banquet-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"Banquet-300x195.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:195;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3817', '284', 'agent_name', 'Shawn Reilly'),
('3818', '284', 'agent_email', 'shawn@lmmre.com'),
('3823', '758', '_edit_lock', '1373252295:3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3824', '758', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/65-w-300x225.jpg'),
('3825', '758', 'address', '57 WOODMONT ROAD, MILFORD, CT'),
('3826', '758', 'price', '7'),
('3827', '758', 'size', '5400 & 3600'),
('3828', '758', 'sale_type', 'rent'),
('3829', '758', 'sale_metric', 'Per Square Foot'),
('3830', '758', 'on_show', 'false'),
('3831', '758', 'agent_name', 'Steve Miller'),
('3832', '758', 'agent_email', 'steve@lmmre.com'),
('3833', '758', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3834', '758', 'woo_maps_zoom', '14'),
('3835', '758', 'woo_maps_type', 'G_NORMAL_MAP'),
('3836', '758', '_yoast_wpseo_focuskw', ''),
('3837', '758', '_yoast_wpseo_title', ''),
('3838', '758', '_yoast_wpseo_metadesc', ''),
('3839', '758', '_yoast_wpseo_meta-robots-noindex', '0'),
('3840', '758', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3841', '758', '_yoast_wpseo_meta-robots-adv', 'none'),
('3842', '758', '_yoast_wpseo_canonical', ''),
('3843', '758', '_yoast_wpseo_redirect', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3844', '758', 'woo_maps_address', '57 Woodmont Road, Milford, CT 06460, USA'),
('3845', '759', '_wp_attached_file', '2013/07/57-woodmont.jpg'),
('3846', '759', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:328;s:6:\"height\";i:246;s:4:\"file\";s:23:\"2013/07/57-woodmont.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"57-woodmont-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"57-woodmont-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3847', '760', '_wp_attached_file', '2013/07/65-w.jpg'),
('3848', '760', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:16:\"2013/07/65-w.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"65-w-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"65-w-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3849', '758', '_thumbnail_id', '760'),
('3850', '758', '_wpas_done_all', '1'),
('3851', '758', 'bathrooms', '15+'),
('3852', '758', 'woo_maps_long', '-73.02383199999997'),
('3853', '758', 'woo_maps_lat', '41.239424');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3854', '758', 'woo_maps_pov_pitch', '-20'),
('3855', '758', 'woo_maps_pov_yaw', '20'),
('3871', '779', 'agent_email', 'BERNIE@LMMRE.COM'),
('3872', '779', 'woo_maps_enable', 'on'),
('3873', '779', 'woo_maps_address', '3035 Whitney Avenue, Hamden, CT 06518, USA'),
('3874', '779', 'woo_maps_long', '-72.89854000000003'),
('3875', '779', 'woo_maps_lat', '41.405712'),
('3876', '779', 'woo_maps_zoom', '14'),
('3877', '779', 'woo_maps_type', 'G_NORMAL_MAP'),
('3878', '779', 'woo_maps_pov_pitch', '-20');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3879', '779', 'woo_maps_pov_yaw', '20'),
('3880', '779', '_yoast_wpseo_focuskw', ''),
('3881', '779', '_yoast_wpseo_title', ''),
('3882', '779', '_yoast_wpseo_metadesc', ''),
('3883', '779', '_yoast_wpseo_meta-robots-noindex', '0'),
('3884', '779', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3885', '779', '_yoast_wpseo_meta-robots-adv', 'none'),
('3886', '779', '_yoast_wpseo_canonical', ''),
('3887', '779', '_yoast_wpseo_redirect', ''),
('3888', '781', '_wp_attached_file', '2013/06/pizzammmmm.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3889', '781', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:424;s:6:\"height\";i:392;s:4:\"file\";s:22:\"2013/06/pizzammmmm.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"pizzammmmm-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"pizzammmmm-300x277.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:277;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3890', '779', 'bathrooms', '9'),
('3891', '779', 'price', 'CALL LISTING AGENT'),
('3892', '779', 'size', '6000+'),
('3893', '779', 'garages', '.78'),
('3894', '779', '_thumbnail_id', '786'),
('3895', '786', '_wp_attached_file', '2013/07/3035-W.jpg'),
('3896', '786', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1632;s:6:\"height\";i:1224;s:4:\"file\";s:18:\"2013/07/3035-W.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"3035-W-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"3035-W-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"3035-W-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.399999999999999911182158029987476766109466552734375;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:9:\"iPhone 4S\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1371115380;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"4.28\";s:3:\"iso\";s:2:\"50\";s:13:\"shutter_speed\";s:16:\"0.00595238095238\";s:5:\"title\";s:0:\"\";}}'),
('3899', '567', '_thumbnail_id', '794'),
('3904', '566', '_thumbnail_id', '797');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3902', '797', '_wp_attached_file', '2013/06/1400-Whalley1.jpg'),
('3903', '797', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:480;s:4:\"file\";s:25:\"2013/06/1400-Whalley1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1400-Whalley1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1400-Whalley1-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.399999999999999911182158029987476766109466552734375;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:9:\"iPhone 4S\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1374136558;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"4.28\";s:3:\"iso\";s:2:\"50\";s:13:\"shutter_speed\";s:17:\"0.000471031559114\";s:5:\"title\";s:0:\"\";}}'),
('3996', '586', '_thumbnail_id', '818'),
('3907', '801', '_wp_attached_file', '2013/07/206-Whalley-e1374609810604.jpg'),
('3908', '801', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1296;s:6:\"height\";i:968;s:4:\"file\";s:38:\"2013/07/206-Whalley-e1374609810604.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"206-Whalley-e1374609810604-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"206-Whalley-e1374609810604-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:39:\"206-Whalley-e1374609810604-1024x764.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:764;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 4\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1372246947;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.85\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00645161290323\";s:5:\"title\";s:0:\"\";}}'),
('3910', '801', '_yoast_wpseo_linkdex', '0'),
('3911', '801', '_wp_attachment_backup_sizes', 'a:4:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:1296;s:6:\"height\";i:968;s:4:\"file\";s:15:\"206-Whalley.jpg\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:23:\"206-Whalley-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:23:\"206-Whalley-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:10:\"large-orig\";a:4:{s:4:\"file\";s:24:\"206-Whalley-1024x764.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:764;s:9:\"mime-type\";s:10:\"image/jpeg\";}}'),
('3912', '801', '_edit_last', '3'),
('3913', '804', '_yoast_wpseo_linkdex', '0'),
('3914', '804', '_edit_last', '3');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3915', '804', '_edit_lock', '1374759902:3'),
('3916', '805', '_wp_attached_file', '2013/07/448-Washington-Ave.jpg'),
('3917', '805', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2275;s:6:\"height\";i:1514;s:4:\"file\";s:30:\"2013/07/448-Washington-Ave.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"448-Washington-Ave-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"448-Washington-Ave-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"448-Washington-Ave-1024x681.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:681;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:4.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:40:\"KODAK EASYSHARE C643 ZOOM DIGITAL CAMERA\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1213959423;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"6\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:15:\"0.0011320002377\";s:5:\"title\";s:0:\"\";}}'),
('3918', '804', '_thumbnail_id', '805'),
('3919', '804', 'image', ''),
('3920', '804', 'address', '444 WASHINGTON AVENUE, NORTH HAVEN'),
('3921', '804', 'price', '12.00 '),
('3922', '804', 'size', '1231'),
('3923', '804', 'garages', '.98'),
('3924', '804', 'bathrooms', '8');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3925', '804', 'sale_type', 'rent'),
('3926', '804', 'sale_metric', 'Per Square Foot'),
('3927', '804', 'on_show', 'false'),
('3928', '804', 'agent_name', 'MARTY RUFF'),
('3929', '804', 'woo_maps_enable', 'on'),
('3930', '804', 'woo_maps_zoom', '13'),
('3931', '804', 'woo_maps_type', 'G_NORMAL_MAP'),
('3932', '804', '_yoast_wpseo_focuskw', ''),
('3933', '804', '_yoast_wpseo_title', ''),
('3934', '804', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3935', '804', '_yoast_wpseo_meta-robots-noindex', '0'),
('3936', '804', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3937', '804', '_yoast_wpseo_meta-robots-adv', 'none'),
('3938', '804', '_yoast_wpseo_canonical', ''),
('3939', '804', '_yoast_wpseo_redirect', ''),
('3940', '804', '_wpas_done_all', '1'),
('3941', '804', 'agent_email', 'marty@lmmre.com'),
('3942', '804', 'woo_maps_address', '444 Washington Avenue, North Haven, CT 06473, USA'),
('3943', '804', 'woo_maps_long', '-72.8422989'),
('3944', '804', 'woo_maps_lat', '41.4111956');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3945', '804', 'woo_maps_pov_pitch', '-20'),
('3946', '804', 'woo_maps_pov_yaw', '20'),
('3949', '809', '_yoast_wpseo_linkdex', '0'),
('3950', '809', '_edit_last', '3'),
('3951', '809', '_edit_lock', '1374773372:3'),
('3952', '810', '_wp_attached_file', '2013/07/1308-State-front.jpg'),
('3953', '810', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:768;s:4:\"file\";s:28:\"2013/07/1308-State-front.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"1308-State-front-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"1308-State-front-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3954', '811', '_wp_attached_file', '2013/07/1308-State-church.jpg'),
('3955', '811', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:29:\"2013/07/1308-State-church.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"1308-State-church-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"1308-State-church-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3956', '812', '_wp_attached_file', '2013/07/108-State-barber.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3957', '812', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:28:\"2013/07/108-State-barber.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"108-State-barber-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"108-State-barber-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3958', '813', '_wp_attached_file', '2013/07/1308-State-side.jpg'),
('3959', '813', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:768;s:4:\"file\";s:27:\"2013/07/1308-State-side.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"1308-State-side-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"1308-State-side-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3960', '814', '_wp_attached_file', '2013/07/1308-State-apt.jpg'),
('3961', '814', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:26:\"2013/07/1308-State-apt.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"1308-State-apt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"1308-State-apt-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";i:0;s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:0;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";i:0;s:3:\"iso\";i:0;s:13:\"shutter_speed\";i:0;s:5:\"title\";s:0:\"\";}}'),
('3962', '809', '_thumbnail_id', '810'),
('3963', '809', '_wpas_done_all', '1'),
('3964', '809', 'image', ''),
('3965', '809', 'address', '1308 STATE STREET, NEW HAVEN'),
('3966', '809', 'price', '185000');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3967', '809', 'size', '2892'),
('3968', '809', 'garages', '.1'),
('3969', '809', 'beds', '4'),
('3970', '809', 'sale_type', 'sale'),
('3971', '809', 'sale_metric', 'Once off'),
('3972', '809', 'on_show', 'false'),
('3973', '809', 'agent_name', 'STEVE MILLER'),
('3974', '809', 'agent_email', 'steve@lmmre.com'),
('3975', '809', 'woo_maps_enable', 'on'),
('3976', '809', 'woo_maps_address', '1308 State Street, New Haven, CT 06511, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3977', '809', 'woo_maps_long', '-72.89940100000001'),
('3978', '809', 'woo_maps_lat', '41.319199'),
('3979', '809', 'woo_maps_zoom', '12'),
('3980', '809', 'woo_maps_type', 'G_NORMAL_MAP'),
('3981', '809', 'woo_maps_pov_pitch', '-20'),
('3982', '809', 'woo_maps_pov_yaw', '20'),
('3983', '809', '_yoast_wpseo_focuskw', ''),
('3984', '809', '_yoast_wpseo_title', ''),
('3985', '809', '_yoast_wpseo_metadesc', ''),
('3986', '809', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('3987', '809', '_yoast_wpseo_meta-robots-nofollow', '0'),
('3988', '809', '_yoast_wpseo_meta-robots-adv', 'none'),
('3989', '809', '_yoast_wpseo_canonical', ''),
('3990', '809', '_yoast_wpseo_redirect', ''),
('3994', '818', '_wp_attached_file', '2013/06/206-Whalley.jpg'),
('3995', '818', '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:239;s:4:\"file\";s:23:\"2013/06/206-Whalley.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"206-Whalley-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"206-Whalley-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:10:{s:8:\"aperture\";d:2.79999999999999982236431605997495353221893310546875;s:6:\"credit\";s:6:\"Picasa\";s:6:\"camera\";s:8:\"iPhone 4\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";i:1372246947;s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.85\";s:3:\"iso\";s:2:\"80\";s:13:\"shutter_speed\";s:16:\"0.00645161290323\";s:5:\"title\";s:0:\"\";}}'),
('4000', '258', '_wp_trash_meta_status', 'publish'),
('4001', '258', '_wp_trash_meta_time', '1376511093'),
('4002', '247', '_wp_trash_meta_status', 'publish'),
('4003', '247', '_wp_trash_meta_time', '1376511113');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4004', '240', '_wp_trash_meta_status', 'publish'),
('4005', '240', '_wp_trash_meta_time', '1376511125'),
('4006', '193', '_wp_trash_meta_status', 'publish'),
('4007', '193', '_wp_trash_meta_time', '1376511143'),
('4008', '58', '_wp_trash_meta_status', 'publish'),
('4009', '58', '_wp_trash_meta_time', '1376511257'),
('4010', '827', '_yoast_wpseo_linkdex', '0'),
('4011', '827', '_edit_last', '3'),
('4012', '827', '_edit_lock', '1376577797:3'),
('4014', '827', '_wpas_done_all', '1');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4015', '827', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/burger-and-fries-300x221.jpg'),
('4016', '827', 'address', 'SOUTHINGTON'),
('4017', '827', 'price', '149000'),
('4018', '827', 'size', '1200'),
('4019', '827', 'sale_type', 'sale'),
('4020', '827', 'sale_metric', 'Once off'),
('4021', '827', 'on_show', 'false'),
('4022', '827', 'agent_name', 'SHAWN REILLY'),
('4023', '827', 'agent_email', 'shawn@lmmre.com'),
('4024', '827', 'woo_maps_enable', 'on');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4025', '827', 'woo_maps_address', 'Southington, CT, USA'),
('4026', '827', 'woo_maps_long', '-72.87760129999998'),
('4027', '827', 'woo_maps_lat', '41.5964869'),
('4028', '827', 'woo_maps_zoom', '9'),
('4029', '827', 'woo_maps_type', 'G_NORMAL_MAP'),
('4030', '827', 'woo_maps_pov_pitch', '-20'),
('4031', '827', 'woo_maps_pov_yaw', '20'),
('4032', '827', '_yoast_wpseo_focuskw', ''),
('4033', '827', '_yoast_wpseo_title', ''),
('4034', '827', '_yoast_wpseo_metadesc', '');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4035', '827', '_yoast_wpseo_meta-robots-noindex', '0'),
('4036', '827', '_yoast_wpseo_meta-robots-nofollow', '0'),
('4037', '827', '_yoast_wpseo_meta-robots-adv', 'none'),
('4038', '827', '_yoast_wpseo_canonical', ''),
('4039', '827', '_yoast_wpseo_redirect', ''),
('4041', '831', '_yoast_wpseo_linkdex', '0'),
('4042', '831', '_edit_last', '3'),
('4043', '831', '_edit_lock', '1377780483:3'),
('4044', '831', '_wpas_done_all', '1'),
('4045', '831', 'image', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Break-lunch-dinner-300x238.jpg');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4046', '831', 'address', 'NEW HAVEN'),
('4047', '831', 'price', '119000'),
('4048', '831', 'size', '3800'),
('4049', '831', 'sale_type', 'sale'),
('4050', '831', 'sale_metric', 'Once off'),
('4051', '831', 'on_show', 'false'),
('4052', '831', 'agent_name', 'SHAWN REILLY'),
('4053', '831', 'agent_email', 'shawn@lmmre.com'),
('4054', '831', 'woo_maps_enable', 'on'),
('4055', '831', 'woo_maps_address', 'New Haven, CT, USA');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4056', '831', 'woo_maps_long', '-72.92788350000001'),
('4057', '831', 'woo_maps_lat', '41.308274'),
('4058', '831', 'woo_maps_zoom', '9'),
('4059', '831', 'woo_maps_type', 'G_NORMAL_MAP'),
('4060', '831', 'woo_maps_pov_pitch', '-20'),
('4061', '831', 'woo_maps_pov_yaw', '20'),
('4062', '831', '_yoast_wpseo_focuskw', ''),
('4063', '831', '_yoast_wpseo_title', ''),
('4064', '831', '_yoast_wpseo_metadesc', ''),
('4065', '831', '_yoast_wpseo_meta-robots-noindex', '0');

INSERT INTO `wp_2_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
('4066', '831', '_yoast_wpseo_meta-robots-nofollow', '0'),
('4067', '831', '_yoast_wpseo_meta-robots-adv', 'none'),
('4068', '831', '_yoast_wpseo_canonical', ''),
('4069', '831', '_yoast_wpseo_redirect', '');

--
-- Table structure for table `wp_2_posts`
--

CREATE TABLE `wp_2_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=837 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_2_posts`
--

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('698', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=698', '0', 'wooframework', '', '0'),
('155', '3', '2013-07-08 01:06:31', '2013-07-08 01:06:31', 'ONE ROOM OFFICE NEAR MERRITT PARKWAY. $550.00/MONTH GROSS.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MICHAEL GORDON\r\n\r\n203-389-5377 X25\r\n\r\nmichael@lmmre.com', '59 AMITY ROAD, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '153-autosave', '', '', '2013-07-08 01:06:31', '2013-07-08 01:06:31', '', '153', 'http://www.lmmre.com/property/2013/05/24/153-autosave/', '0', 'revision', '', '0'),
('6', '1', '2013-05-03 14:06:37', '2013-05-03 14:06:37', '', 'Company', '', 'publish', 'open', 'open', '', 'company', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=6', '3', 'nav_menu_item', '', '0'),
('7', '1', '2013-05-03 14:06:37', '2013-05-03 14:06:37', '', 'Search Properties', '', 'publish', 'open', 'open', '', 'search-properties', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=7', '2', 'nav_menu_item', '', '0'),
('8', '1', '2013-05-03 14:06:37', '2013-05-03 14:06:37', '', 'Services', '', 'publish', 'open', 'open', '', 'services', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=8', '4', 'nav_menu_item', '', '0'),
('9', '1', '2013-05-03 14:06:37', '2013-05-03 14:06:37', '', 'Clients', '', 'publish', 'open', 'open', '', 'clients', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=9', '5', 'nav_menu_item', '', '0'),
('10', '1', '2013-05-03 14:06:37', '2013-05-03 14:06:37', '', 'Blog', '', 'publish', 'open', 'open', '', 'blog', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=10', '6', 'nav_menu_item', '', '0'),
('11', '1', '2013-05-03 14:06:37', '2013-05-03 14:06:37', '', 'Contact', '', 'publish', 'open', 'open', '', 'contact', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=11', '7', 'nav_menu_item', '', '0'),
('14', '1', '2013-05-03 14:08:44', '2013-05-03 14:08:44', '', 'Home', '', 'publish', 'open', 'open', '', 'home', '', '', '2013-05-03 14:08:44', '2013-05-03 14:08:44', '', '0', 'http://www.lmmre.com/property/?p=14', '1', 'nav_menu_item', '', '0'),
('15', '1', '2013-05-03 14:23:35', '2013-05-03 14:23:35', 'WAS OPERATING CONVALESCENT HOME UNTIL SEPTEMBER 2004. ALSO GOOD FOR MEDICAL, ELDERLY, ASSISTED LIVING,\r\nPOSSIBLE RESIDENTIAL. NOTES: 1. ASSESSMENT AND TAXES ARE UNDER REVIEW AND ARE SUBJECT TO CHANGE.\r\nTHIS PROPERTY ALLOWS UP TO 4 FLOORS IF PARKING REQUIREMENT IS MET. SOME OWNER FINANCING AVAILABLE TO\r\nHIGH QUALIFIED BUYERS>\r\n\r\n\r\nSales Agent : Marty Ruff\r\n\r\nEmail Agent: Marty@lmmre.com\r\n\r\nPhone : 203-389-5377  x18\r\n\r\nStories : 1\r\n\r\nLot Size : 4.87 Acres\r\n\r\nParking: 100 Cars\r\n\r\nYear Built : 1977\r\n\r\n&nbsp;\r\n\r\n&nbsp;', '240 WINTHROP AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '240-winthrop-avenue-new-haven-ct', '', '', '2013-08-29 14:46:12', '2013-08-29 14:46:12', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=15', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('28', '3', '2013-07-07 13:22:55', '2013-07-07 13:22:55', 'GORGEOUS 16,540 SF OFFICE BUILDING IN EXCELLENT LOCATION. 4 FLOORS WITH ELEVATOR. BEAUTIFUL WOODWORK THROUGHOUT. 20 CAR PARKING LOT. CURRENTLY 3,400 SF OF SPACE AVAILABLE. EXCELLENT OPPORTUNITY FOR AN OWNER OCCUPANT OR AN INVESTOR.\r\n<table width=\"270\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"2\">442 ORANGE STREET</td>\r\n</tr>\r\n<tr>\r\n<td>Property Type:</td>\r\n<td>Office</td>\r\n</tr>\r\n<tr>\r\n<td>Availability:</td>\r\n<td>Available</td>\r\n</tr>\r\n<tr>\r\n<td>Asking Price:</td>\r\n<td>$2,100,000</td>\r\n</tr>\r\n<tr>\r\n<td>Est. Property Tax:</td>\r\n<td>44736</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\">Location:</td>\r\n<td>442 ORANGE STREET\r\nNEW HAVEN, Connecticut\r\n06511</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Sales Agent:</td>\r\n<td>Noah Meyer</td>\r\n</tr>\r\n<tr>\r\n<td>Email Sales Agent:</td>\r\n<td><a>Noah@lmmre.com</a></td>\r\n</tr>\r\n<tr>\r\n<td>Phone Number:</td>\r\n<td>203-389-5377 x 14</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Stories:</td>\r\n<td>3</td>\r\n</tr>\r\n<tr>\r\n<td>Square Feet:</td>\r\n<td>16540 Square Feet</td>\r\n</tr>\r\n<tr>\r\n<td>Lot Size:</td>\r\n<td>0.32 Acres</td>\r\n</tr>\r\n<tr>\r\n<td>Year Built:</td>\r\n<td>1882</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;', '442 Orange Street New Haven', '', 'inherit', 'open', 'open', '', '18-autosave', '', '', '2013-07-07 13:22:55', '2013-07-07 13:22:55', '', '18', 'http://www.lmmre.com/property/2013/05/03/18-autosave/', '0', 'revision', '', '0'),
('16', '1', '2013-05-03 14:23:25', '2013-05-03 14:23:25', '', '240-winthrop-ave-new-haven', '', 'inherit', 'open', 'open', '', '240-winthrop-ave-new-haven', '', '', '2013-05-03 14:23:25', '2013-05-03 14:23:25', '', '15', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/240-winthrop-ave-new-haven.jpg', '0', 'attachment', 'image/jpeg', '0'),
('17', '3', '2013-08-29 14:46:08', '2013-08-29 14:46:08', 'WAS OPERATING CONVALESCENT HOME UNTIL SEPTEMBER 2004. ALSO GOOD FOR MEDICAL, ELDERLY, ASSISTED LIVING,\nPOSSIBLE RESIDENTIAL. NOTES: 1. ASSESSMENT AND TAXES ARE UNDER REVIEW AND ARE SUBJECT TO CHANGE.\nTHIS PROPERTY ALLOWS UP TO 4 FLOORS IF PARKING REQUIREMENT IS MET. SOME OWNER FINANCING AVAILABLE TO\nHIGH QUALIFIED BUYERS>\n\n\nSales Agent : Marty Ruff\n\nEmail Agent: Marty@lmmre.com\n\nPhone : 203-389-5377  x18\n\nStories : 1\n\nLot Size : 4.87 Acres\n\nParking: 100 Cars\n\nYear Built : 1977\n\n&nbsp;\n\n&nbsp;', '240 WINTHROP AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '15-autosave', '', '', '2013-08-29 14:46:08', '2013-08-29 14:46:08', '', '15', 'http://www.lmmre.com/property/2013/05/03/15-autosave/', '0', 'revision', '', '0'),
('59', '3', '2013-05-15 14:51:10', '2013-05-15 14:51:10', '', '1060 S Colony', '', 'inherit', 'open', 'open', '', '1060-s-colony', '', '', '2013-05-15 14:51:10', '2013-05-15 14:51:10', '', '58', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1060-S-Colony.jpg', '0', 'attachment', 'image/jpeg', '0'),
('18', '1', '2013-05-03 14:40:19', '2013-05-03 14:40:19', 'GORGEOUS 16,540 SF OFFICE BUILDING IN EXCELLENT LOCATION. 4 FLOORS WITH ELEVATOR. BEAUTIFUL WOODWORK THROUGHOUT. 20 CAR PARKING LOT. CURRENTLY 3,400 SF OF SPACE AVAILABLE. EXCELLENT OPPORTUNITY FOR AN OWNER OCCUPANT OR AN INVESTOR.\r\n<table width=\"270\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"2\">442 ORANGE STREET</td>\r\n</tr>\r\n<tr>\r\n<td>Property Type:</td>\r\n<td>Office</td>\r\n</tr>\r\n<tr>\r\n<td>Availability:</td>\r\n<td>Available</td>\r\n</tr>\r\n<tr>\r\n<td>Asking Price:</td>\r\n<td>$2,100,000</td>\r\n</tr>\r\n<tr>\r\n<td>Est. Property Tax:</td>\r\n<td>44736</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\">Location:</td>\r\n<td>442 ORANGE STREET\r\nNEW HAVEN, Connecticut\r\n06511</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Sales Agent:</td>\r\n<td>Noah Meyer</td>\r\n</tr>\r\n<tr>\r\n<td>Email Sales Agent:</td>\r\n<td><a>Noah@lmmre.com</a></td>\r\n</tr>\r\n<tr>\r\n<td>Phone Number:</td>\r\n<td>203-389-5377 x 14</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Stories:</td>\r\n<td>3</td>\r\n</tr>\r\n<tr>\r\n<td>Square Feet:</td>\r\n<td>16540 Square Feet</td>\r\n</tr>\r\n<tr>\r\n<td>Lot Size:</td>\r\n<td>0.32 Acres</td>\r\n</tr>\r\n<tr>\r\n<td>Year Built:</td>\r\n<td>1882</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;', '442 Orange Street New Haven', '', 'publish', 'closed', 'closed', '', '442-orange-street-new-haven', '', '', '2013-07-07 13:22:50', '2013-07-07 13:22:50', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=18', '0', 'woo_estate', '', '0'),
('19', '1', '2013-05-03 14:38:58', '2013-05-03 14:38:58', '', 'orange-street-new-haven', '', 'inherit', 'open', 'open', '', 'orange-street-new-haven', '', '', '2013-05-03 14:38:58', '2013-05-03 14:38:58', '', '18', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/orange-street-new-haven.jpg', '0', 'attachment', 'image/jpeg', '0'),
('20', '1', '2013-05-03 14:43:19', '2013-05-03 14:43:19', 'PROPERTY IS FORMERLY AN OLD FACTORY WITH CHARM LEFT OVER ON THE WATER IN WEST HAVEN. WOOD FLOORS, MANY WINDOWS, HIGH CEILINGS WITH DIRECT ACCESS TO A BULK HEAD FOR A 3 LARGE BOAT MARINA CONNECTING TO NEW HAVEN HARBOR. THIS 3 STORY STRUCTURE (A FORMER MILL) WILL OFFER MANY POSSIBILITIES. ALL SHOWINGS MUST CONCLUDE BEFORE 3:00 PM ON THE DATE SCHEDULED.\r\n\r\n&nbsp;\r\n<table width=\"270\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Property Type:</td>\r\n<td>Retail</td>\r\n</tr>\r\n<tr>\r\n<td>Availability:</td>\r\n<td>Available</td>\r\n</tr>\r\n<tr>\r\n<td>Asking Price:</td>\r\n<td>$1,350,000</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\">Location:</td>\r\n<td>30 ELM STREET\r\nWEST HAVEN, Connecticut\r\n06516</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Sales Agent:</td>\r\n<td>Marty Ruff</td>\r\n</tr>\r\n<tr>\r\n<td>Email Sales Agent:</td>\r\n<td><a>marty@lmmre.com</a></td>\r\n</tr>\r\n<tr>\r\n<td>Phone Number:</td>\r\n<td>203-389-5377 x18</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Stories:</td>\r\n<td>3</td>\r\n</tr>\r\n<tr>\r\n<td>Square Feet:</td>\r\n<td>38038 Square Feet</td>\r\n</tr>\r\n<tr>\r\n<td>Lot Size:</td>\r\n<td>0.88 Acres</td>\r\n</tr>\r\n<tr>\r\n<td>Year Built:</td>\r\n<td>1910</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;', '30 Elm Street West Haven CT', '', 'publish', 'closed', 'closed', '', '30-elm-street-west-haven-ct', '', '', '2013-07-07 13:24:31', '2013-07-07 13:24:31', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=20', '0', 'woo_estate', '', '0'),
('153', '3', '2013-05-24 18:17:39', '2013-05-24 18:17:39', 'ONE ROOM OFFICE NEAR MERRITT PARKWAY. $550.00/MONTH GROSS.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MICHAEL GORDON\r\n\r\n203-389-5377 X25\r\n\r\nmichael@lmmre.com', '59 AMITY ROAD, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '59-amity-road-woodbridge', '', '', '2013-07-08 01:06:26', '2013-07-08 01:06:26', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=153', '0', 'woo_estate', '', '0'),
('50', '3', '2013-05-13 13:32:54', '2013-05-13 13:32:54', '', 'land for sale', '', 'inherit', 'open', 'open', '', 'land-for-sale', '', '', '2013-05-13 13:32:54', '2013-05-13 13:32:54', '', '47', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/land-for-sale.jpg', '0', 'attachment', 'image/jpeg', '0'),
('26', '3', '2013-07-07 13:24:36', '2013-07-07 13:24:36', 'PROPERTY IS FORMERLY AN OLD FACTORY WITH CHARM LEFT OVER ON THE WATER IN WEST HAVEN. WOOD FLOORS, MANY WINDOWS, HIGH CEILINGS WITH DIRECT ACCESS TO A BULK HEAD FOR A 3 LARGE BOAT MARINA CONNECTING TO NEW HAVEN HARBOR. THIS 3 STORY STRUCTURE (A FORMER MILL) WILL OFFER MANY POSSIBILITIES. ALL SHOWINGS MUST CONCLUDE BEFORE 3:00 PM ON THE DATE SCHEDULED.\r\n\r\n&nbsp;\r\n<table width=\"270\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>Property Type:</td>\r\n<td>Retail</td>\r\n</tr>\r\n<tr>\r\n<td>Availability:</td>\r\n<td>Available</td>\r\n</tr>\r\n<tr>\r\n<td>Asking Price:</td>\r\n<td>$1,350,000</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\">Location:</td>\r\n<td>30 ELM STREET\r\nWEST HAVEN, Connecticut\r\n06516</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Sales Agent:</td>\r\n<td>Marty Ruff</td>\r\n</tr>\r\n<tr>\r\n<td>Email Sales Agent:</td>\r\n<td><a>marty@lmmre.com</a></td>\r\n</tr>\r\n<tr>\r\n<td>Phone Number:</td>\r\n<td>203-389-5377 x18</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Stories:</td>\r\n<td>3</td>\r\n</tr>\r\n<tr>\r\n<td>Square Feet:</td>\r\n<td>38038 Square Feet</td>\r\n</tr>\r\n<tr>\r\n<td>Lot Size:</td>\r\n<td>0.88 Acres</td>\r\n</tr>\r\n<tr>\r\n<td>Year Built:</td>\r\n<td>1910</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;', '30 Elm Street West Haven CT', '', 'inherit', 'open', 'open', '', '20-autosave', '', '', '2013-07-07 13:24:36', '2013-07-07 13:24:36', '', '20', 'http://www.lmmre.com/property/2013/05/03/20-autosave/', '0', 'revision', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('75', '3', '2013-05-16 15:35:33', '2013-05-16 15:35:33', 'ALMOST 2 ACRES WITH 8,800 SF OF SELF STORAGE UNITS, 94 UNITS TOTAL. TWO BUILDINGS SURROUNDED BY CHAIN LINK FENCE WITH A CARD KEY OPERATED SECURITY GATE. GREAT OPPORTUNITY FOR INVESTOR/BUSINESS WITH 94 UNITS. SOME ARE TEMPERATURE CONTROLLED.\r\n\r\nFOR FURTHER INFORMATION CONTACT STEVE MILLER 203-389-5377 OR steve@lmmre.com.', '15 RESEARCH DRIVE, BRANFORD - PRICE REDUCED!!!!', '', 'publish', 'closed', 'closed', '', '15-research-drive-branford-price-reduced', '', '', '2013-07-08 00:19:14', '2013-07-08 00:19:14', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=75', '0', 'woo_estate', '', '0'),
('204', '3', '2013-05-29 20:44:08', '2013-05-29 20:44:08', 'NICE BUILDING. ALL UNITS WITH BALCONY, GARAGE, WASHER,\r\nDRYER-CONVENIENT TO HOSPITAL ZONE - RT 15 AND I-95.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X 18\r\n\r\nmarty@lmmre.com', '1079 ELLA T GRASSO BLVD. NEW HAVEN', '', 'publish', 'closed', 'closed', '', '1079-ella-t-grasso-blvd-new-haven', '', '', '2013-07-08 02:27:57', '2013-07-08 02:27:57', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=204', '0', 'woo_estate', '', '0'),
('205', '3', '2013-05-29 20:49:52', '2013-05-29 20:49:52', 'CIRCA 1871 - GREAT OFFICE LOCATION NEAR TOWN CENTER. 1,250 SF\r\nAVAILABLE FOR LAW PRACTICE, ACCOUNTANTS, THERAPISTS - AMPLE PARKING.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nmarty@lmmre.com', '30 WASHINGTON AVENUE, NORTH HAVEN', '', 'publish', 'closed', 'closed', '', '30-washington-avenue-north-haven', '', '', '2013-07-08 02:28:47', '2013-07-08 02:28:47', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=205', '0', 'woo_estate', '', '0'),
('31', '3', '2013-05-08 20:45:30', '2013-05-08 20:45:30', '', '30 Elm', '', 'inherit', 'open', 'open', '', '30-elm', '', '', '2013-05-08 20:45:30', '2013-05-08 20:45:30', '', '0', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/30-Elm.jpg', '0', 'attachment', 'image/jpeg', '0'),
('159', '3', '2013-05-28 17:10:30', '2013-05-28 17:10:30', 'HIGH TRAFFIC LOCATION ON BUSY WHALLEY AVENUE. AUTOMOTIVE USE ALLOWED. 5,000 SF 1 ST FLOOR, 388 SF 2ND FLOOR, 1,200 SF BASEMENT. 2 DRIVE-IN OVERHEAD DOOR.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON\r\n\r\n203-389-5377 OR michael@lmmre.com', '223 WHALLEY AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '223-whalley-avenue-new-haven', '', '', '2013-07-07 13:34:37', '2013-07-07 13:34:37', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=159', '0', 'woo_estate', '', '0'),
('36', '3', '2013-05-09 14:27:45', '2013-05-09 14:27:45', 'ONLY SPACE AVAILABLE IN 34,000 SF MIXED USE, ENERGY EFFICIENT PLAZA IN WOODBRIDGE, A HIGH INCOME SUBURB OF NEW HAVEN. AVAILABLE SUITE IS 1,350 SF ON THE 2ND FLOOR. CORNER OFFICE SUITE WAS FIT OUT IN 2010 FOR ARCHITECTS. BEAUTIFUL NATURAL LIGHT, WITH SKY LIGHTS, AND OPEN FLOOR PLAN. PLAZA HOUSES 21 TENANTS, INCLUDING DOCTORS, PHYSICAL THERAPISTS, ATTORNEYS AND UPSCALE RETAILERS. CONVENIENT SURFACE PARKING. $14.00 SF NNN.\r\n\r\nFOR FURTHER INFORMATION CALL DIANE URBANO 203-389-5377 X27 OR diane@lmmre.com.', '245 AMITY ROAD, WOODBRIDGE, CT  06525', '', 'publish', 'closed', 'closed', '', '245-amity-road', '', '', '2013-07-07 13:16:12', '2013-07-07 13:16:12', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=36', '0', 'woo_estate', '', '0'),
('37', '3', '2013-07-07 13:16:17', '2013-07-07 13:16:17', 'ONLY SPACE AVAILABLE IN 34,000 SF MIXED USE, ENERGY EFFICIENT PLAZA IN WOODBRIDGE, A HIGH INCOME SUBURB OF NEW HAVEN. AVAILABLE SUITE IS 1,350 SF ON THE 2ND FLOOR. CORNER OFFICE SUITE WAS FIT OUT IN 2010 FOR ARCHITECTS. BEAUTIFUL NATURAL LIGHT, WITH SKY LIGHTS, AND OPEN FLOOR PLAN. PLAZA HOUSES 21 TENANTS, INCLUDING DOCTORS, PHYSICAL THERAPISTS, ATTORNEYS AND UPSCALE RETAILERS. CONVENIENT SURFACE PARKING. $14.00 SF NNN.\r\n\r\nFOR FURTHER INFORMATION CALL DIANE URBANO 203-389-5377 X27 OR diane@lmmre.com.', '245 AMITY ROAD, WOODBRIDGE, CT  06525', '', 'inherit', 'open', 'open', '', '36-autosave', '', '', '2013-07-07 13:16:17', '2013-07-07 13:16:17', '', '36', 'http://www.lmmre.com/property/2013/05/09/36-autosave/', '0', 'revision', '', '0'),
('106', '3', '2013-05-21 17:16:06', '2013-05-21 17:16:06', '', '23 Raccio Park', '', 'inherit', 'open', 'open', '', '23-raccio-park', '', '', '2013-05-21 17:16:06', '2013-05-21 17:16:06', '', '105', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/23-Raccio-Park.jpg', '0', 'attachment', 'image/jpeg', '0'),
('41', '3', '2013-05-10 18:28:55', '2013-05-10 18:28:55', 'OFFICE BUILDING ON DEAD END OF OFFICE PARK. WE OFFER UP TO 6,000 SF OF EITHER OFFICE OR LABORATORY.\r\n\r\nTHIS IS THE BEST OFFICE RATE IN WOODBRIDGE, HANDS DOWN. RENT INCLUDES EVERYTHING EXCEPT PHONE AND INTERNET COSTS.\r\n\r\n&nbsp;\r\n\r\nFor Further Information: Contact Marty Ruff\r\n\r\nmarty@lmmre.com\r\n\r\n203-389-5377\r\n\r\n&nbsp;\r\n\r\n&nbsp;', '131 BRADLEY ROAD, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '131-bradley-road-woodbridge', '', '', '2013-07-07 13:17:27', '2013-07-07 13:17:27', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=41', '0', 'woo_estate', '', '0'),
('42', '3', '2013-07-07 13:17:32', '2013-07-07 13:17:32', 'OFFICE BUILDING ON DEAD END OF OFFICE PARK. WE OFFER UP TO 6,000 SF OF EITHER OFFICE OR LABORATORY.\r\n\r\nTHIS IS THE BEST OFFICE RATE IN WOODBRIDGE, HANDS DOWN. RENT INCLUDES EVERYTHING EXCEPT PHONE AND INTERNET COSTS.\r\n\r\n&nbsp;\r\n\r\nFor Further Information: Contact Marty Ruff\r\n\r\nmarty@lmmre.com\r\n\r\n203-389-5377\r\n\r\n&nbsp;\r\n\r\n&nbsp;', '131 BRADLEY ROAD, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '41-autosave', '', '', '2013-07-07 13:17:32', '2013-07-07 13:17:32', '', '41', 'http://www.lmmre.com/property/2013/05/10/41-autosave/', '0', 'revision', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('43', '3', '2013-05-10 18:32:45', '2013-05-10 18:32:45', '', '131 Bradley Road', '', 'inherit', 'open', 'open', '', '131-bradley', '', '', '2013-05-10 18:32:45', '2013-05-10 18:32:45', '', '41', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/131-Bradley.jpg', '0', 'attachment', 'image/jpeg', '0'),
('154', '3', '2013-05-24 18:16:09', '2013-05-24 18:16:09', '', '59 Amity', '', 'inherit', 'open', 'open', '', '59-amity', '', '', '2013-05-24 18:16:09', '2013-05-24 18:16:09', '', '153', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/59-Amity.jpg', '0', 'attachment', 'image/jpeg', '0'),
('45', '3', '2013-05-10 18:47:42', '2013-05-10 18:47:42', '', '245 Amity 5', '', 'inherit', 'open', 'open', '', '245-amity-5', '', '', '2013-05-10 18:47:42', '2013-05-10 18:47:42', '', '36', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/245-Amity-5.jpg', '0', 'attachment', 'image/jpeg', '0'),
('47', '3', '2013-05-10 19:08:23', '2013-05-10 19:08:23', '<table width=\"680\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td valign=\"top\" width=\"270\">\r\n<table width=\"270\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"2\">410 ELLA GRASSO BLVD.</td>\r\n</tr>\r\n<tr>\r\n<td>Property Type:</td>\r\n<td>Land</td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td></td>\r\n</tr>\r\n<tr>\r\n<td>Availability:</td>\r\n<td>Available</td>\r\n</tr>\r\n<tr>\r\n<td>Asking Price:</td>\r\n<td>$6,363,450</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\">Location:</td>\r\n<td>410 ELLA GRASSO BLVD.\r\nNEW HAVEN, Connecticut\r\n06519</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Sales Agent:</td>\r\n<td>Marty Ruff</td>\r\n</tr>\r\n<tr>\r\n<td>Email Sales Agent:</td>\r\n<td><a>Marty@lmmre.com</a></td>\r\n</tr>\r\n<tr>\r\n<td>Phone Number:</td>\r\n<td>203-389-5377 x18</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;\r\n\r\nTWO CITY BLOCKS FROM I-95 IN NEW HAVEN. UP TO 16 ACRES OF LAND IN A IL ZONE. OWNER WILL SELL SMALLER QUANTITIES IF ACERAGE AT $395,000 PER ACRE. ALSO, LAND LEASE IS AVAILABLE AT 2,000 PER ACRE PER MONTH NNN. GREAT FOR INDUSTRIAL PARK. CALL MARTY FOR MORE INFORMATION.', '410 Ella Grasso Blvd.', '', 'publish', 'closed', 'closed', '', '410-ella-grasso-blvd', '', '', '2013-07-07 14:13:26', '2013-07-07 14:13:26', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=47', '0', 'woo_estate', '', '0'),
('105', '3', '2013-05-21 17:41:22', '2013-05-21 17:41:22', 'APPROXIMATELY 10,000 SF WAREHOUSE, 3 DOCKS, 5 OVERHEAD DOORS, ALARM SYSTEM, 90 PARKING SPACES.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MICHAEL GORDON 203-389-5377 OR michael@lmmre.com\r\n\r\n&nbsp;', '23 RACCIO PARK ROAD, HAMDEN', '', 'publish', 'closed', 'closed', '', '23-raccio-park-road-hamden', '', '', '2013-07-08 00:29:38', '2013-07-08 00:29:38', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=105', '0', 'woo_estate', '', '0'),
('48', '3', '2013-07-07 14:13:31', '2013-07-07 14:13:31', '<table width=\"680\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td valign=\"top\" width=\"270\">\r\n<table width=\"270\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"2\">410 ELLA GRASSO BLVD.</td>\r\n</tr>\r\n<tr>\r\n<td>Property Type:</td>\r\n<td>Land</td>\r\n</tr>\r\n<tr>\r\n<td></td>\r\n<td></td>\r\n</tr>\r\n<tr>\r\n<td>Availability:</td>\r\n<td>Available</td>\r\n</tr>\r\n<tr>\r\n<td>Asking Price:</td>\r\n<td>$6,363,450</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\">Location:</td>\r\n<td>410 ELLA GRASSO BLVD.\r\nNEW HAVEN, Connecticut\r\n06519</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n\r\n<hr />\r\n\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>Sales Agent:</td>\r\n<td>Marty Ruff</td>\r\n</tr>\r\n<tr>\r\n<td>Email Sales Agent:</td>\r\n<td><a>Marty@lmmre.com</a></td>\r\n</tr>\r\n<tr>\r\n<td>Phone Number:</td>\r\n<td>203-389-5377 x18</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;\r\n\r\nTWO CITY BLOCKS FROM I-95 IN NEW HAVEN. UP TO 16 ACRES OF LAND IN A IL ZONE. OWNER WILL SELL SMALLER QUANTITIES IF ACERAGE AT $395,000 PER ACRE. ALSO, LAND LEASE IS AVAILABLE AT 2,000 PER ACRE PER MONTH NNN. GREAT FOR INDUSTRIAL PARK. CALL MARTY FOR MORE INFORMATION.', '410 Ella Grasso Blvd.', '', 'inherit', 'open', 'open', '', '47-autosave', '', '', '2013-07-07 14:13:31', '2013-07-07 14:13:31', '', '47', 'http://www.lmmre.com/property/2013/05/10/47-autosave/', '0', 'revision', '', '0'),
('58', '3', '2013-05-15 14:50:04', '2013-05-15 14:50:04', 'OPPORTUNITY TO LEASE SPACE 4,000-5,200 SF CLOSE TO I91 EXIT 13. GREAT VISIBILITY. EXCELLENT PARKING. HIGH TRAFFIC COUNT.\r\n\r\nFOR FURTHER INFORMATION CONTACT MARTY RUFF 203-389-5377 X18 OR marty@lmmre.com.', '1060 SOUTH COLONY ROAD, WALLINGFORD', '', 'trash', 'closed', 'closed', '', '1060-south-colony-road-wallingford', '', '', '2013-08-14 20:14:17', '2013-08-14 20:14:17', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=58', '0', 'woo_estate', '', '0'),
('61', '3', '2013-07-07 14:39:54', '2013-07-07 14:39:54', 'OPPORTUNITY TO LEASE SPACE 4,000-5,200 SF CLOSE TO I91 EXIT 13. GREAT VISIBILITY. EXCELLENT PARKING. HIGH TRAFFIC COUNT.\n\nFOR FURTHER INFORMATION CONTACT MARTY RUFF 203-389-5377 X18 OR marty@lmmre.com.', '1060 SOUTH COLONY ROAD, WALLINGFORD', '', 'inherit', 'open', 'open', '', '58-autosave', '', '', '2013-07-07 14:39:54', '2013-07-07 14:39:54', '', '58', 'http://www.lmmre.com/property/2013/05/15/58-autosave/', '0', 'revision', '', '0'),
('135', '3', '2013-05-23 16:07:25', '2013-05-23 16:07:25', 'BEST LOCATION IN DOWNTOWN AREA SURROUNDED BY RESTAURANTS, THEATER AND HOTELS. MIXED USE PROPERTY ON HIGH TRAFFIC ROUTE WITH BOTH WALKING AND DRIVING TRAFFIC WITH BEAUTIFUL HISTORIC PROPERTY THAT HAS UNDERGONE A COMPLETE REHAB IN THE LAST 2 YEARS AND IS IN PRISTINE CONDITION, WHICH INCLUDES 3 FLOORS OF BEAUTIFUL RESIDENTIAL AND 3 FLOORS OF COMMERCIAL SPACE. TO OF THE LINE REHAB FROM BASEMENT TO ROOF WITH NEW MECHANICALS. RESIDENTIAL IS BY FAR THE NICEST LIVING IN DOWNTOWN, EASILY RENTING TO A PROFESSIONAL FOR $3,500 + A MONTH AND COULD EASILY BE DIVIDED INTO 2 UNITS. COMMERCIAL SPACE CURRENTLY OCCUPIED BY HIGH GROSSING RESTAURANT. THE MARKET RENT OF $35NNN PER SQUARE FOOT GIVEN ITS PRIME LOCATION AND SALES VOLUME MAKES THIS A REAL BARGAIN. ASKING $2,900,000 FOR THE BUSINESS AND PROPERTY, $2,600,000 FOR JUST THE PROPERTY OR $599,000 FOR JUST THE BUSINESS.', 'RESTAURANT WITH PROPERTY AVAILABLE', '', 'publish', 'closed', 'closed', '', 'restaurant-with-property-available', '', '', '2013-07-07 14:18:44', '2013-07-07 14:18:44', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=135', '0', 'woo_estate', '', '0'),
('63', '3', '2013-05-15 15:12:56', '2013-05-15 15:12:56', '3,300 SF BRICK BUILDING THAT SITS ON APPROXIMATELY 1.5 ACRES. - BUILDING IS USED TO REPAIR CONSTRUCTION EQUIPMENT BELONGING TO THE OWNER. PROPERTY IS ENCLOSED BY PAGE FENCE AND WIRE WITH A POWER GATE BACK SIDE OF BUILDING. INSIDE BUILDING HAS STORAGE AND A SMALL OFFICE. NEED 30-60 DAYS TO CLEAR OUT BUILDING AND YARD. OWNER WOULD CONSIDER FINANCING PURCHASE TO WELL QUALIFIED OWNER. AVAILABLE FOR SALE AT $395,000\r\n\r\n<em><strong>ALSO AVAILABLE FOR LEASE AT $3,000/MONTH.</strong></em>\r\n\r\nFOR FURTHER INFORMATION:\r\n\r\nCONTACT MARTY RUFF 203-389-5377 X18 OR\r\nmarty@lmmre.com.', '390 EASTERN STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '390-eastern-street-new-haven', '', '', '2013-07-08 00:14:09', '2013-07-08 00:14:09', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=63', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('64', '3', '2013-05-15 15:08:25', '2013-05-15 15:08:25', '', '390Eastern1', '', 'inherit', 'open', 'open', '', '390eastern1', '', '', '2013-05-15 15:08:25', '2013-05-15 15:08:25', '', '63', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/390Eastern1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('65', '3', '2013-05-15 15:21:43', '2013-05-15 15:21:43', 'GREAT LOCATION ACROSS FROM STOP AND SHOP PLAZA AT EXIT 59 ENTRANCE TO PARKWAY. 3 BUILDINGS - RESIDENCE, GREENHOUSE/RETAIL, INDUSTRIAL BUILDING. PROPERTY VALUE IS IN THE LOCATION. CURRENTLY FLORIST/GREENHOUSE, WHOLESALE AND RETAIL. 3 BUILDINGS ON BUSY AMITY ROAD ACROSS FROM STOP AND SHOP.\r\n\r\nFOR FURTHER INFORMATION CONTACT STEVE MILLER 203-389-5377 X22 OR steve@lmmre.com.', '129 AMITY ROAD, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '129-amity-road-woodbridge', '', '', '2013-07-07 14:46:02', '2013-07-07 14:46:02', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=65', '0', 'woo_estate', '', '0'),
('66', '3', '2013-05-15 15:20:05', '2013-05-15 15:20:05', '', '129 Amity.jpg', '', 'inherit', 'open', 'open', '', '129-amity-jpg', '', '', '2013-05-15 15:20:05', '2013-05-15 15:20:05', '', '65', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/129-Amity.jpg.jpg', '0', 'attachment', 'image/jpeg', '0'),
('270', '3', '2013-05-31 14:34:03', '2013-05-31 14:34:03', '', '1 Acre - Litchfield Tpke.', '', 'inherit', 'open', 'open', '', '1-acre-litchfield-tpke', '', '', '2013-05-31 14:34:03', '2013-05-31 14:34:03', '', '263', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1-Acre-Litchfield-Tpke..jpg', '0', 'attachment', 'image/jpeg', '0'),
('68', '3', '2013-05-15 15:33:01', '2013-05-15 15:33:01', '3 ROOM OFFICE SUITE IN MT CARMEL SECTION OF HAMDEN. CONVENIENT TO PARKWAY, I-91 CONNECTOR (RT 40) AND WHITNEY AVENUE. AROUND THE CORNER FROM QUINNIPIAC UNIVERSITY. BEAUTIFUL RURAL LOCATION.\r\n\r\nFOR FURTHER INFORMATION CONTACT STEVE MILLER 203-389-5377 X22 OR steve@lmmre.com.', '2 BROADWAY, HAMDEN', '', 'publish', 'closed', 'closed', '', '2-broadway-hamden', '', '', '2013-07-08 00:16:12', '2013-07-08 00:16:12', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=68', '0', 'woo_estate', '', '0'),
('69', '3', '2013-05-15 15:31:25', '2013-05-15 15:31:25', '', '2 Broadway', '', 'inherit', 'open', 'open', '', '2-broadway', '', '', '2013-05-15 15:31:25', '2013-05-15 15:31:25', '', '68', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/2-Broadway.jpg', '0', 'attachment', 'image/jpeg', '0'),
('70', '3', '2013-05-15 15:44:23', '2013-05-15 15:44:23', 'RETAIL AND OFFICE SPACE AVAILABLE IMMEDIATELY. GOOD SIGNAGE AND VISIBILITY. PLENTY OF PARKING. 1ST FLOOR OFFICE/RETAIL IS $12.00 SF PLUS UTILITIES. ONE SPACE AVAILABLE: 1,000 SF.\r\n\r\nFOR FURTHER INFORMATION CONTACT MARTY RUFF 203-389-5377 X18 OR marty@lmmre.com.', '214 AMITY ROAD, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '214-amity-road-woodbridge', '', '', '2013-07-08 00:17:37', '2013-07-08 00:17:37', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=70', '0', 'woo_estate', '', '0'),
('71', '3', '2013-05-15 15:42:27', '2013-05-15 15:42:27', '', '214 Amity Rd. 1', '', 'inherit', 'open', 'open', '', '214-amity-rd-1', '', '', '2013-05-15 15:42:27', '2013-05-15 15:42:27', '', '70', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/214-Amity-Rd.-1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('76', '3', '2013-05-16 15:29:53', '2013-05-16 15:29:53', '', '15 RESEARCH - BRANFORD', '', 'inherit', 'open', 'open', '', '15-research-branford', '', '', '2013-05-16 15:29:53', '2013-05-16 15:29:53', '', '75', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/15-RESEARCH-BRANFORD.jpg', '0', 'attachment', 'image/jpeg', '0'),
('78', '3', '2013-07-08 00:17:44', '2013-07-08 00:17:44', 'RETAIL AND OFFICE SPACE AVAILABLE IMMEDIATELY. GOOD SIGNAGE AND VISIBILITY. PLENTY OF PARKING. 1ST FLOOR OFFICE/RETAIL IS $12.00 SF PLUS UTILITIES. ONE SPACE AVAILABLE: 1,000 SF.\r\n\r\nFOR FURTHER INFORMATION CONTACT MARTY RUFF 203-389-5377 X18 OR marty@lmmre.com.', '214 AMITY ROAD, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '70-autosave', '', '', '2013-07-08 00:17:44', '2013-07-08 00:17:44', '', '70', 'http://www.lmmre.com/property/2013/05/16/70-autosave/', '0', 'revision', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('79', '3', '2013-07-08 00:15:50', '2013-07-08 00:15:50', '3 ROOM OFFICE SUITE IN MT CARMEL SECTION OF HAMDEN. CONVENIENT TO PARKWAY, I-91 CONNECTOR (RT 40) AND WHITNEY AVENUE. AROUND THE CORNER FROM QUINNIPIAC UNIVERSITY. BEAUTIFUL RURAL LOCATION.\r\n\r\nFOR FURTHER INFORMATION CONTACT STEVE MILLER 203-389-5377 X22 OR steve@lmmre.com.', '2 BROADWAY, HAMDEN', '', 'inherit', 'open', 'open', '', '68-autosave', '', '', '2013-07-08 00:15:50', '2013-07-08 00:15:50', '', '68', 'http://www.lmmre.com/property/2013/05/16/68-autosave/', '0', 'revision', '', '0'),
('81', '3', '2013-07-07 14:46:27', '2013-07-07 14:46:27', 'GREAT LOCATION ACROSS FROM STOP AND SHOP PLAZA AT EXIT 59 ENTRANCE TO PARKWAY. 3 BUILDINGS - RESIDENCE, GREENHOUSE/RETAIL, INDUSTRIAL BUILDING. PROPERTY VALUE IS IN THE LOCATION. CURRENTLY FLORIST/GREENHOUSE, WHOLESALE AND RETAIL. 3 BUILDINGS ON BUSY AMITY ROAD ACROSS FROM STOP AND SHOP.\r\n\r\nFOR FURTHER INFORMATION CONTACT STEVE MILLER 203-389-5377 X22 OR steve@lmmre.com.', '129 AMITY ROAD, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '65-autosave', '', '', '2013-07-07 14:46:27', '2013-07-07 14:46:27', '', '65', 'http://www.lmmre.com/property/2013/05/16/65-autosave/', '0', 'revision', '', '0'),
('82', '3', '2013-05-16 16:09:37', '2013-05-16 16:09:37', 'FANTASTIC SPACE - FRONT-CAN BE ALMOST ANYTHING. BACK WAS DETAIL SHOP. GREAT VISIBILITY FROM WHALLEY AVENUE. GREAT ACCESS TO WHALLEY AVENUE AND MERRITT PARKWAY. FRONT SPACE IS ALMOST 3,300 + SF. LARGE DOOR AND RAMP FOR LOADING DOCK. PLUMBING AND HEATING ALL THERE TO BUILD OFF OF. PERFECT SET FOR ANYTHING AUTO.\r\n\r\nFOR SALE $249,000 OR FOR LEASE $6.00 SF\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT ARIN HAYDEN 203-389-5377 X38\r\n\r\nOR\r\n\r\narin@lmmre.com.', '149 RAMSDELL STREET, NEW HAVEN', '', 'trash', 'closed', 'closed', '', '149-ramsdell-street-new-haven', '', '', '2013-08-06 01:36:59', '2013-08-06 01:36:59', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=82', '0', 'woo_estate', '', '0'),
('83', '3', '2013-05-16 16:07:27', '2013-05-16 16:07:27', '', '149 Ramsdell 002', '', 'inherit', 'open', 'open', '', '149-ramsdell-002', '', '', '2013-05-16 16:07:27', '2013-05-16 16:07:27', '', '82', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/149-Ramsdell-002.jpg', '0', 'attachment', 'image/jpeg', '0'),
('147', '3', '2013-05-24 17:43:24', '2013-05-24 17:43:24', '4,972 SF MIXED USE BUILDING ON BUSY STATE STREET, NEW HAVEN. 2 ONE BEDROOM APARTMENTS, RETAIL SPACE WITH NEW TENANT AND REAR INDUSTRIAL/STORAGE/WAREHOUSE SPACE. APARTMENTS CURRENTLY LEASED.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X 14 OR noah@lmmre.com', '1314 STATE STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '1314-state-street-new-haven', '', '', '2013-07-07 13:46:46', '2013-07-07 13:46:46', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=147', '0', 'woo_estate', '', '0'),
('86', '3', '2013-05-16 19:06:03', '2013-05-16 19:06:03', '2,500 SF RETAIL/OFFICE. BEAUTIFUL VICTORIAN BUILDING.\r\nWESTVILLE CENTER LOCATION. PLENTY OF FREE PARKING.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT STEVE MILLER 203-389-5377 X22\r\n\r\nsteve@lmmre.com.\r\n\r\n&nbsp;', '881 WHALLEY AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '881-whalley-avenue-new-haven', '', '', '2013-07-07 13:50:33', '2013-07-07 13:50:33', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=86', '0', 'woo_estate', '', '0'),
('87', '3', '2013-05-16 19:04:25', '2013-05-16 19:04:25', '', '881 Whalley', '', 'inherit', 'open', 'open', '', '881-whalley', '', '', '2013-05-16 19:04:25', '2013-05-16 19:04:25', '', '86', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/881-Whalley.jpg', '0', 'attachment', 'image/jpeg', '0'),
('89', '3', '2013-05-16 19:17:20', '2013-05-16 19:17:20', '8,400 SF BUILDING ON .73 ACRE. A CORNER LOT THAT IS CONVENIENT TO I-91 - RETAIL, SERVICE AND ADMINISTRATION FOR MANY PRODUCT LINES OF POWER EQUIPMENT. SHOWINGS AND DISCUSSIONS <strong>MUST </strong>TAKE PLACE AFTER 5:30 PM. OWNER WILL CONSIDER FINANCING TO WELL QUALIFIED COMPANIES. INDUSTRIES TOP PRODUCT LINES REPRESENTED. SCAG, EXMARK, STIHL, SCHINDAIWA, ARIENS AND MANY MORE. AKA 12 ROSSOTTO DRIVE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 X 18 OR marty@lmmre.com.', '1130 SHERMAN AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '1130-sherman-avenue-hamden', '', '', '2013-07-08 00:21:53', '2013-07-08 00:21:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=89', '0', 'woo_estate', '', '0'),
('90', '3', '2013-05-16 19:14:46', '2013-05-16 19:14:46', '', '1130 Sherman1', '', 'inherit', 'open', 'open', '', '1130-sherman1', '', '', '2013-05-16 19:14:46', '2013-05-16 19:14:46', '', '89', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1130-Sherman1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('91', '3', '2013-05-16 19:15:12', '2013-05-16 19:15:12', '', '1130 Sherman', '', 'inherit', 'open', 'open', '', '1130-sherman', '', '', '2013-05-16 19:15:12', '2013-05-16 19:15:12', '', '89', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1130-Sherman.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('92', '3', '2013-05-16 19:30:25', '2013-05-16 19:30:25', 'WASHINGTON SQUARE RETAIL CENTER HAS OPENING AVAILABLE; 1,225 OR 2,400 SF. CONVENIENT LOCATION ON WASHINGTON AVENUE AT TRAFFIC LIGHT TO DEFCO ROAD. AMPLE PARKING WITH SECURITY LIGHTING. ALL UNITS HAVE CITY SEWERS, WATER, GAS AND CENTRAL AIR.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 OR marty@lmmre.com.', '565 WASHINGTON AVENUE, NORTH HAVEN', '', 'publish', 'closed', 'closed', '', '565-washington-avenue-north-haven', '', '', '2013-07-08 00:23:00', '2013-07-08 00:23:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=92', '0', 'woo_estate', '', '0'),
('93', '3', '2013-05-16 19:26:45', '2013-05-16 19:26:45', '', '565 Washington Avenue 001', '', 'inherit', 'open', 'open', '', '565-washington-avenue-001', '', '', '2013-05-16 19:26:45', '2013-05-16 19:26:45', '', '92', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/565-Washington-Avenue-001.jpg', '0', 'attachment', 'image/jpeg', '0'),
('94', '3', '2013-05-16 19:44:31', '2013-05-16 19:44:31', 'VERY CONVENIENT 20,000 SF OFFICE SPACE THAT CAN BE SUB-DIVIDED INTO SPACE AS LOW AS 1,200 SF. THIS PROPERTY CAN BE DIVIDED OR RENTED IN WHOLE. 6 BAYS, SINGLE 3,000 SF GARAGE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 X18 OR marty@lmmre.com.', '230 OLD GATE LANE, MILFORD', '', 'publish', 'closed', 'closed', '', '230-old-gate-lane-milford', '', '', '2013-07-08 00:24:42', '2013-07-08 00:24:42', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=94', '0', 'woo_estate', '', '0'),
('95', '3', '2013-05-16 19:41:43', '2013-05-16 19:41:43', '', '230 Old Gate', '', 'inherit', 'open', 'open', '', '230-old-gate', '', '', '2013-05-16 19:41:43', '2013-05-16 19:41:43', '', '94', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/230-Old-Gate.jpg', '0', 'attachment', 'image/jpeg', '0'),
('96', '3', '2013-05-16 19:55:15', '2013-05-16 19:55:15', '1,900 SF IN ONE OF THE MOST BUSY STRIP SHOPPING CENTERS IN HAMDEN. VERY HIGH TRAFFIC FROM OTHER OCCUPANTS OF THIS CENTER. TENANT WILL ALSO GET THE USE OF A FULL BASEMENT. THIS SPACE IS WIDE OPEN. IF FIT-UP IS REQUIRED, LANDLORD CAN ASSIST FOR RIGHT OCCUPANT. OWNER IS VERY AGGRESSIVE, BRING ALL POSSIBLE USES TO THE TABLE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 OF marty@lmmre.com.\r\n\r\n&nbsp;', '3030 WHITNEY AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '3030-whitney-avenue-hamden', '', '', '2013-07-08 00:26:37', '2013-07-08 00:26:37', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=96', '0', 'woo_estate', '', '0'),
('97', '3', '2013-05-16 19:53:31', '2013-05-16 19:53:31', '', '3030 Whitney, photo', '', 'inherit', 'open', 'open', '', '3030-whitney-photo', '', '', '2013-05-16 19:53:31', '2013-05-16 19:53:31', '', '96', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/3030-Whitney-photo.jpg', '0', 'attachment', 'image/jpeg', '0'),
('100', '3', '2013-05-17 18:41:13', '2013-05-17 18:41:13', 'GREAT OPPORTUNITY!!!  BASEMENT HAS DIRT FLOOR. OVERSIZED LOT. MAY ALLOW ADDITIONAL APARTMENTS.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON 203-389-5377 X 30 OR michael@lmmre.com', '176 BOYDEN STREET, WATERBURY ', '', 'publish', 'closed', 'closed', '', '176-boyden-street-waterbury', '', '', '2013-07-08 00:28:42', '2013-07-08 00:28:42', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=100', '0', 'woo_estate', '', '0'),
('101', '3', '2013-05-17 18:35:46', '2013-05-17 18:35:46', '', '176 Boyden', '', 'inherit', 'open', 'open', '', '176-boyden', '', '', '2013-05-17 18:35:46', '2013-05-17 18:35:46', '', '100', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/176-Boyden.jpg', '0', 'attachment', 'image/jpeg', '0'),
('107', '3', '2013-05-21 17:49:24', '2013-05-21 17:49:24', '1900 SF BUILDING ON SKIFF STREET BETWEEN WHITNEY AND DIXWELL ACROSS FROM STOP AND SHOP.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MICHAEL GORDON 203-389-5377 X 25 OR michael@lmmre.com', '80 SKIFF STREET, HAMDEN', '', 'publish', 'closed', 'closed', '', '80-skiff-street-hamden', '', '', '2013-07-08 00:31:34', '2013-07-08 00:31:34', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=107', '0', 'woo_estate', '', '0'),
('108', '3', '2013-05-21 17:47:23', '2013-05-21 17:47:23', '', '80 Skiff St.', '', 'inherit', 'open', 'open', '', '80-skiff-st', '', '', '2013-05-21 17:47:23', '2013-05-21 17:47:23', '', '107', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/80-Skiff-St..jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('109', '3', '2013-07-08 00:31:47', '2013-07-08 00:31:47', '1900 SF BUILDING ON SKIFF STREET BETWEEN WHITNEY AND DIXWELL ACROSS FROM STOP AND SHOP.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MICHAEL GORDON 203-389-5377 X 25 OR michael@lmmre.com', '80 SKIFF STREET, HAMDEN', '', 'inherit', 'open', 'open', '', '107-autosave', '', '', '2013-07-08 00:31:47', '2013-07-08 00:31:47', '', '107', 'http://www.lmmre.com/property/2013/05/21/107-autosave/', '0', 'revision', '', '0'),
('110', '3', '2013-05-21 18:19:03', '2013-05-21 18:19:03', 'CORNER OF BROAD STREET. HIGH TRAFFIC RETAIL/OFFICE LOCATION. VACANT SPACE HAS BEEN GUTTED. CAN BE FIT OUT TO SUIT ALMOST ANY NEED.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MICHAEL GORDON 203-389-5377 OR michael@lmmre.com\r\n\r\n&nbsp;', '77 WHITING STREET, PLAINVILLE', '', 'publish', 'closed', 'closed', '', '77-whiting-street-plainville', '', '', '2013-07-08 00:32:42', '2013-07-08 00:32:42', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=110', '0', 'woo_estate', '', '0'),
('111', '3', '2013-05-21 18:24:56', '2013-05-21 18:24:56', 'CORNER OF BROAD STREET. HIGH TRAFFIC RETAIL/OFFICE LOCATION. VACANT SPACE HAS BEEN GUTTED. CAN BE FIT OUT TO SUIT ALMOST ANY NEED.\n\nFOR FURTHER INFORMATION\n\nCONTACT MICHAEL GORDON 203-389-5377 OR michael@lmmre.com\n\n&nbsp;', '77 WHITING STREET, PLAINVILLE', '', 'inherit', 'open', 'open', '', '110-autosave', '', '', '2013-05-21 18:24:56', '2013-05-21 18:24:56', '', '110', 'http://www.lmmre.com/property/2013/05/21/110-autosave/', '0', 'revision', '', '0'),
('112', '3', '2013-05-21 18:22:53', '2013-05-21 18:22:53', '', '77 Whiting Street', '', 'inherit', 'open', 'open', '', '77-whiting-street', '', '', '2013-05-21 18:22:53', '2013-05-21 18:22:53', '', '110', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/77-Whiting-Street.jpg', '0', 'attachment', 'image/jpeg', '0'),
('113', '3', '2013-05-21 18:41:27', '2013-05-21 18:41:27', 'PRICED REDUCED!!!!\r\n32,964 SF 2 STORY BLOCK WAREHOUSE BUILDING ON 1.01 ACRES. REGIONAL BUSINESS ZONE, SIGNALIZED INTERSECTION, PERFECT FOR RETAIL, WAREHOUSE, OFFICE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT STEVE MILLER\r\n\r\n203-389-5377 X 22\r\n\r\nsteve@lmmre.com', '715 BOSTON POST ROAD, WEST HAVEN', '', 'publish', 'closed', 'closed', '', '715-boston-post-road-west-haven', '', '', '2013-08-15 15:51:47', '2013-08-15 15:51:47', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=113', '0', 'woo_estate', '', '0'),
('114', '3', '2013-05-21 19:12:24', '2013-05-21 19:12:24', 'VERY WELL MAINTAINED INDUSTRIAL BUILDING WITH A WELL APPOINTED OFFICE AREA. 20\'CEILINGS IN WAREHOUSE/SHOP AREA, TWO (HIGH) OVERHEAD DOORS. EXHAUST SYSTEM IN CEILING WITH TIMER. ALSO A ROOF DRAIN IS IN PLACE. OFFICE AREA HAS CENTRAL AIR CONDITIONING AND AN ATTIC FOR STORAGE. ROOF REDONE IN 2011. ADDITIONAL  LAND IS AVAILABLE  FROM THE CITY 60X75/50X75. PHASE 3 DELTA SYSTEM IS IN  PLACE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nmarty@lmmre.com', '196 FRONTAGE ROAD, WEST HAVEN', '', 'publish', 'closed', 'closed', '', '196-frontage-road-west-haven', '', '', '2013-07-08 00:40:55', '2013-07-08 00:40:55', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=114', '0', 'woo_estate', '', '0'),
('115', '3', '2013-05-21 19:08:50', '2013-05-21 19:08:50', '', '196 Frontage', '', 'inherit', 'open', 'open', '', '196-frontage', '', '', '2013-05-21 19:08:50', '2013-05-21 19:08:50', '', '114', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/196-Frontage.jpg', '0', 'attachment', 'image/jpeg', '0'),
('117', '3', '2013-08-15 15:44:24', '2013-08-15 15:44:24', '<strong>PRICE REDUCED!!!!</strong>\n\n32,964 SF 2 STORY BLOCK WAREHOUSE BUILDING ON 1.01 ACRES. REGIONAL BUSINESS ZONE, SIGNALIZED INTERSECTION, PERFECT FOR RETAIL, WAREHOUSE, OFFICE.\n\nFOR FURTHER INFORMATION\n\nCONTACT STEVE MILLER\n\n203-389-5377 X 22\n\nsteve@lmmre.com', '715 BOSTON POST ROAD, WEST HAVEN', '', 'inherit', 'open', 'open', '', '113-autosave', '', '', '2013-08-15 15:44:24', '2013-08-15 15:44:24', '', '113', 'http://www.lmmre.com/property/2013/05/21/113-autosave/', '0', 'revision', '', '0'),
('118', '3', '2013-05-21 19:27:31', '2013-05-21 19:27:31', 'GREAT LOCATION IN THIS PROFESSIONAL BUILDING. IT IS THE FIRST DOOR AT THE ENTRANCE TO THE PROPERTY. 800 SF OFFICE NEAR NEW TOWN HALL. 2 PRIVATE OFFICES, CONFERENCE ROOM, RECEPTION. NO STAIRS. RENT INCLUDES EVERYTHING INCLUDING HEAT AND AIR CONDITIONING. EXCELLENT SIGNAGE ALLOWED. $1,000 PER MONTH. ALSO 440 SF RECEPTION AND PRIVATE OFFICE. 440 SF  - RENT IS $550 FULL GROSS.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nmarty@lmmre.com', '105 SANFORD STREET, HAMDEN', '', 'publish', 'closed', 'closed', '', '105-sanford-street-hamden', '', '', '2013-07-18 13:45:22', '2013-07-18 13:45:22', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=118', '0', 'woo_estate', '', '0'),
('119', '3', '2013-05-21 19:23:02', '2013-05-21 19:23:02', '', '105 Sanford', '', 'inherit', 'open', 'open', '', '105-sanford', '', '', '2013-05-21 19:23:02', '2013-05-21 19:23:02', '', '118', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/105-Sanford.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('120', '3', '2013-05-21 19:40:03', '2013-05-21 19:40:03', '14+ ACRES WITH I-691 FRONTAGE. WAS ZONED FOR CATERING HALL. HOUSE AND GARAGE NEED TO BE REMOVED.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON\r\n\r\n203-389-5377 X 25\r\n\r\nmichael@lmmre.com', '306 EAST JOHNSON AVENUE, CHESHIRE', '', 'publish', 'closed', 'closed', '', '306-east-johnson-avenue-cheshire', '', '', '2013-07-08 00:47:00', '2013-07-08 00:47:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=120', '0', 'woo_estate', '', '0'),
('121', '3', '2013-05-21 19:56:24', '2013-05-21 19:56:24', '5,000 SF INDUSTRIAL/GARAGE BUILDING. 2,500 SF EACH FLOOR. 1ST FLOOR WILL BE VACANT AT THE TIME OF SALE. 3 BAYS, OFFICE. NEWLY RENOVATED.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X14\r\n\r\nnoah@lmmre.com', '19 MARIETTA STREET, HAMDEN', '', 'publish', 'closed', 'closed', '', '19-marietta-street-hamden', '', '', '2013-07-08 00:48:34', '2013-07-08 00:48:34', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=121', '0', 'woo_estate', '', '0'),
('122', '3', '2013-05-21 19:54:28', '2013-05-21 19:54:28', '', '19 marietta (3)', '', 'inherit', 'open', 'open', '', '19-marietta-3', '', '', '2013-05-21 19:54:28', '2013-05-21 19:54:28', '', '121', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/19-marietta-3.jpg', '0', 'attachment', 'image/jpeg', '0'),
('123', '3', '2013-05-21 20:04:40', '2013-05-21 20:04:40', '20 ACRES LOCATED IN DESIRABLE INDUSTRIAL PARK OFF ROUTE 15 TO BERLIN TURNPIKE TO DEMING ROAD TO CHRISTIAN LANE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT STEVE MILLER\r\n\r\n203-389-5377 X22\r\n\r\nsteve@lmmre.com', '0 CHRISTIAN LANE, BERLIN', '', 'publish', 'closed', 'closed', '', '0-christian-lane-berlin', '', '', '2013-07-08 00:49:40', '2013-07-08 00:49:40', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=123', '0', 'woo_estate', '', '0'),
('124', '3', '2013-05-21 20:17:20', '2013-05-21 20:17:20', 'UP TO 40,000 SF OF BEAUTIFUL OFFICE SPACE LOCATED IN THE HEART OF WESTVILLE, NEW HAVEN. FULLY HANDICAP ACCESSIBLE. AMPLE FREE PARKING IN PARK LIKE SETTING. THIS IS A GREAT OPPORTUNITY TO TAKE ADVANTAGE OF AN IMPRESSIVE OFFICE BUILD OUT BY A FORTUNE 500 COMPANY. OFFICE SPACE INCLUDES EXECUTIVE SUITES. MULTIPLE CONFERENCE ROOMS, KITCHEN, ETC. CEILING HEIGHTS 9-18 FEET.', '495 BLAKE STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '495-blake-street-new-haven', '', '', '2013-07-08 00:51:43', '2013-07-08 00:51:43', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=124', '0', 'woo_estate', '', '0'),
('125', '3', '2013-05-21 20:15:35', '2013-05-21 20:15:35', '', '495 Blake Street-1', '', 'inherit', 'open', 'open', '', '495-blake-street-1', '', '', '2013-05-21 20:15:35', '2013-05-21 20:15:35', '', '124', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/495-Blake-Street-1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('126', '3', '2013-05-21 20:26:11', '2013-05-21 20:26:11', 'WELL LOCATED LIGHT INDUSTRIAL BUILDING ON BUSY ROAD. COULD BE RETAIL, DISTRIBUTION, INDUSTRIAL. LARGE PIECE OF PROPERTY WITH DETACHED 720 SF GARAGE. NEXT DOOR TO RITE AID PHARMACY.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT\r\n\r\nSTEVE MILLER\r\n\r\n203-389-5377 X 22\r\n\r\nsteve@lmmre.com', '22 HEMINGWAY AVENUE, EAST HAVEN', '', 'publish', 'closed', 'closed', '', '22-hemingway-avenue-east-haven', '', '', '2013-07-08 00:57:00', '2013-07-08 00:57:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=126', '0', 'woo_estate', '', '0'),
('193', '3', '2013-05-29 19:25:14', '2013-05-29 19:25:14', '<b>LOCATION - LOCATION - LOCATION</b> - WAREHOUSE WITH ONE TRUCK DOCK AND ONE WALK-IN DOOR. THERE IS A FRAMED ADDITIONAL OVERHEAD DOOR IF TENANT WANTS TO CHANGE FORMAT. GREAT LOCATION - ONLY 2 TRAFFIC LIGHTS TO I-95 EAST AND WEST.\r\n\r\n&nbsp;\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nOR marty@lmmre.com', '155 TRUMAN STREET, NEW HAVEN', '', 'trash', 'closed', 'closed', '', '155-truman-street-new-haven', '', '', '2013-08-14 20:12:23', '2013-08-14 20:12:23', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=193', '0', 'woo_estate', '', '0'),
('128', '3', '2013-07-08 00:53:40', '2013-07-08 00:53:40', 'WELL LOCATED LIGHT INDUSTRIAL BUILDING ON BUSY ROAD. COULD BE RETAIL, DISTRIBUTION, INDUSTRIAL. LARGE PIECE OF PROPERTY WITH DETACHED 720 SF GARAGE. NEXT DOOR TO RITE AID PHARMACY.\n\nFOR FURTHER INFORMATION\n\nCONTACT\n\nSTEVE MILLER\n\n203-389-5377 X 22\n\nsteve@lmmre.com', '22 HEMINGWAY AVENUE, EAST HAVEN', '', 'inherit', 'open', 'open', '', '126-autosave', '', '', '2013-07-08 00:53:40', '2013-07-08 00:53:40', '', '126', 'http://www.lmmre.com/property/2013/05/21/126-autosave/', '0', 'revision', '', '0'),
('129', '3', '2013-05-21 20:38:38', '2013-05-21 20:38:38', 'A VERY SIZABLE, LOFTY AMERICAN BISTRO, WITH TABLES RUNNING THE PERIPHERY AROUND AN ENORMOUS BAR SPACE IN THE MIDDLE OF THE ROOM. WALKING THROUGH THE FRONT DOOR, DINERS ARE GREETED BY SELECTIONS FROM THE REMARKABLE AND REASONABLE WINE LIST, KEPT UNDER GLASS. THERE IS A PREPONDERANCE OF POLISHED BLACK AND RED THE RESULT WARM AND INVITING WHILE STILL EDGY AND ELEGANT. THE 70 SEAT PATIO IS A MAJOR ATTRACTION IN SUMMER MONTHS. YOU CAN\'T GO WRONG IN THIS LOCATION AND IS A MUST SEE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT SHAWN REILLY\r\n\r\n203-389-5377 X 29\r\n\r\nshawn@lmmre.com\r\n\r\n&nbsp;', 'SUPERB RESTAURANT/BAR', '', 'publish', 'closed', 'closed', '', 'superb-restaurantbar', '', '', '2013-07-08 00:59:40', '2013-07-08 00:59:40', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=129', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('131', '3', '2013-07-08 00:59:46', '2013-07-08 00:59:46', 'A VERY SIZABLE, LOFTY AMERICAN BISTRO, WITH TABLES RUNNING THE PERIPHERY AROUND AN ENORMOUS BAR SPACE IN THE MIDDLE OF THE ROOM. WALKING THROUGH THE FRONT DOOR, DINERS ARE GREETED BY SELECTIONS FROM THE REMARKABLE AND REASONABLE WINE LIST, KEPT UNDER GLASS. THERE IS A PREPONDERANCE OF POLISHED BLACK AND RED THE RESULT WARM AND INVITING WHILE STILL EDGY AND ELEGANT. THE 70 SEAT PATIO IS A MAJOR ATTRACTION IN SUMMER MONTHS. YOU CAN\'T GO WRONG IN THIS LOCATION AND IS A MUST SEE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT SHAWN REILLY\r\n\r\n203-389-5377 X 29\r\n\r\nshawn@lmmre.com\r\n\r\n&nbsp;', 'SUPERB RESTAURANT/BAR', '', 'inherit', 'open', 'open', '', '129-autosave', '', '', '2013-07-08 00:59:46', '2013-07-08 00:59:46', '', '129', 'http://www.lmmre.com/property/2013/05/21/129-autosave/', '0', 'revision', '', '0'),
('228', '3', '2013-05-30 14:20:26', '2013-05-30 14:20:26', '', '41 Middletown 001', '', 'inherit', 'open', 'open', '', '41-middletown-001', '', '', '2013-05-30 14:20:26', '2013-05-30 14:20:26', '', '226', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/41-Middletown-001.jpg', '0', 'attachment', 'image/jpeg', '0'),
('136', '3', '2013-05-23 16:08:06', '2013-05-23 16:08:06', '', 'business', '', 'inherit', 'open', 'open', '', 'business', '', '', '2013-05-23 16:08:06', '2013-05-23 16:08:06', '', '135', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/business.jpg', '0', 'attachment', 'image/jpeg', '0'),
('138', '3', '2013-05-23 16:38:02', '2013-05-23 16:38:02', '10,000 SF OFFICE SPACE IN SHERMAN AVENUE BUSINESS PARK. MANY PRIVATE OFFICES. AMPLE PARKING.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X14\r\n\r\nnoah@lmmre.com', '33 ROSSOTTO DRIVE, HAMDEN', '', 'publish', 'closed', 'closed', '', '33-rossotto-drive-hamden', '', '', '2013-07-08 01:01:17', '2013-07-08 01:01:17', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=138', '0', 'woo_estate', '', '0'),
('139', '3', '2013-05-23 16:36:13', '2013-05-23 16:36:13', '', '33 Rossotto Dr', '', 'inherit', 'open', 'open', '', '33-rossotto-dr', '', '', '2013-05-23 16:36:13', '2013-05-23 16:36:13', '', '138', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/33-Rossotto-Dr.jpg', '0', 'attachment', 'image/jpeg', '0'),
('269', '3', '2013-05-31 14:31:03', '2013-05-31 14:31:03', '', '30 Hazel St.', '', 'inherit', 'open', 'open', '', '30-hazel-st', '', '', '2013-05-31 14:31:03', '2013-05-31 14:31:03', '', '267', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/30-Hazel-St..jpg', '0', 'attachment', 'image/jpeg', '0'),
('141', '3', '2013-05-23 16:52:02', '2013-05-23 16:52:02', 'TWO STORY OFFICE BUILDING LOCATED OPPOSITE HALL OF RECORDS ON ORANGE STREET. PAID PARKING BEHIND BUILDING. 3,000 SF OF REALLY BEAUTIFUL OFFICE SPACE.  CONFERENCE ROOM, PRIVATE SHOWER, SEVERAL OFFICES, LIBRARY, RECEPTION, ETC.\r\n\r\nTHIS IS TRULY A MAGNIFICENT OFFICE THAT WILL PLEASE THE MOST DISCRIMINATE  PERSON. THIS SPACE IS LISTED AT $18.00 SF NNN.', '201 ORANGE STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '201-orange-street-new-haven', '', '', '2013-07-07 13:49:33', '2013-07-07 13:49:33', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=141', '0', 'woo_estate', '', '0'),
('142', '3', '2013-05-23 16:50:00', '2013-05-23 16:50:00', '', '201 Orange Street', '', 'inherit', 'open', 'open', '', '201-orange-street', '', '', '2013-05-23 16:50:00', '2013-05-23 16:50:00', '', '141', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/201-Orange-Street.jpg', '0', 'attachment', 'image/jpeg', '0'),
('143', '3', '2013-05-23 17:01:12', '2013-05-23 17:01:12', '744 SF OF RETAIL SPACE AVAILABLE IN BUSY MT CARMEL CENTER. LOCATED RIGHT OFF THE I-91 CONNECTOR AND 1 MILE FROM QUINNIPIAC UNIVERSITY.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X14\r\n\r\nnoah@lmmre.com', '2985 WHITNEY AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '2985-whitney-avenue-new-haven', '', '', '2013-07-08 01:04:33', '2013-07-08 01:04:33', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=143', '0', 'woo_estate', '', '0'),
('144', '3', '2013-05-23 16:59:19', '2013-05-23 16:59:19', '', '2977 Whitney', '', 'inherit', 'open', 'open', '', '2977-whitney', '', '', '2013-05-23 16:59:19', '2013-05-23 16:59:19', '', '143', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/2977-Whitney.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('151', '3', '2013-05-24 18:10:38', '2013-05-24 18:10:38', '20,000 SF OF INDUSTRIAL/FLEX SPACE AVAILABLE. FULLY AIR CONDITIONED. 1 LOADING DOCK. NICELY FINISHED OFFICE SPACE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X14\r\n\r\nnoah@lmmre.com\r\n\r\n&nbsp;', '33 ROSSOTTO DRIVE, HAMDEN', '', 'publish', 'closed', 'closed', '', '33-rossotto-drive-hamden-2', '', '', '2013-07-08 01:08:08', '2013-07-08 01:08:08', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=151', '0', 'woo_estate', '', '0'),
('152', '3', '2013-05-24 18:09:02', '2013-05-24 18:09:02', '', '33 Rossotto Dr', '', 'inherit', 'open', 'open', '', '33-rossotto-dr-2', '', '', '2013-05-24 18:09:02', '2013-05-24 18:09:02', '', '151', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/33-Rossotto-Dr1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('160', '3', '2013-07-07 13:34:42', '2013-07-07 13:34:42', 'HIGH TRAFFIC LOCATION ON BUSY WHALLEY AVENUE. AUTOMOTIVE USE ALLOWED. 5,000 SF 1 ST FLOOR, 388 SF 2ND FLOOR, 1,200 SF BASEMENT. 2 DRIVE-IN OVERHEAD DOOR.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON\r\n\r\n203-389-5377 OR michael@lmmre.com', '223 WHALLEY AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '159-autosave', '', '', '2013-07-07 13:34:42', '2013-07-07 13:34:42', '', '159', 'http://www.lmmre.com/property/2013/05/28/159-autosave/', '0', 'revision', '', '0'),
('161', '3', '2013-05-28 17:13:09', '2013-05-28 17:13:09', '', '223 Whalley Ave-1', '', 'inherit', 'open', 'open', '', '223-whalley-ave-1', '', '', '2013-05-28 17:13:09', '2013-05-28 17:13:09', '', '159', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/223-Whalley-Ave-1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('832', '3', '2013-08-28 13:16:43', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2013-08-28 13:16:43', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?p=832', '0', 'post', '', '0'),
('833', '3', '2013-08-29 12:48:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2013-08-29 12:48:31', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?p=833', '0', 'post', '', '0'),
('834', '3', '2013-08-29 14:28:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2013-08-29 14:28:31', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?p=834', '0', 'post', '', '0'),
('164', '3', '2013-05-28 17:22:45', '2013-05-28 17:22:45', '', '1522 Whalley', '', 'inherit', 'open', 'open', '', '1522-whalley', '', '', '2013-05-28 17:22:45', '2013-05-28 17:22:45', '', '0', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1522-Whalley.jpg', '0', 'attachment', 'image/jpeg', '0'),
('294', '3', '2013-06-25 16:32:12', '2013-06-25 16:32:12', 'THERE HAS BEEN AN AUTO BODY SHOP AT THIS LOCATION FOR OVER FORTY YEARS AND CAN BE YOURS FOR THIS OUTRAGEOUS LOW PRICE. THIS EXCEPTIONAL OPPORTUNITY WITH USED CAR LICENSE, PAINT BOOTH, TWO LIFTS, IN A AAA LOCATION ON BUSY ROAD ACROSS FROM STOP &amp; SHOP CENTER AND NEW S &amp; S GAS STATION JUST BEGGING FOR THE RIGHT QUALIFIED EXPERIENCED OPERATOR. DON\'T MISS OUT CALL NOW TO BOOK APPOINTMENT TO SEE THIS SPECIAL CHANCE TO BE IN BUSINESS FOR THIS RIDICULOUS UNHEARD OF PRICE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', '490 ORCHARD STREET, NEW HAVEN, CT', '', 'inherit', 'open', 'open', '', '293-autosave', '', '', '2013-06-25 16:32:12', '2013-06-25 16:32:12', '', '293', 'http://www.lmmre.com/property/2013/06/06/293-autosave/', '0', 'revision', '', '0'),
('166', '3', '2013-05-28 19:25:19', '2013-05-28 19:25:19', 'GOOD CLEAN SPACE AND GOOD VISIBILITY FROM SOUTH BROAD STREET. 2,000 SF AND 3,000 SF AVAILABLE IMMEDIATELY IN STRIP CENTER TOTALING 13,628 SF WITH BUSY RETAIL TENANTS INCLUDING HARSTANS JEWELERS AND AT&amp;T. GREAT SPACE FOR ANY RETAIL, COMMERCIAL, RESTAURANT OR MEDICAL USE. PLENTY OF PARKING.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON\r\n\r\n203-389-5377 X 25 OR michael@lmmre.com\r\n\r\n&nbsp;', '1207 SOUTH BROAD, WALLINGFORD', '', 'publish', 'closed', 'closed', '', '1207-south-broad-wallingford', '', '', '2013-07-07 14:41:25', '2013-07-07 14:41:25', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=166', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('167', '3', '2013-05-28 19:23:58', '2013-05-28 19:23:58', '', '1207 South Broad', '', 'inherit', 'open', 'open', '', '1207-south-broad', '', '', '2013-05-28 19:23:58', '2013-05-28 19:23:58', '', '166', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1207-South-Broad.jpg', '0', 'attachment', 'image/jpeg', '0'),
('168', '3', '2013-05-28 19:31:33', '2013-05-28 19:31:33', 'GOOD CLEAN SPACE AND GOOD VISIBILITY FROM SOUTH BROAD STREET. 2,000 SF AND 3,000 SF AVAILABLE IMMEDIATELY IN STRIP CENTER TOTALING 13,628 SF WITH BUSY RETAIL TENANTS INCLUDING HARSTANS JEWELERS AND AT&amp;T. GREAT SPACE FOR ANY RETAIL, COMMERCIAL, RESTAURANT OR MEDICAL USE. PLENTY OF PARKING.\n\nFOR FURTHER INFORMATION\n\nCONTACT MIKE GORDON\n\n203-389-5377 X 25 OR michael@lmmre.com\n\n&nbsp;', '1207 SOUTH BROAD, WALLINGFORD', '', 'inherit', 'open', 'open', '', '166-autosave', '', '', '2013-05-28 19:31:33', '2013-05-28 19:31:33', '', '166', 'http://www.lmmre.com/property/2013/05/28/166-autosave/', '0', 'revision', '', '0'),
('169', '3', '2013-05-28 19:38:42', '2013-05-28 19:38:42', '1,200 SF FORMER VETERINARY OFFICE - GREAT FOR ANY RETAIL, OFFICE, MEDICAL USE. VET IS MOVING TO A LARGER SPACE NEXT DOOR.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON\r\n\r\n203-389-5377 OR michael@lmmre.com\r\n\r\n&nbsp;', '2364 FOXON ROAD, NORTH BRANFORD', '', 'publish', 'closed', 'closed', '', '2364-foxon-road-north-branford', '', '', '2013-07-08 01:42:00', '2013-07-08 01:42:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=169', '0', 'woo_estate', '', '0'),
('170', '3', '2013-05-28 19:39:46', '2013-05-28 19:39:46', '1,200 SF FORMER VETERINARY OFFICE - GREAT FOR ANY RETAIL, OFFICE, MEDICAL USE. VET IS MOVING TO A LARGER SPACE NEXT DOOR.\n\nFOR FURTHER INFORMATION\n\nCONTACT MIKE GORDON\n\n203-389-5377 OR michael@lmmre.com\n\n&nbsp;', '2364 FOXON ROAD, NORTH BRANFORD', '', 'inherit', 'open', 'open', '', '169-autosave', '', '', '2013-05-28 19:39:46', '2013-05-28 19:39:46', '', '169', 'http://www.lmmre.com/property/2013/05/28/169-autosave/', '0', 'revision', '', '0'),
('171', '3', '2013-05-28 19:40:10', '2013-05-28 19:40:10', '', '2364 Foxon', '', 'inherit', 'open', 'open', '', '2364-foxon', '', '', '2013-05-28 19:40:10', '2013-05-28 19:40:10', '', '169', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/2364-Foxon.jpg', '0', 'attachment', 'image/jpeg', '0'),
('173', '3', '2013-05-28 20:02:46', '2013-05-28 20:02:46', 'THESE OFFICES WERE FORMERLY OCCUPIED BY A MAJOR CPA FIRM. THERE IS A WAY TO BREAK UP THE FOOTAGE FOR CERTAIN NEEDS. MEDICAL PRACTICES ARE ON THE FIRST FLOOR OF THIS A CATEGORY OFFICE. GENEROUS SIZE PRIVATE OFFICES, CONFERENCE SIZE PRIVATE OFFICE, CONFERENCE ROOMS AND STAFF AREAS.BUILDING IS LOCATED AT A TRAFFIC LIGHT. HIGH TRAFFIC AREA. PERHAPS YOU (AGENT) CAN BRING AN ADMINISTRATIVE OFFICE THAT COULD BENEFIT FROM THE TRAFFIC.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 OR marty@lmmre.com', '497 MAIN STREET, ANSONIA', '', 'publish', 'closed', 'closed', '', '497-main-street-ansonia', '', '', '2013-07-08 01:43:32', '2013-07-08 01:43:32', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=173', '0', 'woo_estate', '', '0'),
('174', '3', '2013-05-28 20:02:29', '2013-05-28 20:02:29', '', '1314 State st', '', 'inherit', 'open', 'open', '', '1314-state-st', '', '', '2013-05-28 20:02:29', '2013-05-28 20:02:29', '', '173', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1314-State-st.jpg', '0', 'attachment', 'image/jpeg', '0'),
('175', '3', '2013-05-28 20:16:55', '2013-05-28 20:16:55', 'THESE OFFICES WERE FORMERLY OCCUPIED BY A MAJOR CPA FIRM. THERE IS A WAY TO BREAK UP THE FOOTAGE FOR CERTAIN NEEDS. MEDICAL PRACTICES ARE ON THE FIRST FLOOR OF THIS A CATEGORY OFFICE. GENEROUS SIZE PRIVATE OFFICES, CONFERENCE SIZE PRIVATE OFFICE, CONFERENCE ROOMS AND STAFF AREAS.BUILDING IS LOCATED AT A TRAFFIC LIGHT. HIGH TRAFFIC AREA. PERHAPS YOU (AGENT) CAN BRING AN ADMINISTRATIVE OFFICE THAT COULD BENEFIT FROM THE TRAFFIC.\n\nFOR MORE INFORMATION\n\nCONTACT MARTY RUFF\n\n203-389-5377 OR marty@lmmre.com', '497 MAIN STREET, ANSONIA', '', 'inherit', 'open', 'open', '', '173-autosave', '', '', '2013-05-28 20:16:55', '2013-05-28 20:16:55', '', '173', 'http://www.lmmre.com/property/2013/05/28/173-autosave/', '0', 'revision', '', '0'),
('176', '3', '2013-05-28 20:14:20', '2013-05-28 20:14:20', '', '497 Main St.', '', 'inherit', 'open', 'open', '', '497-main-st', '', '', '2013-05-28 20:14:20', '2013-05-28 20:14:20', '', '173', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/497-Main-St..jpg', '0', 'attachment', 'image/jpeg', '0'),
('177', '3', '2013-05-28 20:24:41', '2013-05-28 20:24:41', 'OUTSTANDING CONSTRUCTION BOTH INSIDE AND OUT. FIRST FLOOR HAS CERAMIC TILE FLOORING.\r\nGREAT SIGNAGE, AMPLE PARKING. THIS PROPERTY CAN BE RENTED AS ONE (UPSTAIRS AND DOWNSTAIRS) OR BY INDIVIDUAL FLOOR.\r\nTRAFFIC APPROXIMATELY 20,000 PER DAY. HANDICAP ACCESSIBLE.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 OR marty@lmmre.com\r\n\r\n&nbsp;', '1684 DIXWELL AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '1684-dixwell-avenue-hamden', '', '', '2013-07-08 01:44:37', '2013-07-08 01:44:37', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=177', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('178', '3', '2013-05-28 20:23:30', '2013-05-28 20:23:30', '', '1684 Dixwell', '', 'inherit', 'open', 'open', '', '1684-dixwell', '', '', '2013-05-28 20:23:30', '2013-05-28 20:23:30', '', '177', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1684-Dixwell.jpg', '0', 'attachment', 'image/jpeg', '0'),
('179', '3', '2013-05-28 20:36:31', '2013-05-28 20:36:31', 'A MULTI-UNIT FLEX BUILDING WITH OFFICE/SHOWROOM AREA KITCHEN. CENTRAL A/C. ACCESS SHOP/GARAGE W/OH DOOR FROM OFFICES OR DRIVE BEHIND BUILDING AT LEVEL - WALK-IN, BRAND NEW, END OF CUL-DE-SAC FOR SECURITY. WE HAVE AN AGGRESSIVE LANDLORD THAT WANTS TO MAKE DEALS. FIT-UP IN THE OFFICE AREA WILL TAKE 30 DAYS. WE HAVE A FINISHED SUITE THAT CAN BE SEEN.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X 18 OR marty@lmmre.com\r\n\r\n&nbsp;', '57 OZICK DRIVE, DURHAM', '', 'publish', 'closed', 'closed', '', '57-ozick-drive-durham', '', '', '2013-07-08 02:13:54', '2013-07-08 02:13:54', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=179', '0', 'woo_estate', '', '0'),
('180', '3', '2013-05-28 20:35:09', '2013-05-28 20:35:09', '', '57 Ozick Front 001', '', 'inherit', 'open', 'open', '', '57-ozick-front-001', '', '', '2013-05-28 20:35:09', '2013-05-28 20:35:09', '', '179', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/57-Ozick-Front-001.jpg', '0', 'attachment', 'image/jpeg', '0'),
('181', '3', '2013-05-28 20:43:35', '2013-05-28 20:43:35', 'AN OPPORTUNITY TO DEVELOP A MIXED USE COMPLEX ALONG THE PICTURESQUE FARMINGTON CANAL. THIS LANDMARK SITE OFFERS OVER 3 ACRES OF PROPERTY AND 7 BUILDINGS. CONVENIENT TO I-84 EXIT 26, RT 10 AND ALL CONVENIENCES DOWNTOWN CHESHIRE HAS TO OFFER.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT STEVE MILLER\r\n\r\n203-389-5377 X18 OR steve@lmmre.com', '493 WEST MAIN STREET, CHESHIRE', '', 'publish', 'closed', 'closed', '', '493-west-main-street-cheshire', '', '', '2013-07-08 02:15:13', '2013-07-08 02:15:13', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=181', '0', 'woo_estate', '', '0'),
('182', '3', '2013-05-28 20:44:15', '2013-05-28 20:44:15', '', '493 E. Main 001', '', 'inherit', 'open', 'open', '', '493-e-main-001', '', '', '2013-05-28 20:44:15', '2013-05-28 20:44:15', '', '181', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/493-E.-Main-001.jpg', '0', 'attachment', 'image/jpeg', '0'),
('183', '3', '2013-05-30 14:15:20', '2013-05-30 14:15:20', 'AN OPPORTUNITY TO DEVELOP A MIXED USE COMPLEX ALONG THE PICTURESQUE FARMINGTON CANAL. THIS LANDMARK SITE OFFERS OVER 3 ACRES OF PROPERTY AND 7 BUILDINGS. CONVENIENT TO I-84 EXIT 26, RT 10 AND ALL CONVENIENCES DOWNTOWN CHESHIRE HAS TO OFFER.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT STEVE MILLER\r\n\r\n203-389-5377 X18 OR steve@lmmre.com', '493 WEST MAIN STREET, CHESHIRE', '', 'inherit', 'open', 'open', '', '181-autosave', '', '', '2013-05-30 14:15:20', '2013-05-30 14:15:20', '', '181', 'http://www.lmmre.com/property/2013/05/28/181-autosave/', '0', 'revision', '', '0'),
('185', '3', '2013-05-29 13:29:04', '2013-05-29 13:29:04', 'HIGH QUALITY PROFESSIONAL OFFICE BUILDING LOCATED AT BUSY TRAFFIC AREA OF HAMDEN. THIS BUILDING HAS HANDICAP ACCESS. THE SUITE IS ON THE 2ND FLOOR WITH ELEVATOR SERVICE. ALSO THIS BUILDING HAS EASY ACCESS TO I-40 CONNECTOR AND WILBUR CROSS PARKWAY. SUITE IS 1,700 SF.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 OR marty@lmmre.com', '2558 WHITNEY AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '2558-whitney-avenue-hamden', '', '', '2013-07-08 02:12:31', '2013-07-08 02:12:31', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=185', '0', 'woo_estate', '', '0'),
('186', '3', '2013-05-29 13:27:10', '2013-05-29 13:27:10', '', '2558 Whitney Ave', '', 'inherit', 'open', 'open', '', '2558-whitney-ave', '', '', '2013-05-29 13:27:10', '2013-05-29 13:27:10', '', '185', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/2558-Whitney-Ave.jpg', '0', 'attachment', 'image/jpeg', '0'),
('187', '3', '2013-05-29 13:38:53', '2013-05-29 13:38:53', '6,200 SF AND 5,400 SF INDUSTRIAL OR WAREHOUSE SPACE AVAILABLE. 1 OVERHEAD DOOR IN EACH SPACE. SPRINKLER. 18 FOOT CEILING HEIGHT.\r\n\r\n<em><strong>FOR LEASE $2 SQ FT   NNN.</strong></em>\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT ARIN HAYDEN\r\n\r\n203-389-5377 X 38 OR arin@lmmre.com\r\n\r\n&nbsp;', '163 ORANGE AVENUE, WEST HAVEN ', '', 'publish', 'closed', 'closed', '', '163-orange-avenue-west-haven', '', '', '2013-07-08 02:19:54', '2013-07-08 02:19:54', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=187', '0', 'woo_estate', '', '0'),
('188', '3', '2013-05-29 13:37:33', '2013-05-29 13:37:33', '', '163 boston post rd (2)', '', 'inherit', 'open', 'open', '', '163-boston-post-rd-2', '', '', '2013-05-29 13:37:33', '2013-05-29 13:37:33', '', '187', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/163-boston-post-rd-2.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('189', '3', '2013-05-29 13:46:51', '2013-05-29 13:46:51', 'TWO CITY BLOCKS FROM I-95 IN NEW HAVEN. UP TO 16 ACRES OF LAND IN IL ZONE.\r\nOWNER WILL SELL SMALLER QUANTITIES OF ACERAGE AT $395,000 PER ACRE.\r\n<em><strong>ALSO, LAND LEASE AVAILABLE AT $2,000 PER ACRE PER MONTH NNN. </strong></em>\r\n\r\nGREAT FOR INDUSTRIAL PARK. CALL AGENT FOR MORE INFORMATION.\r\n\r\nMARTY RUFF\r\n\r\n203-389-5377 X18 OR marty@lmmre.com\r\n\r\n&nbsp;', '410 ELLA GRASSO BLVD. NEW HAVEN', '', 'publish', 'closed', 'closed', '', '410-ella-grasso-blvd-new-haven', '', '', '2013-07-07 14:08:07', '2013-07-07 14:08:07', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=189', '0', 'woo_estate', '', '0'),
('190', '3', '2013-07-07 14:08:12', '2013-07-07 14:08:12', 'TWO CITY BLOCKS FROM I-95 IN NEW HAVEN. UP TO 16 ACRES OF LAND IN IL ZONE.\r\nOWNER WILL SELL SMALLER QUANTITIES OF ACERAGE AT $395,000 PER ACRE.\r\n<em><strong>ALSO, LAND LEASE AVAILABLE AT $2,000 PER ACRE PER MONTH NNN. </strong></em>\r\n\r\nGREAT FOR INDUSTRIAL PARK. CALL AGENT FOR MORE INFORMATION.\r\n\r\nMARTY RUFF\r\n\r\n203-389-5377 X18 OR marty@lmmre.com\r\n\r\n&nbsp;', '410 ELLA GRASSO BLVD. NEW HAVEN', '', 'inherit', 'open', 'open', '', '189-autosave', '', '', '2013-07-07 14:08:12', '2013-07-07 14:08:12', '', '189', 'http://www.lmmre.com/property/2013/05/29/189-autosave/', '0', 'revision', '', '0'),
('293', '3', '2013-06-06 14:17:25', '2013-06-06 14:17:25', 'THERE HAS BEEN AN AUTO BODY SHOP AT THIS LOCATION FOR OVER FORTY YEARS AND CAN BE YOURS FOR THIS OUTRAGEOUS LOW PRICE. THIS EXCEPTIONAL OPPORTUNITY WITH USED CAR LICENSE, PAINT BOOTH, TWO LIFTS, IN A AAA LOCATION ON BUSY ROAD ACROSS FROM STOP &amp; SHOP CENTER AND NEW S &amp; S GAS STATION JUST BEGGING FOR THE RIGHT QUALIFIED EXPERIENCED OPERATOR. DON\'T MISS OUT CALL NOW TO BOOK APPOINTMENT TO SEE THIS SPECIAL CHANCE TO BE IN BUSINESS FOR THIS RIDICULOUS UNHEARD OF PRICE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', '490 ORCHARD STREET, NEW HAVEN, CT', '', 'trash', 'closed', 'closed', '', '490-orchard-street-new-haven-ct', '', '', '2013-08-30 17:14:18', '2013-08-30 17:14:18', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=293', '0', 'woo_estate', '', '0'),
('194', '3', '2013-05-29 19:23:41', '2013-05-29 19:23:41', '', '155 Truman St', '', 'inherit', 'open', 'open', '', '155-truman-st', '', '', '2013-05-29 19:23:41', '2013-05-29 19:23:41', '', '193', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/155-Truman-St.jpg', '0', 'attachment', 'image/jpeg', '0'),
('195', '3', '2013-05-29 19:34:37', '2013-05-29 19:34:37', 'GREAT OPPORTUNITY FOR MAKING THIS PROPERTY INTO A COMMERCIAL SITE. WALKING DISTANCE TO SCSU. THIS PROPERTY IS LOCATED BETWEEN TWO VERY POPULAR RETAIL ESTABLISHMENTS. ALSO, THE TOWN AT ONE TIME APPROVED EXPANSION OF 4 APARTMENTS AT THE SITE. FLAT LAND BEHIND BUILDING WITH MANY POSSIBILITIES BASED ON ZONE CODE. PRICED TO SELL QUICKLY.\r\n\r\n<em><strong>AVAILABLE FOR LEASE AT $2,300 PER MONTH.</strong></em>\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X 18 OR marty@lmmre.com\r\n\r\n&nbsp;', '42 FAIRVIEW AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '42-fairview-avenue-hamden', '', '', '2013-07-08 02:21:55', '2013-07-08 02:21:55', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=195', '0', 'woo_estate', '', '0'),
('196', '3', '2013-05-29 19:33:40', '2013-05-29 19:33:40', '', '42 Fairview Avenue', '', 'inherit', 'open', 'open', '', '42-fairview-avenue', '', '', '2013-05-29 19:33:40', '2013-05-29 19:33:40', '', '195', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/42-Fairview-Avenue.jpg', '0', 'attachment', 'image/jpeg', '0'),
('197', '3', '2013-05-29 19:47:14', '2013-05-29 19:47:14', 'GREAT OPPORTUNITY TO OWN AN INDUSTRIAL BUILDING WITH OFFICE SPACE, AT A VERY LOW PRICE. SOLID STAND ALONE BUILDING WITH LOADING DOCK AND PARKING FOR 16 VEHICLES. BUILDING RECEIVED MANY UPDATES IN 1981. INNER CITY LOCATION HAS QUIET SURROUNDINGS AND IS NEAR A FORMER MERIDEN HOSPITAL ON KING STREET. DO NOT MISS THIS OPPORTUNITY TO OWN A BUILDING BELOW APPRAISED VALUE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nOR marty@lmmre.com', '35 MERIDIAN STREET, MERIDEN', '', 'publish', 'closed', 'closed', '', '35-meridian-street-meriden', '', '', '2013-07-08 02:22:39', '2013-07-08 02:22:39', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=197', '0', 'woo_estate', '', '0'),
('198', '3', '2013-05-29 19:45:03', '2013-05-29 19:45:03', '', '35 Meridian', '', 'inherit', 'open', 'open', '', '35-meridian', '', '', '2013-05-29 19:45:03', '2013-05-29 19:45:03', '', '197', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/35-Meridian.jpg', '0', 'attachment', 'image/jpeg', '0'),
('199', '3', '2013-05-29 22:36:23', '2013-05-29 22:36:23', 'GREAT OPPORTUNITY TO OWN AN INDUSTRIAL BUILDING WITH OFFICE SPACE, AT A VERY LOW PRICE. SOLID STAND ALONE BUILDING WITH LOADING DOCK AND PARKING FOR 16 VEHICLES. BUILDING RECEIVED MANY UPDATES IN 1981. INNER CITY LOCATION HAS QUIET SURROUNDINGS AND IS NEAR A FORMER MERIDEN HOSPITAL ON KING STREET. DO NOT MISS THIS OPPORTUNITY TO OWN A BUILDING BELOW APPRAISED VALUE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nOR marty@lmmre.com', '35 MERIDIAN STREET, MERIDEN', '', 'inherit', 'open', 'open', '', '197-autosave', '', '', '2013-05-29 22:36:23', '2013-05-29 22:36:23', '', '197', 'http://www.lmmre.com/property/2013/05/29/197-autosave/', '0', 'revision', '', '0'),
('510', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=510', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('201', '3', '2013-05-29 20:35:11', '2013-05-29 20:35:11', 'IN THE SCENIC NORTH END OF HAMDEN, WE OFFER 18.2 ACRES FOR DEVELOPMENT. DEVELOPER CAN CHOOSE FROM MANY POSSIBLE USES SUCH AS: APARTMENTS (OVER 200 UNITS), 55 &amp; OVER RENTAL OR SALE COMPLETE, STUDENT HOUSING (THE OTHER SIDE OF THIS PARCEL IS OWNED BY QUINNIPIAC UNIVERSITY FOR ONE OF THEIR CAMPUSES. ALSO ON THIS SITE OF QUINNIPIAC IS THE TD BANK SPORTS COMPLEX, ALONG WITH 2,000 BEDS FOR STUDENT HOUSING. SIGNIFICANT OPPORTUNITY IF REMOVAL OF QUALITY STONE/MATERIAL.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18 OR marty@lmmre.com', '64 ROCKY TOP ROAD, HAMDEN', '', 'publish', 'closed', 'closed', '', '64-rocky-top-road-hamden', '', '', '2013-08-29 14:41:31', '2013-08-29 14:41:31', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=201', '0', 'woo_estate', '', '0'),
('203', '2', '2013-05-29 20:32:31', '2013-05-29 20:32:31', '', 'lmmre-new-haven-ct-property', '', 'inherit', 'open', 'open', '', 'lmmre-new-haven-ct-property', '', '', '2013-05-29 20:32:31', '2013-05-29 20:32:31', '', '0', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/lmmre-new-haven-ct-property.jpg', '0', 'attachment', 'image/jpeg', '0'),
('206', '3', '2013-05-29 20:48:05', '2013-05-29 20:48:05', '', '30 Washington Ave. jpg', '', 'inherit', 'open', 'open', '', '30-washington-ave-jpg', '', '', '2013-05-29 20:48:05', '2013-05-29 20:48:05', '', '205', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/30-Washington-Ave.-jpg.jpg', '0', 'attachment', 'image/jpeg', '0'),
('208', '3', '2013-05-30 13:57:44', '2013-05-30 13:57:44', 'CIRCA 1871 - GREAT OFFICE LOCATION NEAR TOWN CENTER. 1,250 SF\nAVAILABLE FOR LAW PRACTICE, ACCOUNTANTS, THERAPISTS - AMPLE PARKING.\n\nFOR MORE INFORMATION\n\nCONTACT MARTY RUFF\n\n203-389-5377 X18\n\nmarty@lmmre.com', '30 WASHINGTON AVENUE, NORTH HAVEN', '', 'inherit', 'open', 'open', '', '205-autosave', '', '', '2013-05-30 13:57:44', '2013-05-30 13:57:44', '', '205', 'http://www.lmmre.com/property/2013/05/29/205-autosave/', '0', 'revision', '', '0'),
('209', '3', '2013-07-07 14:14:33', '2013-07-07 14:14:33', 'NICE BUILDING. ALL UNITS WITH BALCONY, GARAGE, WASHER,\r\nDRYER-CONVENIENT TO HOSPITAL ZONE - RT 15 AND I-95.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X 18\r\n\r\nmarty@lmmre.com', '1079 ELLA T GRASSO BLVD. NEW HAVEN', '', 'inherit', 'open', 'open', '', '204-autosave', '', '', '2013-07-07 14:14:33', '2013-07-07 14:14:33', '', '204', 'http://www.lmmre.com/property/2013/05/29/204-autosave/', '0', 'revision', '', '0'),
('210', '3', '2013-07-07 13:44:21', '2013-07-07 13:44:21', '<b>LOCATION - LOCATION - LOCATION</b> - WAREHOUSE WITH ONE TRUCK DOCK AND ONE WALK-IN DOOR. THERE IS A FRAMED ADDITIONAL OVERHEAD DOOR IF TENANT WANTS TO CHANGE FORMAT. GREAT LOCATION - ONLY 2 TRAFFIC LIGHTS TO I-95 EAST AND WEST.\r\n\r\n&nbsp;\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nOR marty@lmmre.com', '155 TRUMAN STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '193-autosave', '', '', '2013-07-07 13:44:21', '2013-07-07 13:44:21', '', '193', 'http://www.lmmre.com/property/2013/05/29/193-autosave/', '0', 'revision', '', '0'),
('211', '3', '2013-07-07 13:50:43', '2013-07-07 13:50:43', '2,500 SF RETAIL/OFFICE. BEAUTIFUL VICTORIAN BUILDING.\r\nWESTVILLE CENTER LOCATION. PLENTY OF FREE PARKING.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT STEVE MILLER 203-389-5377 X22\r\n\r\nsteve@lmmre.com.\r\n\r\n&nbsp;', '881 WHALLEY AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '86-autosave', '', '', '2013-07-07 13:50:43', '2013-07-07 13:50:43', '', '86', 'http://www.lmmre.com/property/2013/05/29/86-autosave/', '0', 'revision', '', '0'),
('212', '3', '2013-07-08 00:20:28', '2013-07-08 00:20:28', 'FANTASTIC SPACE - FRONT-CAN BE ALMOST ANYTHING. BACK WAS DETAIL SHOP. GREAT VISIBILITY FROM WHALLEY AVENUE. GREAT ACCESS TO WHALLEY AVENUE AND MERRITT PARKWAY. FRONT SPACE IS ALMOST 3,300 + SF. LARGE DOOR AND RAMP FOR LOADING DOCK. PLUMBING AND HEATING ALL THERE TO BUILD OFF OF. PERFECT SET FOR ANYTHING AUTO.\r\n\r\nFOR SALE $249,000 OR FOR LEASE $6.00 SF\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT ARIN HAYDEN 203-389-5377 X38\r\n\r\nOR\r\n\r\narin@lmmre.com.', '149 RAMSDELL STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '82-autosave', '', '', '2013-07-08 00:20:28', '2013-07-08 00:20:28', '', '82', 'http://www.lmmre.com/property/2013/05/29/82-autosave/', '0', 'revision', '', '0'),
('213', '3', '2013-07-03 00:23:13', '2013-07-03 00:23:13', 'OUTSTANDING CONSTRUCTION BOTH INSIDE AND OUT. FIRST FLOOR HAS CERAMIC TILE FLOORING.\nGREAT SIGNAGE, AMPLE PARKING. THIS PROPERTY CAN BE RENTED AS ONE (UPSTAIRS AND DOWNSTAIRS) OR BY INDIVIDUAL FLOOR.\nTRAFFIC APPROXIMATELY 20,000 PER DAY. HANDICAP ACCESSIBLE.\n\nFOR MORE INFORMATION\n\nCONTACT MARTY RUFF\n\n203-389-5377 OR marty@lmmre.com\n\n&nbsp;', '1684 DIXWELL AVENUE, HAMDEN', '', 'inherit', 'open', 'open', '', '177-autosave', '', '', '2013-07-03 00:23:13', '2013-07-03 00:23:13', '', '177', 'http://www.lmmre.com/property/2013/05/29/177-autosave/', '0', 'revision', '', '0'),
('217', '3', '2013-05-30 13:53:13', '2013-05-30 13:53:13', '77,896 SF BUILDING FOR LEASE -- 4 STORY, 1.7 ACRES.\r\nLOCATED IN DOWNTOWN ANSONIA BUSINESS DISTRICT.\r\n\r\n<em><strong>ALSO AVAILABLE FOR SALE-- CONTACT AGENT FOR DETAILS</strong></em>\r\n\r\n<em><strong> GREAT OPPORTUNITY FOR REDEVELOPMENT</strong></em>.\r\n\r\n&nbsp;\r\n\r\nCALL AGENT FOR MORE DETAILS\r\n\r\nMARTY RUFF  OR NOAH MEYER\r\n\r\n203-389-5377 X 18 OR X14\r\n\r\nmarty@lmmre.com OR noah@lmmre.com\r\n\r\n&nbsp;', '540 EAST MAIN STREET, ANSONIA', '', 'publish', 'closed', 'closed', '', '540-east-main-street-ansonia', '', '', '2013-07-07 15:25:16', '2013-07-07 15:25:16', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=217', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('218', '2', '2013-05-30 13:51:06', '2013-05-30 13:51:06', '', 'Home', '', 'publish', 'open', 'open', '', 'home-2', '', '', '2013-07-09 00:35:38', '2013-07-09 00:35:38', '', '0', 'http://www.lmmre.com/property/?p=218', '1', 'nav_menu_item', '', '0'),
('220', '3', '2013-07-07 15:24:35', '2013-07-07 15:24:35', '77,896 SF BUILDING FOR LEASE OR SALE. 4 STORY, 1.7 ACRES.\nLOCATED IN DOWNTOWN ANSONIA BUSINESS DISTRICT.\nGREAT OPPORTUNITY FOR REDEVELOPMENT.\n\n&nbsp;\n\nCALL AGENT FOR MORE DETAILS\n\nMARTY RUFF  OR NOAH MEYER\n\n203-389-5377 X 18 OR X14\n\nmarty@lmmre.com OR noah@lmmre.com\n\n&nbsp;', '540 EAST MAIN STREET, ANSONIA', '', 'inherit', 'open', 'open', '', '217-autosave', '', '', '2013-07-07 15:24:35', '2013-07-07 15:24:35', '', '217', 'http://www.lmmre.com/property/2013/05/30/217-autosave/', '0', 'revision', '', '0'),
('221', '3', '2013-05-30 14:02:19', '2013-05-30 14:02:19', '', '540 east main st Ansonia', '', 'inherit', 'open', 'open', '', '540-east-main-st-ansonia', '', '', '2013-05-30 14:02:19', '2013-05-30 14:02:19', '', '217', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/540-east-main-st-Ansonia.jpg', '0', 'attachment', 'image/jpeg', '0'),
('222', '3', '2013-05-30 14:06:39', '2013-05-30 14:06:39', '', '1079', '', 'inherit', 'open', 'open', '', '1079', '', '', '2013-05-30 14:06:39', '2013-05-30 14:06:39', '', '204', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1079.jpg', '0', 'attachment', 'image/jpeg', '0'),
('239', '3', '2013-05-30 15:14:36', '2013-05-30 15:14:36', '', '12 Foch', '', 'inherit', 'open', 'open', '', '12-foch', '', '', '2013-05-30 15:14:36', '2013-05-30 15:14:36', '', '238', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/12-Foch.jpg', '0', 'attachment', 'image/jpeg', '0'),
('225', '3', '2013-05-30 14:12:09', '2013-05-30 14:12:09', 'ALMOST 2 ACRES WITH 8,800 SF OF SELF STORAGE UNITS, 94 UNITS TOTAL. TWO BUILDINGS SURROUNDED BY CHAIN LINK FENCE WITH A CARD KEY OPERATED SECURITY GATE. GREAT OPPORTUNITY FOR INVESTOR/BUSINESS WITH 94 UNITS. SOME ARE TEMPERATURE CONTROLLED.\r\n\r\nFOR FURTHER INFORMATION CONTACT STEVE MILLER 203-389-5377 OR steve@lmmre.com.', '15 RESEARCH DRIVE, BRANFORD - PRICE REDUCED!!!!', '', 'inherit', 'open', 'open', '', '75-autosave', '', '', '2013-05-30 14:12:09', '2013-05-30 14:12:09', '', '75', 'http://www.lmmre.com/property/2013/05/30/75-autosave/', '0', 'revision', '', '0'),
('226', '3', '2013-05-30 14:22:43', '2013-05-30 14:22:43', '2 STORY FREE STANDING BUILDING WITH 4,800 SF ON EACH FLOOR.\r\nROOF IS A FLAT RUBBER ROOF. REPLACEMENT COST PER TOWN IS\r\n$949,981.\r\n\r\n<em><strong>ALSO FOR LEASE AT $7.00 SF</strong></em>.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT BERNIE DIANA\r\n\r\n203-389-5377\r\n\r\nbernie@lmmre.com', '41 MIDDLETOWN AVENUE, NORTH HAVEN', '', 'publish', 'closed', 'closed', '', '41-middletown-avenue-north-haven', '', '', '2013-07-08 13:55:27', '2013-07-08 13:55:27', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=226', '0', 'woo_estate', '', '0'),
('227', '3', '2013-06-26 17:46:48', '2013-06-26 17:46:48', 'A MULTI-UNIT FLEX BUILDING WITH OFFICE/SHOWROOM AREA KITCHEN. CENTRAL A/C. ACCESS SHOP/GARAGE W/OH DOOR FROM OFFICES OR DRIVE BEHIND BUILDING AT LEVEL - WALK-IN, BRAND NEW, END OF CUL-DE-SAC FOR SECURITY. WE HAVE AN AGGRESSIVE LANDLORD THAT WANTS TO MAKE DEALS. FIT-UP IN THE OFFICE AREA WILL TAKE 30 DAYS. WE HAVE A FINISHED SUITE THAT CAN BE SEEN.\n\nFOR MORE INFORMATION\n\nCONTACT MARTY RUFF\n\n203-389-5377 X 18 OR marty@lmmre.com\n\n&nbsp;', '57 OZICK DRIVE, DURHAM', '', 'inherit', 'open', 'open', '', '179-autosave', '', '', '2013-06-26 17:46:48', '2013-06-26 17:46:48', '', '179', 'http://www.lmmre.com/property/2013/05/30/179-autosave/', '0', 'revision', '', '0'),
('229', '3', '2013-05-30 14:27:47', '2013-05-30 14:27:47', 'GREAT VISIBILITY OFF EXIT 9 OF I-91 NEXT TO LONGHORN STEAKHOUSE,\r\nRAYMOUR &amp; FLANIGAN, AND PC RICHARDS &amp; SONS.\r\n\r\nPOSSIBLE FASTFOOD WITH DRIVE-THRU, ICE CREAM SHOP, RETAIL.\r\n\r\n<em><strong>$75,000 GROUND LEASE PER YEAR</strong></em>\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT BERNIE DIANA\r\n\r\n203-389-5377 X30\r\n\r\nbernie@lmmre.com', '19 UNIVERSAL DRIVE, NORTH HAVEN', '', 'publish', 'closed', 'closed', '', '19-universal-drive-north-haven', '', '', '2013-07-08 02:31:58', '2013-07-08 02:31:58', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=229', '0', 'woo_estate', '', '0'),
('231', '3', '2013-05-30 14:27:36', '2013-05-30 14:27:36', '', 'LMM Logo 1', '', 'inherit', 'open', 'open', '', 'lmm-logo-1', '', '', '2013-05-30 14:27:36', '2013-05-30 14:27:36', '', '229', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/LMM-Logo-1.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('232', '3', '2013-07-08 02:31:55', '2013-07-08 02:31:55', 'GREAT VISIBILITY OFF EXIT 9 OF I-91 NEXT TO LONGHORN STEAKHOUSE,\nRAYMOUR &amp; FLANIGAN, AND PC RICHARDS &amp; SONS.\n\nPOSSIBLE FASTFOOD WITH DRIVE-THRU, ICE CREAM SHOP, RETAIL.\n\n<em><strong>$75,000 GROUND LEASE PER YEAR</strong></em>\n\nFOR MORE INFORMATION\n\nCONTACT BERNIE DIANA\n\n203-389-5377 X30\n\nbernie@lmmre.com', '19 UNIVERSAL DRIVE, NORTH HAVEN', '', 'inherit', 'open', 'open', '', '229-autosave', '', '', '2013-07-08 02:31:55', '2013-07-08 02:31:55', '', '229', 'http://www.lmmre.com/property/2013/05/30/229-autosave/', '0', 'revision', '', '0'),
('233', '3', '2013-05-30 14:43:04', '2013-05-30 14:43:04', 'WELL LOCATED LAND FOR LEASE NEAR THE INTERSECTION OF MIDDLETOWN\r\nAVENUE AND QUINNIPIAC AVENUE.\r\n\r\n<em><strong>$50,000 NNN ANNUAL.</strong></em>\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT STEVE MILLER\r\n\r\n203-389-5377 X 22\r\n\r\nsteve@lmmre.com', '82 MIDDLETOWN AVENUE, NORTH HAVEN', '', 'publish', 'closed', 'closed', '', '82-middletown-avenue-north-haven', '', '', '2013-07-08 02:33:48', '2013-07-08 02:33:48', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=233', '0', 'woo_estate', '', '0'),
('234', '3', '2013-07-08 02:33:39', '2013-07-08 02:33:39', 'WELL LOCATED LAND FOR LEASE NEAR THE INTERSECTION OF MIDDLETOWN\nAVENUE AND QUINNIPIAC AVENUE. $50,000 NNN ANNUAL.\n\nFOR MORE INFORMATION\n\nCONTACT STEVE MILLER\n\n203-389-5377 X 22\n\nsteve@lmmre.com', '82 MIDDLETOWN AVENUE, NORTH HAVEN', '', 'inherit', 'open', 'open', '', '233-autosave', '', '', '2013-07-08 02:33:39', '2013-07-08 02:33:39', '', '233', 'http://www.lmmre.com/property/2013/05/30/233-autosave/', '0', 'revision', '', '0'),
('235', '3', '2013-05-30 15:01:18', '2013-05-30 15:01:18', 'PROPERTY INCLUDES 135 &amp; 145 SANFORD STREET. FULLY APPROVED CONDO PROJECT - 36 UNITS ON 2.76 ACRES WITH SCENIC VIEWS OF TURNER\'S POND.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '135 SANFORD STREET, HAMDEN', '', 'publish', 'closed', 'closed', '', '135-sanford-street-hamden', '', '', '2013-07-08 02:35:24', '2013-07-08 02:35:24', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=235', '0', 'woo_estate', '', '0'),
('236', '3', '2013-05-30 15:08:39', '2013-05-30 15:08:39', 'PROPERTY INCLUDES 135 &amp; 145 SANFORD STREET. FULLY APPROVED CONDO PROJECT - 36 UNITS ON 2.76 ACRES WITH SCENIC VIEWS OF TURNER\'S POND.\n\nFOR FURTHER INFORMATION\nCONTACT STEVE MILLER\n203-389-5377 X22\nsteve@lmmre.com', '135 SANFORD STREET, HAMDEN', '', 'inherit', 'open', 'open', '', '235-autosave', '', '', '2013-05-30 15:08:39', '2013-05-30 15:08:39', '', '235', 'http://www.lmmre.com/property/2013/05/30/235-autosave/', '0', 'revision', '', '0'),
('238', '3', '2013-05-30 15:17:19', '2013-05-30 15:17:19', 'SMALL RETAIL/OFFICE BUILDING - VISIBLE FROM DIXWELL AVENUE\r\nPLENTY OF PARKING.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '12 FOCH STREET, HAMDEN', '', 'publish', 'closed', 'closed', '', '12-foch-street-hamden', '', '', '2013-07-08 02:36:34', '2013-07-08 02:36:34', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=238', '0', 'woo_estate', '', '0'),
('240', '3', '2013-05-30 15:27:27', '2013-05-30 15:27:27', '100 FEET FROM WHALLEY AVENUE, REAR OF DUNKIN DONUTS,\r\nNICE PROFESSIONAL OFFICE, BATHROOM, AMITY SECTION\r\nOF WESTVILLE, ON BUSLINE.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '116 ANTHONY STREET, NEW HAVEN', '', 'trash', 'closed', 'closed', '', '116-anthony-street-new-haven', '', '', '2013-08-14 20:12:05', '2013-08-14 20:12:05', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=240', '0', 'woo_estate', '', '0'),
('241', '3', '2013-05-30 15:26:07', '2013-05-30 15:26:07', '', '116 Anthony', '', 'inherit', 'open', 'open', '', '116-anthony', '', '', '2013-05-30 15:26:07', '2013-05-30 15:26:07', '', '240', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/116-Anthony.jpg', '0', 'attachment', 'image/jpeg', '0'),
('242', '3', '2013-07-07 13:43:16', '2013-07-07 13:43:16', '100 FEET FROM WHALLEY AVENUE, REAR OF DUNKIN DONUTS,\r\nNICE PROFESSIONAL OFFICE, BATHROOM, AMITY SECTION\r\nOF WESTVILLE, ON BUSLINE.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '116 ANTHONY STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '240-autosave', '', '', '2013-07-07 13:43:16', '2013-07-07 13:43:16', '', '240', 'http://www.lmmre.com/property/2013/05/30/240-autosave/', '0', 'revision', '', '0'),
('243', '3', '2013-05-30 15:36:03', '2013-05-30 15:36:03', 'GREAT SERVICE/WAREHOUSE BUILDING LOCATED JUST\r\nOFF DIXWELL AVENUE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '1635 R DIXWELL AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '1635-r-dixwell-avenue-hamden', '', '', '2013-07-07 15:23:00', '2013-07-07 15:23:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=243', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('245', '3', '2013-07-03 00:19:38', '2013-07-03 00:19:38', 'GREAT SERVICE/WAREHOUSE BUILDING LOCATED JUST\nOFF DIXWELL AVENUE.\n\nFOR MORE INFORMATION\nCONTACT STEVE MILLER\n203-389-5377 X 22\nsteve@lmmre.com', '1635 R DIXWELL AVENUE, HAMDEN', '', 'inherit', 'open', 'open', '', '243-autosave', '', '', '2013-07-03 00:19:38', '2013-07-03 00:19:38', '', '243', 'http://www.lmmre.com/property/2013/05/30/243-autosave/', '0', 'revision', '', '0'),
('246', '3', '2013-05-30 15:38:23', '2013-05-30 15:38:23', '', '1635R Dixwell', '', 'inherit', 'open', 'open', '', '1635r-dixwell', '', '', '2013-05-30 15:38:23', '2013-05-30 15:38:23', '', '243', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1635R-Dixwell.jpg', '0', 'attachment', 'image/jpeg', '0'),
('247', '3', '2013-05-30 16:02:56', '2013-05-30 16:02:56', 'SOLID BUILDING ON CORNER OF GRAND AND POPLAR ADJACENT TO\r\nFREE MUNICIPAL PARKING LOT.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '275 POPLAR STREET, NEW HAVEN', '', 'trash', 'closed', 'closed', '', '275-poplar-street-new-haven', '', '', '2013-08-14 20:11:53', '2013-08-14 20:11:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=247', '0', 'woo_estate', '', '0'),
('248', '3', '2013-05-30 16:01:07', '2013-05-30 16:01:07', '', '275 Poplar', '', 'inherit', 'open', 'open', '', '275-poplar', '', '', '2013-05-30 16:01:07', '2013-05-30 16:01:07', '', '247', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/275-Poplar.jpg', '0', 'attachment', 'image/jpeg', '0'),
('249', '3', '2013-05-30 16:12:08', '2013-05-30 16:12:08', 'BEAUTIFUL WOODED LAND - 2 ABUTTING LOTS ON EACH SIDE OF THE ROAD\r\nTOTALING 80+/- ACRES. WATER - SEPTIC 30,000 SF ZONING - LOTS OF ROAD\r\nFRONTAGE - NO ENGINEERING HAS BEEN DONE - STARTER HOMES IN\r\n$200,000 PRICE RANGE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '0 HUNTER\'S MOUNTAIN ROAD, NAUGATUCK', '', 'publish', 'closed', 'closed', '', '0-hunters-mountain-road-naugatuck', '', '', '2013-07-07 15:21:53', '2013-07-07 15:21:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=249', '0', 'woo_estate', '', '0'),
('250', '3', '2013-05-30 16:25:39', '2013-05-30 16:25:39', '2 STORY MASONRY OFFICE CONDOMINIUMS LOCATED JUST OFF\r\nMERRITT PARKWAY IN WOODBRIDGE, CT. BUILDING IS IN PARK\r\nLIKE SETTING OVERLOOKING WESTROCK. SPACE AVAILABLE FROM\r\n575 SF UP TO 4200 SF OF CONTIGUOUS SPACE. SPACE IS ALL SUB-\r\nDIVIDABLE PERMITTING ALMOST ANY SIZE SPACE THAT IS NEEDED.\r\nPRICED AT $11.00 SF PLUS UTILITIES.\r\n\r\nCONDOS CAN BE SOLD FOR\r\n$85.00 PER SF. PLEASE LOOK AT BESTOFFICES.COM.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '88 BRADLEY ROAD, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '88-bradley-road-woodbridge', '', '', '2013-07-07 14:48:38', '2013-07-07 14:48:38', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=250', '0', 'woo_estate', '', '0'),
('251', '3', '2013-05-30 16:23:50', '2013-05-30 16:23:50', '', '88 Bradley', '', 'inherit', 'open', 'open', '', '88-bradley', '', '', '2013-05-30 16:23:50', '2013-05-30 16:23:50', '', '250', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/88-Bradley.jpg', '0', 'attachment', 'image/jpeg', '0'),
('254', '3', '2013-05-30 17:06:45', '2013-05-30 17:06:45', 'UNIQUE INDUSTRIAL PROPERTY LOCATED OFF ELLA GRASSO BLVD.\r\nNEAR I-95 EXIT 44. LARGE YARD FOR OUTSIDE STORAGE. BUILDING\r\nALSO FRONTS ON COLUMBUS AVENUE NEXT TO HILL HEALTH. *ALSO\r\nKNOWN AS 530 ELLA GRASSO BLVD.\r\n\r\nFOR MORE INFORMATION\r\nCALL STEVE MILLER\r\n203-389-5377 X22\r\n\r\nsteve@lmmre.com', '2 THORN STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '2-thorn-street-new-haven', '', '', '2013-07-08 02:38:07', '2013-07-08 02:38:07', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=254', '0', 'woo_estate', '', '0'),
('255', '3', '2013-07-07 13:41:26', '2013-07-07 13:41:26', 'UNIQUE INDUSTRIAL PROPERTY LOCATED OFF ELLA GRASSO BLVD.\r\nNEAR I-95 EXIT 44. LARGE YARD FOR OUTSIDE STORAGE. BUILDING\r\nALSO FRONTS ON COLUMBUS AVENUE NEXT TO HILL HEALTH. *ALSO\r\nKNOWN AS 530 ELLA GRASSO BLVD.\r\n\r\nFOR MORE INFORMATION\r\nCALL STEVE MILLER\r\n203-389-5377 X22\r\n\r\nsteve@lmmre.com', '2 THORN STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '254-autosave', '', '', '2013-07-07 13:41:26', '2013-07-07 13:41:26', '', '254', 'http://www.lmmre.com/property/2013/05/30/254-autosave/', '0', 'revision', '', '0'),
('256', '3', '2013-05-30 17:09:07', '2013-05-30 17:09:07', '', '2 THORN STREET', '', 'inherit', 'open', 'open', '', '2-thorn-street', '', '', '2013-05-30 17:09:07', '2013-05-30 17:09:07', '', '254', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/2-THORN-STREET.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('258', '3', '2013-05-31 13:36:16', '2013-05-31 13:36:16', 'SOLID 5,000 SF INDUSTRIAL/MANUFACTURING BUILDING FULLY AIR CONDITIONED. HEAVY POWER RECENTLY UPDATED. EXCELLENT LOCATION BETWEEN RT 1 AND I-95 EXIT 45 OFF ELLA GRASSO BLVD.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '30 PRINTERS LANE, NEW HAVEN', '', 'trash', 'closed', 'closed', '', '30-printers-lane-new-haven', '', '', '2013-08-14 20:11:33', '2013-08-14 20:11:33', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=258', '0', 'woo_estate', '', '0'),
('259', '3', '2013-05-31 13:33:49', '2013-05-31 13:33:49', '', '30 Printers Lane', '', 'inherit', 'open', 'open', '', '30-printers-lane', '', '', '2013-05-31 13:33:49', '2013-05-31 13:33:49', '', '258', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/30-Printers-Lane.jpg', '0', 'attachment', 'image/jpeg', '0'),
('260', '3', '2013-05-31 13:46:29', '2013-05-31 13:46:29', 'HIGH BAY INDUSTRIAL BUILDING - PREVIOUS USES INCLUDE SPORTS CENTER, DISTRIBUTION CENTER, TRUCK TERMINAL/ 16 DRIVE-IN DOORS. 2,400 SF OFFICE. ALSO AVAILABLE FOR LEASE AT $5.00 SF.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '136 BRADLEY ROAD, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '136-bradley-road-woodbridge', '', '', '2013-07-07 14:47:45', '2013-07-07 14:47:45', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=260', '0', 'woo_estate', '', '0'),
('261', '3', '2013-07-07 14:47:50', '2013-07-07 14:47:50', 'HIGH BAY INDUSTRIAL BUILDING - PREVIOUS USES INCLUDE SPORTS CENTER, DISTRIBUTION CENTER, TRUCK TERMINAL/ 16 DRIVE-IN DOORS. 2,400 SF OFFICE. ALSO AVAILABLE FOR LEASE AT $5.00 SF.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '136 BRADLEY ROAD, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '260-autosave', '', '', '2013-07-07 14:47:50', '2013-07-07 14:47:50', '', '260', 'http://www.lmmre.com/property/2013/05/31/260-autosave/', '0', 'revision', '', '0'),
('262', '3', '2013-05-31 13:47:34', '2013-05-31 13:47:34', '', '136 BRADLEY001', '', 'inherit', 'open', 'open', '', '136-bradley001', '', '', '2013-05-31 13:47:34', '2013-05-31 13:47:34', '', '260', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/136-BRADLEY001.jpg', '0', 'attachment', 'image/jpeg', '0'),
('263', '3', '2013-05-31 14:33:24', '2013-05-31 14:33:24', 'GREAT RETAIL OR RESTAURANT SITE. GROUND LEASE FOR $50,000/YEAR.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '1646 LITCHFIELD TPKE., WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '1646-litchfield-tpke-woodbridge', '', '', '2013-07-07 13:11:47', '2013-07-07 13:11:47', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=263', '0', 'woo_estate', '', '0'),
('264', '3', '2013-05-31 14:00:59', '2013-05-31 14:00:59', 'WELL LOCATED RETAIL/AUTOMOTIVE BUILDING ON AMITY ROAD (RT 63) NEXT TO A1 TOYOTA.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '80 AMITY ROAD, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '80-amity-road-new-haven', '', '', '2013-07-07 13:53:32', '2013-07-07 13:53:32', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=264', '0', 'woo_estate', '', '0'),
('265', '3', '2013-05-31 14:01:21', '2013-05-31 14:01:21', '', '80 Amity', '', 'inherit', 'open', 'open', '', '80-amity', '', '', '2013-05-31 14:01:21', '2013-05-31 14:01:21', '', '264', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/80-Amity.jpg', '0', 'attachment', 'image/jpeg', '0'),
('266', '3', '2013-07-08 14:00:00', '2013-07-08 14:00:00', 'WELL LOCATED RETAIL/AUTOMOTIVE BUILDING ON AMITY ROAD (RT 63) NEXT TO A1 TOYOTA.\n\nFOR MORE INFORMATION\nCONTACT STEVE MILLER\n203-389-5377 X22\nsteve@lmmre.com', '80 AMITY ROAD, NEW HAVEN', '', 'inherit', 'open', 'open', '', '264-autosave', '', '', '2013-07-08 14:00:00', '2013-07-08 14:00:00', '', '264', 'http://www.lmmre.com/property/2013/05/31/264-autosave/', '0', 'revision', '', '0'),
('267', '3', '2013-05-31 14:32:18', '2013-05-31 14:32:18', 'SPACES AVAILABLE FROM A SINGLE ROOM OF 360 SF UP TO 4,000 SF. THERE ARE SEVERAL COMBINATIONS OF SPACE AVAILABLE THAT WE CAN MEET WHATEVER YOUR REQUIREMENTS MIGHT BE. THIS A A FLEX SPACE OFFICE BUILDING.\r\n\r\nRATES START AT $5.00 SF PLUS UTILITIES. CALL AGENT FOR YOUR NEEDS AND SHOWINGS.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '30 HAZEL TERRACE, WOODBRIDGE', '', 'publish', 'closed', 'closed', '', '30-hazel-terrace-woodbridge', '', '', '2013-07-07 14:53:28', '2013-07-07 14:53:28', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=267', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('271', '3', '2013-05-31 14:42:12', '2013-05-31 14:42:12', 'FORMER AUTO DEALERSHIP - SHOWROOM - WAREHOUSE/GARAGE. AVAILABLE TOGETHER OR SEPARATE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '1635 DIXWELL AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '1635-dixwell-avenue-hamden', '', '', '2013-07-07 15:19:29', '2013-07-07 15:19:29', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=271', '0', 'woo_estate', '', '0'),
('272', '3', '2013-07-03 01:09:58', '2013-07-03 01:09:58', 'FORMER AUTO DEALERSHIP - SHOWROOM - WAREHOUSE/GARAGE. AVAILABLE TOGETHER OR SEPARATE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '1635 DIXWELL AVENUE, HAMDEN', '', 'inherit', 'open', 'open', '', '271-autosave', '', '', '2013-07-03 01:09:58', '2013-07-03 01:09:58', '', '271', 'http://www.lmmre.com/property/2013/05/31/271-autosave/', '0', 'revision', '', '0'),
('273', '3', '2013-05-31 14:51:48', '2013-05-31 14:51:48', '', '1635 Dixwell', '', 'inherit', 'open', 'open', '', '1635-dixwell', '', '', '2013-05-31 14:51:48', '2013-05-31 14:51:48', '', '271', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/1635-Dixwell.jpg', '0', 'attachment', 'image/jpeg', '0'),
('274', '3', '2013-05-31 14:59:49', '2013-05-31 14:59:49', '1,955 SF OFFICE\r\n\r\nFOR MORE INFORMATION\r\nCONTACT NOAH MEYER\r\n203-389-5377 X14\r\nnoah@lmmre.com', '383 ORANGE STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '383-orange-street-new-haven', '', '', '2013-07-07 13:28:19', '2013-07-07 13:28:19', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=274', '0', 'woo_estate', '', '0'),
('275', '3', '2013-05-31 15:07:03', '2013-05-31 15:07:03', 'THIS PROPERTY HAS BEEN A GARDEN CENTER AND GIFT SHOP FOR 50 YEARS. BUILDINGS CAN BE REUSED FOR COMMERCIAL/OFFICE - ADAPTIVE REUSE. BUSINESS AVAILABLE SEPARATELY. 1.50 ACRES THAT HAS BEEN A COMMERCIAL IN A RESIDENTIAL ZONE. TOWN HAS SAID ADAPTIVE REUSE OF THE EXISTING BUILDINGS FOR COMMERCIAL USE WOULD BE ALLOWED.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '4244 MADISON AVENUE, TRUMBULL', '', 'publish', 'closed', 'closed', '', '4244-madison-avenue-trumbull', '', '', '2013-07-07 15:18:36', '2013-07-07 15:18:36', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=275', '0', 'woo_estate', '', '0'),
('276', '3', '2013-05-31 15:05:32', '2013-05-31 15:05:32', '', '4244 Madison Ave', '', 'inherit', 'open', 'open', '', '4244-madison-ave', '', '', '2013-05-31 15:05:32', '2013-05-31 15:05:32', '', '275', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/4244-Madison-Ave.jpg', '0', 'attachment', 'image/jpeg', '0'),
('277', '3', '2013-05-31 15:16:22', '2013-05-31 15:16:22', 'WELL KEPT 4,435 SF LT INDUSTRIAL BUILDING WITH 2 DRIVE-IN DOORS, AIR COMPRESSOR, ELECTRIC AUTO LIFT. APPROXIMATELY 430 SF OF OFFICE SPACE WITH AIR CONDITIONING. CEILING VARIES FROM 11\' TO 15\' IN HEIGHT. ALARMED. EXCELLENT FOR ANY AUTO LT MANUFACTURING OR ASSEMBLY USES. PARTIAL ROOF REPLACEMENT IN 2005.\r\n\r\n&nbsp;\r\n\r\nALSO AVAILABLE FOR LEASE AT $4.95 sf.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '39 OREGON AVENUE, HAMDEN', '', 'publish', 'closed', 'closed', '', '39-oregon-avenue-hamden', '', '', '2013-07-07 15:17:48', '2013-07-07 15:17:48', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=277', '0', 'woo_estate', '', '0'),
('278', '3', '2013-05-31 15:14:34', '2013-05-31 15:14:34', '', '39 Oregon Ave.1', '', 'inherit', 'open', 'open', '', '39-oregon-ave-1', '', '', '2013-05-31 15:14:34', '2013-05-31 15:14:34', '', '277', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/39-Oregon-Ave.1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('281', '3', '2013-05-31 16:15:10', '2013-05-31 16:15:10', 'APPROXIMATELY 35,000 SF OF 1.49 ACRES. OFFICE, SHOP, WAREHOUSE. VERY SUCCESSFUL WASTEPAPER DISTRIBUTION BUSINESS. RR SIDING NOT CURRENTLY IN USE. SPRINKLERED, FRONTAGE ON TWO STREETS. RIGHT OF WAY AT REAR OF PARCEL. BUSINESS AND PROPERTY $3,500,000.\r\n\r\nBUSINESS ONLY $2,500,000.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT HAROLD KENT\r\n203-389-5377 X 24\r\nharold@lmmre.com', '610 FIRST AVENUE, WEST HAVEN', '', 'publish', 'closed', 'closed', '', '610-first-avenue-west-haven', '', '', '2013-07-07 15:16:49', '2013-07-07 15:16:49', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=281', '0', 'woo_estate', '', '0'),
('607', '3', '2013-06-26 16:15:19', '2013-06-26 16:15:19', '', 'space for rent', '', 'inherit', 'open', 'open', '', 'space-for-rent', '', '', '2013-06-26 16:15:19', '2013-06-26 16:15:19', '', '567', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/space-for-rent.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('367', '3', '2013-07-07 13:28:35', '2013-07-07 13:28:35', '1,955 SF OFFICE\r\n\r\nFOR MORE INFORMATION\r\nCONTACT NOAH MEYER\r\n203-389-5377 X14\r\nnoah@lmmre.com', '383 ORANGE STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '274-autosave', '', '', '2013-07-07 13:28:35', '2013-07-07 13:28:35', '', '274', 'http://www.lmmre.com/property/2013/06/18/274-autosave/', '0', 'revision', '', '0'),
('284', '3', '2013-05-31 17:31:33', '2013-05-31 17:31:33', 'THIS 29,000 SF RESTAURANT/BANQUET FACILITY ESTABLISHED FOR OVER 30 YEARS GROSSING $2,800,000 IS INDEED A RARE OPPORTUNITY. THE SEATING IN THE MAIN BANQUET ROOM IS 350 WITH 2 ADDITIONAL ROOMS SEATING 100 EACH, THE ALA CARTE SECTION SEATS 200 WITH TWO BARS AND THIS SECTION COULD E USED FOR ONLY BANQUETS AND DISCONTINUE ALA CARTE. THE KITCHEN IS IN EXCELLENT CONDITION WITH ALL STATE OF THE ART EQUIPMENT AND TOP OF THE LINE SMALL WARES. THE OUTSIDE GARDEN AREA IS A MUST SEE AND IS METICULOUSLY MAINTAINED. THE AMBIANCE CREATED THROUGH OUT THE RESTAURANT IS HARD TO MATCH IT HAS A WARMTH AND CHARM THAT WOULD COST MORE THAN THE ASKING PRICE TO CREATE TODAY. THIS IS ONLY FOR A QUALIFIED OPERATOR.DON\'T MISS THIS OPPORTUNITY.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'BANQUET/RESTAURANT ', '', 'publish', 'closed', 'closed', '', 'banquetrestaurant', '', '', '2013-07-08 02:43:24', '2013-07-08 02:43:24', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=284', '0', 'woo_estate', '', '0'),
('285', '3', '2013-07-08 02:41:30', '2013-07-08 02:41:30', 'THIS 29,000 SF RESTAURANT/BANQUET FACILITY ESTABLISHED FOR OVER 30 YEARS GROSSING $2,800,000 IS INDEED A RARE OPPORTUNITY. THE SEATING IN THE MAIN BANQUET ROOM IS 350 WITH 2 ADDITIONAL ROOMS SEATING 100 EACH, THE ALA CARTE SECTION SEATS 200 WITH TWO BARS AND THIS SECTION COULD E USED FOR ONLY BANQUETS AND DISCONTINUE ALA CARTE. THE KITCHEN IS IN EXCELLENT CONDITION WITH ALL STATE OF THE ART EQUIPMENT AND TOP OF THE LINE SMALL WARES. THE OUTSIDE GARDEN AREA IS A MUST SEE AND IS METICULOUSLY MAINTAINED. THE AMBIANCE CREATED THROUGH OUT THE RESTAURANT IS HARD TO MATCH IT HAS A WARMTH AND CHARM THAT WOULD COST MORE THAN THE ASKING PRICE TO CREATE TODAY. THIS IS ONLY FOR A QUALIFIED OPERATOR.DON\'T MISS THIS OPPORTUNITY.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'BANQUET/RESTAURANT ', '', 'inherit', 'open', 'open', '', '284-autosave', '', '', '2013-07-08 02:41:30', '2013-07-08 02:41:30', '', '284', 'http://www.lmmre.com/property/2013/05/31/284-autosave/', '0', 'revision', '', '0'),
('287', '3', '2013-05-31 18:19:37', '2013-05-31 18:19:37', 'IN GOOD TIMES OR BAD THE NEED FOR CHILD CARE DOES NOT GO AWAY, ESPECIALLY IN THIS UPSCALE SHORELINE TOWN. DAY CARE OPERATIONS DO NOT COME ON THE MARKET OFTEN AND VERY FEW LIKE THIS OPPORTUNITY. THIS IS A MUST SEE! JUST INVESTED $5,000 INTO NEW COMPUTER, PRINTER, REFRIGERATOR, RUBBER MULCH FOR THE PLAYGROUND, AND MORE. THIS IS A TURN KEY OPPORTUNITY FOR A GREAT PRICE.....ACT NOW!!!!!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'DAY CARE W/UPSIDE POTENTIAL', '', 'publish', 'closed', 'closed', '', 'day-care-wupside-potential', '', '', '2013-07-07 15:14:27', '2013-07-07 15:14:27', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=287', '0', 'woo_estate', '', '0'),
('288', '3', '2013-05-31 18:33:07', '2013-05-31 18:33:07', 'A VERY SIZABLE, LOFTY AMERICAN BISTRO, WITH TABLES RUNNING THE PERIPHERY AROUND AN ENORMOUS BAR SPACE IN THE MIDDLE OF THE ROOM. WALKING THROUGH THE FRONT DOOR, DINERS ARE GREETED BY SELECTIONS FROM THE REMARKABLE AND REASONABLE WINE LIST, KEPT UNDER GLASS. THERE IS A PREPONDERANCE OF POLISHED BLACK AND RED, THE RESULT IS WARM AND INVITING WHILE STILL EDGY AND ELEGANT. THE 70 SEAT PATIO IS A MAJOR ATTRACTION IN SUMMER MONTHS. YOU CAN\'T GO WRONG IN THIS LOCATION AND IS A MUST SEE.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'SUPERB RESTAURANT/BAR', '', 'publish', 'closed', 'closed', '', 'superb-restaurantbar-2', '', '', '2013-07-07 15:11:50', '2013-07-07 15:11:50', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=288', '0', 'woo_estate', '', '0'),
('289', '3', '2013-07-07 15:09:14', '2013-07-07 15:09:14', 'A VERY SIZABLE, LOFTY AMERICAN BISTRO, WITH TABLES RUNNING THE PERIPHERY AROUND AN ENORMOUS BAR SPACE IN THE MIDDLE OF THE ROOM. WALKING THROUGH THE FRONT DOOR, DINERS ARE GREETED BY SELECTIONS FROM THE REMARKABLE AND REASONABLE WINE LIST, KEPT UNDER GLASS. THERE IS A PREPONDERANCE OF POLISHED BLACK AND RED, THE RESULT IS WARM AND INVITING WHILE STILL EDGY AND ELEGANT. THE 70 SEAT PATIO IS A MAJOR ATTRACTION IN SUMMER MONTHS. YOU CAN\'T GO WRONG IN THIS LOCATION AND IS A MUST SEE.\n\nFOR FURTHER INFORMATION\nCONTACT SHAWN REILLY\n203-389-5377 X 29\nshawn@lmmre.com', 'SUPERB RESTAURANT/BAR', '', 'inherit', 'open', 'open', '', '288-autosave', '', '', '2013-07-07 15:09:14', '2013-07-07 15:09:14', '', '288', 'http://www.lmmre.com/property/2013/05/31/288-autosave/', '0', 'revision', '', '0'),
('290', '3', '2013-05-31 18:44:57', '2013-05-31 18:44:57', 'PRIME LOCATION! THIS IS A SUPERB OPPORTUNITY TO PURCHASE A RESTAURANT, TAVERN, WITH UNBELIEVABLE PATIO IN THE HEART OF DOWNTOWN THAT HAS GONE THROUGH A MILLION DOLLAR RENOVATION. THE PATIO IS A TRUE BONANZA FOR THE LAZY HAZY DAYS OF SUMMER, WHEN THINGS SLOW DOWN THE PATIO PICKS UP. ALL OF THE FURNITURE, EQUIPMENT AND FIXTURES ARE LIKE BRAND NEW. THE WOOD FLOORS AND BAR CREATE A WARM INVITING AMBIANCE THAT CAN\'T BE MATCHED. WITH ALL OF THE ABOVE, BELOW MARKET RENT, $1,000,000 RENOVATION, ROOM FOR PRIVATE PARTIES, PATIO, 120 SEAT DINING ROOM, GREAT BAR, FIREPLACE, ALL, LIKE NEW EQUIPMENT, AAA LOCATION AT A SELLING PRICE AT A FRACTION OF THE RENOVATION COST MAKES THIS A REAL DEAL AND A MUST SEE!!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'AWESOME RESTAURANT/TAVERN', '', 'publish', 'closed', 'closed', '', 'awesome-restauranttavern', '', '', '2013-07-08 02:42:53', '2013-07-08 02:42:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=290', '0', 'woo_estate', '', '0'),
('296', '3', '2013-06-10 15:20:28', '2013-06-10 15:20:28', 'QUALITY LAND ZONED IH. .36 ACRE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT MARTY RUFF\r\n203-389-5377 X 18\r\nmarty@lmmre.com', '399 QUINNIPIAC AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '399-quinnipiac-avenue-new-haven', '', '', '2013-07-07 14:31:13', '2013-07-07 14:31:13', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=296', '0', 'woo_estate', '', '0'),
('297', '3', '2013-06-25 16:26:13', '2013-06-25 16:26:13', 'QUALITY LAND ZONED IH. .36 ACRE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT MARTY RUFF\r\n203-389-5377 X 18\r\nmarty@lmmre.com', '399 QUINNIPIAC AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '296-autosave', '', '', '2013-06-25 16:26:13', '2013-06-25 16:26:13', '', '296', 'http://www.lmmre.com/property/2013/06/10/296-autosave/', '0', 'revision', '', '0'),
('299', '3', '2013-06-12 19:36:59', '2013-06-12 19:36:59', '', '490 Orchard Street 001-001', '', 'inherit', 'open', 'open', '', '490-orchard-street-001-001', '', '', '2013-06-12 19:36:59', '2013-06-12 19:36:59', '', '293', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/490-Orchard-Street-001-001.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('300', '3', '2013-06-12 19:50:57', '2013-06-12 19:50:57', 'ABSOLUTELY NO RISK HERE! YOU CAN SEARCH HIGH AND LOW FOR AN OPPORTUNITY LIKE THIS, WHERE YOU GET YOUR INVESTMENT BACK IN A FEW YEARS. WHEN WAS THE LAST TIME YOU SAW A RESTAURANT EARN $500 PER SQUARE FOOT WITH HIGH PROFIT FOOD ITEMS. THE NEWLY ADDED PRIVATE ROOM FOR 50 IS RAPIDLY BECOMING VERY POPULAR FOR BIRTHDAYS AND MORE. THE RESTAURANT HAS ONLY BEEN OPEN FOR THREE YEARS AND CONTINUES TO GROW, SERVING PASTA, PIZZA, DINNERS TO GO SHOW CASE, POPULAR SALAD BAR, DESERTS FROM ITALY AND MORE. IF YOU ARE LOOKING FOR A HIGH INCOME WITH NO RISK....LOOK NO FURTHER.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'FANTASTIC UNIQUE PASTA/PIZZA', '', 'publish', 'closed', 'closed', '', 'fantastic-unique-pastapizza', '', '', '2013-07-07 15:04:49', '2013-07-07 15:04:49', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=300', '0', 'woo_estate', '', '0'),
('301', '3', '2013-06-12 20:04:21', '2013-06-12 20:04:21', 'IT IS INDEED MY PLEASURE TO OFFER THIS 325 SEAT CHARMING BANQUET FACILITY LOCATED ON 4.5 EXQUISITE COUNTRY ACRES WITH WATERFALL, FOUNTAIN, GAZEBO FOR THOSE VERY SPECIAL COUNTRYSIDE WEDDINGS AND A TREE LINED PICNIC AREA FOR THOSE OUTDOOR BARBECUES. IT IS RARE TO FIND A GEM LIKE THIS WITH THE PROPERTY THAT HAS BEEN OPERATING FOR DECADES SUCCESSFULLY WITH UPSIDE POTENTIAL, AND ENOUGH PROPERTY FOR RESIDENTIAL DEVELOPMENT. CALL NOW FOR APPOINTMENT FOR COUNTRY STROLL.\r\n\r\n&nbsp;\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377\r\nshawn@lmmre.com', 'COUNTRYSIDE PREMIER BANQUET FACILITY', '', 'publish', 'closed', 'closed', '', 'countryside-premier-banquet-facility', '', '', '2013-06-28 14:09:39', '2013-06-28 14:09:39', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=301', '0', 'woo_estate', '', '0'),
('302', '3', '2013-06-27 12:30:01', '2013-06-27 12:30:01', 'IT IS INDEED MY PLEASURE TO OFFER THIS 325 SEAT CHARMING BANQUET FACILITY LOCATED ON 4.5 EXQUISITE COUNTRY ACRES WITH WATERFALL, FOUNTAIN, GAZEBO FOR THOSE VERY SPECIAL COUNTRYSIDE WEDDINGS AND A TREE LINED PICNIC AREA FOR THOSE OUTDOOR BARBECUES. IT IS RARE TO FIND A GEM LIKE THIS WITH THE PROPERTY THAT HAS BEEN OPERATING FOR DECADES SUCCESSFULLY WITH UPSIDE POTENTIAL, AND ENOUGH PROPERTY FOR RESIDENTIAL DEVELOPMENT. CALL NOW FOR APPOINTMENT FOR COUNTRY STROLL.\r\n\r\n&nbsp;\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377\r\nshawn@lmmre.com', 'COUNTRYSIDE PREMIER BANQUET FACILITY', '', 'inherit', 'open', 'open', '', '301-autosave', '', '', '2013-06-27 12:30:01', '2013-06-27 12:30:01', '', '301', 'http://www.lmmre.com/property/2013/06/12/301-autosave/', '0', 'revision', '', '0'),
('303', '3', '2013-06-12 20:24:00', '2013-06-12 20:24:00', 'THIS POPULAR PLEASANT ITALIAN FAMILY OWNED RESTAURANT WITH A CHARMING LOUNGE AND STUNNING WOOD BAR CAN BE YOURS FOR A VERY REASONABLE PRICE FOR WHAT YOU GET. YOU WILL HAVE TO SEARCH FAR AND WIDE TO FIND AN ALLURING 125 SEAT FULLY EQUIPPED RESTAURANT/BAR ON A MAJOR ROAD WITH UNBELIEVABLE LOW RENT THAT INCLUDES WATER, TRASH, CAM, PROPERTY TAXES AND WOULD DEFINITELY WORK FOR ANY CONCEPT. CALL NOW TO TAKE ADVANTAGE OF THIS OPPORTUNITY.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com\r\n\r\n&nbsp;', 'SUPERB ITALIAN RESTAURANT ', '', 'publish', 'closed', 'closed', '', 'superb-italian-restaurant', '', '', '2013-07-08 02:44:37', '2013-07-08 02:44:37', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=303', '0', 'woo_estate', '', '0'),
('779', '3', '2013-07-09 00:02:19', '2013-07-09 00:02:19', 'Located next to Quinnipiac Universities Irish Museum------2 Floors 3,000 +/- sf each floor\r\n\r\nFinished Loft 400 +/- sf--------Finished Basement 2,000 +/- sf w/2 Bathrooms and Kitchen\r\n\r\nPossible Student Housing----------Present Use Office Building\r\n\r\nPrime Hamden Location, Close to the Connector and I-91.  Parking, High Traffic.\r\n\r\n&nbsp;\r\n\r\n<em><strong>For Sale or For Lease</strong></em>\r\n\r\n&nbsp;\r\n\r\nCall Bernie Diana\r\n\r\n203-389-5377  OR\r\n\r\nemail Bernie@lmmre.com', '3035 Whitney Ave, Hamden', '', 'publish', 'closed', 'closed', '', '3035-whitney-ave-hamden', '', '', '2013-07-10 16:52:49', '2013-07-10 16:52:49', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=779', '0', 'woo_estate', '', '0'),
('305', '3', '2013-06-12 20:42:36', '2013-06-12 20:42:36', 'AFTER 25 LUCRATIVE YEARS THE OWNER SAYS ITS TIME TO SELL AND RETIRE. THIS IS AN INCREDIBLE RARE OPPORTUNITY TO STEP INTO A LUCRATIVE BUSINESS ESTABLISHED FOR A QUARTER OF A CENTURY WITH AN ENORMOUS LOYAL CUSTOMER BASE. THE PRIME LOCATION WITH AMPLE FREE PARKING, UNHEARD OF LOW MONTHLY RENT, HIGH GROSSING, ESTABLISHED THIS LONG IS A FORMULA FOR SUCCESS FOR A VERY LOW ASKING PRICE. BEST BUY THIS SPRING ...CALL NOW TO SEE THIS ONE.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'NOTABLE BREAKFAST/LUNCH', '', 'publish', 'closed', 'closed', '', 'notable-breakfastlunch', '', '', '2013-07-07 15:01:10', '2013-07-07 15:01:10', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=305', '0', 'woo_estate', '', '0'),
('368', '3', '2013-06-18 14:03:25', '2013-06-18 14:03:25', '', '383 Orange St', '', 'inherit', 'open', 'open', '', '383-orange-st', '', '', '2013-06-18 14:03:25', '2013-06-18 14:03:25', '', '274', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/383-Orange-St.jpg', '0', 'attachment', 'image/jpeg', '0'),
('606', '3', '2013-06-26 16:10:57', '2013-06-26 16:10:57', '', 'office for lease', '', 'inherit', 'open', 'open', '', 'office-for-lease-2', '', '', '2013-06-26 16:10:57', '2013-06-26 16:10:57', '', '569', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/office-for-lease1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('308', '3', '2013-06-13 13:39:38', '2013-06-13 13:39:38', 'IT IS INDEED MY PLEASURE TO OFFER THIS BONA FIDE VERY PROFITABLE MEXICAN RESTAURANT CHAIN LIKE LOCATION IN THE MIDDLE OF A SHOPPING MECCA FOR A PRICE HALF OF WHAT ITS WORTH. THIS ALREADY HIGH GROSSING RESTAURANT ONLY HAS A SERVICE BAR WHICH IS BIG ENOUGH TO ADD STOOLS AND ADD A HUGE PROFIT CENTER ALONG WITH PROPER MARKETING AND OWNER OPERATOR WOULD PUT THIS OFF THE CHARTS. THIS IS DEFINITELY ONE OF THE BEST BUYS YOU WILL OR HAVE SEEN IN A LONG TIME, AN OPPORTUNITY WHERE YOU CAN GET MOST OF YOUR INVESTMENT BACK IN THE FIRST YEAR, 20 PROFITABLE YEARS, AAA LOCATION, ABSENTEE OWNER, IMMENSE LOYAL CUSTOMER BASE AND MORE.  DON\'T MISS THIS ONE ...CALL NOW!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'LEGENDARY MEXICAN RESTAURANT', '', 'publish', 'closed', 'closed', '', 'legendary-mexican-restaurant', '', '', '2013-07-07 14:59:57', '2013-07-07 14:59:57', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=308', '0', 'woo_estate', '', '0'),
('309', '3', '2013-06-13 13:57:22', '2013-06-13 13:57:22', 'IT I S INDEED MY PLEASURE TO OFFER THIS ONE OF A KIND FABULOUS  HI GROSSING 5000 SF WATERFRONT RESTAURANT/BAR WITH AMAZING WATER AND BOAT VIEWS FROM EVERY ROOM. THIS HAS BEEN A POPULAR RESTAURANT DESTINATION SINCE THE LATE 1920\'S AND THE PRESENT OWNER\'S HAVE BEEN THERE FOR OVER A DECADE. THE INCREDIBLE LOCATION WHICH DOES BUSINESS ALL YEAR IS NOT ITS ONLY ATTRIBUTE, THE FACILITY ITSELF HAS UNDERGONE A COMPLETE MAJOR RENOVATION NOT EVEN 1 YEAR OLD FROM TOP TO BOTTOM WITH NEW EQUIPMENT AS WELL. YOU JUST CAN\'T GO WRONG WITH THIS VERY SPECIAL RESTAURANT IN A VERY SPECIAL LOCATION, THE OLD EXPRESSION THEY AREN\'T MAKING ANY MORE WATERFRONT REALLY APPLIES HERE. CALL NOW TO MAKE AN APPOINTMENT TO VIEW THIS MUST SEE RESTAURANT.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'WATERFRONT LOCATION LOCATION LOCATION', '', 'publish', 'closed', 'closed', '', 'waterfront-location-location-location', '', '', '2013-07-07 14:37:56', '2013-07-07 14:37:56', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=309', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('310', '3', '2013-06-13 14:05:32', '2013-06-13 14:05:32', 'THIS APPEALING BUSY BREAKFAST/LUNCH IN A GREAT LOCATION ON VERY BUSY ROAD HAS DEVELOPED A LARGE LOYAL CUSTOMER BASE. THE RESTAURANT IS BEGGING TO BE OPEN FOR DINNER TO FULLY CAPITALIZE ON THE DINER\'S FULL POTENTIAL. THIS IS A TRUE TURN KEY OPPORTUNITY, ESTABLISHED CLIENTELE AND SHOULDN\'T BE MISSED. CALL NOW!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'POPULAR BREAKFAST/LUNCH', '', 'publish', 'closed', 'closed', '', 'popular-breakfastlunch', '', '', '2013-07-07 14:57:31', '2013-07-07 14:57:31', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=310', '0', 'woo_estate', '', '0'),
('313', '3', '2013-06-13 14:20:13', '2013-06-13 14:20:13', 'THIS HIGH GROSSING RESTAURANT HAS THE BEST OF ALL WORLDS WITH THE IDEAL COMBINATION OF FOOD, DRINK, PRIVATE PARTY AND FUN IN A UNIQUE FAMILY ATMOSPHERE AND HAS BEEN THE GO TO PLACE FOR OVER TWENTY YEARS WITH A PROVEN PROSPEROUS TRACK RECORD. A WARM AND INVITING AMERICAN GRILLE IN THIS AFFLUENT QUAINT EAST SHORELINE TOWN LOCATION IN A HISTORIC BUILDING WITH AMPLE FREE PARKING IS AN OUTSTANDING OPPORTUNITY WITH TREMENDOUS CASH FLOW. CALL NOW TO SEE THIS ONE YOU WON\'T BE SORRY!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'SPECTACULAR FAMILY RESTAURANT/BAR', '', 'publish', 'closed', 'closed', '', 'spectacular-family-restaurantbar', '', '', '2013-07-07 14:58:53', '2013-07-07 14:58:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=313', '0', 'woo_estate', '', '0'),
('314', '3', '2013-06-13 14:49:05', '2013-06-13 14:49:05', 'IT IS INDEED MY PLEASURE TO OFFER THIS PLEASURABLE, REWARDING LONG ESTABLISHED HIGH PROFIT ICE CREAM SHOP IN A AAA LOCATION ON A MAJOR ROADWAY. IT IS EXTREMELY RARE TO FIND A VASTLY LUCRATIVE BUSINESS ESTABLISHED 60 YEARS ONLY OPEN 9 MONTHS A YEAR FOR AN ASKING PRICE AND RENT THIS LOW. THE RESTAURANT IS ALSO EQUIPPED WITH A FULL HOOD SYSTEM, FRYER AND GRILL WHICH ARE NOT USED AND COULD BE GREAT SOURCE OF ADDITIONAL INCOME. THIS OPPORTUNITY GIVEN ITS LOCATION, TRAFFIC COUNT, DRIVE THROUGH, PARKING, ASKING PRICE AND RENT WOULD WORK FOR ANY TAKEOUT FOOD WITH A DRIVE THROUGH. ACT NOW TO TAKE ADVANTAGE OF THIS INCREDIBLE OPPORTUNITY. ADDITION OF HOT DOGS OR FOOD WOULD ENHANCE THIS BUSINESS TWO FOLD.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'TAKE OUT ICE CREAM', '', 'publish', 'closed', 'closed', '', 'take-out-ice-cream', '', '', '2013-07-07 14:56:23', '2013-07-07 14:56:23', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=314', '0', 'woo_estate', '', '0'),
('315', '3', '2013-07-07 14:56:29', '2013-07-07 14:56:29', 'IT IS INDEED MY PLEASURE TO OFFER THIS PLEASURABLE, REWARDING LONG ESTABLISHED HIGH PROFIT ICE CREAM SHOP IN A AAA LOCATION ON A MAJOR ROADWAY. IT IS EXTREMELY RARE TO FIND A VASTLY LUCRATIVE BUSINESS ESTABLISHED 60 YEARS ONLY OPEN 9 MONTHS A YEAR FOR AN ASKING PRICE AND RENT THIS LOW. THE RESTAURANT IS ALSO EQUIPPED WITH A FULL HOOD SYSTEM, FRYER AND GRILL WHICH ARE NOT USED AND COULD BE GREAT SOURCE OF ADDITIONAL INCOME. THIS OPPORTUNITY GIVEN ITS LOCATION, TRAFFIC COUNT, DRIVE THROUGH, PARKING, ASKING PRICE AND RENT WOULD WORK FOR ANY TAKEOUT FOOD WITH A DRIVE THROUGH. ACT NOW TO TAKE ADVANTAGE OF THIS INCREDIBLE OPPORTUNITY. ADDITION OF HOT DOGS OR FOOD WOULD ENHANCE THIS BUSINESS TWO FOLD.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'TAKE OUT ICE CREAM', '', 'inherit', 'open', 'open', '', '314-autosave', '', '', '2013-07-07 14:56:29', '2013-07-07 14:56:29', '', '314', 'http://www.lmmre.com/property/2013/06/13/314-autosave/', '0', 'revision', '', '0'),
('316', '3', '2013-06-13 15:05:45', '2013-06-13 15:05:45', 'IT IS MY PLEASURE TO OFFER A VERY UNIQUE TAKE OUT RESTAURANT OPPORTUNITY IN AN EXCEPTIONAL LOCATION IN A POPULAR COMMUNITY SHOPPING CENTER , IN AN UPSCALE QUAINT POPULATED EAST SHORE TOWN WITH UPSIDE POTENTIAL. THE AWARD WINNING RESTAURANT SERVING LUNCH, DINNER, BAKED GOODS AND PICK UP CATERING FOR LARGE AND SMALL EVENTS HAVE OVER THE YEARS DEVELOPED A LARGE LOYAL CUSTOMER BASE THAT JUST KEEPS GROWING. THERE AREN\'T MANY IF SUCCESSFUL RESTAURANTS THAT OFFER A QUALITY OF LIFE THIS ONE, WHERE YOU GO IN LATE, GO HOME EARLY AND HAVE TWO DAYS OFF. YOU OWE IT TO YOURSELF TO CALL NOW AND SEE THIS ONE!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'UNIQUE TAKE OUT RESTAURANT', '', 'publish', 'closed', 'closed', '', 'unique-take-out-restaurant', '', '', '2013-07-07 13:26:49', '2013-07-07 13:26:49', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=316', '0', 'woo_estate', '', '0'),
('317', '3', '2013-06-13 15:26:23', '2013-06-13 15:26:23', 'IT IS MY PLEASURE TO OFFER THE OLDEST HIGH GROSSING PIZZA OPERATION IN TOWN. IT WILL BE A LONG TIME BEFORE YOU WILL SEE A HIGH GROSSING PIZZA RESTAURANT OF THIS CALIBER BEING OFFERED FOR A PRICE THIS LOW. WITH ALL THE ELEMENTS, TREMENDOUS CAR COUNT OF 17,000, OVER 5,OOO,OOO SF OF RETAIL SPACE WITHIN A 1.5 MILE RADIUS, A WHOPPING 1,000,000 SF OF APARTMENT SPACE WITHIN A 1.5 MILE RADIUS, FREE STANDING BUILDING, AMPLE FREE PARKING, HUGE DELIVERY, HIGH TRAFFIC COUNT, BUSTLING AREA, 3 BRICK OVENS, HIGH GROSSING, WITH UPSIDE POTENTIAL MAKES THIS ONE A TRUE WINNER. CALL NOW DON\'T MISS THIS ONE!!!! REAL ESTATE ALSO AVAILABLE FOR $900,000.\r\n\r\nFOR MORE INFORMATION\r\nCALL SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'FANTASTIC PIZZA RESTAURANT', '', 'publish', 'closed', 'closed', '', 'fantastic-pizza-restaurant', '', '', '2013-07-09 00:12:38', '2013-07-09 00:12:38', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=317', '0', 'woo_estate', '', '0'),
('318', '3', '2013-06-13 15:36:29', '2013-06-13 15:36:29', 'A VERY UNIQUE HI PROFIT PIZZA OPPORTUNITY WHICH IS PART OF A POPULAR PIZZA RESTAURANT GROUP CONSISTING OF 11, WHICH YOU CAN JOIN OR CHANGE THE NAME OF THIS HI GROSSING LOCATION AND SAVE $14,000 A YEAR. IT IS A RARE BIRD TO BE ABLE TO BUY A RESTAURANT OF ANY TYPE FOR A LITTLE MORE THAN THEY ARE NETTING IN THEIR POCKET. WITH THE ADDITION OF DELIVERY ITS POSSIBLE TO GET YOUR INVESTMENT BACK IN ONE YEAR WHICH IS UNHEARD OF. CALL NOW FOR THIS MUST SEE OPPORTUNITY.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'HI PROFIT PIZZA', '', 'publish', 'closed', 'closed', '', 'hi-profit-pizza', '', '', '2013-07-07 14:32:18', '2013-07-07 14:32:18', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=318', '0', 'woo_estate', '', '0'),
('319', '3', '2013-06-13 15:56:23', '2013-06-13 15:56:23', 'IT IS INDEED MY PLEASURE TO OFFER THIS INCREDIBLE, JUST COMPLETELY TASTEFULLY RENOVATED BURGER PLUS RESTAURANT WITH ALL NEW EQUIPMENT AT A COST OF WELL OVER $250,000. THIS GREAT LOCATION WITH ITS ENVIABLE PARKING LOT HAS A PROVEN PREVIOUS TRACK RECORD OF GROSSING OVER $2,000,000. THE PRESENT OPERATOR OWNS ANOTHER HIGHLY SUCCESSFUL RESTAURANT AND HAS FOUND RUNNING A SECOND LOCATION ABSENTEE DOESN\'T WORK FOR HIM. ALL THIS RESTAURANT NEEDS IS AN ON PREMISE OPERATOR WITH A FEW TWEAKS AND THE ADDITION OF SOME CRAFT BEERS WOULD PUT THIS PLACE RIGHT BACK ON TOP. THIS IS A MUST SEE .... PLEASE CALL TO SET UP APPOINTMENT NOW!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'NEW HOT BURGER CONCEPT', '', 'publish', 'closed', 'closed', '', 'new-hot-burger-concept', '', '', '2013-07-07 14:55:31', '2013-07-07 14:55:31', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=319', '0', 'woo_estate', '', '0'),
('506', '3', '2013-06-21 17:30:26', '2013-06-21 17:30:26', 'YOU CAN TRAVEL FAR AND WIDE FOR A LONG TIME AND NOT BE ABLE TO FIND A PHENOMENAL OPPORTUNITY LIKE THIS ONE. THIS IS IN THE BUSY RESTAURANT PART OF STATE STREET ALSO KNOWN AS THE YALE GROTTO. THE RESTAURANT DOES NEED SOME WORK BUT TO BE ABLE TO GET A RESTAURANT LIKE THIS IN AN INCREDIBLE LOCATION FOR THIS KIND OF MONEY IS UNHEARD OF.\r\n\r\nTHIS IS A MARVELOUS OPPORTUNITY THAT SHOULDN\'T BE MISSED!!!!\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com\r\n\r\n&nbsp;', '970 STATE STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '970-state-street-new-haven', '', '', '2013-07-08 15:15:50', '2013-07-08 15:15:50', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=506', '0', 'woo_estate', '', '0'),
('734', '3', '2013-07-07 15:03:56', '2013-07-07 15:03:56', 'ABSOLUTELY NO RISK HERE! YOU CAN SEARCH HIGH AND LOW FOR AN OPPORTUNITY LIKE THIS, WHERE YOU GET YOUR INVESTMENT BACK IN A FEW YEARS. WHEN WAS THE LAST TIME YOU SAW A RESTAURANT EARN $500 PER SQUARE FOOT WITH HIGH PROFIT FOOD ITEMS. THE NEWLY ADDED PRIVATE ROOM FOR 50 IS RAPIDLY BECOMING VERY POPULAR FOR BIRTHDAYS AND MORE. THE RESTAURANT HAS ONLY BEEN OPEN FOR THREE YEARS AND CONTINUES TO GROW, SERVING PASTA, PIZZA, DINNERS TO GO SHOW CASE, POPULAR SALAD BAR, DESERTS FROM ITALY AND MORE. IF YOU ARE LOOKING FOR A HIGH INCOME WITH NO RISK....LOOK NO FURTHER.\n\nFOR FURTHER INFORMATION\nCONTACT SHAWN REILLY\n203-389-5377 X29\nshawn@lmmre.com', 'FANTASTIC UNIQUE PASTA/PIZZA', '', 'inherit', 'open', 'open', '', '300-autosave', '', '', '2013-07-07 15:03:56', '2013-07-07 15:03:56', '', '300', 'http://www.lmmre.com/property/300-autosave/', '0', 'revision', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('324', '3', '2013-07-08 00:14:19', '2013-07-08 00:14:19', '3,300 SF BRICK BUILDING THAT SITS ON APPROXIMATELY 1.5 ACRES. - BUILDING IS USED TO REPAIR CONSTRUCTION EQUIPMENT BELONGING TO THE OWNER. PROPERTY IS ENCLOSED BY PAGE FENCE AND WIRE WITH A POWER GATE BACK SIDE OF BUILDING. INSIDE BUILDING HAS STORAGE AND A SMALL OFFICE. NEED 30-60 DAYS TO CLEAR OUT BUILDING AND YARD. OWNER WOULD CONSIDER FINANCING PURCHASE TO WELL QUALIFIED OWNER. AVAILABLE FOR SALE AT $395,000\r\n\r\n<em><strong>ALSO AVAILABLE FOR LEASE AT $3,000/MONTH.</strong></em>\r\n\r\nFOR FURTHER INFORMATION:\r\n\r\nCONTACT MARTY RUFF 203-389-5377 X18 OR\r\nmarty@lmmre.com.', '390 EASTERN STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '63-autosave', '', '', '2013-07-08 00:14:19', '2013-07-08 00:14:19', '', '63', 'http://www.lmmre.com/property/2013/06/13/63-autosave/', '0', 'revision', '', '0'),
('697', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=697', '0', 'wooframework', '', '0'),
('326', '3', '2013-06-13 20:44:45', '2013-06-13 20:44:45', 'FIRST FLOOR ONE ROOM OFFICE - APPROXIMATELY 120 SQUARE FEET. NEAR MERRITT PARKWAY. $400/MONTH GROSS.\r\n\r\nFOR MORE INFORMATION\r\nCALL MIKE GORDON\r\n203-389-5377 X 25\r\nmichael@lmmre.com', '59 AMITY ROAD, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '59-amity-road-new-haven', '', '', '2013-07-07 13:38:00', '2013-07-07 13:38:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=326', '0', 'woo_estate', '', '0'),
('328', '3', '2013-06-13 20:43:38', '2013-06-13 20:43:38', '', '59 Amity', '', 'inherit', 'open', 'open', '', '59-amity-2', '', '', '2013-06-13 20:43:38', '2013-06-13 20:43:38', '', '326', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/59-Amity.jpg', '0', 'attachment', 'image/jpeg', '0'),
('329', '3', '2013-07-07 13:38:05', '2013-07-07 13:38:05', 'FIRST FLOOR ONE ROOM OFFICE - APPROXIMATELY 120 SQUARE FEET. NEAR MERRITT PARKWAY. $400/MONTH GROSS.\r\n\r\nFOR MORE INFORMATION\r\nCALL MIKE GORDON\r\n203-389-5377 X 25\r\nmichael@lmmre.com', '59 AMITY ROAD, NEW HAVEN', '', 'inherit', 'open', 'open', '', '326-autosave', '', '', '2013-07-07 13:38:05', '2013-07-07 13:38:05', '', '326', 'http://www.lmmre.com/property/2013/06/13/326-autosave/', '0', 'revision', '', '0'),
('710', '3', '2013-07-09 00:12:01', '2013-07-09 00:12:01', 'IT IS MY PLEASURE TO OFFER THE OLDEST HIGH GROSSING PIZZA OPERATION IN TOWN. IT WILL BE A LONG TIME BEFORE YOU WILL SEE A HIGH GROSSING PIZZA RESTAURANT OF THIS CALIBER BEING OFFERED FOR A PRICE THIS LOW. WITH ALL THE ELEMENTS, TREMENDOUS CAR COUNT OF 17,000, OVER 5,OOO,OOO SF OF RETAIL SPACE WITHIN A 1.5 MILE RADIUS, A WHOPPING 1,000,000 SF OF APARTMENT SPACE WITHIN A 1.5 MILE RADIUS, FREE STANDING BUILDING, AMPLE FREE PARKING, HUGE DELIVERY, HIGH TRAFFIC COUNT, BUSTLING AREA, 3 BRICK OVENS, HIGH GROSSING, WITH UPSIDE POTENTIAL MAKES THIS ONE A TRUE WINNER. CALL NOW DON\'T MISS THIS ONE!!!! REAL ESTATE ALSO AVAILABLE FOR $900,000.\n\nFOR MORE INFORMATION\nCALL SHAWN REILLY\n203-389-5377 X 29\nshawn@lmmre.com', 'FANTASTIC PIZZA RESTAURANT', '', 'inherit', 'open', 'open', '', '317-autosave', '', '', '2013-07-09 00:12:01', '2013-07-09 00:12:01', '', '317', 'http://www.lmmre.com/property/317-autosave/', '0', 'revision', '', '0'),
('333', '3', '2013-06-16 15:05:27', '2013-06-16 15:05:27', 'HUGE OPPORTUNITY TO LOCATE INTO A GREAT FLEX BUILDING FOR OFFICES, AND SHOWROOM - WITH LARGE STORAGE AREA AND OVERHEAD DOOR. OFFICE HAS CENTRAL AIR CONDITIONING - ALL WALLS NON LOAD BEARING, SO FORMAT CAN BE EASILY CHANGED. VERY CONVENIENT TO I-95 RAMP # 55 IN BOTH DIRECTIONS.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT MARTY RUFF\r\n203-389-5377 X18\r\nmarty@lmmre.com', '29 FLAX MILL ROAD, BRANFORD', '', 'publish', 'closed', 'closed', '', '29-flax-mill-road', '', '', '2013-07-07 14:25:41', '2013-07-07 14:25:41', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=333', '0', 'woo_estate', '', '0'),
('726', '3', '2013-07-07 13:11:56', '2013-07-07 13:11:56', 'GREAT RETAIL OR RESTAURANT SITE. GROUND LEASE FOR $50,000/YEAR.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '1646 LITCHFIELD TPKE., WOODBRIDGE', '', 'inherit', 'open', 'open', '', '263-autosave', '', '', '2013-07-07 13:11:56', '2013-07-07 13:11:56', '', '263', 'http://www.lmmre.com/property/263-autosave/', '0', 'revision', '', '0'),
('509', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=509', '0', 'wooframework', '', '0'),
('337', '3', '2013-07-07 14:25:31', '2013-07-07 14:25:31', 'HUGE OPPORTUNITY TO LOCATE INTO A GREAT FLEX BUILDING FOR OFFICES, AND SHOWROOM - WITH LARGE STORAGE AREA AND OVERHEAD DOOR. OFFICE HAS CENTRAL AIR CONDITIONING - ALL WALLS NON LOAD BEARING, SO FORMAT CAN BE EASILY CHANGED. VERY CONVENIENT TO I-95 RAMP # 55 IN BOTH DIRECTIONS.\n\nFOR MORE INFORMATION\nCONTACT MARTY RUFF\n203-389-5377 X18\nmarty@lmmre.com', '29 FLAX MILL ROAD, BRANFORD', '', 'inherit', 'open', 'open', '', '333-autosave', '', '', '2013-07-07 14:25:31', '2013-07-07 14:25:31', '', '333', 'http://www.lmmre.com/property/2013/06/15/333-autosave/', '0', 'revision', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('567', '3', '2013-06-25 14:36:25', '2013-06-25 14:36:25', '1000 SF OF 2ND FLOOR OFFICE  - CAN BE DIVIDED - 450 SF &amp; 550 SF. GREAT VISIBILITY AND ACCESS TO PARKWAY. $1200/MONTH.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377\r\nsteve@lmmre.com\r\n\r\n&nbsp;', '1400 WHALLEY AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '1400-whalley-avenue-new-haven-2', '', '', '2013-07-18 17:22:59', '2013-07-18 17:22:59', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=567', '0', 'woo_estate', '', '0'),
('566', '3', '2013-06-25 14:30:57', '2013-06-25 14:30:57', 'STOREFRONT - 1200 SF ON WHALLEY AVENUE NEAR INTERSECTION OF RT 63 &amp; 69. RETAIL, OFFICE,  HAIR SALON.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377\r\nsteve@lmmre.com', '1400 WHALLEY AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '1400-whalley-avenue-new-haven', '', '', '2013-07-18 17:25:00', '2013-07-18 17:25:00', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=566', '0', 'woo_estate', '', '0'),
('612', '3', '2013-07-07 14:57:43', '2013-07-07 14:57:43', 'THIS APPEALING BUSY BREAKFAST/LUNCH IN A GREAT LOCATION ON VERY BUSY ROAD HAS DEVELOPED A LARGE LOYAL CUSTOMER BASE. THE RESTAURANT IS BEGGING TO BE OPEN FOR DINNER TO FULLY CAPITALIZE ON THE DINER\'S FULL POTENTIAL. THIS IS A TRUE TURN KEY OPPORTUNITY, ESTABLISHED CLIENTELE AND SHOULDN\'T BE MISSED. CALL NOW!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'POPULAR BREAKFAST/LUNCH', '', 'inherit', 'open', 'open', '', '310-autosave', '', '', '2013-07-07 14:57:43', '2013-07-07 14:57:43', '', '310', 'http://www.lmmre.com/property/310-autosave/', '0', 'revision', '', '0'),
('351', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=351', '0', 'wooframework', '', '0'),
('352', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=352', '0', 'wooframework', '', '0'),
('353', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=353', '0', 'wooframework', '', '0'),
('354', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=354', '0', 'wooframework', '', '0'),
('355', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=355', '0', 'wooframework', '', '0'),
('356', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=356', '0', 'wooframework', '', '0'),
('357', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=357', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('358', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=358', '0', 'wooframework', '', '0'),
('359', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=359', '0', 'wooframework', '', '0'),
('360', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=360', '0', 'wooframework', '', '0'),
('361', '2', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-18 13:19:44', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=361', '0', 'wooframework', '', '0'),
('608', '3', '2013-06-26 16:20:44', '2013-06-26 16:20:44', '', 'Now leasing', '', 'inherit', 'open', 'open', '', 'now-leasing', '', '', '2013-06-26 16:20:44', '2013-06-26 16:20:44', '', '566', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Now-leasing.jpg', '0', 'attachment', 'image/jpeg', '0'),
('602', '3', '2013-06-26 16:04:03', '2013-06-26 16:04:03', '', 'Daycare', '', 'inherit', 'open', 'open', '', 'daycare', '', '', '2013-06-26 16:04:03', '2013-06-26 16:04:03', '', '287', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/Daycare.jpg', '0', 'attachment', 'image/jpeg', '0'),
('599', '3', '2013-06-26 15:53:46', '2013-06-26 15:53:46', '', 'Pizza', '', 'inherit', 'open', 'open', '', 'pizza', '', '', '2013-06-26 15:53:46', '2013-06-26 15:53:46', '', '318', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Pizza.jpg', '0', 'attachment', 'image/jpeg', '0'),
('597', '3', '2013-06-26 15:44:46', '2013-06-26 15:44:46', '', 'Ice Cream', '', 'inherit', 'open', 'open', '', 'ice-cream', '', '', '2013-06-26 15:44:46', '2013-06-26 15:44:46', '', '314', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Ice-Cream.jpg', '0', 'attachment', 'image/jpeg', '0'),
('383', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=383', '0', 'wooframework', '', '0'),
('384', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=384', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('385', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=385', '0', 'wooframework', '', '0'),
('386', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=386', '0', 'wooframework', '', '0'),
('387', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=387', '0', 'wooframework', '', '0'),
('388', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=388', '0', 'wooframework', '', '0'),
('389', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=389', '0', 'wooframework', '', '0'),
('390', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=390', '0', 'wooframework', '', '0'),
('391', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=391', '0', 'wooframework', '', '0'),
('392', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=392', '0', 'wooframework', '', '0'),
('393', '2', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 07:18:48', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=393', '0', 'wooframework', '', '0'),
('394', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=394', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('395', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=395', '0', 'wooframework', '', '0'),
('396', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=396', '0', 'wooframework', '', '0'),
('397', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=397', '0', 'wooframework', '', '0'),
('398', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=398', '0', 'wooframework', '', '0'),
('399', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=399', '0', 'wooframework', '', '0'),
('400', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=400', '0', 'wooframework', '', '0'),
('401', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=401', '0', 'wooframework', '', '0'),
('402', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=402', '0', 'wooframework', '', '0'),
('403', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=403', '0', 'wooframework', '', '0'),
('404', '2', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 07:32:45', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=404', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('406', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=406', '0', 'wooframework', '', '0'),
('407', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=407', '0', 'wooframework', '', '0'),
('408', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=408', '0', 'wooframework', '', '0'),
('409', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=409', '0', 'wooframework', '', '0'),
('410', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=410', '0', 'wooframework', '', '0'),
('411', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=411', '0', 'wooframework', '', '0'),
('412', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=412', '0', 'wooframework', '', '0'),
('413', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=413', '0', 'wooframework', '', '0'),
('414', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=414', '0', 'wooframework', '', '0'),
('415', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=415', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('416', '2', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 09:20:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=416', '0', 'wooframework', '', '0'),
('417', '2', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', 'Framework Woo Default Image', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_default_image', '', '', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=417', '0', 'wooframework', '', '0'),
('418', '2', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', 'Framework Woo Backend Header Image', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_backend_header_image', '', '', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=418', '0', 'wooframework', '', '0'),
('419', '2', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', 'Framework Woo Backend Icon', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_backend_icon', '', '', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=419', '0', 'wooframework', '', '0'),
('420', '2', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', 'Framework Woo Custom Login Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_custom_login_logo', '', '', '2013-06-19 09:27:13', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=420', '0', 'wooframework', '', '0'),
('421', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=421', '0', 'wooframework', '', '0'),
('422', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=422', '0', 'wooframework', '', '0'),
('423', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=423', '0', 'wooframework', '', '0'),
('424', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=424', '0', 'wooframework', '', '0'),
('425', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=425', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('426', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=426', '0', 'wooframework', '', '0'),
('427', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=427', '0', 'wooframework', '', '0'),
('428', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=428', '0', 'wooframework', '', '0'),
('429', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=429', '0', 'wooframework', '', '0'),
('430', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=430', '0', 'wooframework', '', '0'),
('431', '2', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 09:27:24', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=431', '0', 'wooframework', '', '0'),
('433', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=433', '0', 'wooframework', '', '0'),
('434', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=434', '0', 'wooframework', '', '0'),
('435', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=435', '0', 'wooframework', '', '0'),
('436', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=436', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('437', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=437', '0', 'wooframework', '', '0'),
('438', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=438', '0', 'wooframework', '', '0'),
('439', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=439', '0', 'wooframework', '', '0'),
('440', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=440', '0', 'wooframework', '', '0'),
('441', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=441', '0', 'wooframework', '', '0'),
('442', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=442', '0', 'wooframework', '', '0'),
('443', '7', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 11:58:52', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=443', '0', 'wooframework', '', '0'),
('444', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=444', '0', 'wooframework', '', '0'),
('445', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=445', '0', 'wooframework', '', '0'),
('446', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=446', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('447', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=447', '0', 'wooframework', '', '0'),
('448', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=448', '0', 'wooframework', '', '0'),
('449', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=449', '0', 'wooframework', '', '0'),
('450', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=450', '0', 'wooframework', '', '0'),
('451', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=451', '0', 'wooframework', '', '0'),
('452', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=452', '0', 'wooframework', '', '0'),
('453', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=453', '0', 'wooframework', '', '0'),
('454', '7', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 13:05:25', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=454', '0', 'wooframework', '', '0'),
('456', '2', '2013-06-19 13:16:23', '2013-06-19 13:16:23', 'IN GOOD TIMES OR BAD HE NEED FOR CHILD CARE DOES NOT GO AWAY, ESPECIALLY IN THIS UPSCALE SHORELINE TOWN. DAY CARE OPERATIONS DO NOT COME ON THE MARKET OFTEN AND VERY FEW LIKE THIS OPPORTUNITY. THIS IS A MUST SEE! JUST INVESTED $5,000 INTO NE COMPUTER, PRINTER, REFRIGERATOR, RUBBER MULCH FOR THE PLAYGROUND, AND MORE. THIS IS A TURN KEY OPPORTUNITY FOR A GREAT PRICE.....ACT NOW!!!!!!!\n\nFOR MORE INFORMATION\nCONTACT SHAWN REILLY\n203-389-5377 X29\nshawn@lmmre.com', 'DAY CARE W/UPSIDE POTENTIAL', '', 'inherit', 'open', 'open', '', '287-autosave', '', '', '2013-06-19 13:16:23', '2013-06-19 13:16:23', '', '287', 'http://www.lmmre.com/property/2013/06/19/287-autosave/', '0', 'revision', '', '0'),
('458', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=458', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('459', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=459', '0', 'wooframework', '', '0'),
('460', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=460', '0', 'wooframework', '', '0'),
('461', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=461', '0', 'wooframework', '', '0'),
('462', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=462', '0', 'wooframework', '', '0'),
('463', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=463', '0', 'wooframework', '', '0'),
('464', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=464', '0', 'wooframework', '', '0'),
('465', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=465', '0', 'wooframework', '', '0'),
('466', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=466', '0', 'wooframework', '', '0'),
('467', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=467', '0', 'wooframework', '', '0'),
('468', '2', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-19 13:21:57', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=468', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('470', '2', '2013-06-20 12:26:15', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-20 12:26:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=470', '0', 'wooframework', '', '0'),
('471', '2', '2013-06-20 12:26:15', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-20 12:26:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=471', '0', 'wooframework', '', '0'),
('472', '2', '2013-06-20 12:26:15', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-20 12:26:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=472', '0', 'wooframework', '', '0'),
('473', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=473', '0', 'wooframework', '', '0'),
('474', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=474', '0', 'wooframework', '', '0'),
('475', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=475', '0', 'wooframework', '', '0'),
('476', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=476', '0', 'wooframework', '', '0'),
('477', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=477', '0', 'wooframework', '', '0'),
('478', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=478', '0', 'wooframework', '', '0'),
('479', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=479', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('480', '2', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-20 12:26:16', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=480', '0', 'wooframework', '', '0'),
('482', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=482', '0', 'wooframework', '', '0'),
('483', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=483', '0', 'wooframework', '', '0'),
('484', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=484', '0', 'wooframework', '', '0'),
('485', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=485', '0', 'wooframework', '', '0'),
('486', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=486', '0', 'wooframework', '', '0'),
('487', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=487', '0', 'wooframework', '', '0'),
('488', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=488', '0', 'wooframework', '', '0'),
('489', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=489', '0', 'wooframework', '', '0'),
('490', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=490', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('491', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=491', '0', 'wooframework', '', '0'),
('492', '2', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-20 12:51:08', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=492', '0', 'wooframework', '', '0'),
('501', '2', '2013-06-21 13:44:16', '2013-06-21 13:44:16', '', '29-Flax-Mill-b', '', 'inherit', 'open', 'open', '', '29-flax-mill-b', '', '', '2013-06-21 13:44:16', '2013-06-21 13:44:16', '', '333', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/29-Flax-Mill-b.jpg', '1', 'attachment', 'image/jpeg', '0'),
('511', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=511', '0', 'wooframework', '', '0'),
('512', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=512', '0', 'wooframework', '', '0'),
('513', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=513', '0', 'wooframework', '', '0'),
('514', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=514', '0', 'wooframework', '', '0'),
('515', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=515', '0', 'wooframework', '', '0'),
('516', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=516', '0', 'wooframework', '', '0'),
('517', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=517', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('518', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=518', '0', 'wooframework', '', '0'),
('519', '2', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-23 15:39:00', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=519', '0', 'wooframework', '', '0'),
('520', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=520', '0', 'wooframework', '', '0'),
('521', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=521', '0', 'wooframework', '', '0'),
('522', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=522', '0', 'wooframework', '', '0'),
('523', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=523', '0', 'wooframework', '', '0'),
('524', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=524', '0', 'wooframework', '', '0'),
('525', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=525', '0', 'wooframework', '', '0'),
('526', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=526', '0', 'wooframework', '', '0'),
('527', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=527', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('528', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=528', '0', 'wooframework', '', '0'),
('529', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=529', '0', 'wooframework', '', '0'),
('530', '2', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-24 14:36:14', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=530', '0', 'wooframework', '', '0'),
('531', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=531', '0', 'wooframework', '', '0'),
('532', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=532', '0', 'wooframework', '', '0'),
('533', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=533', '0', 'wooframework', '', '0'),
('534', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=534', '0', 'wooframework', '', '0'),
('535', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=535', '0', 'wooframework', '', '0'),
('536', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=536', '0', 'wooframework', '', '0'),
('537', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=537', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('538', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=538', '0', 'wooframework', '', '0'),
('539', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=539', '0', 'wooframework', '', '0'),
('540', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=540', '0', 'wooframework', '', '0'),
('541', '2', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-24 14:39:15', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=541', '0', 'wooframework', '', '0'),
('542', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=542', '0', 'wooframework', '', '0'),
('543', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=543', '0', 'wooframework', '', '0'),
('544', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=544', '0', 'wooframework', '', '0'),
('545', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=545', '0', 'wooframework', '', '0'),
('546', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=546', '0', 'wooframework', '', '0'),
('547', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=547', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('548', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=548', '0', 'wooframework', '', '0'),
('549', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=549', '0', 'wooframework', '', '0'),
('550', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=550', '0', 'wooframework', '', '0'),
('551', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=551', '0', 'wooframework', '', '0'),
('552', '2', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-24 14:43:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=552', '0', 'wooframework', '', '0'),
('818', '3', '2013-07-26 16:51:13', '2013-07-26 16:51:13', '', '206 Whalley', '', 'inherit', 'open', 'open', '', '206-whalley-2', '', '', '2013-07-26 16:51:13', '2013-07-26 16:51:13', '', '586', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/206-Whalley.jpg', '0', 'attachment', 'image/jpeg', '0'),
('554', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=554', '0', 'wooframework', '', '0'),
('555', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=555', '0', 'wooframework', '', '0'),
('556', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=556', '0', 'wooframework', '', '0'),
('557', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=557', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('558', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=558', '0', 'wooframework', '', '0'),
('559', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=559', '0', 'wooframework', '', '0'),
('560', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=560', '0', 'wooframework', '', '0'),
('561', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=561', '0', 'wooframework', '', '0'),
('562', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=562', '0', 'wooframework', '', '0'),
('563', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=563', '0', 'wooframework', '', '0'),
('564', '2', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-24 14:57:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=564', '0', 'wooframework', '', '0'),
('609', '3', '2013-06-26 16:21:43', '2013-06-26 16:21:43', '', 'Take out', '', 'inherit', 'open', 'open', '', 'take-out', '', '', '2013-06-26 16:21:43', '2013-06-26 16:21:43', '', '316', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Take-out.jpg', '0', 'attachment', 'image/jpeg', '0'),
('568', '3', '2013-07-18 15:51:17', '2013-07-18 15:51:17', 'STOREFRONT - 1200 SF ON WHALLEY AVENUE NEAR INTERSECTION OF RT 63 &amp; 69. RETAIL, OFFICE,  HAIR SALON.\n\nFOR MORE INFORMATION\nCONTACT STEVE MILLER\n203-389-5377\nsteve@lmmre.com', '1400 WHALLEY AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '566-autosave', '', '', '2013-07-18 15:51:17', '2013-07-18 15:51:17', '', '566', 'http://www.lmmre.com/property/566-autosave/', '0', 'revision', '', '0'),
('569', '3', '2013-06-25 14:44:33', '2013-06-25 14:44:33', '1ST FLOOR STOREFRONT, JUST OFF WHALLEY AVENUE. CONVENIENT TO PARKWAY, RESTAURANTS, SHOPPING, DOWNTOWN. $1000/MONTH.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377\r\nsteve@lmmre.com', '154 WESTERLEIGH ROAD, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '154-westerleigh-road-new-haven', '', '', '2013-07-07 13:54:53', '2013-07-07 13:54:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=569', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('570', '3', '2013-06-26 16:35:45', '2013-06-26 16:35:45', '1ST FLOOR STOREFRONT, JUST OFF WHALLEY AVENUE. CONVENIENT TO PARKWAY, RESTAURANTS, SHOPPING, DOWNTOWN. $1000/MONTH.\n\nFOR MORE INFORMATION\nCONTACT STEVE MILLER\n203-389-5377\nsteve@lmmre.com', '154 WESTERLEIGH ROAD, NEW HAVEN', '', 'inherit', 'open', 'open', '', '569-autosave', '', '', '2013-06-26 16:35:45', '2013-06-26 16:35:45', '', '569', 'http://www.lmmre.com/property/569-autosave/', '0', 'revision', '', '0'),
('573', '3', '2013-07-18 17:23:05', '2013-07-18 17:23:05', '1000 SF OF 2ND FLOOR OFFICE  - CAN BE DIVIDED - 450 SF &amp; 550 SF. GREAT VISIBILITY AND ACCESS TO PARKWAY. $1200/MONTH.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377\r\nsteve@lmmre.com\r\n\r\n&nbsp;', '1400 WHALLEY AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '567-autosave', '', '', '2013-07-18 17:23:05', '2013-07-18 17:23:05', '', '567', 'http://www.lmmre.com/property/567-autosave/', '0', 'revision', '', '0'),
('574', '3', '2013-07-08 15:15:58', '2013-07-08 15:15:58', 'YOU CAN TRAVEL FAR AND WIDE FOR A LONG TIME AND NOT BE ABLE TO FIND A PHENOMENAL OPPORTUNITY LIKE THIS ONE. THIS IS IN THE BUSY RESTAURANT PART OF STATE STREET ALSO KNOWN AS THE YALE GROTTO. THE RESTAURANT DOES NEED SOME WORK BUT TO BE ABLE TO GET A RESTAURANT LIKE THIS IN AN INCREDIBLE LOCATION FOR THIS KIND OF MONEY IS UNHEARD OF.\r\n\r\nTHIS IS A MARVELOUS OPPORTUNITY THAT SHOULDN\'T BE MISSED!!!!\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com\r\n\r\n&nbsp;', '970 STATE STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '506-autosave', '', '', '2013-07-08 15:15:58', '2013-07-08 15:15:58', '', '506', 'http://www.lmmre.com/property/506-autosave/', '0', 'revision', '', '0'),
('575', '3', '2013-07-07 14:30:10', '2013-07-07 14:30:10', 'IT IS INDEED MY PLEASURE TO OFFER THIS INCREDIBLE, JUST COMPLETELY TASTEFULLY RENOVATED BURGER PLUS RESTAURANT WITH ALL NEW EQUIPMENT AT A COST OF WELL OVER $250,000. THIS GREAT LOCATION WITH ITS ENVIABLE PARKING LOT HAS A PROVEN PREVIOUS TRACK RECORD OF GROSSING OVER $2,000,000. THE PRESENT OPERATOR OWNS ANOTHER HIGHLY SUCCESSFUL RESTAURANT AND HAS FOUND RUNNING A SECOND LOCATION ABSENTEE DOESN\'T WORK FOR HIM. ALL THIS RESTAURANT NEEDS IS AN ON PREMISE OPERATOR WITH A FEW TWEAKS AND THE ADDITION OF SOME CRAFT BEERS WOULD PUT THIS PLACE RIGHT BACK ON TOP. THIS IS A MUST SEE .... PLEASE CALL TO SET UP APPOINTMENT NOW!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'NEW HOT BURGER CONCEPT', '', 'inherit', 'open', 'open', '', '319-autosave', '', '', '2013-07-07 14:30:10', '2013-07-07 14:30:10', '', '319', 'http://www.lmmre.com/property/319-autosave/', '0', 'revision', '', '0'),
('576', '3', '2013-06-25 16:08:35', '2013-06-25 16:08:35', '', 'images', '', 'inherit', 'open', 'open', '', 'images', '', '', '2013-06-25 16:08:35', '2013-06-25 16:08:35', '', '319', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/images.jpg', '0', 'attachment', 'image/jpeg', '0'),
('577', '3', '2013-07-03 00:09:11', '2013-07-03 00:09:11', 'A VERY UNIQUE HI PROFIT PIZZA OPPORTUNITY WHICH IS PART OF A POPULAR PIZZA RESTAURANT GROUP CONSISTING OF 11, WHICH YOU CAN JOIN OR CHANGE THE NAME OF THIS HI GROSSING LOCATION AND SAVE $14,000 A YEAR. IT IS A RARE BIRD TO BE ABLE TO BUY A RESTAURANT OF ANY TYPE FOR A LITTLE MORE THAN THEY ARE NETTING IN THEIR POCKET. WITH THE ADDITION OF DELIVERY ITS POSSIBLE TO GET YOUR INVESTMENT BACK IN ONE YEAR WHICH IS UNHEARD OF. CALL NOW FOR THIS MUST SEE OPPORTUNITY.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X 29\r\nshawn@lmmre.com', 'HI PROFIT PIZZA', '', 'inherit', 'open', 'open', '', '318-autosave', '', '', '2013-07-03 00:09:11', '2013-07-03 00:09:11', '', '318', 'http://www.lmmre.com/property/318-autosave/', '0', 'revision', '', '0'),
('578', '3', '2013-06-25 16:20:10', '2013-06-25 16:20:10', 'THIS HIGH GROSSING RESTAURANT HAS THE BEST OF ALL WORLDS WITH THE IDEAL COMBINATION OF FOOD, DRINK, PRIVATE PARTY AND FUN IN A UNIQUE FAMILY ATMOSPHERE AND HAS BEEN THE GO TO PLACE FOR OVER TWENTY YEARS WITH A PROVEN PROSPEROUS TRACK RECORD. A WARM AND INVITING AMERICAN GRILLE IN THIS AFFLUENT QUAINT EAST SHORELINE TOWN LOCATION IN A HISTORIC BUILDING WITH AMPLE FREE PARKING IS AN OUTSTANDING OPPORTUNITY WITH TREMENDOUS CASH FLOW. CALL NOW TO SEE THIS ONE YOU WON\'T BE SORRY!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'SPECTACULAR FAMILY RESTAURANT/BAR', '', 'inherit', 'open', 'open', '', '313-autosave', '', '', '2013-06-25 16:20:10', '2013-06-25 16:20:10', '', '313', 'http://www.lmmre.com/property/313-autosave/', '0', 'revision', '', '0'),
('580', '3', '2013-07-03 01:06:45', '2013-07-03 01:06:45', 'WELL KEPT 4,435 SF LT INDUSTRIAL BUILDING WITH 2 DRIVE-IN DOORS, AIR COMPRESSOR, ELECTRIC AUTO LIFT. APPROXIMATELY 430 SF OF OFFICE SPACE WITH AIR CONDITIONING. CEILING VARIES FROM 11\' TO 15\' IN HEIGHT. ALARMED. EXCELLENT FOR ANY AUTO LT MANUFACTURING OR ASSEMBLY USES. PARTIAL ROOF REPLACEMENT IN 2005.\r\n\r\n&nbsp;\r\n\r\nALSO AVAILABLE FOR LEASE AT $4.95 sf.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '39 OREGON AVENUE, HAMDEN', '', 'inherit', 'open', 'open', '', '277-autosave', '', '', '2013-07-03 01:06:45', '2013-07-03 01:06:45', '', '277', 'http://www.lmmre.com/property/277-autosave/', '0', 'revision', '', '0'),
('582', '3', '2013-06-25 16:50:17', '2013-06-25 16:50:17', 'SECOND FLOOR OFFICE, GREAT CONVENIENT LOCATION, CORNER WHALLEY AND WESTERLEIGH, NEAR PARKWAY, RESTAURANTS, SHOPPING. CAN BE DIVIDED. $1400/MONTH.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 25\r\nsteve@lmmre.com', '150 WESTERLEIGH ROAD, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '150-westerleigh-road-new-haven', '', '', '2013-08-06 12:29:53', '2013-08-06 12:29:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=582', '0', 'woo_estate', '', '0'),
('586', '3', '2013-06-25 20:54:39', '2013-06-25 20:54:39', 'SUPER CITY LOCATION AT LIGHT ACROSS FROM CVS AND STOP &amp; SHOP. HOT CORNER LOCATION.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT MIKE GORDON\r\n203-389-5377 X 22\r\nmichael@lmmre.com', '206 WHALLEY AVENUE, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '206-whalley-avenue-new-haven', '', '', '2013-07-26 16:52:12', '2013-07-26 16:52:12', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=586', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('604', '3', '2013-06-26 16:10:31', '2013-06-26 16:10:31', '', 'Italian', '', 'inherit', 'open', 'open', '', 'italian', '', '', '2013-06-26 16:10:31', '2013-06-26 16:10:31', '', '303', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Italian.jpg', '0', 'attachment', 'image/jpeg', '0'),
('592', '3', '2013-07-26 16:51:50', '2013-07-26 16:51:50', 'SUPER CITY LOCATION AT LIGHT ACROSS FROM CVS AND STOP &amp; SHOP. HOT CORNER LOCATION.\n\nFOR FURTHER INFORMATION\nCONTACT MIKE GORDON\n203-389-5377 X 22\nmichael@lmmre.com', '206 WHALLEY AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '586-autosave', '', '', '2013-07-26 16:51:50', '2013-07-26 16:51:50', '', '586', 'http://www.lmmre.com/property/586-autosave/', '0', 'revision', '', '0'),
('593', '3', '2013-06-26 13:43:35', '2013-06-26 13:43:35', '', 'office for lease', '', 'inherit', 'open', 'open', '', 'office-for-lease', '', '', '2013-06-26 13:43:35', '2013-06-26 13:43:35', '', '582', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/office-for-lease.jpg', '0', 'attachment', 'image/jpeg', '0'),
('594', '3', '2013-07-26 16:48:42', '2013-07-26 16:48:42', 'SECOND FLOOR OFFICE, GREAT CONVENIENT LOCATION, CORNER WHALLEY AND WESTERLEIGH, NEAR PARKWAY, RESTAURANTS, SHOPPING. CAN BE DIVIDED. $1400/MONTH.\n\nFOR FURTHER INFORMATION\nCONTACT STEVE MILLER\n203-389-5377 X 25\nsteve@lmmre.com', '150 WESTERLEIGH ROAD, NEW HAVEN', '', 'inherit', 'open', 'open', '', '582-autosave', '', '', '2013-07-26 16:48:42', '2013-07-26 16:48:42', '', '582', 'http://www.lmmre.com/property/582-autosave/', '0', 'revision', '', '0'),
('613', '3', '2013-06-26 16:29:30', '2013-06-26 16:29:30', '', 'Break lunch dinner', '', 'inherit', 'open', 'open', '', 'break-lunch-dinner', '', '', '2013-06-26 16:29:30', '2013-06-26 16:29:30', '', '310', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Break-lunch-dinner.jpg', '0', 'attachment', 'image/jpeg', '0'),
('614', '3', '2013-06-26 16:33:21', '2013-06-26 16:33:21', ',OTIV FOOD DRINKS COCKTAILS RESTAURANT LOUNGE', 'MOTIV', '', 'inherit', 'open', 'open', '', 'motiv', '', '', '2013-06-26 16:33:21', '2013-06-26 16:33:21', '', '319', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/burger-and-fries.jpg', '0', 'attachment', 'image/jpeg', '0'),
('615', '3', '2013-06-26 16:37:30', '2013-06-26 16:37:30', '', 'Westerleigh', '', 'inherit', 'open', 'open', '', 'westerleigh', '', '', '2013-06-26 16:37:30', '2013-06-26 16:37:30', '', '569', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Westerleigh.jpg', '0', 'attachment', 'image/jpeg', '0'),
('617', '3', '2013-06-26 16:41:00', '2013-06-26 16:41:00', '', 'Waterfront', '', 'inherit', 'open', 'open', '', 'waterfront', '', '', '2013-06-26 16:41:00', '2013-06-26 16:41:00', '', '309', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Waterfront.jpg', '0', 'attachment', 'image/jpeg', '0'),
('618', '3', '2013-06-26 16:42:42', '2013-06-26 16:42:42', '', 'Mexican', '', 'inherit', 'open', 'open', '', 'mexican', '', '', '2013-06-26 16:42:42', '2013-06-26 16:42:42', '', '308', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Mexican.jpg', '0', 'attachment', 'image/jpeg', '0'),
('624', '2', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', 'Framework Woo Default Image', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_default_image', '', '', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=624', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('625', '2', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', 'Framework Woo Backend Header Image', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_backend_header_image', '', '', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=625', '0', 'wooframework', '', '0'),
('626', '2', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', 'Framework Woo Backend Icon', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_backend_icon', '', '', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=626', '0', 'wooframework', '', '0'),
('627', '2', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', 'Framework Woo Custom Login Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_custom_login_logo', '', '', '2013-06-26 17:52:58', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=627', '0', 'wooframework', '', '0'),
('630', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=630', '0', 'wooframework', '', '0'),
('631', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=631', '0', 'wooframework', '', '0'),
('632', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=632', '0', 'wooframework', '', '0'),
('633', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=633', '0', 'wooframework', '', '0'),
('634', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=634', '0', 'wooframework', '', '0'),
('635', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=635', '0', 'wooframework', '', '0'),
('636', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=636', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('637', '2', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-26 18:02:34', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=637', '0', 'wooframework', '', '0'),
('638', '2', '2013-06-26 18:02:35', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-26 18:02:35', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=638', '0', 'wooframework', '', '0'),
('639', '2', '2013-06-26 18:02:35', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-26 18:02:35', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=639', '0', 'wooframework', '', '0'),
('640', '2', '2013-06-26 18:02:35', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-26 18:02:35', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=640', '0', 'wooframework', '', '0'),
('643', '3', '2013-06-27 12:29:18', '2013-06-27 12:29:18', '', 'wedding', '', 'inherit', 'open', 'open', '', 'wedding', '', '', '2013-06-27 12:29:18', '2013-06-27 12:29:18', '', '301', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/wedding.jpg', '0', 'attachment', 'image/jpeg', '0'),
('648', '2', '2013-06-28 07:55:49', '0000-00-00 00:00:00', '', 'Framework Woo Default Image', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_default_image', '', '', '2013-06-28 07:55:49', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=648', '0', 'wooframework', '', '0'),
('649', '2', '2013-06-28 07:55:49', '0000-00-00 00:00:00', '', 'Framework Woo Backend Header Image', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_backend_header_image', '', '', '2013-06-28 07:55:49', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=649', '0', 'wooframework', '', '0'),
('650', '2', '2013-06-28 07:55:49', '0000-00-00 00:00:00', '', 'Framework Woo Backend Icon', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_backend_icon', '', '', '2013-06-28 07:55:49', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=650', '0', 'wooframework', '', '0'),
('651', '2', '2013-06-28 07:55:50', '0000-00-00 00:00:00', '', 'Framework Woo Custom Login Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-framework_woo_custom_login_logo', '', '', '2013-06-28 07:55:50', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=651', '0', 'wooframework', '', '0'),
('652', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=652', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('653', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=653', '0', 'wooframework', '', '0'),
('654', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=654', '0', 'wooframework', '', '0'),
('655', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=655', '0', 'wooframework', '', '0'),
('656', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=656', '0', 'wooframework', '', '0'),
('657', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=657', '0', 'wooframework', '', '0'),
('658', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=658', '0', 'wooframework', '', '0'),
('659', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=659', '0', 'wooframework', '', '0'),
('660', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=660', '0', 'wooframework', '', '0'),
('661', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=661', '0', 'wooframework', '', '0'),
('662', '2', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-28 07:56:33', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=662', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('663', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=663', '0', 'wooframework', '', '0'),
('664', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=664', '0', 'wooframework', '', '0'),
('665', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=665', '0', 'wooframework', '', '0'),
('666', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=666', '0', 'wooframework', '', '0'),
('667', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=667', '0', 'wooframework', '', '0'),
('668', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=668', '0', 'wooframework', '', '0'),
('669', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=669', '0', 'wooframework', '', '0'),
('670', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=670', '0', 'wooframework', '', '0'),
('671', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=671', '0', 'wooframework', '', '0'),
('672', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=672', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('673', '2', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-28 08:02:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=673', '0', 'wooframework', '', '0'),
('835', '3', '2013-08-29 18:07:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2013-08-29 18:07:26', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?p=835', '0', 'post', '', '0'),
('836', '3', '2013-08-30 17:12:58', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2013-08-30 17:12:58', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?p=836', '0', 'post', '', '0'),
('727', '3', '2013-07-07 13:29:48', '2013-07-07 13:29:48', 'SOLID BUILDING ON CORNER OF GRAND AND POPLAR ADJACENT TO\r\nFREE MUNICIPAL PARKING LOT.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '275 POPLAR STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '247-autosave', '', '', '2013-07-07 13:29:48', '2013-07-07 13:29:48', '', '247', 'http://www.lmmre.com/property/247-autosave/', '0', 'revision', '', '0'),
('677', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=677', '0', 'wooframework', '', '0'),
('678', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=678', '0', 'wooframework', '', '0'),
('679', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=679', '0', 'wooframework', '', '0'),
('680', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=680', '0', 'wooframework', '', '0'),
('681', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=681', '0', 'wooframework', '', '0'),
('682', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=682', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('683', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=683', '0', 'wooframework', '', '0'),
('684', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=684', '0', 'wooframework', '', '0'),
('685', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=685', '0', 'wooframework', '', '0'),
('686', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=686', '0', 'wooframework', '', '0'),
('687', '2', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-28 09:07:12', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=687', '0', 'wooframework', '', '0'),
('699', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=699', '0', 'wooframework', '', '0'),
('700', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=700', '0', 'wooframework', '', '0'),
('701', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=701', '0', 'wooframework', '', '0'),
('702', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=702', '0', 'wooframework', '', '0'),
('703', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=703', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('704', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=704', '0', 'wooframework', '', '0'),
('705', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=705', '0', 'wooframework', '', '0'),
('706', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=706', '0', 'wooframework', '', '0'),
('707', '2', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-06-29 17:01:56', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=707', '0', 'wooframework', '', '0'),
('711', '3', '2013-07-03 00:16:17', '2013-07-03 00:16:17', '20,000 SF OF INDUSTRIAL/FLEX SPACE AVAILABLE. FULLY AIR CONDITIONED. 1 LOADING DOCK. NICELY FINISHED OFFICE SPACE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X14\r\n\r\nnoah@lmmre.com\r\n\r\n&nbsp;', '33 ROSSOTTO DRIVE, HAMDEN', '', 'inherit', 'open', 'open', '', '151-autosave', '', '', '2013-07-03 00:16:17', '2013-07-03 00:16:17', '', '151', 'http://www.lmmre.com/property/151-autosave/', '0', 'revision', '', '0'),
('786', '3', '2013-07-10 16:52:33', '2013-07-10 16:52:33', '', '3035 W', '', 'inherit', 'open', 'open', '', '3035-w', '', '', '2013-07-10 16:52:33', '2013-07-10 16:52:33', '', '779', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/3035-W.jpg', '0', 'attachment', 'image/jpeg', '0'),
('713', '3', '2013-07-03 00:35:38', '2013-07-03 00:35:38', 'IT IS INDEED MY PLEASURE TO OFFER THIS BONA FIDE VERY PROFITABLE MEXICAN RESTAURANT CHAIN LIKE LOCATION IN THE MIDDLE OF A SHOPPING MECCA FOR A PRICE HALF OF WHAT ITS WORTH. THIS ALREADY HIGH GROSSING RESTAURANT ONLY HAS A SERVICE BAR WHICH IS BIG ENOUGH TO ADD STOOLS AND ADD A HUGE PROFIT CENTER ALONG WITH PROPER MARKETING AND OWNER OPERATOR WOULD PUT THIS OFF THE CHARTS. THIS IS DEFINITELY ONE OF THE BEST BUYS YOU WILL OR HAVE SEEN IN A LONG TIME, AN OPPORTUNITY WHERE YOU CAN GET MOST OF YOUR INVESTMENT BACK IN THE FIRST YEAR, 20 PROFITABLE YEARS, AAA LOCATION, ABSENTEE OWNER, IMMENSE LOYAL CUSTOMER BASE AND MORE.  DON\'T MISS THIS ONE ...CALL NOW!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'LEGENDARY MEXICAN RESTAURANT', '', 'inherit', 'open', 'open', '', '308-autosave', '', '', '2013-07-03 00:35:38', '2013-07-03 00:35:38', '', '308', 'http://www.lmmre.com/property/308-autosave/', '0', 'revision', '', '0'),
('794', '3', '2013-07-18 15:31:47', '2013-07-18 15:31:47', '', '1400 Whalley 2nd Floor', '', 'inherit', 'open', 'open', '', '1400-whalley-2nd-floor', '', '', '2013-07-18 15:31:47', '2013-07-18 15:31:47', '', '567', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/1400-Whalley-2nd-Floor.jpg', '0', 'attachment', 'image/jpeg', '0'),
('715', '3', '2013-07-07 13:26:55', '2013-07-07 13:26:55', 'IT IS MY PLEASURE TO OFFER A VERY UNIQUE TAKE OUT RESTAURANT OPPORTUNITY IN AN EXCEPTIONAL LOCATION IN A POPULAR COMMUNITY SHOPPING CENTER , IN AN UPSCALE QUAINT POPULATED EAST SHORE TOWN WITH UPSIDE POTENTIAL. THE AWARD WINNING RESTAURANT SERVING LUNCH, DINNER, BAKED GOODS AND PICK UP CATERING FOR LARGE AND SMALL EVENTS HAVE OVER THE YEARS DEVELOPED A LARGE LOYAL CUSTOMER BASE THAT JUST KEEPS GROWING. THERE AREN\'T MANY IF SUCCESSFUL RESTAURANTS THAT OFFER A QUALITY OF LIFE THIS ONE, WHERE YOU GO IN LATE, GO HOME EARLY AND HAVE TWO DAYS OFF. YOU OWE IT TO YOURSELF TO CALL NOW AND SEE THIS ONE!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'UNIQUE TAKE OUT RESTAURANT', '', 'inherit', 'open', 'open', '', '316-autosave', '', '', '2013-07-07 13:26:55', '2013-07-07 13:26:55', '', '316', 'http://www.lmmre.com/property/316-autosave/', '0', 'revision', '', '0'),
('716', '3', '2013-07-07 14:48:46', '2013-07-07 14:48:46', '2 STORY MASONRY OFFICE CONDOMINIUMS LOCATED JUST OFF\r\nMERRITT PARKWAY IN WOODBRIDGE, CT. BUILDING IS IN PARK\r\nLIKE SETTING OVERLOOKING WESTROCK. SPACE AVAILABLE FROM\r\n575 SF UP TO 4200 SF OF CONTIGUOUS SPACE. SPACE IS ALL SUB-\r\nDIVIDABLE PERMITTING ALMOST ANY SIZE SPACE THAT IS NEEDED.\r\nPRICED AT $11.00 SF PLUS UTILITIES.\r\n\r\nCONDOS CAN BE SOLD FOR\r\n$85.00 PER SF. PLEASE LOOK AT BESTOFFICES.COM.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '88 BRADLEY ROAD, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '250-autosave', '', '', '2013-07-07 14:48:46', '2013-07-07 14:48:46', '', '250', 'http://www.lmmre.com/property/250-autosave/', '0', 'revision', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('717', '3', '2013-07-03 00:55:33', '2013-07-03 00:55:33', '', 'ServeAttachment', '', 'inherit', 'open', 'open', '', 'serveattachment', '', '', '2013-07-03 00:55:33', '2013-07-03 00:55:33', '', '20', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/ServeAttachment.jpeg', '0', 'attachment', 'image/jpeg', '0'),
('718', '3', '2013-07-03 01:11:25', '2013-07-03 01:11:25', 'SPACES AVAILABLE FROM A SINGLE ROOM OF 360 SF UP TO 4,000 SF. THERE ARE SEVERAL COMBINATIONS OF SPACE AVAILABLE THAT WE CAN MEET WHATEVER YOUR REQUIREMENTS MIGHT BE. THIS A A FLEX SPACE OFFICE BUILDING.\r\n\r\nRATES START AT $5.00 SF PLUS UTILITIES. CALL AGENT FOR YOUR NEEDS AND SHOWINGS.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '30 HAZEL TERRACE, WOODBRIDGE', '', 'inherit', 'open', 'open', '', '267-autosave', '', '', '2013-07-03 01:11:25', '2013-07-03 01:11:25', '', '267', 'http://www.lmmre.com/property/267-autosave/', '0', 'revision', '', '0'),
('719', '3', '2013-07-03 01:15:45', '2013-07-03 01:15:45', '8,400 SF BUILDING ON .73 ACRE. A CORNER LOT THAT IS CONVENIENT TO I-91 - RETAIL, SERVICE AND ADMINISTRATION FOR MANY PRODUCT LINES OF POWER EQUIPMENT. SHOWINGS AND DISCUSSIONS <strong>MUST </strong>TAKE PLACE AFTER 5:30 PM. OWNER WILL CONSIDER FINANCING TO WELL QUALIFIED COMPANIES. INDUSTRIES TOP PRODUCT LINES REPRESENTED. SCAG, EXMARK, STIHL, SCHINDAIWA, ARIENS AND MANY MORE. AKA 12 ROSSOTTO DRIVE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 X 18 OR marty@lmmre.com.', '1130 SHERMAN AVENUE, HAMDEN', '', 'inherit', 'open', 'open', '', '89-autosave', '', '', '2013-07-03 01:15:45', '2013-07-03 01:15:45', '', '89', 'http://www.lmmre.com/property/89-autosave/', '0', 'revision', '', '0'),
('720', '3', '2013-07-08 00:24:50', '2013-07-08 00:24:50', 'VERY CONVENIENT 20,000 SF OFFICE SPACE THAT CAN BE SUB-DIVIDED INTO SPACE AS LOW AS 1,200 SF. THIS PROPERTY CAN BE DIVIDED OR RENTED IN WHOLE. 6 BAYS, SINGLE 3,000 SF GARAGE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 X18 OR marty@lmmre.com.', '230 OLD GATE LANE, MILFORD', '', 'inherit', 'open', 'open', '', '94-autosave', '', '', '2013-07-08 00:24:50', '2013-07-08 00:24:50', '', '94', 'http://www.lmmre.com/property/94-autosave/', '0', 'revision', '', '0'),
('729', '3', '2013-07-07 13:46:51', '2013-07-07 13:46:51', '4,972 SF MIXED USE BUILDING ON BUSY STATE STREET, NEW HAVEN. 2 ONE BEDROOM APARTMENTS, RETAIL SPACE WITH NEW TENANT AND REAR INDUSTRIAL/STORAGE/WAREHOUSE SPACE. APARTMENTS CURRENTLY LEASED.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X 14 OR noah@lmmre.com', '1314 STATE STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '147-autosave', '', '', '2013-07-07 13:46:51', '2013-07-07 13:46:51', '', '147', 'http://www.lmmre.com/property/147-autosave/', '0', 'revision', '', '0'),
('730', '3', '2013-07-07 13:49:43', '2013-07-07 13:49:43', 'TWO STORY OFFICE BUILDING LOCATED OPPOSITE HALL OF RECORDS ON ORANGE STREET. PAID PARKING BEHIND BUILDING. 3,000 SF OF REALLY BEAUTIFUL OFFICE SPACE.  CONFERENCE ROOM, PRIVATE SHOWER, SEVERAL OFFICES, LIBRARY, RECEPTION, ETC.\r\n\r\nTHIS IS TRULY A MAGNIFICENT OFFICE THAT WILL PLEASE THE MOST DISCRIMINATE  PERSON. THIS SPACE IS LISTED AT $18.00 SF NNN.', '201 ORANGE STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '141-autosave', '', '', '2013-07-07 13:49:43', '2013-07-07 13:49:43', '', '141', 'http://www.lmmre.com/property/141-autosave/', '0', 'revision', '', '0'),
('732', '3', '2013-07-07 14:10:37', '2013-07-07 14:10:37', 'SOLID 5,000 SF INDUSTRIAL/MANUFACTURING BUILDING FULLY AIR CONDITIONED. HEAVY POWER RECENTLY UPDATED. EXCELLENT LOCATION BETWEEN RT 1 AND I-95 EXIT 45 OFF ELLA GRASSO BLVD.\r\n\r\nFOR MORE INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X22\r\nsteve@lmmre.com', '30 PRINTERS LANE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '258-autosave', '', '', '2013-07-07 14:10:37', '2013-07-07 14:10:37', '', '258', 'http://www.lmmre.com/property/258-autosave/', '0', 'revision', '', '0'),
('733', '3', '2013-07-07 14:18:49', '2013-07-07 14:18:49', 'BEST LOCATION IN DOWNTOWN AREA SURROUNDED BY RESTAURANTS, THEATER AND HOTELS. MIXED USE PROPERTY ON HIGH TRAFFIC ROUTE WITH BOTH WALKING AND DRIVING TRAFFIC WITH BEAUTIFUL HISTORIC PROPERTY THAT HAS UNDERGONE A COMPLETE REHAB IN THE LAST 2 YEARS AND IS IN PRISTINE CONDITION, WHICH INCLUDES 3 FLOORS OF BEAUTIFUL RESIDENTIAL AND 3 FLOORS OF COMMERCIAL SPACE. TO OF THE LINE REHAB FROM BASEMENT TO ROOF WITH NEW MECHANICALS. RESIDENTIAL IS BY FAR THE NICEST LIVING IN DOWNTOWN, EASILY RENTING TO A PROFESSIONAL FOR $3,500 + A MONTH AND COULD EASILY BE DIVIDED INTO 2 UNITS. COMMERCIAL SPACE CURRENTLY OCCUPIED BY HIGH GROSSING RESTAURANT. THE MARKET RENT OF $35NNN PER SQUARE FOOT GIVEN ITS PRIME LOCATION AND SALES VOLUME MAKES THIS A REAL BARGAIN. ASKING $2,900,000 FOR THE BUSINESS AND PROPERTY, $2,600,000 FOR JUST THE PROPERTY OR $599,000 FOR JUST THE BUSINESS.', 'RESTAURANT WITH PROPERTY AVAILABLE', '', 'inherit', 'open', 'open', '', '135-autosave', '', '', '2013-07-07 14:18:49', '2013-07-07 14:18:49', '', '135', 'http://www.lmmre.com/property/135-autosave/', '0', 'revision', '', '0'),
('735', '3', '2013-07-07 15:04:08', '2013-07-07 15:04:08', '', 'Pizza  YUMM', '', 'inherit', 'open', 'open', '', 'pizza-yumm', '', '', '2013-07-07 15:04:08', '2013-07-07 15:04:08', '', '300', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/Pizza-YUMM.jpg', '0', 'attachment', 'image/jpeg', '0'),
('736', '3', '2013-07-07 15:07:20', '2013-07-07 15:07:20', '', 'patio', '', 'inherit', 'open', 'open', '', 'patio', '', '', '2013-07-07 15:07:20', '2013-07-07 15:07:20', '', '290', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/patio.jpeg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('737', '3', '2013-07-07 15:11:39', '2013-07-07 15:11:39', '', 'bistro', '', 'inherit', 'open', 'open', '', 'bistro', '', '', '2013-07-07 15:11:39', '2013-07-07 15:11:39', '', '288', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/bistro.jpg', '0', 'attachment', 'image/jpeg', '0'),
('738', '3', '2013-07-07 15:16:37', '2013-07-07 15:16:37', 'APPROXIMATELY 35,000 SF OF 1.49 ACRES. OFFICE, SHOP, WAREHOUSE. VERY SUCCESSFUL WASTEPAPER DISTRIBUTION BUSINESS. RR SIDING NOT CURRENTLY IN USE. SPRINKLERED, FRONTAGE ON TWO STREETS. RIGHT OF WAY AT REAR OF PARCEL. BUSINESS AND PROPERTY $3,500,000. BUSINESS ONLY $2,500,000.\n\nFOR MORE INFORMATION\nCONTACT HAROLD KENT\n203-389-5377 X 24\nharold@lmmre.com', '610 FIRST AVENUE, WEST HAVEN', '', 'inherit', 'open', 'open', '', '281-autosave', '', '', '2013-07-07 15:16:37', '2013-07-07 15:16:37', '', '281', 'http://www.lmmre.com/property/281-autosave/', '0', 'revision', '', '0'),
('739', '3', '2013-07-07 15:21:41', '2013-07-07 15:21:41', 'BEAUTIFUL WOODED LAND - 2 ABUTTING LOTS ON EACH SIDE OF THE ROAD\nTOTALING 80+/- ACRES. WATER - SEPTIC 30,000 SF ZONING - LOTS OF ROAD\nFRONTAGE - NO ENGINEERING HAS BEEN DONE - STARTER HOMES IN\n$200,000 PRICE RANGE.\n\nFOR MORE INFORMATION\nCONTACT STEVE MILLER\n203-389-5377 X22\nsteve@lmmre.com', '0 HUNTER\'S MOUNTAIN ROAD, NAUGATUCK', '', 'inherit', 'open', 'open', '', '249-autosave', '', '', '2013-07-07 15:21:41', '2013-07-07 15:21:41', '', '249', 'http://www.lmmre.com/property/249-autosave/', '0', 'revision', '', '0'),
('741', '3', '2013-07-08 00:23:05', '2013-07-08 00:23:05', 'WASHINGTON SQUARE RETAIL CENTER HAS OPENING AVAILABLE; 1,225 OR 2,400 SF. CONVENIENT LOCATION ON WASHINGTON AVENUE AT TRAFFIC LIGHT TO DEFCO ROAD. AMPLE PARKING WITH SECURITY LIGHTING. ALL UNITS HAVE CITY SEWERS, WATER, GAS AND CENTRAL AIR.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 OR marty@lmmre.com.', '565 WASHINGTON AVENUE, NORTH HAVEN', '', 'inherit', 'open', 'open', '', '92-autosave', '', '', '2013-07-08 00:23:05', '2013-07-08 00:23:05', '', '92', 'http://www.lmmre.com/property/92-autosave/', '0', 'revision', '', '0'),
('742', '3', '2013-07-08 00:26:43', '2013-07-08 00:26:43', '1,900 SF IN ONE OF THE MOST BUSY STRIP SHOPPING CENTERS IN HAMDEN. VERY HIGH TRAFFIC FROM OTHER OCCUPANTS OF THIS CENTER. TENANT WILL ALSO GET THE USE OF A FULL BASEMENT. THIS SPACE IS WIDE OPEN. IF FIT-UP IS REQUIRED, LANDLORD CAN ASSIST FOR RIGHT OCCUPANT. OWNER IS VERY AGGRESSIVE, BRING ALL POSSIBLE USES TO THE TABLE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF 203-389-5377 OF marty@lmmre.com.\r\n\r\n&nbsp;', '3030 WHITNEY AVENUE, HAMDEN', '', 'inherit', 'open', 'open', '', '96-autosave', '', '', '2013-07-08 00:26:43', '2013-07-08 00:26:43', '', '96', 'http://www.lmmre.com/property/96-autosave/', '0', 'revision', '', '0'),
('743', '3', '2013-07-08 00:28:18', '2013-07-08 00:28:18', 'GREAT OPPORTUNITY!!!  BASEMENT HAS DIRT FLOOR. OVERSIZED LOT. MAY ALLOW ADDITIONAL APARTMENTS.\n\nFOR FURTHER INFORMATION\n\nCONTACT MIKE GORDON 203-389-5377 X 30 OR michael@lmmre.com', '176 BOYDEN STREET, WATERBURY ', '', 'inherit', 'open', 'open', '', '100-autosave', '', '', '2013-07-08 00:28:18', '2013-07-08 00:28:18', '', '100', 'http://www.lmmre.com/property/100-autosave/', '0', 'revision', '', '0'),
('744', '3', '2013-07-08 00:29:58', '2013-07-08 00:29:58', 'APPROXIMATELY 10,000 SF WAREHOUSE, 3 DOCKS, 5 OVERHEAD DOORS, ALARM SYSTEM, 90 PARKING SPACES.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MICHAEL GORDON 203-389-5377 OR michael@lmmre.com\r\n\r\n&nbsp;', '23 RACCIO PARK ROAD, HAMDEN', '', 'inherit', 'open', 'open', '', '105-autosave', '', '', '2013-07-08 00:29:58', '2013-07-08 00:29:58', '', '105', 'http://www.lmmre.com/property/105-autosave/', '0', 'revision', '', '0'),
('745', '3', '2013-07-08 00:38:19', '2013-07-08 00:38:19', '', 'opportunity', '', 'inherit', 'open', 'open', '', 'opportunity', '', '', '2013-07-08 00:38:19', '2013-07-08 00:38:19', '', '113', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/opportunity.jpg', '0', 'attachment', 'image/jpeg', '0'),
('746', '3', '2013-07-08 00:41:00', '2013-07-08 00:41:00', 'VERY WELL MAINTAINED INDUSTRIAL BUILDING WITH A WELL APPOINTED OFFICE AREA. 20\'CEILINGS IN WAREHOUSE/SHOP AREA, TWO (HIGH) OVERHEAD DOORS. EXHAUST SYSTEM IN CEILING WITH TIMER. ALSO A ROOF DRAIN IS IN PLACE. OFFICE AREA HAS CENTRAL AIR CONDITIONING AND AN ATTIC FOR STORAGE. ROOF REDONE IN 2011. ADDITIONAL  LAND IS AVAILABLE  FROM THE CITY 60X75/50X75. PHASE 3 DELTA SYSTEM IS IN  PLACE.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18\r\n\r\nmarty@lmmre.com', '196 FRONTAGE ROAD, WEST HAVEN', '', 'inherit', 'open', 'open', '', '114-autosave', '', '', '2013-07-08 00:41:00', '2013-07-08 00:41:00', '', '114', 'http://www.lmmre.com/property/114-autosave/', '0', 'revision', '', '0'),
('747', '3', '2013-07-08 00:46:51', '2013-07-08 00:46:51', '', 'success-sign-300x199', '', 'inherit', 'open', 'open', '', 'success-sign-300x199', '', '', '2013-07-08 00:46:51', '2013-07-08 00:46:51', '', '120', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/success-sign-300x199.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('748', '3', '2013-07-08 00:47:06', '2013-07-08 00:47:06', '14+ ACRES WITH I-691 FRONTAGE. WAS ZONED FOR CATERING HALL. HOUSE AND GARAGE NEED TO BE REMOVED.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT MIKE GORDON\r\n\r\n203-389-5377 X 25\r\n\r\nmichael@lmmre.com', '306 EAST JOHNSON AVENUE, CHESHIRE', '', 'inherit', 'open', 'open', '', '120-autosave', '', '', '2013-07-08 00:47:06', '2013-07-08 00:47:06', '', '120', 'http://www.lmmre.com/property/120-autosave/', '0', 'revision', '', '0'),
('749', '3', '2013-07-08 00:51:50', '2013-07-08 00:51:50', 'UP TO 40,000 SF OF BEAUTIFUL OFFICE SPACE LOCATED IN THE HEART OF WESTVILLE, NEW HAVEN. FULLY HANDICAP ACCESSIBLE. AMPLE FREE PARKING IN PARK LIKE SETTING. THIS IS A GREAT OPPORTUNITY TO TAKE ADVANTAGE OF AN IMPRESSIVE OFFICE BUILD OUT BY A FORTUNE 500 COMPANY. OFFICE SPACE INCLUDES EXECUTIVE SUITES. MULTIPLE CONFERENCE ROOMS, KITCHEN, ETC. CEILING HEIGHTS 9-18 FEET.', '495 BLAKE STREET, NEW HAVEN', '', 'inherit', 'open', 'open', '', '124-autosave', '', '', '2013-07-08 00:51:50', '2013-07-08 00:51:50', '', '124', 'http://www.lmmre.com/property/124-autosave/', '0', 'revision', '', '0'),
('750', '3', '2013-07-08 00:59:30', '2013-07-08 00:59:30', '', 'glass_of_wine_by_hiddenrainbow-d4y756j', '', 'inherit', 'open', 'open', '', 'glass_of_wine_by_hiddenrainbow-d4y756j', '', '', '2013-07-08 00:59:30', '2013-07-08 00:59:30', '', '129', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/glass_of_wine_by_hiddenrainbow-d4y756j.png', '0', 'attachment', 'image/png', '0'),
('751', '3', '2013-07-08 01:04:56', '2013-07-08 01:04:56', '744 SF OF RETAIL SPACE AVAILABLE IN BUSY MT CARMEL CENTER. LOCATED RIGHT OFF THE I-91 CONNECTOR AND 1 MILE FROM QUINNIPIAC UNIVERSITY.\r\n\r\nFOR FURTHER INFORMATION\r\n\r\nCONTACT NOAH MEYER\r\n\r\n203-389-5377 X14\r\n\r\nnoah@lmmre.com', '2985 WHITNEY AVENUE, NEW HAVEN', '', 'inherit', 'open', 'open', '', '143-autosave', '', '', '2013-07-08 01:04:56', '2013-07-08 01:04:56', '', '143', 'http://www.lmmre.com/property/143-autosave/', '0', 'revision', '', '0'),
('752', '3', '2013-07-08 02:19:43', '2013-07-08 02:19:43', '6,200 SF AND 5,400 SF INDUSTRIAL OR WAREHOUSE SPACE AVAILABLE. 1 OVERHEAD DOOR IN EACH SPACE. SPRINKLER. 18 FOOT CEILING HEIGHT.\n\n<em><strong>FOR LEASE $2 SQ FT   NNN.</strong></em>\n\nFOR MORE INFORMATION\n\nCONTACT ARIN HAYDEN\n\n203-389-5377 X 38 OR arin@lmmre.com\n\n&nbsp;', '163 ORANGE AVENUE, WEST HAVEN ', '', 'inherit', 'open', 'open', '', '187-autosave', '', '', '2013-07-08 02:19:43', '2013-07-08 02:19:43', '', '187', 'http://www.lmmre.com/property/187-autosave/', '0', 'revision', '', '0'),
('753', '3', '2013-07-08 02:26:51', '2013-07-08 02:26:51', 'IN THE SCENIC NORTH END OF HAMDEN, WE OFFER 18.2 ACRES FOR DEVELOPMENT. DEVELOPER CAN CHOOSE FROM MANY POSSIBLE USES SUCH AS: APARTMENTS (OVER 200 UNITS), 55 &amp; OVER RENTAL OR SALE COMPLETE, STUDENT HOUSING (THE OTHER SIDE OF THIS PARCEL IS OWNED BY QUINNIPIAC UNIVERSITY FOR ONE OF THEIR CAMPUSES. ALSO ON THIS SITE OF QUINNIPIAC IS THE TD BANK SPORTS COMPLEX, ALONG WITH 2,000 BEDS FOR STUDENT HOUSING. SIGNIFICANT OPPORTUNITY IF REMOVAL OF QUALITY STONE/MATERIAL.\r\n\r\nFOR MORE INFORMATION\r\n\r\nCONTACT MARTY RUFF\r\n\r\n203-389-5377 X18 OR marty@lmmre.com', '64 ROCKY TOP ROAD, HAMDEN', '', 'inherit', 'open', 'open', '', '201-autosave', '', '', '2013-07-08 02:26:51', '2013-07-08 02:26:51', '', '201', 'http://www.lmmre.com/property/201-autosave/', '0', 'revision', '', '0'),
('754', '3', '2013-07-08 02:40:52', '2013-07-08 02:40:52', '', 'Banquet', '', 'inherit', 'open', 'open', '', 'banquet', '', '', '2013-07-08 02:40:52', '2013-07-08 02:40:52', '', '284', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/05/Banquet.jpg', '0', 'attachment', 'image/jpeg', '0'),
('758', '3', '2013-07-08 02:57:58', '2013-07-08 02:57:58', 'TWO FREESTANDING BUILDINGS THAT COULD BE A MULTITUDE OF THINGS.  INDUSTRIAL/MANUFACTURING/RETAIL\r\n\r\nOWNER WILL ENTERTAIN BUILDING OUT.  LONG TERM LEASE, FANTASTIC LOCATION\r\n\r\nEACH BUILDING HAS AN OVERHEAD DOOR FOR EASY IN AND OUT\r\n\r\n<em><strong>$7 SQ FOOT NNN</strong></em>\r\n\r\n&nbsp;\r\n\r\nCALL STEVE MILLER FOR INFORMATION\r\n\r\n203-389-5377 X 22\r\n\r\nOR STEVE@LMMRE.COM', '57 & 65 Woodmont Road, Milford, CT', '', 'publish', 'closed', 'closed', '', '57-65-woodmont-road', '', '', '2013-07-08 02:58:14', '2013-07-08 02:58:14', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=758', '0', 'woo_estate', '', '0'),
('759', '3', '2013-07-08 02:56:41', '2013-07-08 02:56:41', '', '57 woodmont', '', 'inherit', 'open', 'open', '', '57-woodmont', '', '', '2013-07-08 02:56:41', '2013-07-08 02:56:41', '', '758', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/57-woodmont.jpg', '0', 'attachment', 'image/jpeg', '0'),
('760', '3', '2013-07-08 02:56:56', '2013-07-08 02:56:56', '', '65 w', '', 'inherit', 'open', 'open', '', '65-w', '', '', '2013-07-08 02:56:56', '2013-07-08 02:56:56', '', '758', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/65-w.jpg', '0', 'attachment', 'image/jpeg', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('761', '3', '2013-07-08 02:58:26', '2013-07-08 02:58:26', 'TWO FREESTANDING BUILDINGS THAT COULD BE A MULTITUDE OF THINGS.  INDUSTRIAL/MANUFACTURING/RETAIL\r\n\r\nOWNER WILL ENTERTAIN BUILDING OUT.  LONG TERM LEASE, FANTASTIC LOCATION\r\n\r\nEACH BUILDING HAS AN OVERHEAD DOOR FOR EASY IN AND OUT\r\n\r\n<em><strong>$7 SQ FOOT NNN</strong></em>\r\n\r\n&nbsp;\r\n\r\nCALL STEVE MILLER FOR INFORMATION\r\n\r\n203-389-5377 X 22\r\n\r\nOR STEVE@LMMRE.COM', '57 & 65 Woodmont Road, Milford, CT', '', 'inherit', 'open', 'open', '', '758-autosave', '', '', '2013-07-08 02:58:26', '2013-07-08 02:58:26', '', '758', 'http://www.lmmre.com/property/758-autosave/', '0', 'revision', '', '0'),
('764', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Logo', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_logo', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=764', '0', 'wooframework', '', '0'),
('765', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Custom Favicon', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_custom_favicon', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=765', '0', 'wooframework', '', '0'),
('766', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Body Img', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_body_img', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=766', '0', 'wooframework', '', '0'),
('767', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Garage Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_big', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=767', '0', 'wooframework', '', '0'),
('768', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Garage Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_garage_logo_small', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=768', '0', 'wooframework', '', '0'),
('769', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Bed Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_big', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=769', '0', 'wooframework', '', '0'),
('770', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Bed Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bed_logo_small', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=770', '0', 'wooframework', '', '0'),
('771', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Bath Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_big', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=771', '0', 'wooframework', '', '0'),
('772', '2', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', 'Woo Bath Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_bath_logo_small', '', '', '2013-07-08 11:49:02', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=772', '0', 'wooframework', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('773', '2', '2013-07-08 11:49:03', '0000-00-00 00:00:00', '', 'Woo Size Logo Big', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_big', '', '', '2013-07-08 11:49:03', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=773', '0', 'wooframework', '', '0'),
('774', '2', '2013-07-08 11:49:03', '0000-00-00 00:00:00', '', 'Woo Size Logo Small', '', 'draft', 'closed', 'closed', '', 'woo-wf-woo_size_logo_small', '', '', '2013-07-08 11:49:03', '0000-00-00 00:00:00', '', '0', 'http://www.lmmre.com/property/?post_type=wooframework&p=774', '0', 'wooframework', '', '0'),
('790', '3', '2013-07-18 13:46:26', '2013-07-18 13:46:26', 'GREAT LOCATION IN THIS PROFESSIONAL BUILDING. IT IS THE FIRST DOOR AT THE ENTRANCE TO THE PROPERTY. 800 SF OFFICE NEAR NEW TOWN HALL. 2 PRIVATE OFFICES, CONFERENCE ROOM, RECEPTION. NO STAIRS. RENT INCLUDES EVERYTHING INCLUDING HEAT AND AIR CONDITIONING. EXCELLENT SIGNAGE ALLOWED. $1,000 PER MONTH. ALSO 440 SF RECEPTION AND PRIVATE OFFICE. 440 SF  - RENT IS $550 FULL GROSS.\n\nFOR FURTHER INFORMATION\n\nCONTACT MARTY RUFF\n\n203-389-5377 X18\n\nmarty@lmmre.com', '105 SANFORD STREET, HAMDEN', '', 'inherit', 'open', 'open', '', '118-autosave', '', '', '2013-07-18 13:46:26', '2013-07-18 13:46:26', '', '118', 'http://www.lmmre.com/property/118-autosave/', '0', 'revision', '', '0'),
('780', '3', '2013-07-10 16:52:58', '2013-07-10 16:52:58', 'Located next to Quinnipiac Universities Irish Museum------2 Floors 3,000 +/- sf each floor\r\n\r\nFinished Loft 400 +/- sf--------Finished Basement 2,000 +/- sf w/2 Bathrooms and Kitchen\r\n\r\nPossible Student Housing----------Present Use Office Building\r\n\r\nPrime Hamden Location, Close to the Connector and I-91.  Parking, High Traffic.\r\n\r\n&nbsp;\r\n\r\n<em><strong>For Sale or For Lease</strong></em>\r\n\r\n&nbsp;\r\n\r\nCall Bernie Diana\r\n\r\n203-389-5377  OR\r\n\r\nemail Bernie@lmmre.com', '3035 Whitney Ave, Hamden', '', 'inherit', 'open', 'open', '', '779-autosave', '', '', '2013-07-10 16:52:58', '2013-07-10 16:52:58', '', '779', 'http://www.lmmre.com/property/779-autosave/', '0', 'revision', '', '0'),
('781', '3', '2013-07-09 00:12:28', '2013-07-09 00:12:28', '', 'pizzammmmm', '', 'inherit', 'open', 'open', '', 'pizzammmmm', '', '', '2013-07-09 00:12:28', '2013-07-09 00:12:28', '', '317', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/pizzammmmm.jpg', '0', 'attachment', 'image/jpeg', '0'),
('797', '3', '2013-07-18 15:49:09', '2013-07-18 15:49:09', '', '1400 Whalley', '', 'inherit', 'open', 'open', '', '1400-whalley-2', '', '', '2013-07-18 15:49:09', '2013-07-18 15:49:09', '', '566', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/06/1400-Whalley1.jpg', '0', 'attachment', 'image/jpeg', '0'),
('801', '3', '2013-07-23 20:01:48', '2013-07-23 20:01:48', '', '206 Whalley', '', 'inherit', 'open', 'open', '', '206-whalley', '', '', '2013-07-23 20:01:48', '2013-07-23 20:01:48', '', '0', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/206-Whalley.jpg', '0', 'attachment', 'image/jpeg', '0'),
('804', '3', '2013-07-25 13:46:43', '2013-07-25 13:46:43', 'PROFESSIONAL OFFICE BUILDING - FIRST FLOOR WALK-IN. GREAT SIGNAGE AVAILABLE. JOIN A BUSY STRIP CENTER WITH STATE FARM INSURANCE, A WELL KNOWN CHIROPRACTOR, AND A POPULAR UNI-SEX SALON. ALSO, TWO VERY POPULAR EATERIES MAKES THIS SPACE DESIRABLE.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT MARTY RUFF\r\n203-389-5377 X 18\r\nmarty@lmmre.com', '444 WASHINGTON AVENUE, NORTH HAVEN', '', 'publish', 'closed', 'closed', '', '444-washington-avenue-north-haven', '', '', '2013-07-25 13:46:43', '2013-07-25 13:46:43', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=804', '0', 'woo_estate', '', '0'),
('805', '3', '2013-07-25 13:43:12', '2013-07-25 13:43:12', '', '448 Washington Ave', '', 'inherit', 'open', 'open', '', '448-washington-ave', '', '', '2013-07-25 13:43:12', '2013-07-25 13:43:12', '', '804', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/448-Washington-Ave.jpg', '0', 'attachment', 'image/jpeg', '0'),
('809', '3', '2013-07-25 17:30:53', '2013-07-25 17:30:53', 'NICE SMALLER INVESTMENT PROPERTY. FULLY LEASED - 2 FIRST FLOOR, BARBER &amp; CHURCH, 1 RESIDENTIAL UNIT - 4 BEDROOM - 1,384 SF. WELL LOCATED SMALLER INVESTMENT PROPERTY. INCOME $2,400 MONTH/$28,800 ANNUAL. EXPENSES $10,000 ANNUAL.\r\n\r\nFOR FURTHER INFORMATION\r\nCONTACT STEVE MILLER\r\n203-389-5377 X 22\r\nsteve@lmmre.com', '1308 STATE STREET, NEW HAVEN', '', 'publish', 'closed', 'closed', '', '1308-state-street-new-haven', '', '', '2013-07-25 17:30:53', '2013-07-25 17:30:53', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=809', '0', 'woo_estate', '', '0');

INSERT INTO `wp_2_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
('810', '3', '2013-07-25 17:27:20', '2013-07-25 17:27:20', '', '1308 State front', '', 'inherit', 'open', 'open', '', '1308-state-front', '', '', '2013-07-25 17:27:20', '2013-07-25 17:27:20', '', '809', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/1308-State-front.jpg', '0', 'attachment', 'image/jpeg', '0'),
('811', '3', '2013-07-25 17:27:38', '2013-07-25 17:27:38', '', '1308 State church', '', 'inherit', 'open', 'open', '', '1308-state-church', '', '', '2013-07-25 17:27:38', '2013-07-25 17:27:38', '', '809', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/1308-State-church.jpg', '0', 'attachment', 'image/jpeg', '0'),
('812', '3', '2013-07-25 17:27:52', '2013-07-25 17:27:52', '', '108 State barber', '', 'inherit', 'open', 'open', '', '108-state-barber', '', '', '2013-07-25 17:27:52', '2013-07-25 17:27:52', '', '809', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/108-State-barber.jpg', '0', 'attachment', 'image/jpeg', '0'),
('813', '3', '2013-07-25 17:28:30', '2013-07-25 17:28:30', '', '1308 State side', '', 'inherit', 'open', 'open', '', '1308-state-side', '', '', '2013-07-25 17:28:30', '2013-07-25 17:28:30', '', '809', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/1308-State-side.jpg', '0', 'attachment', 'image/jpeg', '0'),
('814', '3', '2013-07-25 17:28:48', '2013-07-25 17:28:48', '', '1308 State apt', '', 'inherit', 'open', 'open', '', '1308-state-apt', '', '', '2013-07-25 17:28:48', '2013-07-25 17:28:48', '', '809', 'http://www.lmmre.com/property/wp-content/uploads/sites/2/2013/07/1308-State-apt.jpg', '0', 'attachment', 'image/jpeg', '0'),
('827', '3', '2013-08-15 14:41:38', '2013-08-15 14:41:38', 'THIS CELEBRATED LUNCHEON RESTAURANT IS AN EXCEPTIONAL DOWNTOWN LOCATION AND HAS DEVELOPED AN INCREDIBLE REPUTATION AND A LARGE LOYAL CUSTOMER BASE THAT INCREASES EVERY DAY IN A VERY SHORT PERIOD OF TIME. DUE TO FAMILY THE OWNER HAS RELUCTANTLY PUT HIS RESTAURANT ON THE MARKET. THE WHOPPING UPSIDE POTENTIAL BY FOCUSING ON EXPANDING BREAKFAST, CORPORATE BY FOCUSING ON EXPANDING BREAKFAST, CORPORATE CATERING AND OPENING FOR DINNER WITH BYOB WOULD INCREASE REVENUE DRAMATICALLY. DON\'T MISS THE EXTRAORDINARY OPPORTUNITY, CALL NOW!!!\r\n\r\nFOR MORE INFORMATION\r\nCONTACT SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'Highly Acclaimed Lunch Restaurant', '', 'publish', 'closed', 'closed', '', 'highly-acclaimed-lunch-restaurant', '', '', '2013-08-15 14:44:47', '2013-08-15 14:44:47', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=827', '0', 'woo_estate', '', '0'),
('828', '3', '2013-08-15 14:42:44', '2013-08-15 14:42:44', 'THIS CELEBRATED LUNCHEON RESTAURANT IS AN EXCEPTIONAL DOWNTOWN LOCATION AND HAS DEVELOPED AN INCREDIBLE REPUTATION AND A LARGE LOYAL CUSTOMER BASE THAT INCREASES EVERY DAY IN A VERY SHORT PERIOD OF TIME. DUE TO FAMILY THE OWNER HAS RELUCTANTLY PUT HIS RESTAURANT ON THE MARKET. THE WHOPPING UPSIDE POTENTIAL BY FOCUSING ON EXPANDING BREAKFAST, CORPORATE BY FOCUSING ON EXPANDING BREAKFAST, CORPORATE CATERING AND OPENING FOR DINNER WITH BYOB WOULD INCREASE REVENUE DRAMATICALLY. DON\'T MISS THE EXTRAORDINARY OPPORTUNITY, CALL NOW!!!\n\nFOR MORE INFORMATION\nCONTACT SHAWN REILLY\n203-389-5377 X29\nshawn@lmmre.com', 'Highly Acclaimed Lunch Restaurant', '', 'inherit', 'open', 'open', '', '827-autosave', '', '', '2013-08-15 14:42:44', '2013-08-15 14:42:44', '', '827', 'http://www.lmmre.com/property/827-autosave/', '0', 'revision', '', '0'),
('831', '3', '2013-08-15 16:09:49', '2013-08-15 16:09:49', 'IT IS INDEED MY PLEASURE TO OFFER THIS RENOWNED HIGH GROSSING PROFITABLE RESTAURANT WITH HUGE BREAKFAST &amp; LUNCH BUSINESS. IN THE MIDST OF TWO MAJOR HOTELS, MOVIE THEATER, SHUBERT, OFFICES, RESIDENTIAL CONDOS ACROSS THE STREET, NEXT DOOR TO YALE AND GATEWAY IS A STEAL AT THIS ABSURD ASKING PRICE AND A MUST SEE. IT\'S TRULY HARD TO BELIEVE THAT THIS IS THE ONLY RESTAURANT OF ITS KIND IN THE WHOLE AREA, NOT OFTEN THAT YOU GET TO ENJOY BEING THE ONLY GAME ON TOWN. THEY HAVE JUST BEGUN TO DEVELOP A REPUTATION WITH THE COMMUNITY FOR CATERING THE UPSIDE POTENTIAL BRINGING IT TO THE FULLEST WILL ADD GREATLY TO THE INCOME. THIS ONE WILL MOVE QUICK, CALL NOW TO TAKE ADVANTAGE OF THIS PHENOMENAL RESTAURANT OPPORTUNITY FOR A PRICE LIKE THIS.\r\n\r\nFOR MORE INFORMATION\r\nCALL SHAWN REILLY\r\n203-389-5377 X29\r\nshawn@lmmre.com', 'STUPENDOUS BREAKFAST/LUNCH BUY', '', 'publish', 'closed', 'closed', '', 'stupendous-breakfastlunch-buy', '', '', '2013-08-29 12:48:02', '2013-08-29 12:48:02', '', '0', 'http://www.lmmre.com/property/?post_type=woo_estate&#038;p=831', '0', 'woo_estate', '', '0');

--
-- Table structure for table `wp_2_term_relationships`
--

CREATE TABLE `wp_2_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_2_term_relationships`
--

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('293', '17', '0'),
('7', '19', '0'),
('6', '19', '0'),
('8', '19', '0'),
('9', '19', '0'),
('10', '19', '0'),
('11', '19', '0'),
('14', '20', '0'),
('15', '17', '0'),
('204', '81', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('18', '17', '0'),
('18', '2', '0'),
('20', '11', '0'),
('20', '4', '0'),
('15', '21', '0'),
('15', '22', '0'),
('15', '23', '0'),
('15', '24', '0'),
('218', '19', '0'),
('15', '26', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('15', '27', '0'),
('15', '28', '0'),
('18', '29', '0'),
('18', '28', '0'),
('18', '30', '0'),
('18', '22', '0'),
('18', '23', '0'),
('18', '21', '0'),
('20', '31', '0'),
('20', '32', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('20', '33', '0'),
('20', '34', '0'),
('20', '35', '0'),
('204', '175', '0'),
('204', '174', '0'),
('506', '17', '0'),
('301', '188', '0'),
('204', '17', '0'),
('41', '38', '0'),
('41', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('41', '39', '0'),
('41', '22', '0'),
('41', '28', '0'),
('41', '40', '0'),
('41', '41', '0'),
('36', '38', '0'),
('36', '2', '0'),
('36', '42', '0'),
('36', '43', '0'),
('36', '84', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('36', '22', '0'),
('36', '23', '0'),
('47', '17', '0'),
('47', '6', '0'),
('47', '46', '0'),
('47', '47', '0'),
('47', '48', '0'),
('58', '9', '0'),
('58', '2', '0'),
('58', '4', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('58', '49', '0'),
('58', '50', '0'),
('58', '51', '0'),
('58', '22', '0'),
('58', '23', '0'),
('58', '52', '0'),
('58', '53', '0'),
('63', '17', '0'),
('63', '5', '0'),
('63', '54', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('63', '55', '0'),
('63', '56', '0'),
('63', '57', '0'),
('65', '38', '0'),
('65', '4', '0'),
('65', '58', '0'),
('65', '59', '0'),
('65', '51', '0'),
('65', '60', '0'),
('65', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('65', '23', '0'),
('65', '52', '0'),
('65', '61', '0'),
('68', '14', '0'),
('68', '2', '0'),
('68', '40', '0'),
('68', '62', '0'),
('68', '22', '0'),
('68', '23', '0'),
('70', '38', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('70', '2', '0'),
('70', '4', '0'),
('70', '50', '0'),
('70', '51', '0'),
('70', '63', '0'),
('70', '22', '0'),
('70', '23', '0'),
('70', '28', '0'),
('75', '12', '0'),
('75', '5', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('75', '58', '0'),
('75', '64', '0'),
('75', '65', '0'),
('75', '22', '0'),
('75', '23', '0'),
('82', '17', '0'),
('82', '58', '0'),
('82', '5', '0'),
('82', '33', '0'),
('82', '66', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('82', '67', '0'),
('82', '68', '0'),
('82', '60', '0'),
('82', '22', '0'),
('82', '23', '0'),
('82', '61', '0'),
('86', '17', '0'),
('86', '2', '0'),
('86', '21', '0'),
('86', '69', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('86', '60', '0'),
('86', '22', '0'),
('86', '23', '0'),
('89', '14', '0'),
('89', '2', '0'),
('89', '4', '0'),
('89', '70', '0'),
('89', '66', '0'),
('89', '51', '0'),
('89', '71', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('89', '22', '0'),
('89', '23', '0'),
('92', '18', '0'),
('92', '2', '0'),
('92', '4', '0'),
('92', '72', '0'),
('92', '73', '0'),
('92', '49', '0'),
('92', '39', '0'),
('92', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('94', '16', '0'),
('94', '2', '0'),
('94', '5', '0'),
('94', '74', '0'),
('94', '75', '0'),
('94', '76', '0'),
('94', '22', '0'),
('94', '23', '0'),
('96', '14', '0'),
('96', '4', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('96', '77', '0'),
('96', '78', '0'),
('96', '79', '0'),
('96', '26', '0'),
('96', '22', '0'),
('96', '23', '0'),
('100', '80', '0'),
('100', '81', '0'),
('100', '67', '0'),
('100', '82', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('100', '22', '0'),
('100', '23', '0'),
('100', '61', '0'),
('105', '14', '0'),
('105', '2', '0'),
('105', '5', '0'),
('105', '85', '0'),
('105', '86', '0'),
('105', '87', '0'),
('105', '88', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('105', '22', '0'),
('105', '23', '0'),
('107', '14', '0'),
('107', '2', '0'),
('107', '5', '0'),
('107', '4', '0'),
('107', '89', '0'),
('107', '55', '0'),
('107', '62', '0'),
('107', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('107', '23', '0'),
('107', '52', '0'),
('110', '90', '0'),
('110', '22', '0'),
('110', '96', '0'),
('110', '95', '0'),
('110', '51', '0'),
('110', '2', '0'),
('110', '4', '0'),
('110', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('114', '11', '0'),
('113', '2', '0'),
('113', '4', '0'),
('113', '97', '0'),
('113', '54', '0'),
('113', '98', '0'),
('113', '22', '0'),
('113', '28', '0'),
('831', '17', '0'),
('114', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('15', '58', '0'),
('114', '100', '0'),
('114', '55', '0'),
('114', '101', '0'),
('114', '88', '0'),
('114', '22', '0'),
('114', '23', '0'),
('114', '52', '0'),
('118', '14', '0'),
('118', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('118', '50', '0'),
('118', '102', '0'),
('118', '62', '0'),
('118', '96', '0'),
('118', '22', '0'),
('118', '23', '0'),
('118', '28', '0'),
('120', '13', '0'),
('120', '6', '0'),
('120', '5', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('120', '103', '0'),
('120', '104', '0'),
('120', '39', '0'),
('121', '14', '0'),
('121', '58', '0'),
('121', '105', '0'),
('121', '106', '0'),
('121', '62', '0'),
('121', '22', '0'),
('121', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('121', '61', '0'),
('123', '107', '0'),
('123', '6', '0'),
('123', '5', '0'),
('123', '108', '0'),
('124', '17', '0'),
('124', '2', '0'),
('124', '109', '0'),
('124', '43', '0'),
('124', '110', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('124', '22', '0'),
('124', '23', '0'),
('124', '28', '0'),
('126', '111', '0'),
('126', '97', '0'),
('126', '4', '0'),
('126', '112', '0'),
('126', '45', '0'),
('126', '22', '0'),
('126', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('126', '34', '0'),
('126', '28', '0'),
('129', '14', '0'),
('129', '8', '0'),
('129', '113', '0'),
('129', '114', '0'),
('129', '115', '0'),
('129', '22', '0'),
('129', '23', '0'),
('129', '116', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('135', '17', '0'),
('135', '58', '0'),
('135', '77', '0'),
('135', '60', '0'),
('135', '22', '0'),
('135', '23', '0'),
('135', '61', '0'),
('138', '14', '0'),
('138', '2', '0'),
('141', '17', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('141', '2', '0'),
('141', '117', '0'),
('141', '118', '0'),
('141', '21', '0'),
('141', '119', '0'),
('141', '120', '0'),
('141', '22', '0'),
('141', '23', '0'),
('141', '61', '0'),
('151', '14', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('151', '5', '0'),
('151', '121', '0'),
('153', '38', '0'),
('153', '2', '0'),
('153', '50', '0'),
('153', '122', '0'),
('153', '22', '0'),
('153', '23', '0'),
('153', '61', '0'),
('153', '60', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('159', '17', '0'),
('159', '2', '0'),
('159', '4', '0'),
('159', '33', '0'),
('159', '123', '0'),
('159', '124', '0'),
('159', '68', '0'),
('159', '125', '0'),
('159', '22', '0'),
('159', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('159', '61', '0'),
('166', '9', '0'),
('166', '4', '0'),
('166', '50', '0'),
('166', '127', '0'),
('166', '78', '0'),
('166', '128', '0'),
('166', '88', '0'),
('166', '22', '0'),
('166', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('166', '28', '0'),
('169', '129', '0'),
('169', '2', '0'),
('169', '4', '0'),
('169', '50', '0'),
('169', '130', '0'),
('169', '131', '0'),
('169', '96', '0'),
('169', '52', '0'),
('173', '132', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('173', '22', '0'),
('173', '28', '0'),
('173', '394', '0'),
('173', '393', '0'),
('173', '392', '0'),
('173', '115', '0'),
('173', '40', '0'),
('173', '23', '0'),
('147', '17', '0'),
('147', '4', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('147', '5', '0'),
('147', '139', '0'),
('147', '140', '0'),
('147', '141', '0'),
('147', '142', '0'),
('147', '60', '0'),
('147', '22', '0'),
('147', '23', '0'),
('147', '61', '0'),
('177', '14', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('177', '2', '0'),
('177', '4', '0'),
('177', '143', '0'),
('177', '144', '0'),
('177', '145', '0'),
('177', '146', '0'),
('177', '22', '0'),
('177', '23', '0'),
('177', '28', '0'),
('179', '147', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('179', '115', '0'),
('179', '396', '0'),
('179', '61', '0'),
('179', '21', '0'),
('179', '395', '0'),
('181', '13', '0'),
('181', '58', '0'),
('181', '5', '0'),
('181', '152', '0'),
('181', '153', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('226', '18', '0'),
('181', '155', '0'),
('181', '156', '0'),
('185', '14', '0'),
('185', '2', '0'),
('185', '50', '0'),
('185', '157', '0'),
('185', '158', '0'),
('185', '78', '0'),
('185', '159', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('185', '88', '0'),
('185', '22', '0'),
('185', '23', '0'),
('185', '61', '0'),
('185', '21', '0'),
('187', '11', '0'),
('187', '5', '0'),
('187', '70', '0'),
('187', '59', '0'),
('187', '78', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('187', '43', '0'),
('187', '160', '0'),
('187', '22', '0'),
('187', '23', '0'),
('187', '28', '0'),
('189', '17', '0'),
('189', '6', '0'),
('189', '5', '0'),
('189', '110', '0'),
('189', '161', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('189', '162', '0'),
('189', '163', '0'),
('189', '164', '0'),
('193', '17', '0'),
('193', '5', '0'),
('193', '54', '0'),
('193', '141', '0'),
('193', '110', '0'),
('193', '88', '0'),
('193', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('193', '23', '0'),
('193', '28', '0'),
('195', '14', '0'),
('195', '2', '0'),
('195', '4', '0'),
('195', '165', '0'),
('195', '124', '0'),
('195', '166', '0'),
('195', '49', '0'),
('195', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('195', '23', '0'),
('195', '167', '0'),
('197', '15', '0'),
('197', '5', '0'),
('197', '141', '0'),
('197', '168', '0'),
('197', '21', '0'),
('197', '169', '0'),
('197', '26', '0'),
('197', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('197', '23', '0'),
('201', '14', '0'),
('201', '6', '0'),
('201', '170', '0'),
('201', '171', '0'),
('204', '57', '0'),
('204', '176', '0'),
('204', '177', '0'),
('204', '22', '0'),
('204', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('205', '18', '0'),
('205', '2', '0'),
('205', '21', '0'),
('205', '178', '0'),
('205', '179', '0'),
('205', '96', '0'),
('205', '22', '0'),
('205', '23', '0'),
('217', '132', '0'),
('217', '5', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('217', '180', '0'),
('217', '181', '0'),
('217', '182', '0'),
('217', '183', '0'),
('217', '22', '0'),
('217', '23', '0'),
('181', '184', '0'),
('226', '58', '0'),
('226', '40', '0'),
('226', '21', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('226', '185', '0'),
('226', '186', '0'),
('226', '187', '0'),
('226', '22', '0'),
('226', '23', '0'),
('229', '18', '0'),
('229', '4', '0'),
('506', '188', '0'),
('229', '6', '0'),
('229', '189', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('229', '190', '0'),
('233', '18', '0'),
('233', '4', '0'),
('233', '6', '0'),
('233', '191', '0'),
('233', '110', '0'),
('233', '162', '0'),
('233', '163', '0'),
('235', '14', '0'),
('235', '81', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('235', '6', '0'),
('235', '192', '0'),
('235', '62', '0'),
('235', '193', '0'),
('235', '194', '0'),
('238', '14', '0'),
('238', '2', '0'),
('238', '4', '0'),
('238', '50', '0'),
('238', '195', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('238', '55', '0'),
('238', '196', '0'),
('238', '62', '0'),
('238', '22', '0'),
('238', '23', '0'),
('238', '28', '0'),
('240', '17', '0'),
('240', '2', '0'),
('240', '50', '0'),
('240', '141', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('240', '60', '0'),
('240', '22', '0'),
('240', '23', '0'),
('240', '28', '0'),
('243', '14', '0'),
('243', '5', '0'),
('243', '2', '0'),
('243', '197', '0'),
('243', '62', '0'),
('243', '22', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('243', '23', '0'),
('243', '52', '0'),
('247', '17', '0'),
('247', '58', '0'),
('247', '40', '0'),
('247', '198', '0'),
('247', '124', '0'),
('247', '22', '0'),
('247', '23', '0'),
('247', '61', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('249', '199', '0'),
('249', '6', '0'),
('249', '200', '0'),
('249', '201', '0'),
('249', '22', '0'),
('250', '38', '0'),
('250', '2', '0'),
('250', '50', '0'),
('250', '202', '0'),
('250', '69', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('250', '21', '0'),
('250', '203', '0'),
('250', '88', '0'),
('250', '22', '0'),
('250', '23', '0'),
('250', '28', '0'),
('254', '17', '0'),
('254', '5', '0'),
('254', '204', '0'),
('254', '205', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('254', '206', '0'),
('254', '207', '0'),
('254', '208', '0'),
('254', '45', '0'),
('254', '22', '0'),
('254', '23', '0'),
('254', '61', '0'),
('258', '17', '0'),
('114', '5', '0'),
('258', '54', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('258', '209', '0'),
('258', '145', '0'),
('258', '210', '0'),
('258', '110', '0'),
('258', '120', '0'),
('258', '22', '0'),
('258', '23', '0'),
('258', '52', '0'),
('258', '61', '0'),
('264', '17', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('264', '2', '0'),
('264', '5', '0'),
('264', '197', '0'),
('264', '211', '0'),
('264', '125', '0'),
('264', '22', '0'),
('264', '23', '0'),
('264', '28', '0'),
('267', '38', '0'),
('267', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('267', '50', '0'),
('267', '212', '0'),
('267', '69', '0'),
('267', '63', '0'),
('267', '45', '0'),
('267', '22', '0'),
('267', '23', '0'),
('267', '28', '0'),
('271', '14', '0'),
('271', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('271', '5', '0'),
('271', '54', '0'),
('271', '213', '0'),
('271', '210', '0'),
('271', '214', '0'),
('271', '62', '0'),
('271', '22', '0'),
('271', '23', '0'),
('271', '52', '0'),
('274', '17', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('274', '2', '0'),
('274', '21', '0'),
('274', '56', '0'),
('274', '124', '0'),
('274', '215', '0'),
('274', '216', '0'),
('274', '22', '0'),
('274', '23', '0'),
('274', '61', '0'),
('275', '217', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('275', '2', '0'),
('275', '4', '0'),
('275', '21', '0'),
('275', '218', '0'),
('275', '22', '0'),
('275', '23', '0'),
('275', '52', '0'),
('277', '14', '0'),
('277', '5', '0'),
('277', '219', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('277', '220', '0'),
('277', '221', '0'),
('277', '222', '0'),
('277', '68', '0'),
('277', '62', '0'),
('277', '96', '0'),
('277', '22', '0'),
('277', '23', '0'),
('277', '28', '0'),
('281', '11', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('281', '5', '0'),
('281', '97', '0'),
('281', '121', '0'),
('281', '223', '0'),
('281', '224', '0'),
('281', '225', '0'),
('281', '26', '0'),
('281', '22', '0'),
('281', '23', '0'),
('281', '116', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('506', '383', '0'),
('506', '294', '0'),
('506', '318', '0'),
('506', '8', '0'),
('204', '202', '0'),
('506', '382', '0'),
('506', '344', '0'),
('284', '8', '0'),
('284', '226', '0'),
('287', '8', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('287', '227', '0'),
('287', '228', '0'),
('287', '229', '0'),
('287', '230', '0'),
('287', '231', '0'),
('287', '232', '0'),
('288', '233', '0'),
('288', '8', '0'),
('288', '234', '0'),
('288', '235', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('288', '236', '0'),
('288', '237', '0'),
('288', '238', '0'),
('288', '239', '0'),
('288', '240', '0'),
('290', '233', '0'),
('290', '8', '0'),
('290', '241', '0'),
('290', '242', '0'),
('290', '243', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('290', '244', '0'),
('290', '237', '0'),
('290', '245', '0'),
('293', '5', '0'),
('293', '4', '0'),
('293', '2', '0'),
('293', '246', '0'),
('293', '106', '0'),
('293', '125', '0'),
('293', '45', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('293', '247', '0'),
('293', '28', '0'),
('293', '22', '0'),
('293', '23', '0'),
('296', '17', '0'),
('296', '110', '0'),
('296', '6', '0'),
('300', '8', '0'),
('300', '249', '0'),
('300', '250', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('300', '251', '0'),
('300', '252', '0'),
('300', '253', '0'),
('300', '254', '0'),
('300', '255', '0'),
('300', '256', '0'),
('300', '257', '0'),
('300', '258', '0'),
('300', '259', '0'),
('300', '260', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('301', '8', '0'),
('301', '261', '0'),
('301', '262', '0'),
('301', '263', '0'),
('301', '264', '0'),
('301', '265', '0'),
('301', '266', '0'),
('301', '267', '0'),
('301', '268', '0'),
('301', '269', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('301', '270', '0'),
('301', '271', '0'),
('301', '272', '0'),
('301', '273', '0'),
('301', '274', '0'),
('301', '275', '0'),
('301', '276', '0'),
('303', '8', '0'),
('303', '277', '0'),
('303', '278', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('303', '279', '0'),
('303', '280', '0'),
('303', '281', '0'),
('303', '282', '0'),
('303', '283', '0'),
('303', '284', '0'),
('303', '285', '0'),
('303', '286', '0'),
('303', '287', '0'),
('305', '8', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('305', '288', '0'),
('305', '289', '0'),
('305', '290', '0'),
('305', '291', '0'),
('305', '292', '0'),
('305', '293', '0'),
('305', '294', '0'),
('305', '295', '0'),
('305', '296', '0'),
('305', '297', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('305', '260', '0'),
('308', '8', '0'),
('308', '298', '0'),
('308', '299', '0'),
('308', '300', '0'),
('308', '301', '0'),
('308', '302', '0'),
('308', '303', '0'),
('308', '282', '0'),
('308', '295', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('308', '238', '0'),
('308', '304', '0'),
('308', '283', '0'),
('309', '8', '0'),
('309', '305', '0'),
('309', '306', '0'),
('309', '307', '0'),
('309', '308', '0'),
('309', '309', '0'),
('309', '310', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('309', '311', '0'),
('309', '312', '0'),
('309', '313', '0'),
('309', '231', '0'),
('309', '314', '0'),
('309', '295', '0'),
('309', '238', '0'),
('310', '8', '0'),
('310', '298', '0'),
('310', '315', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('310', '316', '0'),
('310', '295', '0'),
('310', '317', '0'),
('310', '318', '0'),
('310', '319', '0'),
('310', '282', '0'),
('310', '320', '0'),
('310', '321', '0'),
('310', '322', '0'),
('313', '8', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('310', '324', '0'),
('310', '325', '0'),
('310', '260', '0'),
('310', '326', '0'),
('313', '327', '0'),
('313', '328', '0'),
('313', '234', '0'),
('313', '329', '0'),
('313', '330', '0'),
('313', '331', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('313', '332', '0'),
('313', '295', '0'),
('313', '238', '0'),
('313', '333', '0'),
('313', '334', '0'),
('313', '335', '0'),
('314', '8', '0'),
('314', '336', '0'),
('314', '337', '0'),
('314', '338', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('314', '339', '0'),
('314', '340', '0'),
('314', '341', '0'),
('314', '342', '0'),
('314', '343', '0'),
('316', '8', '0'),
('316', '344', '0'),
('316', '345', '0'),
('316', '346', '0'),
('316', '347', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('316', '231', '0'),
('316', '348', '0'),
('316', '349', '0'),
('316', '350', '0'),
('316', '351', '0'),
('316', '352', '0'),
('316', '260', '0'),
('317', '8', '0'),
('317', '305', '0'),
('317', '353', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('317', '354', '0'),
('317', '355', '0'),
('317', '356', '0'),
('317', '357', '0'),
('317', '358', '0'),
('317', '359', '0'),
('317', '360', '0'),
('317', '361', '0'),
('317', '362', '0'),
('318', '8', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('318', '363', '0'),
('318', '337', '0'),
('318', '364', '0'),
('318', '283', '0'),
('318', '365', '0'),
('318', '282', '0'),
('318', '366', '0'),
('318', '367', '0'),
('318', '368', '0'),
('318', '334', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('318', '260', '0'),
('319', '8', '0'),
('319', '369', '0'),
('319', '242', '0'),
('319', '370', '0'),
('319', '371', '0'),
('319', '372', '0'),
('319', '373', '0'),
('319', '313', '0'),
('319', '318', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('319', '374', '0'),
('319', '375', '0'),
('319', '282', '0'),
('319', '376', '0'),
('319', '377', '0'),
('319', '378', '0'),
('319', '295', '0'),
('319', '379', '0'),
('326', '17', '0'),
('326', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('326', '21', '0'),
('326', '50', '0'),
('326', '60', '0'),
('326', '22', '0'),
('326', '23', '0'),
('326', '28', '0'),
('333', '12', '0'),
('333', '2', '0'),
('333', '5', '0'),
('333', '112', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('333', '380', '0'),
('333', '55', '0'),
('333', '34', '0'),
('333', '381', '0'),
('333', '22', '0'),
('333', '23', '0'),
('506', '384', '0'),
('506', '282', '0'),
('506', '385', '0'),
('506', '386', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('506', '387', '0'),
('506', '388', '0'),
('293', '389', '0'),
('263', '389', '0'),
('264', '389', '0'),
('326', '389', '0'),
('267', '389', '0'),
('260', '389', '0'),
('566', '17', '0'),
('566', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('566', '4', '0'),
('566', '60', '0'),
('567', '17', '0'),
('567', '2', '0'),
('567', '50', '0'),
('567', '390', '0'),
('567', '141', '0'),
('567', '391', '0'),
('567', '22', '0'),
('567', '23', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('569', '17', '0'),
('569', '2', '0'),
('569', '4', '0'),
('569', '60', '0'),
('582', '17', '0'),
('582', '2', '0'),
('586', '233', '0'),
('586', '4', '0'),
('586', '125', '0'),
('305', '188', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('319', '188', '0'),
('318', '188', '0'),
('317', '188', '0'),
('316', '188', '0'),
('173', '2', '0'),
('173', '4', '0'),
('258', '5', '0'),
('260', '38', '0'),
('260', '5', '0'),
('308', '188', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('313', '188', '0'),
('310', '188', '0'),
('250', '398', '0'),
('89', '389', '0'),
('147', '97', '0'),
('135', '188', '0'),
('135', '140', '0'),
('300', '188', '0'),
('288', '188', '0'),
('303', '188', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('100', '58', '0'),
('126', '5', '0'),
('143', '4', '0'),
('151', '399', '0'),
('290', '188', '0'),
('284', '188', '0'),
('758', '16', '0'),
('758', '219', '0'),
('758', '5', '0'),
('758', '2', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('758', '4', '0'),
('758', '28', '0'),
('758', '400', '0'),
('758', '401', '0'),
('758', '402', '0'),
('779', '14', '0'),
('779', '2', '0'),
('779', '40', '0'),
('779', '403', '0'),
('779', '404', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('779', '58', '0'),
('804', '18', '0'),
('804', '2', '0'),
('804', '4', '0'),
('809', '17', '0'),
('809', '58', '0'),
('809', '4', '0'),
('809', '140', '0'),
('809', '405', '0'),
('809', '141', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('809', '60', '0'),
('809', '22', '0'),
('809', '23', '0'),
('809', '61', '0'),
('827', '8', '0'),
('827', '406', '0'),
('827', '407', '0'),
('827', '334', '0'),
('827', '408', '0'),
('827', '409', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('827', '410', '0'),
('827', '411', '0'),
('827', '282', '0'),
('827', '237', '0'),
('827', '412', '0'),
('827', '413', '0'),
('827', '260', '0'),
('113', '11', '0'),
('831', '8', '0'),
('831', '414', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('831', '415', '0'),
('831', '416', '0'),
('831', '334', '0'),
('831', '417', '0'),
('831', '418', '0'),
('831', '419', '0'),
('831', '420', '0'),
('831', '421', '0'),
('831', '295', '0'),
('831', '422', '0');

INSERT INTO `wp_2_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
('831', '423', '0');

--
-- Table structure for table `wp_2_term_taxonomy`
--

CREATE TABLE `wp_2_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=424 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_2_term_taxonomy`
--

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('1', '1', 'category', '', '0', '0'),
('2', '2', 'propertytype', '', '0', '43'),
('4', '4', 'propertytype', '', '0', '28'),
('5', '5', 'propertytype', '', '0', '25'),
('6', '6', 'propertytype', '', '0', '10'),
('8', '8', 'propertytype', '', '0', '21'),
('9', '9', 'location', '', '0', '1'),
('10', '10', 'location', '', '0', '0'),
('11', '11', 'location', '', '0', '5'),
('12', '12', 'location', '', '0', '2');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('13', '13', 'location', '', '0', '2'),
('14', '14', 'location', '', '0', '20'),
('15', '15', 'location', '', '0', '1'),
('16', '16', 'location', '', '0', '2'),
('17', '17', 'location', '', '0', '24'),
('18', '18', 'location', '', '0', '6'),
('19', '19', 'nav_menu', '', '0', '7'),
('20', '20', 'nav_menu', '', '0', '1'),
('21', '21', 'propertyfeatures', '', '0', '13'),
('22', '22', 'propertyfeatures', '', '0', '56');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('23', '23', 'propertyfeatures', '', '0', '52'),
('24', '24', 'propertyfeatures', '', '0', '1'),
('25', '25', 'propertyfeatures', '', '0', '0'),
('26', '26', 'propertyfeatures', '', '0', '4'),
('27', '27', 'propertyfeatures', '', '0', '1'),
('28', '28', 'propertyfeatures', '', '0', '19'),
('29', '29', 'propertyfeatures', '', '0', '1'),
('30', '30', 'propertyfeatures', '', '0', '1'),
('31', '31', 'propertyfeatures', '', '0', '1'),
('32', '32', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('33', '33', 'propertyfeatures', '', '0', '2'),
('34', '34', 'propertyfeatures', '', '0', '3'),
('35', '35', 'propertyfeatures', '', '0', '1'),
('36', '36', 'post_tag', '', '0', '0'),
('37', '37', 'post_tag', '', '0', '0'),
('38', '38', 'location', '', '0', '8'),
('39', '39', 'propertyfeatures', '', '0', '3'),
('40', '40', 'propertyfeatures', '', '0', '5'),
('41', '41', 'propertyfeatures', '', '0', '1'),
('42', '42', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('43', '43', 'propertyfeatures', '', '0', '3'),
('44', '44', 'propertyfeatures', '', '0', '0'),
('45', '45', 'propertyfeatures', '', '0', '3'),
('46', '46', 'propertyfeatures', '', '0', '1'),
('47', '47', 'propertyfeatures', '', '0', '1'),
('48', '48', 'propertyfeatures', '', '0', '1'),
('49', '49', 'propertyfeatures', '', '0', '2'),
('50', '50', 'propertyfeatures', '', '0', '11'),
('51', '51', 'propertyfeatures', '', '0', '4'),
('52', '52', 'propertyfeatures', '', '0', '7');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('53', '53', 'propertyfeatures', '', '0', '0'),
('54', '54', 'propertyfeatures', '', '0', '3'),
('55', '55', 'propertyfeatures', '', '0', '5'),
('56', '56', 'propertyfeatures', '', '0', '2'),
('57', '57', 'propertyfeatures', '', '0', '2'),
('58', '58', 'propertytype', '', '0', '10'),
('59', '59', 'propertyfeatures', '', '0', '2'),
('60', '60', 'propertyfeatures', '', '0', '9'),
('61', '61', 'propertyfeatures', '', '0', '13'),
('62', '62', 'propertyfeatures', '', '0', '9');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('63', '63', 'propertyfeatures', '', '0', '2'),
('64', '64', 'propertyfeatures', '', '0', '1'),
('65', '65', 'propertyfeatures', '', '0', '1'),
('66', '66', 'propertyfeatures', '', '0', '1'),
('67', '67', 'propertyfeatures', '', '0', '1'),
('68', '68', 'propertyfeatures', '', '0', '2'),
('69', '69', 'propertyfeatures', '', '0', '3'),
('70', '70', 'propertyfeatures', '', '0', '2'),
('71', '71', 'propertyfeatures', '', '0', '1'),
('72', '72', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('73', '73', 'propertyfeatures', '', '0', '1'),
('74', '74', 'propertyfeatures', '', '0', '1'),
('75', '75', 'propertyfeatures', '', '0', '1'),
('76', '76', 'propertyfeatures', '', '0', '1'),
('77', '77', 'propertyfeatures', '', '0', '2'),
('78', '78', 'propertyfeatures', '', '0', '4'),
('79', '79', 'propertyfeatures', '', '0', '1'),
('80', '80', 'location', '', '0', '1'),
('81', '81', 'propertytype', '', '0', '3'),
('82', '82', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('83', '83', 'propertyfeatures', '', '0', '0'),
('84', '84', 'propertyfeatures', '', '0', '1'),
('85', '85', 'propertyfeatures', '', '0', '1'),
('86', '86', 'propertyfeatures', '', '0', '1'),
('87', '87', 'propertyfeatures', '', '0', '1'),
('88', '88', 'propertyfeatures', '', '0', '5'),
('89', '89', 'propertyfeatures', '', '0', '1'),
('90', '90', 'location', '', '0', '1'),
('392', '387', 'propertyfeatures', '', '0', '1'),
('395', '145', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('95', '94', 'propertyfeatures', '', '0', '1'),
('96', '95', 'propertyfeatures', '', '0', '5'),
('97', '96', 'propertytype', '', '0', '4'),
('98', '91', 'propertyfeatures', '', '0', '1'),
('100', '98', 'propertyfeatures', '', '0', '1'),
('101', '99', 'propertyfeatures', '', '0', '1'),
('102', '100', 'propertyfeatures', '', '0', '1'),
('103', '101', 'propertyfeatures', '', '0', '1'),
('104', '102', 'propertyfeatures', '', '0', '1'),
('105', '103', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('106', '104', 'propertyfeatures', '', '0', '1'),
('107', '105', 'location', '', '0', '1'),
('108', '106', 'propertyfeatures', '', '0', '1'),
('109', '107', 'propertyfeatures', '', '0', '1'),
('110', '108', 'propertyfeatures', '', '0', '4'),
('111', '109', 'location', '', '0', '1'),
('112', '110', 'propertyfeatures', '', '0', '2'),
('113', '111', 'propertyfeatures', '', '0', '1'),
('114', '112', 'propertyfeatures', '', '0', '1'),
('115', '113', 'propertyfeatures', '', '0', '3');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('116', '114', 'propertyfeatures', '', '0', '2'),
('117', '115', 'propertyfeatures', '', '0', '1'),
('118', '116', 'propertyfeatures', '', '0', '1'),
('119', '117', 'propertyfeatures', '', '0', '1'),
('120', '118', 'propertyfeatures', '', '0', '1'),
('121', '119', 'propertyfeatures', '', '0', '2'),
('122', '120', 'propertyfeatures', '', '0', '1'),
('123', '121', 'propertyfeatures', '', '0', '1'),
('124', '122', 'propertyfeatures', '', '0', '3'),
('125', '123', 'propertyfeatures', '', '0', '3');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('126', '124', 'propertyfeatures', '', '0', '0'),
('127', '125', 'propertyfeatures', '', '0', '1'),
('128', '126', 'propertyfeatures', '', '0', '1'),
('129', '127', 'location', '', '0', '1'),
('130', '128', 'propertyfeatures', '', '0', '1'),
('131', '129', 'propertyfeatures', '', '0', '1'),
('132', '130', 'location', '', '0', '2'),
('396', '146', 'propertyfeatures', '', '0', '1'),
('397', '390', 'nav_menu', '', '0', '0'),
('139', '135', 'propertytype', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('140', '136', 'propertytype', '', '0', '3'),
('141', '137', 'propertyfeatures', '', '0', '4'),
('142', '138', 'propertyfeatures', '', '0', '1'),
('143', '139', 'propertyfeatures', '', '0', '1'),
('144', '140', 'propertyfeatures', '', '0', '1'),
('145', '141', 'propertyfeatures', '', '0', '1'),
('146', '142', 'propertyfeatures', '', '0', '1'),
('147', '143', 'location', '', '0', '1'),
('152', '147', 'propertyfeatures', '', '0', '1'),
('153', '148', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('154', '149', 'propertyfeatures', '', '0', '0'),
('155', '150', 'propertyfeatures', '', '0', '1'),
('156', '151', 'propertyfeatures', '', '0', '1'),
('185', '180', 'propertyfeatures', '', '0', '1'),
('157', '152', 'propertyfeatures', '', '0', '1'),
('158', '153', 'propertyfeatures', '', '0', '1'),
('159', '154', 'propertyfeatures', '', '0', '1'),
('160', '155', 'propertyfeatures', '', '0', '1'),
('161', '156', 'propertyfeatures', '', '0', '1'),
('162', '157', 'propertyfeatures', '', '0', '2');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('163', '158', 'propertyfeatures', '', '0', '2'),
('164', '159', 'propertyfeatures', '', '0', '1'),
('165', '160', 'propertyfeatures', '', '0', '1'),
('166', '161', 'propertyfeatures', '', '0', '1'),
('167', '162', 'propertyfeatures', '', '0', '1'),
('168', '163', 'propertyfeatures', '', '0', '1'),
('169', '164', 'propertyfeatures', '', '0', '1'),
('170', '165', 'propertyfeatures', '', '0', '1'),
('171', '166', 'propertyfeatures', '', '0', '1'),
('173', '168', 'propertyfeatures', '', '0', '0');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('174', '169', 'propertyfeatures', '', '0', '1'),
('175', '170', 'propertyfeatures', '', '0', '1'),
('176', '171', 'propertyfeatures', '', '0', '1'),
('177', '172', 'propertyfeatures', '', '0', '1'),
('178', '173', 'propertyfeatures', '', '0', '1'),
('179', '174', 'propertyfeatures', '', '0', '1'),
('180', '175', 'propertyfeatures', '', '0', '1'),
('181', '176', 'propertyfeatures', '', '0', '1'),
('182', '177', 'propertyfeatures', '', '0', '1'),
('183', '178', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('184', '179', 'propertyfeatures', '', '0', '1'),
('186', '181', 'propertyfeatures', '', '0', '1'),
('187', '182', 'propertyfeatures', '', '0', '1'),
('188', '183', 'propertytype', '', '0', '16'),
('189', '184', 'propertyfeatures', '', '0', '1'),
('190', '185', 'propertyfeatures', '', '0', '1'),
('191', '186', 'propertyfeatures', '', '0', '1'),
('192', '187', 'propertyfeatures', '', '0', '1'),
('193', '188', 'propertyfeatures', '', '0', '1'),
('194', '189', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('195', '190', 'propertyfeatures', '', '0', '1'),
('196', '191', 'propertyfeatures', '', '0', '1'),
('197', '192', 'propertyfeatures', '', '0', '2'),
('198', '193', 'propertyfeatures', '', '0', '0'),
('199', '194', 'location', '', '0', '1'),
('200', '195', 'propertyfeatures', '', '0', '1'),
('201', '196', 'propertyfeatures', '', '0', '1'),
('202', '197', 'propertyfeatures', '', '0', '2'),
('203', '198', 'propertyfeatures', '', '0', '1'),
('204', '199', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('205', '200', 'propertyfeatures', '', '0', '1'),
('206', '201', 'propertyfeatures', '', '0', '1'),
('207', '202', 'propertyfeatures', '', '0', '1'),
('208', '203', 'propertyfeatures', '', '0', '1'),
('209', '204', 'propertyfeatures', '', '0', '0'),
('210', '205', 'propertyfeatures', '', '0', '1'),
('211', '206', 'propertyfeatures', '', '0', '1'),
('212', '207', 'propertyfeatures', '', '0', '1'),
('213', '208', 'propertyfeatures', '', '0', '1'),
('214', '209', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('215', '210', 'propertyfeatures', '', '0', '1'),
('216', '211', 'propertyfeatures', '', '0', '1'),
('217', '212', 'location', '', '0', '1'),
('218', '213', 'propertyfeatures', '', '0', '1'),
('219', '214', 'propertytype', '', '0', '2'),
('220', '215', 'propertyfeatures', '', '0', '1'),
('221', '216', 'propertyfeatures', '', '0', '1'),
('222', '217', 'propertyfeatures', '', '0', '1'),
('223', '218', 'propertyfeatures', '', '0', '1'),
('224', '219', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('225', '220', 'propertyfeatures', '', '0', '1'),
('226', '221', 'propertyfeatures', '', '0', '1'),
('227', '222', 'propertyfeatures', '', '0', '1'),
('228', '223', 'propertyfeatures', '', '0', '1'),
('229', '224', 'propertyfeatures', '', '0', '1'),
('230', '225', 'propertyfeatures', '', '0', '1'),
('231', '226', 'propertyfeatures', '', '0', '3'),
('232', '227', 'propertyfeatures', '', '0', '1'),
('233', '228', 'location', '', '0', '3'),
('234', '229', 'propertyfeatures', '', '0', '2');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('235', '230', 'propertyfeatures', '', '0', '1'),
('236', '231', 'propertyfeatures', '', '0', '1'),
('237', '232', 'propertyfeatures', '', '0', '3'),
('238', '233', 'propertyfeatures', '', '0', '4'),
('239', '234', 'propertyfeatures', '', '0', '1'),
('240', '235', 'propertyfeatures', '', '0', '1'),
('241', '236', 'propertyfeatures', '', '0', '1'),
('242', '237', 'propertyfeatures', '', '0', '2'),
('243', '238', 'propertyfeatures', '', '0', '1'),
('244', '239', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('245', '240', 'propertyfeatures', '', '0', '1'),
('246', '241', 'propertyfeatures', '', '0', '0'),
('247', '242', 'propertyfeatures', '', '0', '0'),
('249', '244', 'propertyfeatures', '', '0', '1'),
('250', '245', 'propertyfeatures', '', '0', '1'),
('251', '246', 'propertyfeatures', '', '0', '1'),
('252', '247', 'propertyfeatures', '', '0', '1'),
('253', '248', 'propertyfeatures', '', '0', '1'),
('254', '249', 'propertyfeatures', '', '0', '1'),
('255', '250', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('256', '251', 'propertyfeatures', '', '0', '1'),
('257', '252', 'propertyfeatures', '', '0', '1'),
('258', '253', 'propertyfeatures', '', '0', '1'),
('259', '254', 'propertyfeatures', '', '0', '1'),
('260', '255', 'propertyfeatures', '', '0', '6'),
('261', '256', 'propertyfeatures', '', '0', '1'),
('262', '257', 'propertyfeatures', '', '0', '1'),
('263', '258', 'propertyfeatures', '', '0', '1'),
('264', '259', 'propertyfeatures', '', '0', '1'),
('265', '260', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('266', '261', 'propertyfeatures', '', '0', '1'),
('267', '262', 'propertyfeatures', '', '0', '1'),
('268', '263', 'propertyfeatures', '', '0', '1'),
('269', '264', 'propertyfeatures', '', '0', '1'),
('270', '265', 'propertyfeatures', '', '0', '1'),
('271', '266', 'propertyfeatures', '', '0', '1'),
('272', '267', 'propertyfeatures', '', '0', '1'),
('273', '268', 'propertyfeatures', '', '0', '1'),
('274', '269', 'propertyfeatures', '', '0', '1'),
('275', '270', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('276', '271', 'propertyfeatures', '', '0', '1'),
('277', '272', 'propertyfeatures', '', '0', '1'),
('278', '273', 'propertyfeatures', '', '0', '1'),
('279', '274', 'propertyfeatures', '', '0', '1'),
('280', '275', 'propertyfeatures', '', '0', '1'),
('281', '276', 'propertyfeatures', '', '0', '1'),
('282', '277', 'propertyfeatures', '', '0', '7'),
('283', '278', 'propertyfeatures', '', '0', '3'),
('284', '279', 'propertyfeatures', '', '0', '1'),
('285', '280', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('286', '281', 'propertyfeatures', '', '0', '1'),
('287', '282', 'propertyfeatures', '', '0', '1'),
('288', '283', 'propertyfeatures', '', '0', '1'),
('289', '284', 'propertyfeatures', '', '0', '1'),
('290', '285', 'propertyfeatures', '', '0', '1'),
('291', '286', 'propertyfeatures', '', '0', '1'),
('292', '287', 'propertyfeatures', '', '0', '1'),
('293', '288', 'propertyfeatures', '', '0', '1'),
('294', '289', 'propertyfeatures', '', '0', '2'),
('295', '290', 'propertyfeatures', '', '0', '7');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('296', '291', 'propertyfeatures', '', '0', '1'),
('297', '292', 'propertyfeatures', '', '0', '1'),
('298', '293', 'propertyfeatures', '', '0', '2'),
('299', '294', 'propertyfeatures', '', '0', '1'),
('300', '295', 'propertyfeatures', '', '0', '1'),
('301', '296', 'propertyfeatures', '', '0', '1'),
('302', '297', 'propertyfeatures', '', '0', '1'),
('303', '298', 'propertyfeatures', '', '0', '1'),
('304', '299', 'propertyfeatures', '', '0', '1'),
('305', '300', 'propertyfeatures', '', '0', '2');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('306', '301', 'propertyfeatures', '', '0', '1'),
('307', '302', 'propertyfeatures', '', '0', '1'),
('308', '303', 'propertyfeatures', '', '0', '1'),
('309', '304', 'propertyfeatures', '', '0', '1'),
('310', '305', 'propertyfeatures', '', '0', '1'),
('311', '306', 'propertyfeatures', '', '0', '1'),
('312', '307', 'propertyfeatures', '', '0', '1'),
('313', '308', 'propertyfeatures', '', '0', '2'),
('314', '309', 'propertyfeatures', '', '0', '1'),
('315', '310', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('316', '311', 'propertyfeatures', '', '0', '1'),
('317', '312', 'propertyfeatures', '', '0', '1'),
('318', '313', 'propertyfeatures', '', '0', '3'),
('319', '314', 'propertyfeatures', '', '0', '1'),
('320', '315', 'propertyfeatures', '', '0', '1'),
('321', '316', 'propertyfeatures', '', '0', '1'),
('322', '317', 'propertyfeatures', '', '0', '1'),
('323', '318', 'propertyfeatures', '', '0', '0'),
('324', '319', 'propertyfeatures', '', '0', '1'),
('325', '320', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('327', '322', 'propertyfeatures', '', '0', '1'),
('326', '321', 'propertyfeatures', '', '0', '1'),
('328', '323', 'propertyfeatures', '', '0', '1'),
('329', '324', 'propertyfeatures', '', '0', '1'),
('330', '325', 'propertyfeatures', '', '0', '1'),
('331', '326', 'propertyfeatures', '', '0', '1'),
('332', '327', 'propertyfeatures', '', '0', '1'),
('333', '328', 'propertyfeatures', '', '0', '1'),
('334', '329', 'propertyfeatures', '', '0', '4'),
('335', '330', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('336', '331', 'propertyfeatures', '', '0', '1'),
('337', '332', 'propertyfeatures', '', '0', '2'),
('338', '333', 'propertyfeatures', '', '0', '1'),
('339', '334', 'propertyfeatures', '', '0', '1'),
('340', '335', 'propertyfeatures', '', '0', '1'),
('341', '336', 'propertyfeatures', '', '0', '1'),
('342', '337', 'propertyfeatures', '', '0', '1'),
('343', '338', 'propertyfeatures', '', '0', '1'),
('344', '339', 'propertyfeatures', '', '0', '2'),
('345', '340', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('346', '341', 'propertyfeatures', '', '0', '1'),
('347', '342', 'propertyfeatures', '', '0', '1'),
('348', '343', 'propertyfeatures', '', '0', '1'),
('349', '344', 'propertyfeatures', '', '0', '1'),
('350', '345', 'propertyfeatures', '', '0', '1'),
('351', '346', 'propertyfeatures', '', '0', '1'),
('352', '347', 'propertyfeatures', '', '0', '1'),
('353', '348', 'propertyfeatures', '', '0', '1'),
('354', '349', 'propertyfeatures', '', '0', '1'),
('355', '350', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('356', '351', 'propertyfeatures', '', '0', '1'),
('357', '352', 'propertyfeatures', '', '0', '1'),
('358', '353', 'propertyfeatures', '', '0', '1'),
('359', '354', 'propertyfeatures', '', '0', '1'),
('360', '355', 'propertyfeatures', '', '0', '1'),
('361', '356', 'propertyfeatures', '', '0', '1'),
('362', '357', 'propertyfeatures', '', '0', '1'),
('363', '358', 'propertyfeatures', '', '0', '1'),
('364', '359', 'propertyfeatures', '', '0', '1'),
('365', '360', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('366', '361', 'propertyfeatures', '', '0', '1'),
('367', '362', 'propertyfeatures', '', '0', '1'),
('368', '363', 'propertyfeatures', '', '0', '1'),
('369', '364', 'propertyfeatures', '', '0', '1'),
('370', '365', 'propertyfeatures', '', '0', '1'),
('371', '366', 'propertyfeatures', '', '0', '1'),
('372', '367', 'propertyfeatures', '', '0', '1'),
('373', '368', 'propertyfeatures', '', '0', '1'),
('374', '369', 'propertyfeatures', '', '0', '1'),
('375', '370', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('376', '371', 'propertyfeatures', '', '0', '1'),
('377', '372', 'propertyfeatures', '', '0', '1'),
('378', '373', 'propertyfeatures', '', '0', '1'),
('379', '374', 'propertyfeatures', '', '0', '1'),
('380', '375', 'propertyfeatures', '', '0', '1'),
('381', '376', 'propertyfeatures', '', '0', '1'),
('382', '377', 'propertyfeatures', '', '0', '1'),
('383', '378', 'propertyfeatures', '', '0', '1'),
('384', '379', 'propertyfeatures', '', '0', '1'),
('385', '380', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('386', '381', 'propertyfeatures', '', '0', '1'),
('387', '382', 'propertyfeatures', '', '0', '1'),
('388', '383', 'propertyfeatures', '', '0', '1'),
('389', '384', 'post_tag', '', '0', '6'),
('390', '385', 'propertyfeatures', '', '0', '1'),
('391', '386', 'propertyfeatures', '', '0', '1'),
('393', '388', 'propertyfeatures', '', '0', '1'),
('394', '389', 'propertyfeatures', '', '0', '1'),
('398', '391', 'propertytype', '', '0', '1'),
('399', '392', 'propertytype', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('400', '393', 'propertyfeatures', '', '0', '1'),
('401', '394', 'propertyfeatures', '', '0', '1'),
('402', '395', 'propertyfeatures', '', '0', '1'),
('403', '396', 'propertyfeatures', '', '0', '1'),
('404', '397', 'propertyfeatures', '', '0', '1'),
('405', '398', 'propertyfeatures', '', '0', '1'),
('406', '399', 'propertyfeatures', '', '0', '1'),
('407', '400', 'propertyfeatures', '', '0', '1'),
('408', '401', 'propertyfeatures', '', '0', '1'),
('409', '402', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('410', '403', 'propertyfeatures', '', '0', '1'),
('411', '404', 'propertyfeatures', '', '0', '1'),
('412', '405', 'propertyfeatures', '', '0', '1'),
('413', '406', 'propertyfeatures', '', '0', '1'),
('414', '407', 'propertyfeatures', '', '0', '1'),
('415', '408', 'propertyfeatures', '', '0', '1'),
('416', '409', 'propertyfeatures', '', '0', '1'),
('417', '410', 'propertyfeatures', '', '0', '1'),
('418', '411', 'propertyfeatures', '', '0', '1'),
('419', '412', 'propertyfeatures', '', '0', '1');

INSERT INTO `wp_2_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
('420', '413', 'propertyfeatures', '', '0', '1'),
('421', '414', 'propertyfeatures', '', '0', '1'),
('422', '415', 'propertyfeatures', '', '0', '1'),
('423', '416', 'propertyfeatures', '', '0', '1');

--
-- Table structure for table `wp_2_terms`
--

CREATE TABLE `wp_2_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_2_terms`
--

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('1', 'Uncategorized', 'uncategorized', '0'),
('2', 'OFFICE', 'office-space-new-haven-ct', '0'),
('4', 'RETAIL', 'retail-building-lease-milford-ct', '0'),
('5', 'INDUSTRIAL', 'industrial-building-lease-ct', '0'),
('6', 'LAND', 'land-sale-ct', '0'),
('8', 'BUSINESS', 'businesses-for-sale-ct', '0'),
('9', 'WALLINGFORD', 'wallingford-ct-lease-sale', '0'),
('10', 'ORANGE', 'orange-ct-lease-sale', '0'),
('11', 'WEST HAVEN', 'west-haven-ct-lease-sale', '0'),
('12', 'BRANFORD', 'branford-retail-ct-lease-sale', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('13', 'CHESHIRE', 'cheshire-ct-lease-sale', '0'),
('14', 'HAMDEN', 'hamden-ct-lease-sale', '0'),
('15', 'MERIDEN', 'meriden-ct-lease-sale', '0'),
('16', 'MILFORD', 'milford-retail-ct-lease-rent', '0'),
('17', 'NEW HAVEN', 'new-haven-ct-lease-sale', '0'),
('18', 'NORTH HAVEN', 'north-haven-ct-lease-sale', '0'),
('19', 'Top Nav Properties', 'top-nav-properties', '0'),
('20', 'Super Top Nav', 'super-top-nav', '0'),
('21', 'Central Air', 'central-air', '0'),
('22', 'Public Water', 'public-water', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('23', 'Public Sewer', 'public-sewer', '0'),
('24', 'Heat: Base', 'heat-base', '0'),
('25', 'Wair', 'wair', '0'),
('26', '800 Amp Electric', '800-amp-electric', '0'),
('27', 'Zone Code: RM-1', 'zone-code-rm-1', '0'),
('28', 'Fuel: Gas', 'fuel-gas', '0'),
('29', 'Zone Code: RM-2', 'zone-code-rm-2', '0'),
('30', 'Heat: Wair', 'heat-wair', '0'),
('31', 'Zone Code: WD', 'zone-code-wd', '0'),
('32', 'Level Site', 'level-site', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('33', '12 Foot Ceilings', '12-foot-ceilings', '0'),
('34', '1 Drive-In Door', '1-drive-in-door', '0'),
('35', '1 Dock', '1-dock', '0'),
('36', 'OFFICE', 'office', '0'),
('37', 'RETAIL', 'retail', '0'),
('38', 'Woodbridge', 'woodbridge', '0'),
('39', 'SEPTIC', 'septic', '0'),
('40', '9 FOOT CEILINGS', '9-foot-ceilings', '0'),
('41', 'ZONING--DEV 2', 'zoning-dev-2', '0'),
('42', 'Clear Span Column Spacing', 'clear-span-column-spacing', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('43', 'Wet Sprinkler', 'wet-sprinkler', '0'),
('44', '200', '200', '0'),
('45', '400 Amp Electric', '400-amp-electric', '0'),
('46', '16.11 Acres', '16-11-acres', '0'),
('47', 'Lease or Sale', 'lease-or-sale', '0'),
('48', 'Easy on/off I-95', 'easy-onoff-i-95', '0'),
('49', '100 AMP ELECTRIC SERVICE', '100-amp-electric-service', '0'),
('50', '8 FOOT CEILINGS', '8-foot-ceilings', '0'),
('51', '25 PARKING SPACES', '25-parking-spaces', '0'),
('52', 'FUEL : OIL', 'fuel-oil', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('53', '$8.00 SF PLUS UTILITIES', '8-00-sf-plus-utilities', '0'),
('54', '14 FOOT CEILINGS', '14-foot-ceilings', '0'),
('55', '20 PARKING SPACES', '20-parking-spaces', '0'),
('56', 'BUILT IN 1955', 'built-in-1955', '0'),
('57', 'ZONING: RM-1', 'zoning-rm-1', '0'),
('58', 'INVESTMENT', 'investment', '0'),
('59', 'BUILT IN 1930', 'built-in-1930', '0'),
('60', 'ZONING: BA', 'zoning-ba', '0'),
('61', 'GAS AT LOCATION', 'gas-at-location', '0'),
('62', 'ZONING: CDD1', 'zoning-cdd1', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('63', 'ZONING: GB', 'zoning-gb', '0'),
('64', '1.97 ACRES', '1-97-acres', '0'),
('65', 'ZONING: IG1', 'zoning-ig1', '0'),
('66', '300 SF OFFICE', '300-sf-office', '0'),
('67', '4 PARKING SPACES', '4-parking-spaces', '0'),
('68', '2 DRIVE-IN DOORS', '2-drive-in-doors', '0'),
('69', '50 PARKING SPACES', '50-parking-spaces', '0'),
('70', '18 FOOT CEILINGS', '18-foot-ceilings', '0'),
('71', 'ZONING: M-1', 'zoning-m-1', '0'),
('72', '90 PARKING PLACES', '90-parking-places', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('73', 'ZONING: IL30', 'zoning-il30', '0'),
('74', '8-30 HIGH CEILINGS', '8-30-high-ceilings', '0'),
('75', '6 DRIVE-IN DOORS', '6-drive-in-doors', '0'),
('76', 'ZONING ID', 'zoning-id', '0'),
('77', '10 FOOT HIGH CEILINGS', '10-foot-high-ceilings', '0'),
('78', '40 PARKING SPACES', '40-parking-spaces', '0'),
('79', 'ZONING: B-1', 'zoning-b-1', '0'),
('80', 'WATERBURY', 'waterbury', '0'),
('81', 'MULTI-FAMILY', 'multi-family', '0'),
('82', 'ZONING: RL', 'zoning-rl', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('83', '200-400 AMP', '200-400-amp', '0'),
('84', 'UP TO 400 AMP ELECTRIC', 'up-to-400-amp-electric', '0'),
('85', '22 FOOT CEILINGS', '22-foot-ceilings', '0'),
('86', '90 PARKING SPACES', '90-parking-spaces', '0'),
('87', 'ZONING: M1', 'zoning-m1', '0'),
('88', '200 AMP ELECTRIC', '200-amp-electric', '0'),
('89', '15 FOOT CEILINGS', '15-foot-ceilings', '0'),
('90', 'PLAINVILLE', 'plainville', '0'),
('91', 'ZONING: RB', 'zoning-rb', '0'),
('94', 'ZONING: CC', 'zoning-cc', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('95', '100 AMP ELECTRIC', '100-amp-electric', '0'),
('96', 'WAREHOUSE/DISTRIBUTION', 'warehousedistribution', '0'),
('98', '8-20 FOOT HIGH CEILINGS', '8-20-foot-high-ceilings', '0'),
('99', 'ZONING: LM', 'zoning-lm', '0'),
('100', '30 PARKING SPACE', '30-parking-space', '0'),
('101', '14 ACRES', '14-acres', '0'),
('102', 'ZONING: I2', 'zoning-i2', '0'),
('103', 'DOCKS', 'docks', '0'),
('104', '3 DRIVE-IN DOORS', '3-drive-in-doors', '0'),
('105', 'BERLIN', 'berlin', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('106', 'ZONING: GI', 'zoning-gi', '0'),
('107', '200 PARKING SPACES', '200-parking-spaces', '0'),
('108', 'ZONING IL', 'zoning-il', '0'),
('109', 'EAST HAVEN', 'east-haven', '0'),
('110', '8-16 FOOT CEILINGS', '8-16-foot-ceilings', '0'),
('111', '4', '4', '0'),
('112', '600 SF', '600-sf', '0'),
('113', 'KITCHEN', 'kitchen', '0'),
('114', 'GAS', 'gas', '0'),
('115', '9 FOOT HIGH CEILINGS', '9-foot-high-ceilings', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('116', 'SPRINKLER', 'sprinkler', '0'),
('117', 'ZONING: BD', 'zoning-bd', '0'),
('118', '600 AMP ELECTRIC', '600-amp-electric', '0'),
('119', '16 FOOT CEILINGS', '16-foot-ceilings', '0'),
('120', '60 PARKING SPACES', '60-parking-spaces', '0'),
('121', 'BUILT IN 1940', 'built-in-1940', '0'),
('122', '6 PARKING SPACES', '6-parking-spaces', '0'),
('123', 'ZONING: BB', 'zoning-bb', '0'),
('124', '20 FOOT CEILINGS', '20-foot-ceilings', '0'),
('125', 'BUILT IN 1945', 'built-in-1945', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('126', 'ZONING: R-40', 'zoning-r-40', '0'),
('127', 'NORTH BRANFORD', 'north-branford', '0'),
('128', 'BUILT IN 1979', 'built-in-1979', '0'),
('129', 'ZONING: B2', 'zoning-b2', '0'),
('130', 'ANSONIA', 'ansonia', '0'),
('135', 'APARTMENTS', 'apartments', '0'),
('136', 'MIXED USE', 'mixed-use', '0'),
('137', 'BUILT IN 1900', 'built-in-1900', '0'),
('138', 'BRICK', 'brick', '0'),
('139', '8-10 FOOT CEILINGS', '8-10-foot-ceilings', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('140', 'BUILT IN 1990', 'built-in-1990', '0'),
('141', '30 PARKING SPACES', '30-parking-spaces', '0'),
('142', 'ZONING: B1', 'zoning-b1', '0'),
('143', 'DURHAM', 'durham', '0'),
('145', 'BUILT IN 2007', 'built-in-2007', '0'),
('146', 'ZONING: DD', 'zoning-dd', '0'),
('147', 'BUILT IN 1850', 'built-in-1850', '0'),
('148', '8 DOCKS', '8-docks', '0'),
('149', '10 DRINE-IN DOORS', '10-drine-in-doors', '0'),
('150', '3.02 ACRES', '3-02-acres', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('151', 'ZONING: I-1', 'zoning-i-1', '0'),
('152', 'ELEVATOR', 'elevator', '0'),
('153', 'BUILD IN 1980', 'build-in-1980', '0'),
('154', 'ZONING: B-2', 'zoning-b-2', '0'),
('155', 'ZONING: RCPD', 'zoning-rcpd', '0'),
('156', 'CLOSE TO I-95', 'close-to-i-95', '0'),
('157', 'ELECTRIC AT LOCATION', 'electric-at-location', '0'),
('158', 'SEWER AT LOCATION', 'sewer-at-location', '0'),
('159', 'WATER AT LOCATION', 'water-at-location', '0'),
('160', 'BUILD IN 1982', 'build-in-1982', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('161', 'ZONING: R-4', 'zoning-r-4', '0'),
('162', 'FUEL:OIL', 'fueloil', '0'),
('163', '16 PARKING SPACES', '16-parking-spaces', '0'),
('164', 'ZONING: M-3', 'zoning-m-3', '0'),
('165', '18.02 ACRES', '18-02-acres', '0'),
('166', 'ZONING R-3', 'zoning-r-3', '0'),
('390', 'None', 'none', '0'),
('168', 'BUILD IN 1986', 'build-in-1986', '0'),
('169', '8 PARKING SPACES', '8-parking-spaces', '0'),
('170', '.26 ACRE', '26-acre', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('171', 'APT5-12', 'apt5-12', '0'),
('172', 'MDL-94', 'mdl-94', '0'),
('173', 'BUILT 1871', 'built-1871', '0'),
('174', '12 PARKING SPACES', '12-parking-spaces', '0'),
('175', '14-16 FOOT CEILINGS', '14-16-foot-ceilings', '0'),
('176', 'OUTSIDE STORAGE', 'outside-storage', '0'),
('177', 'BUILT IN 1912', 'built-in-1912', '0'),
('178', 'ZONING: HI', 'zoning-hi', '0'),
('179', '10 DRIVE-IN DOORS', '10-drive-in-doors', '0'),
('180', 'BUILT IN 1974', 'built-in-1974', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('181', '54 PARKING SPACES', '54-parking-spaces', '0'),
('182', 'ZONING: CB20', 'zoning-cb20', '0'),
('183', 'RESTAURANT', 'restaurant', '0'),
('184', '1 ACRE', '1-acre', '0'),
('185', 'zONING: IG 80', 'zoning-ig-80', '0'),
('186', '.69 ACRE', '69-acre', '0'),
('187', '2.76 ACRES', '2-76-acres', '0'),
('188', 'SEWER AVAILABLE', 'sewer-available', '0'),
('189', 'WATER AVAILABLE', 'water-available', '0'),
('190', 'YEAR BUILD 1988', 'year-build-1988', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('191', 'AIR CONDITIONING IN OFFICE', 'air-conditioning-in-office', '0'),
('192', 'BUILT IN 1960', 'built-in-1960', '0'),
('193', 'BUILD IN 1890', 'build-in-1890', '0'),
('194', 'NAUGATUCK', 'naugatuck', '0'),
('195', '80 ACRES', '80-acres', '0'),
('196', 'ZONING: RES', 'zoning-res', '0'),
('197', 'BUILT IN 1986', 'built-in-1986', '0'),
('198', 'ZONING: DEV2', 'zoning-dev2', '0'),
('199', '16-25 FOOT CEILINGS', '16-25-foot-ceilings', '0'),
('200', 'BUILT 1983', 'built-1983', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('201', '100 PARKING SPACES', '100-parking-spaces', '0'),
('202', '2 DOCKS', '2-docks', '0'),
('203', '4 DRIVE-IN DOORS', '4-drive-in-doors', '0'),
('204', 'BUILT IN 1959', 'built-in-1959', '0'),
('205', 'LOADING DOCK', 'loading-dock', '0'),
('206', '.28 ACRE', '28-acre', '0'),
('207', 'BUILT IN 1969', 'built-in-1969', '0'),
('208', 'BUILD IN 1960', 'build-in-1960', '0'),
('209', 'CENTRAL AIR IN OFFICE', 'central-air-in-office', '0'),
('210', '.13 ACRE', '13-acre', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('211', 'ZONING: RD', 'zoning-rd', '0'),
('212', 'TRUMBULL', 'trumbull', '0'),
('213', 'ZONING: RA', 'zoning-ra', '0'),
('214', 'LT MANUFACTURING', 'lt-manufacturing', '0'),
('215', '11-15 FOOT CEILINGS', '11-15-foot-ceilings', '0'),
('216', 'BUILT IN 1946', 'built-in-1946', '0'),
('217', '3 APRKING SPACES', '3-aprking-spaces', '0'),
('218', '15 PARKING SPACES', '15-parking-spaces', '0'),
('219', '1.49 ACRES', '1-49-acres', '0'),
('220', 'ZONING: WD', 'zoning-wd', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('221', 'ESTABLISHED 1974', 'established-1974', '0'),
('222', '200 SF', '200-sf', '0'),
('223', '6 WEEKS-12 YEARS', '6-weeks-12-years', '0'),
('224', '44 CHILD CAPACITY', '44-child-capacity', '0'),
('225', 'UPSCALE SHORELINE TOWN', 'upscale-shoreline-town', '0'),
('226', 'UPSIDE POTENTIAL', 'upside-potential', '0'),
('227', 'VERY SAFE LOCATION', 'very-safe-location', '0'),
('228', 'NEW HAVEN COUNTY', 'new-haven-county', '0'),
('229', '130 DINING SEATS', '130-dining-seats', '0'),
('230', '40 BAR', '40-bar', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('231', '70 SEAT GREAT PATIO', '70-seat-great-patio', '0'),
('232', 'OPEN 6 DAYS', 'open-6-days', '0'),
('233', 'LUNCH AND DINNER', 'lunch-and-dinner', '0'),
('234', 'HIGH VISIBILITY', 'high-visibility', '0'),
('235', 'PRIVATE ROOM FOR PARTIES', 'private-room-for-parties', '0'),
('236', 'BASEMENT STORAGE', 'basement-storage', '0'),
('237', '120 DINING SEATS', '120-dining-seats', '0'),
('238', 'BANQUET ROOM', 'banquet-room', '0'),
('239', '50 SEAT PATIO', '50-seat-patio', '0'),
('240', 'PRIME LOCATION', 'prime-location', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('241', '2 LIFTS', '2-lifts', '0'),
('242', 'DRY LEVEL LOT', 'dry-level-lot', '0'),
('244', '2000 SF', '2000-sf', '0'),
('245', '50 SEAT PRIVATE PARTY ROOM', '50-seat-private-party-room', '0'),
('246', '30 SEAT DINING ROOM', '30-seat-dining-room', '0'),
('247', 'OUTSIDE SEATING AVAILABLE', 'outside-seating-available', '0'),
('248', 'CODE 1000 GALLON GREASE TRAP', 'code-1000-gallon-grease-trap', '0'),
('249', 'ENORMOUS TAKE OUT BUSINESS', 'enormous-take-out-business', '0'),
('250', 'SALAD BAR', 'salad-bar', '0'),
('251', 'OPEN MONDAY - THURSDAY 10-9', 'open-monday-thursday-10-9', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('252', 'OPEN FRIDAY 10-10', 'open-friday-10-10', '0'),
('253', 'OPEN SATURDAY 10-9', 'open-saturday-10-9', '0'),
('254', 'OPEN SUNDAY 3-9', 'open-sunday-3-9', '0'),
('255', 'TRAINING IF NEEDED', 'training-if-needed', '0'),
('256', 'THREE BUILDINGS', 'three-buildings', '0'),
('257', 'ONE STORY', 'one-story', '0'),
('258', '4 ROOM HOUSE AND GARAGE', '4-room-house-and-garage', '0'),
('259', 'SMALL LOG CABIN', 'small-log-cabin', '0'),
('260', 'IMMACULATE FLAWLESS CONDITION', 'immaculate-flawless-condition', '0'),
('261', 'STRIKING HARDWOOD FLOORS', 'striking-hardwood-floors', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('262', 'OLD WORLD CHARM', 'old-world-charm', '0'),
('263', 'WOOD BEAMS', 'wood-beams', '0'),
('264', 'NEW HOOD SYSTEM', 'new-hood-system', '0'),
('265', 'FOUNTAIN', 'fountain', '0'),
('266', 'STUNNING WATERFALL', 'stunning-waterfall', '0'),
('267', 'GAZEBO', 'gazebo', '0'),
('268', 'LOVELY GARDENS', 'lovely-gardens', '0'),
('269', 'OUTDOOR BBQ AREA', 'outdoor-bbq-area', '0'),
('270', '8301 SF BANQUET FACILITY', '8301-sf-banquet-facility', '0'),
('271', 'AMPLE PARKING WITH EXTRA LOT', 'ample-parking-with-extra-lot', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('272', '2500 SF', '2500-sf', '0'),
('273', '$3049 GROSS RENT', '3049-gross-rent', '0'),
('274', 'LONG LEASE', 'long-lease', '0'),
('275', 'FULL SERVICE 15 SEAT BAR', 'full-service-15-seat-bar', '0'),
('276', '125 DINING SEATS', '125-dining-seats', '0'),
('277', 'CODE GREASE TRAP', 'code-grease-trap', '0'),
('278', 'ABUNDANT PARKING', 'abundant-parking', '0'),
('279', 'GREAT FRONTAGE', 'great-frontage', '0'),
('280', '7 DAYS LUNCH &amp; DINNER', '7-days-lunch-dinner', '0'),
('281', 'BANQUET SEATING FOR 85', 'banquet-seating-for-85', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('282', 'HIGH END WOOD TABLE AND CHAIRS', 'high-end-wood-table-and-chairs', '0'),
('283', '1300 SF', '1300-sf', '0'),
('284', '$1700 GROSS RENT', '1700-gross-rent', '0'),
('285', '60 SEATS', '60-seats', '0'),
('286', 'COUNTER SEATING', 'counter-seating', '0'),
('287', 'FULL BASEMENT WITH OFFICE', 'full-basement-with-office', '0'),
('288', 'WALK-IN', 'walk-in', '0'),
('289', 'DRY STORAGE', 'dry-storage', '0'),
('290', 'OPEN 7 DAYS', 'open-7-days', '0'),
('291', 'NO LUNCH SUNDAY OR MONDAY', 'no-lunch-sunday-or-monday', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('292', 'LOYAL CUSTOMER BASE', 'loyal-customer-base', '0'),
('293', '2600 SF', '2600-sf', '0'),
('294', '$4500/MO RENT', '4500mo-rent', '0'),
('295', 'MAJOR ROAD', 'major-road', '0'),
('296', '30000 CARS PER DAY\'', '30000-cars-per-day', '0'),
('297', '110 SEAT', '110-seat', '0'),
('298', '20 MONEY MAKING YEARS', '20-money-making-years', '0'),
('299', 'NEW A/C UNIT', 'new-ac-unit', '0'),
('300', '5000 SF', '5000-sf', '0'),
('301', '40 PATIO SEATS', '40-patio-seats', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('302', '150 DINING SEATS', '150-dining-seats', '0'),
('303', 'FULL BAR', 'full-bar', '0'),
('304', 'FLAT SCREENTV\'S', 'flat-screentvs', '0'),
('305', '$8000 BASE RENT', '8000-base-rent', '0'),
('306', 'FREE STANDING BUILDING', 'free-standing-building', '0'),
('307', 'AMPLE PRIVATE PARKING', 'ample-private-parking', '0'),
('308', 'NEW EQUIPMENT', 'new-equipment', '0'),
('309', 'LONG TERM LEASE AVAILABLE', 'long-term-lease-available', '0'),
('310', '$2850 GROSS RENT', '2850-gross-rent', '0'),
('311', '100+ SEATS', '100-seats', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('312', '6 TO 2:30', '6-to-230', '0'),
('313', 'FULL BASEMENT', 'full-basement', '0'),
('314', 'MAJOR RENOVATION 3 YEARS AGO', 'major-renovation-3-years-ago', '0'),
('315', 'WELL MAINTAINED', 'well-maintained', '0'),
('316', 'IMMACULATE', 'immaculate', '0'),
('317', 'LONG LEASE AVAILABLE', 'long-lease-available', '0'),
('318', 'BUST MAIN ROAD', 'bust-main-road', '0'),
('319', 'SUBSTANTIAL UPSIDE POTENTIAL', 'substantial-upside-potential', '0'),
('320', 'PICTURES UPON REQUEST', 'pictures-upon-request', '0'),
('321', 'BUSY MAIN ROAD', 'busy-main-road', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('322', '3500 SF', '3500-sf', '0'),
('323', '$6400 GROSS RENT', '6400-gross-rent', '0'),
('324', 'OVER 20 YEARS WITH SAME OWNER', 'over-20-years-with-same-owner', '0'),
('325', '50 SEAT PRIVATE ROOM', '50-seat-private-room', '0'),
('326', 'EXCITING BAR', 'exciting-bar', '0'),
('327', '7 FLAT SCREEN TV\'S', '7-flat-screen-tvs', '0'),
('328', 'BANDS EVERY WEEKEND', 'bands-every-weekend', '0'),
('329', 'AAA LOCATION', 'aaa-location', '0'),
('330', 'POPULAR HAPPY HOUR', 'popular-happy-hour', '0'),
('331', '1000 SF', '1000-sf', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('332', '$2500 GROSS RENT', '2500-gross-rent', '0'),
('333', 'POST ROAD LIKE TRAFFIC', 'post-road-like-traffic', '0'),
('334', 'CLOSED DECEMBER JANUARY FEBRUARY', 'closed-december-january-february', '0'),
('335', '7 DAYS 11 TO 11', '7-days-11-to-11', '0'),
('336', 'OUTSIDE PICNIC TABLES', 'outside-picnic-tables', '0'),
('337', 'OVERSIZE PARKING LOT', 'oversize-parking-lot', '0'),
('338', 'LOW OVERHEAD', 'low-overhead', '0'),
('339', '1200 SF', '1200-sf', '0'),
('340', '$2200 GROSS RENT', '2200-gross-rent', '0'),
('341', 'LUNCH AND DINNER TAKE-OUT', 'lunch-and-dinner-take-out', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('342', 'WELL EQUIPPED FULL KITCHEN', 'well-equipped-full-kitchen', '0'),
('343', 'TUESDAY-FRIDAY 10 TO 6:30', 'tuesday-friday-10-to-630', '0'),
('344', 'SATURDAYS 9-5:00', 'saturdays-9-500', '0'),
('345', 'CLOSED SUNDAY AND MONDAYS', 'closed-sunday-and-mondays', '0'),
('346', 'HIGHLY ACCLAIMED', 'highly-acclaimed', '0'),
('347', 'NUMEROUS AWARDS', 'numerous-awards', '0'),
('348', '$5000 GROSS RENT', '5000-gross-rent', '0'),
('349', '100 ALA CARTE AND CATERING SEATS', '100-ala-carte-and-catering-seats', '0'),
('350', '2 FLOOR FREE STANDING BUILDING', '2-floor-free-standing-building', '0'),
('351', 'CODE IN GROUND GREASE TRAP', 'code-in-ground-grease-trap', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('352', '3 HIGH TEMP BRICK OVENS', '3-high-temp-brick-ovens', '0'),
('353', 'ENORMOUS DELIVERY BUSINESS', 'enormous-delivery-business', '0'),
('354', 'OLDEST PIZZA IN TOWN', 'oldest-pizza-in-town', '0'),
('355', 'PRIVATE PARTY ROOM', 'private-party-room', '0'),
('356', 'SUNDAY-THURSDAY 11-10', 'sunday-thursday-11-10', '0'),
('357', 'FRIDAY AND SATURDAY 11-11', 'friday-and-saturday-11-11', '0'),
('358', '2000SF', '2000sf', '0'),
('359', 'SHOPPING CENTER LOCATION', 'shopping-center-location', '0'),
('360', '40 SEATS', '40-seats', '0'),
('361', 'LARGE HOBART MIXER', 'large-hobart-mixer', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('362', 'CATERING CONTRACTS', 'catering-contracts', '0'),
('363', 'HIGH GROSSING', 'high-grossing', '0'),
('364', '3600 SF', '3600-sf', '0'),
('365', 'TWO ROOMS', 'two-rooms', '0'),
('366', 'FULL BAR AND LOUNGE', 'full-bar-and-lounge', '0'),
('367', '25 SEAT PATIO', '25-seat-patio', '0'),
('368', 'LARGE KITCHEN', 'large-kitchen', '0'),
('369', 'LIQUOR AND DRY STORAGE', 'liquor-and-dry-storage', '0'),
('370', 'WALK-IN WITH DRAFT BEER SYSTEM', 'walk-in-with-draft-beer-system', '0'),
('371', '12 LARGE SCREEN TVS', '12-large-screen-tvs', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('372', 'MICROS POS SYSTEM', 'micros-pos-system', '0'),
('373', 'LARGE FREE PARKING LOT DIRECTLY ACROSS THE STREET', 'large-free-parking-lot-directly-across-the-street', '0'),
('374', 'PROVEN HIGH GROSSING LOCATION', 'proven-high-grossing-location', '0'),
('375', 'BUILT IN 1975', 'built-in-1975', '0'),
('376', 'ZONING: IG-2', 'zoning-ig-2', '0'),
('377', '34 SEATS', '34-seats', '0'),
('378', 'OFFICE AREA', 'office-area', '0'),
('379', 'ADDITIONAL PREP AREA', 'additional-prep-area', '0'),
('380', 'HARDWOOD FLOORS', 'hardwood-floors', '0'),
('381', '$1975 PER MONTH RENT', '1975-per-month-rent', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('382', 'POTENTIAL IN CATERING', 'potential-in-catering', '0'),
('383', 'MINUTES TO DOWN AND COLLEGES', 'minutes-to-down-and-colleges', '0'),
('384', 'featured', 'featured', '0'),
('385', 'DIVISIBLE', 'divisible', '0'),
('386', 'ZONING: VA', 'zoning-va', '0'),
('387', 'Built in 1968', 'built-in-1968-2', '0'),
('388', 'Class A', 'class-a-2', '0'),
('389', 'Executive Suites', 'executive-suites-2', '0'),
('391', 'CONDO', 'condo', '0'),
('392', 'FLEX', 'flex', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('393', 'HEAVY ELECTRIC', 'heavy-electric', '0'),
('394', 'AMPLE PARKING', 'ample-parking', '0'),
('395', 'RIGHT OFF I-95', 'right-off-i-95', '0'),
('396', 'Finished Basement', 'finished-basement', '0'),
('397', 'Zoning T-4', 'zoning-t-4', '0'),
('398', '3 FLOORS', '3-floors', '0'),
('399', '$1800 GROSS', '1800-gross', '0'),
('400', '18 SEATS', '18-seats', '0'),
('401', 'ESPN CATERING', 'espn-catering', '0'),
('402', 'NEXT TO COLLEGE', 'next-to-college', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('403', '4 PT EMPLOYEES', '4-pt-employees', '0'),
('404', 'OUTSTANDING MENU', 'outstanding-menu', '0'),
('405', 'CLOSED SUNDAYS', 'closed-sundays', '0'),
('406', 'EXCELLENT REVIEWS', 'excellent-reviews', '0'),
('407', '$3500 GROSS', '3500-gross', '0'),
('408', '30 SEAT PRIVATE ROOM', '30-seat-private-room', '0'),
('409', '80 DINING SEATS', '80-dining-seats', '0'),
('410', 'NO OTHER COMPETITION NEARBY', 'no-other-competition-nearby', '0'),
('411', 'LARGE CATERING POTENTIAL', 'large-catering-potential', '0'),
('412', 'BETWEEN TWO MAJOR SCHOOLS', 'between-two-major-schools', '0');

INSERT INTO `wp_2_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
('413', 'GREASE TRAP COMPLIANT', 'grease-trap-compliant', '0'),
('414', 'EXCELLENT REPUTATION', 'excellent-reputation', '0'),
('415', 'LOW 18% PAYROLL COST', 'low-18-payroll-cost', '0'),
('416', 'A MUST SEE', 'a-must-see', '0');

